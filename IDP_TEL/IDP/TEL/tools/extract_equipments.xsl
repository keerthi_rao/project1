<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="text" indent="yes"/>

  <xsl:template match="/"># This script create the equipments list
# CBI: 
#	- {ID: @ID, NAME: @Name}
# ZC: 
#	- {ID: @ID, NAME: @Name}
# SERVER: 
#	- {ID: @ID, NAME: @Name, AREA_TYPE: Area_Type, LEVEL: if(Server_Type = Level_1) then [_LV1] else if (Server_Type=Level_2) then [_LV2] else if (Server_Type=Both_Levels) then [_LV1, _LV2] else NA}
# ATS_Equipment:
#     - {ID: @ID, NAME: @Name, ATS_Equipment_type: ATS_Equipment_Type}
CBI: <xsl:apply-templates select="//CBI"/>
ZC: <xsl:apply-templates select="//ZC"/>
SERVER: <xsl:apply-templates select="//Signalisation_Area[(contains(Area_Type, 'Server')) and (Server_Type='Level_1' or Server_Type='Level_2' or Server_Type='NA' or Server_Type='Both_Levels')]"/>
Function:<xsl:apply-templates select="//Functions/Function"/>
ATS: <xsl:apply-templates select="//Functions/Function[@ID=//ATS_Equipment[ATS_Equipment_Type='FEP']/Function_ID_List/Function_ID or @Signalisation_Area_ID = //ATS_Equipment[ATS_Equipment_Type='FEP']/Signalisation_Area_ID]"/>
ATS_EQUIPMENTS: <xsl:apply-templates select="//ATS_Equipments/ATS_Equipment"/>
ATS_TEST_EQUIPMENTS: <xsl:apply-templates select="//ATS_Test_Equipments/ATS_Test_Equipment"/>
URBALIS_SECTORS: <xsl:apply-templates select="//Urbalis_Sectors/Urbalis_Sector"/>
Project:<xsl:apply-templates select="//Project"/>
LC:<xsl:apply-templates select="//ATC_Equipments/ATC_Equipment[ATC_Equipment_Type = 'LC']"/></xsl:template>
  
 <xsl:template match="Function">
  <xsl:variable name="sa" select="//Signalisation_Area"/>
  <xsl:variable name="atsequip" select="//ATS_Equipment[Function_ID_List/Function_ID = current()/@ID]/Function_ID_List[Function_ID != current()/@ID]/Function_ID"/>
  <xsl:variable name="bothlevel" select="if(@Function_Type = 'Level_2' and $atsequip) then 'yes' else 'no'"/>
  <xsl:variable name="assolevel" select="if(//Function[@Signalisation_Area_ID=$sa[@ID=current()/@Signalisation_Area_ID and current()/@Function_Type='FEP']/sys:sigarea/@id and @Function_Type='Level_1']) then ('LV1') else 'NA'"/>
- {ID: <xsl:value-of select="@ID"/>, NAME: <xsl:value-of  select="@Name"/>, Signalisation_Area_ID: <xsl:value-of select="@Signalisation_Area_ID"/>, LEVEL: [<xsl:value-of select="if(@Function_Type = 'Level_1') then '_LV1' else if (@Function_Type = 'Level_2') then '_LV2' else if (@Function_Type = 'Both') then 'NA' else if(@Function_Type = 'FEP') then 'FEP' else 'NA'"/>], AREA_TYPE: <xsl:value-of  select="//Signalisation_Area[@ID=current()/@Signalisation_Area_ID]/Area_Type"/>, AssocaitedLevel: <xsl:value-of select="if($assolevel) then $assolevel else 'NA'"/>}
  </xsl:template>  
  
   <xsl:template match="CBI|ZC|Urbalis_Sector">  
- {ID: <xsl:value-of select="@ID"/>, NAME: <xsl:value-of select="@Name"/>}
  </xsl:template>
  
   <xsl:template match="Signalisation_Area">  
- {ID: <xsl:value-of select="@ID"/>, NAME: <xsl:value-of  select="@Name"/>, AREA_TYPE: <xsl:value-of  select="Area_Type"/>, LEVEL: [<xsl:value-of select="if(Server_Type = 'Level_1') then '_LV1' else if (Server_Type = 'Level_2') then '_LV2' else if (Server_Type = 'Both_Levels') then '_LV1, _LV2' else 'NA'"/>]}
  </xsl:template>
  
    <xsl:template match="ATS_Equipment">
    <xsl:variable name="sigAreaID" select="if (Signalisation_Area_ID) then (Signalisation_Area_ID) else (Signalisation_Area_ID_List/Signalisation_Area_ID)"/>
	<xsl:variable name="machines" select="//ATS_Equipment[(ATS_Equipment_Type='Local Server' or ATS_Equipment_Type='Gateway') and (Signalisation_Area_ID=$sigAreaID or Signalisation_Area_ID_List/Signalisation_Area_ID=$sigAreaID)]"/>
	<xsl:variable name="serverNumber" select="if(number(@ID) &lt; number($machines[1]/@ID) and not(@ID = $machines[1]/@ID)) then '1' else if (number(@ID) &lt; number($machines[last()]/@ID) and not(@ID = $machines[last()]/@ID)) then '1' else '2'"/>
	<xsl:variable name="deployment" select="//DeploymentConfiguration/Deployment[@Type=current()/ATS_Equipment_Type]"/>
	<xsl:variable name="localisationtype" select="//Signalisation_Area[@ID=$sigAreaID]/Localisation_Type"/>

  - {ID: <xsl:value-of select="@ID"/>,Localisation_Type: <xsl:value-of select="$localisationtype"/>,NAME: <xsl:value-of select="@Name"/>, ATS_EQUIP_TYPE: <xsl:value-of select="ATS_Equipment_Type[1]"/>, SIG_AREA_TYPE: <xsl:value-of select="//Signalisation_Area[@ID=$sigAreaID]/Area_Type"/>,SIG_AREA_ID: <xsl:value-of select="$sigAreaID[1]"/>, MACHINE1: <xsl:value-of select="if($machines) then $machines[1]/@Name else 'NA'"/>, MACHINE2: <xsl:value-of select="if($machines) then $machines[last()]/@Name else 'NA'"/>, SERVER_NUMBER: <xsl:value-of select="$serverNumber"/>, Deployment: <xsl:value-of select="if($deployment) then 'yes' else 'no'"/>, DeploymentID: <xsl:value-of select="if($deployment) then $deployment[1]/@ID else 'NA'"/>}</xsl:template>
  <xsl:template match="Project">
    Name: <xsl:value-of select="@Name"/></xsl:template>
    <xsl:template match="ATS_Test_Equipment">
    <xsl:variable name="sigAreaID" select="if (Signalisation_Area_ID) then (Signalisation_Area_ID) else (Signalisation_Area_ID_List/Signalisation_Area_ID)"/>
	<xsl:variable name="machines" select="//ATS_Test_Equipment[(ATS_Equipment_Type='Local Server' or ATS_Equipment_Type='Gateway') and (Signalisation_Area_ID=$sigAreaID or Signalisation_Area_ID_List/Signalisation_Area_ID=$sigAreaID)]"/>
	<xsl:variable name="serverNumber" select="if(number(@ID) &lt; number($machines[1]/@ID) and not(@ID = $machines[1]/@ID)) then '1' else if (number(@ID) &lt; number($machines[last()]/@ID) and not(@ID = $machines[last()]/@ID)) then '1' else '2'"/>
	<xsl:variable name="deployment" select="//DeploymentConfiguration/Deployment[@Type=current()/ATS_Equipment_Type]"/>
  - {ID: <xsl:value-of select="@ID"/>, NAME: <xsl:value-of select="@Name"/>, ATS_EQUIP_TYPE: <xsl:value-of select="ATS_Equipment_Type[1]"/>, SIG_AREA_TYPE: <xsl:value-of select="//Signalisation_Area[@ID=$sigAreaID]/Area_Type"/>,SIG_AREA_ID: <xsl:value-of select="$sigAreaID[1]"/>, MACHINE1: <xsl:value-of select="if($machines) then $machines[1]/@Name else 'NA'"/>, MACHINE2: <xsl:value-of select="if($machines) then $machines[last()]/@Name else 'NA'"/>, SERVER_NUMBER: <xsl:value-of select="$serverNumber"/>, Deployment: <xsl:value-of select="if($deployment) then 'yes' else 'no'"/>, DeploymentID: <xsl:value-of select="if($deployment) then $deployment[1]/@ID else 'NA'"/>}</xsl:template>
  <xsl:template match="ATC_Equipment">
 -  {ID: <xsl:value-of select="@ID"/>, NAME: <xsl:value-of select="@Name"/>}
  </xsl:template>
</xsl:stylesheet>
