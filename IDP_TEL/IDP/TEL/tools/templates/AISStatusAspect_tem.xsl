<%inherit file="/Object_tem.mako"/>
<%! class_ = "AISStatusAspect" %>

<%block name="classes">
	<Class name="AISStatusAspect">
	     <Objects>
			 <xsl:apply-templates select="//Signal[@ID=//Route[sys:sigarea/@id=$SERVER_ID and @ID = //Aspect_Indicator/Route_ID_List/Route_ID]/Origin_Signal_ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Signal">
	<xsl:variable name="rt" select="//Route[Origin_Signal_ID= current()/@ID]"/>
	<xsl:variable name="aspectIndicator" select="//Aspect_Indicator[Route_ID_List/Route_ID = $rt/@ID and (contains(@Name, '_A') or contains(@Name, '_B'))]"/>
	<xsl:if test="$aspectIndicator[contains(@Name, '_A')] and $aspectIndicator[contains(@Name, '_B')]">
		<Object name="APPROACH_{@Name}.StatusAspect" rules="update_or_create">	
			<Properties>
				<Property name="Shared_OPCGroup" dt="string">Split_<xsl:value-of select="$SERVER_ID"/></Property>
				<Property name="PV_AI_A" dt="string"><xsl:if test="$aspectIndicator[contains(@Name, '_A')][1]/@Name">AISAC_<xsl:value-of select="$aspectIndicator[contains(@Name, '_A')][1]/@Name"/>.Value</xsl:if></Property>
				<Property name="PV_AI_B" dt="string"><xsl:if test="$aspectIndicator[contains(@Name, '_B')][1]/@Name">AISAC_<xsl:value-of select="$aspectIndicator[contains(@Name, '_B')][1]/@Name"/>.Value</xsl:if></Property>
				<Property name="RouteSettingPointName" dt="string"><xsl:value-of select="concat('APPROACH_',@Name)"/></Property>
				<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="concat('APPROACH_', @Name, '.StatusAspect')"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean">True</Property>
			</Properties>
		</Object>
	 </xsl:if>
</xsl:template>

