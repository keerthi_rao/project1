<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8" use-character-maps="quotes" />
  <xsl:character-map name="quotes">
      <xsl:output-character character="&#34;" string="&amp;quot;"/>
   </xsl:character-map>
<xsl:param name="COMPUTER_NAME"/>
<xsl:param name="ATS_EQUIP_TYPE"/>
<xsl:param name="DEP_ID"/>
<xsl:param name="SIG_AREA_ID"/>

<xsl:template match="/">
<Deployment xsi:noNamespaceSchemaLocation="Deployment.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:variable name="dep" select="//DeploymentConfiguration/Deployment[@ID=$DEP_ID]"/>	
	<xsl:if test="$dep/@DataPrepSource='Yes'">
		<DataPrepSource Version="1.0.0" Simultaneous="1">
			<DataPrepComputer Path="D:\DataPrep\ISD"/>
			<DataPrepComputer Path="D:\DataPrep\Output" Zip="1"/>
		</DataPrepSource>
	</xsl:if>
	<Computers>
		<xsl:for-each select="$dep/Target">
			<xsl:variable name="trgt" select="@Type"/>
			<xsl:apply-templates select="//ATS_Equipments/ATS_Equipment[ATS_Equipment_Type=$trgt]|//ATS_Test_Equipments/ATS_Equipment[ATS_Equipment_Type=$trgt]"><xsl:with-param name="targetID" select="@ID"/></xsl:apply-templates>
		</xsl:for-each>
	</Computers>
</Deployment>
</xsl:template>


<!-- Template to generate the deployment commands for ATS_Equipment type: Workstation, Workstation_OffLineTools and Overview_Display_System -->
<xsl:template match="ATS_Equipment|ATS_Test_Equipment">
	<xsl:param name="targetID"/>
	<xsl:variable name="Server_ID" select="if (Signalisation_Area_ID) then (Signalisation_Area_ID) else (Signalisation_Area_ID_List/Signalisation_Area_ID)"/>
	<xsl:variable name="targt" select="//DeploymentConfiguration/Deployment[@ID=$DEP_ID]/Target[@ID=$targetID]"/>
	<xsl:variable name="tt" select="@Name"/>
	<xsl:variable name="ip" select="IP_Address_Light_Grey[1]"/>
	<xsl:variable name="ip1" select="IP_Address_Light_Grey[1]"/>
	<xsl:variable name="ip2" select="IP_Address_Light_Grey[1]"/>
	<xsl:variable name="othercomp" select="if($targt/@ExecMachine='Target') then . else if(local-name() = 'ATS_Test_Equipment') then //ATS_Test_Equipment[@Name=$COMPUTER_NAME] else //ATS_Equipment[@Name=$COMPUTER_NAME]"/>
	<Computer Name="{if($othercomp) then $othercomp/@Name else @Name}" IP="{if($othercomp) then $othercomp/IP_Address_Light_Grey else IP_Address_Light_Grey[1]}" Description="{$targt/@Description}" ToReboot="{$targt/@ToReboot}">
		<xsl:if test="$targt/Command/@Type='Dataprep'">
			<DataPrep>
				<DataPrepSingle Path="D:\DataPrep\IDC\Appli\ATSTEL_{$Server_ID[1]}_LV2" Zip="1"/>
				<DataPrepSingle Path="D:\DataPrep\IMC\IMCProject.bak" Zip="1"/>					
			</DataPrep>
		</xsl:if>
		<xsl:if test="$targt/Command/@Type='DataprepEmpty'">
			<DataPrep/>
		</xsl:if>
		<xsl:variable name="macros" select="//MacroDefinition[@Name = $targt/Macro/@Name]"/>
		<xsl:variable name="PlatformType" select="$targt/@PlatformType"/>
		<!--xsl:for-each select="$targt/Command[@Type !='DataprepEmpty' or @Type !='Dataprep'], $macros/Command[@Type !='DataprepEmpty' or @Type !='Dataprep']"-->
		<xsl:for-each select="$targt/*">
			 <xsl:if test="local-name() = 'Command' and (@Type !='DataprepEmpty' or @Type !='Dataprep')">
				 <xsl:variable name="command" select="//Commands/Command[@name=current()/@Type]"/>
				 <xsl:variable name="comline" select="if($command/Commandline[@type=$PlatformType]) then $command/Commandline[@type=$PlatformType] else $command/Commandline"/>
				 <xsl:variable name="commandline" select="replace(replace(replace(replace(replace($comline[1],'~~SERVER~~',$Server_ID[1]),'~~IP~~',$ip),'~~tt~~',$tt),'~~Var2~~', @Var2), '~~Var1~~', @Var1)"/>
				 <xsl:variable name="description" select="replace(replace(replace($command/CommandDescription[1], '~~tt~~',$tt),'~~Var2~~', @Var2), '~~Var1~~', @Var1)"/>
				 <!--xsl:variable name="commandline"><xsl:value-of select="replace($command/Commandline, '~~Var1~~', string(@Var1))"/></xsl:variable-->
				 <!--xsl:variable name="description" select="$command/CommandDescription"/-->
				 <xsl:variable name="workingdircetory" select="if($command/WorkingDirectory) then replace($command/WorkingDirectory, '~~Var2~~', @Var2) else 'False'"/>
				 <xsl:if test="$command[@name !='GenDCS']/@type='Install'">
					${Install('<xsl:value-of select="$commandline"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
				 </xsl:if>
				 <xsl:if test="$command[@name='GenDCS']/@type='Install'">
					<xsl:for-each select="//ATSs/ATS">
						<xsl:variable name="commandline1" select="replace(replace(replace($command/Commandline,'~~ip1~~',IP_Address_Blue[1]),'~~tt~~',$tt),'~~ip2~~', IP_Address_Red[1])"/>
						${Install('<xsl:value-of select="$commandline1"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
					</xsl:for-each>
				 </xsl:if>
				 <xsl:if test="$command/@type='Uninstall'">
					${Uninstall('<xsl:value-of select="$commandline"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
				 </xsl:if>
			 </xsl:if>	 
			 <xsl:if test="local-name() = 'Macro'">
					<xsl:for-each select="//MacroDefinition[@Name = current()/@Name]/Command[@Type !='DataprepEmpty' or @Type !='Dataprep']">
						 <xsl:variable name="command" select="//Commands/Command[@name=current()/@Type]"/>
						 <xsl:if test="$command">
						     <xsl:variable name="comline" select="if($command/Commandline[@type=$PlatformType]) then $command/Commandline[@type=$PlatformType] else $command/Commandline"/>
							 <xsl:variable name="commandline" select="replace(replace(replace(replace(replace($comline[1],'~~SERVER~~',$Server_ID[1]),'~~IP~~',$ip),'~~tt~~',$tt),'~~Var2~~', @Var2), '~~Var1~~', @Var1)"/>
						 <xsl:variable name="description" select="replace(replace(replace($command/CommandDescription[1], '~~tt~~',$tt),'~~Var2~~', @Var2), '~~Var1~~', @Var1)"/>
						 <!--xsl:variable name="commandline"><xsl:value-of select="replace($command/Commandline, '~~Var1~~', string(@Var1))"/></xsl:variable-->
						 <!--xsl:variable name="description" select="$command/CommandDescription"/-->
						 <xsl:variable name="workingdircetory" select="if($command/WorkingDirectory) then replace($command/WorkingDirectory, '~~Var2~~', @Var2) else 'False'"/>
						 <xsl:if test="$command[@name !='GenDCS']/@type='Install'">
							${Install('<xsl:value-of select="$commandline"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
						 </xsl:if>
						 <xsl:if test="$command[@name='GenDCS']/@type='Install'">
							<xsl:for-each select="//ATSs/ATS">
								<xsl:variable name="commandline1" select="replace(replace(replace($command/Commandline,'~~ip1~~',IP_Address_Blue[1]),'~~tt~~',$tt),'~~ip2~~', IP_Address_Red[1])"/>
								${Install('<xsl:value-of select="$commandline1"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
							</xsl:for-each>
						 </xsl:if>
						
						 <xsl:if test="$command/@type='Uninstall'">
							${Uninstall('<xsl:value-of select="$commandline"/>','<xsl:value-of select="$description"/>','<xsl:value-of select="$workingdircetory"/>')}
							</xsl:if>
						</xsl:if>
				 </xsl:for-each>
			 </xsl:if>
			 
		</xsl:for-each>
	</Computer>
</xsl:template>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="DataPrep(commandline, Description, isgenerate='True')">
	<DataPrep>
		% if WorkingDirectory == 'True':
			<DataPrepSingle>
				<xsl:attribute name="Path">${commandline}</xsl:attribute>
				<xsl:attribute name="Zip">${Description}</xsl:attribute>
			</DataPrepSingle>
			<DataPrepSingle>
				<xsl:attribute name="Path">${commandline}</xsl:attribute>
				<xsl:attribute name="Zip">${Description}</xsl:attribute>
			</DataPrepSingle>
		% endif
	</DataPrep>
</%def>

<%def name="Install(commandline, Description, WorkingDirectory='False')">
	<InstallCommand>
		<xsl:variable name="wrdir">${WorkingDirectory}</xsl:variable>
		<xsl:attribute name="CommandLine">${commandline}</xsl:attribute>
		<xsl:attribute name="Description">${Description}</xsl:attribute>
		<xsl:if test="$wrdir != 'False'">
			<xsl:attribute name="WorkingDirectory">${WorkingDirectory}</xsl:attribute>
		</xsl:if>
	</InstallCommand>
</%def>

<%def name="Uninstall(commandline, Description, WorkingDirectory='False')">
	<UnInstallCommand>
		<xsl:variable name="wrdir">${WorkingDirectory}</xsl:variable>
		<xsl:attribute name="CommandLine">${commandline}</xsl:attribute>
		<xsl:attribute name="Description">${Description}</xsl:attribute>
		<xsl:if test="$wrdir != 'False'">
			<xsl:attribute name="WorkingDirectory">${WorkingDirectory}</xsl:attribute>
		</xsl:if>
	</UnInstallCommand>
</%def>
