<%inherit file="/Object_tem.mako"/>
<%! 
class_ = "S2KAlarms_Gen"
xs_ = True
%>

<xsl:variable name="mlv" select="//MLV"/>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
  			<xsl:apply-templates select="//Train_Unit[number(@ID) &lt;= 30]"/>
		</Objects>
  	</Class>
</%block>


<xsl:template match="Train_Unit">
	<xsl:variable name="train" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="tid" select="xs:integer(@ID)"/>
	<xsl:for-each select="//MSS_CC/CC[Type='Train_Alarm']">
		<xsl:variable name="alarm_label" select="Label"/>
		<xsl:variable name="bstrparam1" select="BstrParam1"/>
		<xsl:variable name="name_pv_trig" select="replace(Name_PV_Trig,'--TrainName--',$train)"/>
		<xsl:variable name="severity" select="Severity"/>
		<xsl:variable name="state_pv_trig" select="State_PV_Trig"/>
		<xsl:variable name="out_pv_trig" select="Out_PV_Trig"/>
		<xsl:variable name="operator" select="'-'"/>
		<xsl:variable name="zzzz" select="concat(xs:integer(2000 + $tid), if (contains(EquipmentID,'CAB1')) then ('1') else ('4'))"/>
		<xsl:variable name="eqid" select="replace(EquipmentID,'zzzz',$zzzz)"/>
		<xsl:variable name="o_m_b" select="OMB"/>
		<xsl:variable name="nm" select="replace(Name,'--TrainName--',$train)"/>
		<xsl:variable name="value" select="'-'"/>
		<xsl:variable name="mms" select="MMS"/>
		<xsl:variable name="avalanche" select="Avalanche"/>
		<xsl:variable name="objname" select="concat($train,@Name,'.Alarm')"/>
		${obj()}	
	</xsl:for-each>
</xsl:template>




## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

<%def name="mulval(value_nm,value_Var)">
           <MultiLingualProperty name="${value_nm}">
			<xsl:for-each select="$mlv">
             <MultiLingualValue roleId="{../@RID}" localeId="{@LCID}"><xsl:value-of select="${value_Var}"/></MultiLingualValue>
			</xsl:for-each>
           </MultiLingualProperty>
</%def>
<%def name="obj()">
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarm_label"/></Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrparam1"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$name_pv_trig"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="$severity"/></Property>
			<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="$state_pv_trig"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$out_pv_trig"/></Property>	
			<Property name="Operator" dt="string"><xsl:value-of select="$operator"/></Property>	
			${mulval('EquipmentID','$eqid')}
			${mulval('OMB','$o_m_b')}
			${mulval('Name','$nm')}
			${mulval('Value','$value')}
			${mulval('MMS','$mms')}
			${mulval('Avalanche','$avalanche')}
		</Properties>
	</Object>
</%def>
