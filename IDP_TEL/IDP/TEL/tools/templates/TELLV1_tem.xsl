<%inherit file="/Object_tem.mako"/>
<%! class_ = "MainTELLV1" %>
<%block name="classes">
	<Class name="MainTELLV1">
		<Objects>
			<Object name="MainTELLV1" rules="update_or_create">
			<xsl:variable name="sigAreaType" select="//Signalisation_Area[@ID=$SERVER_ID]/Area_Type"/>
			<xsl:variable name="BSRspeed" select="//Project/BSR_Speed"/>
			<xsl:variable name="serType" select="if ($sigAreaType='ATS_Central_Server') then 'CATS' else if ($sigAreaType='ATS_Local_Server') then 'LATS' else null"/>
			<xsl:variable name="centralSigArea" select="//Signalisation_Area[Area_Type='ATS_Central_Server' and sys:sigarea/@id=$SERVER_ID and $serType='LATS']"/>
			<xsl:variable name="occ" select="//ATS_Equipment[ATS_Equipment_Type='Central Server' and Signalisation_Area_ID=$SERVER_ID][1]/Signalisation_Area_ID"/>
			<xsl:variable name="iscsStationIDs">
				<xsl:apply-templates select="//ISCSStation[Signalisation_Area=$SERVER_ID]"><xsl:sort select="@ID"/></xsl:apply-templates>
			</xsl:variable>
			<xsl:variable name="ET" select="if($centralSigArea)then concat('ATS_',$SERVER_ID, '.OTHERATSTEL.ATS_',$centralSigArea/@ID, '.State') else null"/>
			<Properties>
				<Property name="ISCSDatTag1Stations" dt="string"><xsl:value-of select="if ($serType='CATS') then (//ISCSCentral/StationIDs) else (tokenize($iscsStationIDs,';')[1])"/></Property>
				<Property name="ISCSDatTag2Stations" dt="string"><xsl:value-of select="if ($serType='CATS') then (null) else (tokenize($iscsStationIDs,';')[2])"/></Property>
				<Property name="ISCSDatTag3Stations" dt="string"><xsl:value-of select="if ($serType='LATS') then (null) else (tokenize($iscsStationIDs,';')[last()-1])"/></Property>
				<Property name="BSRSpeedParam" dt="i4"><xsl:value-of select="//Project/BSR_Speed"/></Property>
				<Property name="EqpTemplate" dt="string"><xsl:value-of select="$ET"/></Property>
				<Property name="ServerLocation" dt="string"><xsl:value-of select="if(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot')) then ('Depot') else ('OCC')"/></Property>
				<Property name="SOI_MMG" dt="string"><xsl:value-of select="concat($serType,'.KB')"/></Property>
				<xsl:if test="$occ!=''"><Property name="SOI_OPCClientOCC" dt="string"><xsl:value-of select="concat('OPCClient_ATS_',$occ)"/></Property>	</xsl:if>
				<xsl:if test="$occ!=''"><Property name="Addr_SupervisorOCC" dt="string"><xsl:value-of select="concat('ATS_',$occ,'.SVR',//ATS_Equipment[Signalisation_Area_ID=$occ][1]/@Name,'.State.iEqpState')"/></Property></xsl:if>			
		</Properties>
			</Object>
		</Objects>
	</Class>
</%block>
<xsl:template match="ISCSStation"><xsl:value-of select="@ID"/>;</xsl:template>