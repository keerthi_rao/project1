<%inherit file="/Group_tem.mako"/>

<%block name="classes">
	<Class name="Interlocking" rules="update" traces="error">
		<Objects>
			<xsl:apply-templates select="//CBI[@ID=$CBI_ID]"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="CBI">
	<xsl:variable name="id" select="@ID"/>
			<Object name="CBIS_{$id}" rules="update" traces="error">
				<Properties>
					<Property name="UserGroup" dt="string">User/A</Property>				
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_group_tem.mako" import="group_object_template"/>