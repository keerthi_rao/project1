<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
  	<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
</%block>

<!-- Template for generating equations from Platforms Table-->
<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="nm" select="@Name"/>
	<xsl:for-each select="1 to 20">
		<xsl:variable name="pos" select="format-number(position(),'#')"/>
		<%
			xx = '<xsl:value-of select="$pos"/>_<xsl:value-of select="$nm"/>.S2KNumericProcessVariable'
		%>
		${Equipment('PSDDoorStatus','ClosedAndLocked',[('PSD_DOOR_NOT_LOCKED[0]','(DOORSTATUS%s &amp; 32) == 0'%xx),
			   ('PSD_DOOR_LOCKED[1]','(DOORSTATUS%s &amp; 32) &gt; 0'%xx),
			   ('PSD_DOOR_LOCKED_UNKNOWN[2]','1')], [], 'N','Y')}

		${Equipment('PSDDoorStatus','ERMOperated',[('PSD_DOOR_ERM_NOT_OPERATED[0]','(DOORSTATUS%s &amp; 1) == 0'%xx),
			   ('PSD_DOOR_ERM_OPERATED[1]','(DOORSTATUS%s &amp; 1) &gt; 0'%xx),
			   ('PSD_DOOR_ERM_OPERATED_UNKNOWN[2]','1')], [], 'N','Y' )}
			   
		${Equipment('PSDDoorStatus','InhibitStatus',[('PSD_DOOR_NOT_INHIBITED[0]','(DOORSTATUS%s &amp; 32768) == 0'%xx),
			   ('PSD_DOOR_INHIBITED[1]','(DOORSTATUS%s &amp; 32768) &gt; 0'%xx),
			   ('PSD_DOOR_INHIBITED_UNKNOWN[2]','1')], [], 'N','Y')}
			   
		${Equipment('PSDDoorStatus','Isolated',[('PSD_DOOR_NOT_ISOLATED[0]','(DOORSTATUS%s &amp; 2) == 0'%xx),
			   ('PSD_DOOR_ISOLATED[1]','(DOORSTATUS%s &amp; 2) &gt; 0'%xx),
			   ('PSD_DOOR_ISOLATED_UNKNOWN[2]','1')], 
			   [('PSD_DOOR_ISOLATED_ALARM[0]', '(DOORSTATUS%s &amp; 2) &gt; 0'%xx)], 'N','Y')}
			   
		${Equipment('PSDDoorStatus','OpenStatus',[('PSD_DOOR_NOT_OPEN[0]','(DOORSTATUS%s &amp; 16) == 0'%xx),
				('PSD_DOOR_OPEN[1]','(DOORSTATUS%s &amp; 16) &gt; 0'%xx),
			    ('PSD_DOOR_OPEN_UNKNOWN[2]','1')], [], 'N','Y')}
			   
		${Equipment('PSDDoorStatus','DoorFault',[('PSD_DOOR_NO_FAULT[0]','(DOORSTATUS%s &amp; 1164) == 0'%xx),
			   ('PSD_DOOR_FAULT[1]','(DOORSTATUS%s &amp; 1164) &gt; 0'%xx),
			   ('PSD_DOOR_FAULT_UNKNOWN[2]','1')], [], 'N','Y')}
		##  Newly Added DCUFault State -->
		${Equipment('PSDDoorStatus','DCUFault',[('PSD_DCU_NO_FAULT_STATUS[0]','(DOORSTATUS%s &amp; 4) == 0'%xx),
		('PSD_DCU_FAULT_STATUS[1]','(DOORSTATUS%s &amp; 4) &gt; 0'%xx),
		('PSD_DCU_FAULT_UNKNOWN[2]','1')],[('PSD_DOOR_DCU_FAULT[0]', '(DOORSTATUS%s &amp; 4) &gt; 0'%xx)], 'N', 'Y')}
		##  Newly Added DriveMotorFault State -->
		${Equipment('PSDDoorStatus','DriveMotorFault',[('PSD_DRIVE_MOTOR_NO_FAULT_STATUS[0]','(DOORSTATUS%s &amp; 8) == 0'%xx),
		('PSD_DRIVE_MOTOR_FAULT_STATUS[1]','(DOORSTATUS%s &amp; 8) &gt; 0'%xx),
		('PSD_DRIVE_MOTOR_FAULT_UNKNOWN[2]','1')],[('PSD_DOOR_DRIVE_MOTOR_FAULT[0]', '(DOORSTATUS%s &amp; 8) &gt; 0'%xx)], 'N', 'Y')}		
		##  Newly Added LockFailure State -->
		${Equipment('PSDDoorStatus','LockFailure',[('PSD_LOCKFAILURE_NO_FAULT_STATUS[0]','(DOORSTATUS%s &amp; 128) == 0'%xx),
		('PSD_LOCKFAILURE_FAULT_STATUS[1]','(DOORSTATUS%s &amp; 128) &gt; 0'%xx),
		('PSD_LOCKFAILURE_FAULT_UNKNOWN[2]','1')],[('PSD_DOOR_LOCK_FAILURE[0]', '(DOORSTATUS%s &amp; 128) &gt; 0'%xx)], 'N', 'Y')}		
		
		${Equipment('PSDDoorStatus','MotorCurrentWarning',[('PSD_DOOR_MOTOR_NO_WARNING[0]','(DOORSTATUS%s &amp; 4096)==0'%xx),
		('PSD_DOOR_MOTOR_WARNING[1]','(DOORSTATUS%s &amp; 4096) &gt; 0'%xx),('PSD_DOOR_MOTOR_UNKNOWN[2]','1')],
		[('PSD_DOOR_MOTOR_CURRENT[0]', '(DOORSTATUS%s &amp; 4096) &gt; 0'%xx)], 'N', 'Y')}		
		
		${Equipment('PSDDoorStatus','PositionError',[('PSD_DOOR_POSITION_ERROR_NO_FAULT[0]','(DOORSTATUS%s &amp; 8192)==0'%xx),
		('PSD_DOOR_POSITION_ERROR_FAULT[1]','(DOORSTATUS%s &amp; 8192) &gt; 0'%xx),('PSD_DOOR_POSITION_ERROR_UNKNOWN[2]','1')],
		[('PSD_DOOR_POSITION_ERROR[0]', '(DOORSTATUS%s &amp; 8192) &gt; 0'%xx)], 'N', 'Y')}		
		
		${Equipment('PSDDoorStatus','MaximumOperatingTorqueExceed',[('PSD_DOOR_MAX_TORQUE_NO_FAULT[0]','(DOORSTATUS%s &amp; 16384)==0'%xx),
		('PSD_DOOR_MAX_TORQUE_FAULT[1]','(DOORSTATUS%s &amp; 16384) &gt; 0'%xx),('PSD_DOOR_MAX_TORQUE_UNKNOWN[2]','1')],
		[('PSD_DOOR_MAX_TORQUE[0]', '(DOORSTATUS%s &amp; 16384) &gt; 0'%xx)], 'N', 'Y')}		

		##  Newly Added Obstruction State -->
		${Equipment('PSDDoorStatus','Obstruction',[('PSD_OBSTRUCTION_NO_FAULT_STATUS[0]','(DOORSTATUS%s &amp; 1024) == 0'%xx),
		('PSD_OBSTRUCTION_FAULT_STATUS[1]','(DOORSTATUS%s &amp; 1024) &gt; 0'%xx),
		('PSD_OBSTRUCTION_FAULT_UNKNOWN[2]','1')],[], 'N', 'Y')}	

				
	</xsl:for-each>			   
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="Equipment(type, flavor, list, diflist, isdiff, isstate)" >
  <Equipment name="{$nm}.PSDStatus.Door{position()}" type="${type}" flavor="${flavor}">
  	% if isdiff == 'Y':
<xsl:attribute name="AlarmTimeOut">0</xsl:attribute>
	% endif
	% if isstate == 'Y':
	<States>
	% for name,equation in list:
     <Equation>${name}::${equation}</Equation>
	% endfor
	</States>
	% endif
	% if isdiff == 'Y':
	<DifferedAlarms>
		% for name,equation in diflist:
		 <Equation>${name}::${equation}</Equation>
		% endfor
	</DifferedAlarms>
	% endif
  </Equipment>
</%def>