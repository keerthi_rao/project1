<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<xsl:variable name="sig_curr" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
<xsl:variable name="sig_all" select="//Signalisation_Area"/>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>	
			<xsl:apply-templates select="//ATC_Equipment[(ATC_Equipment_Type='LC' and @ID=$sig_curr/sys:lc/@id) or (ATC_Equipment_Type='ZC' and @ID=$sig_curr/sys:zc/@id)]"/>
			<xsl:apply-templates select="//CBI[@ID=$sig_curr/sys:cbi/@id]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="ATC_Equipment|CBI">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="ty" select="if (ATC_Equipment_Type) then (ATC_Equipment_Type) else 'CBI'"/>
	<xsl:variable name="lc_zc_cbi" select="if ($ty='LC') then ('LCS_') else if ($ty='ZC') then ('ZCS_') else ('CBIS_')"/>
	<xsl:variable name="ssid" select="SSID"/>
	%for v in ["A","B"]:
		<xsl:variable name="loc_and_fepnm" select="if ($sig_all[@ID=//ATS[SSID=$ssid]/ATS_Area_ID and contains(Localisation_Type,'Depot')]) then (' DATS FEP ',concat('DATS_FEP_',$ssid,'${v}')) else (' LATS FEP ',concat('FEP_','${v}'))"/>
		<xsl:variable name="alarm_label" select="concat('Sector %s1:',$loc_and_fepnm[1],'%s2 - ',$ty,' Total Communication Failure')"/>
		<xsl:variable name="bstrparam1" select="//Urbalis_Sector[@ID=//ATS[SSID=$ssid]/Urbalis_Sector_ID]/@Name"/>
		<xsl:variable name="bstrparam2" select="if ($sig_all[@ID=//ATS[SSID=$ssid]/ATS_Area_ID and contains(Localisation_Type,'Depot')]) then (concat('FEP ',$ssid,'${v}')) else concat('FEP ','${v}')"/>
		<xsl:variable name="name_pv_trig" select="concat($lc_zc_cbi,$id,'.Links.PV_Links_','${v}')"/>
		<xsl:variable name="mul_name" select="concat($lc_zc_cbi,$id)"/>
		<xsl:variable name="t" select="//Technical_Room[@ID=current()/Technical_Room_ID]"/>
		<xsl:variable name="mul_eqpid" select="concat(substring-after($t/@Name,'SER_'),'/SIG/ATS/',$loc_and_fepnm[2])"/>
		<xsl:variable name="objnm" select="concat($lc_zc_cbi,@ID,'.Alarm_TotalCom_FEP_${v}')"/>
		${EqpObj('{$objnm}','<xsl:value-of select="$alarm_label"/>','<xsl:value-of select="$bstrparam1"/>','<xsl:value-of select="$bstrparam2"/>','<xsl:value-of select="$name_pv_trig"/>','$mul_eqpid','$mul_name')}
	%endfor
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,prop"/>

<%def name="EqpObj(objnm,alarm_label,bstrparam1,bstrparam2,name_pv_trig,mul_eqpid,mul_name,out_pv_trig='Value',severity='1',state_pv_trig='0',mul_omb='B',mul_valon='Fault',mul_valoff='Normal',mul_avalanche='N')">
	<Object name="${objnm}" rules="update_or_create">
		<Properties>					
			<Property name="Alarm_Label" dt="string">${alarm_label}</Property>		
			<Property name="BstrParam1" dt="string">${bstrparam1}</Property>		
			<Property name="BstrParam2" dt="string">${bstrparam2}</Property>		
			<Property name="Name_PV_Trig" dt="string">${name_pv_trig}</Property>		
			<Property name="Out_PV_Trig" dt="string">${out_pv_trig}</Property>		
			<Property name="Severity" dt="i4">${severity}</Property>		
			<Property name="State_PV_Trig" dt="i4">${state_pv_trig}</Property>	
			${multilingualvalue("%s"%mul_eqpid,'EquipmentID')}
			${multilingualvalue("'%s'"%mul_omb,'OMB')}							
			${multilingualvalue("%s"%mul_name,'Name')}
			${multilingualvalue("'%s'"%mul_valon,'Value_on')}			
			${multilingualvalue("'%s'"%mul_valoff,'Value_off')}			
			${multilingualvalue("'%s'"%mul_avalanche,'Avalanche')}	
		</Properties>
	</Object>
</%def>
