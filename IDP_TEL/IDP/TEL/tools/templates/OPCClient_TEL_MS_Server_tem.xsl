<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KIOOPCServerMonitor" %>

<%block name="classes">
		<Class name="S2KIOOPCServerMonitor">
			<Objects>
					<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID and   @Signalisation_Area_ID=//Signalisation_Area[(Area_Type='ATS_Central_Server') or (Area_Type='ATS_Local_Server')]/@ID]"/>
			</Objects>
		</Class>
		<Class name="OPCGroup">
			<Objects>
				<Object name="Split_MS" rules="update_or_create">
					<Properties>
						${prop("OPCClientRef",'OPCClient_MS')}
						${prop("SharedOnIdentifier",'Split_MS')}
					</Properties>
				</Object>
			</Objects>
		</Class>
</%block>


<xsl:template match="Function">
	<xsl:variable name="ats_eqps" select="//ATS_Test_Equipments/ATS_Equipment[ATS_Equipment_Type='Maintenance Server' and  (Function_ID_List/Function_ID=current()/@ID or Signalisation_Area_ID=$SERVER_ID)]/@Name"/>
	<Object name="OPCClient_MS" rules="update_or_create">
		<Properties>
			${prop("ConnectAttemptsPeriod","10","i4")}
			${prop("DoubleLinks",1,"boolean")}
			${prop("MonitoringPeriod","30","i4")}
			${prop("MultiActive","1","boolean")}
			${prop("ServerNodeName1",'<xsl:value-of select="$ats_eqps[1]"/>')}
			${prop("ServerNodeName2",'<xsl:value-of select="$ats_eqps[2]"/>')}
			${prop("ServerProgID1","S2K.OpcServer.1")}
			${prop("ServerProgID2","S2K.OpcServer.1")}
			${prop("SharedOnIdentifierDefinition","1","boolean")}
			${prop("SharedOnIdentifier",'OPCClient_MS')}
		</Properties>
	</Object>
</xsl:template>




## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>

