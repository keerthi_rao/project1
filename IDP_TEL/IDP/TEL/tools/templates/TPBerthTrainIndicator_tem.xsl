<%inherit file="/Object_tem.mako"/>
<%! class_ = "TPBerthTFView" %>

<%block name="classes">
	<Class name="TPBerthTFView">
    	<Objects>
    	  <xsl:apply-templates select="//Block[sys:sigarea/@id =$SERVER_ID]"/>
  		</Objects>
    </Class>
</%block>

<!-- This block will calculate properties and multilingual value of TPPerthTFView -->
<xsl:template match="Block">
	<xsl:variable name="ttnm" select="concat('TI_', @Name, '.TrainIndicator')"/>
	<Object name="{$ttnm}" rules="update_or_create">
   		<Properties>
	   		 ${props([	('long_7','string','CCDrivingMode'),
	   		 			('long_8','string','CCEBStatus'), 
     		 			('bstr_1','string','RollingStockID'),
        	 			('long_2','string','Direction'),
						('long_1','string','TRAIN_READINESS_STATE'),
						('bool_1','string','bDelay'),
						('bool_2','string','bAdvance'),
						('long_3','string','AlarmActivityLV1'),
						('long_4','string','AlarmActivityLV2'),
						('bool_3','string','CCHold'),
						('long_5','string','CCCouplingStatus'),
						('long_6','string','CCInMotion'),
						('bstr_2','string','ServiceID'),
						('bstr_3','string','TripID'),
						('bstr_4','string','DestinationCode'),						
						('bool_4','string','CREEP_MODE_STATUS'),
						('bool_5','string','CCTalkative'),
						('bool_6','string','ForbidForUnstabling'),
						('bool_7','string','REMOTE_PROHIBITION_CBTC'),
						('long_9','string','ATCTrainID'),
						('ustr_1','string','HoldSkipList'),
        	 			('TrainNumber','i4','1')])}        		 			
        	<PropertyList name="TrackPortion">
            	<ListElem index="1" dt="string">TI_<xsl:value-of select="@Name"/>.TrackPortion</ListElem>
            </PropertyList>
            ${multilingualvalue("$ttnm")}            		
		</Properties>
	</Object>
	<xsl:variable name="ttnm" select="concat('TI_', @Name , '.TrainIndicator2')"/>
	<Object name="{$ttnm}" rules="update_or_create">
		<Properties>
			${props([('long_1','string','CCEffectiveEvacuationRequest'),
					('long_2','string','TrainState'),
					('long_3','string','CCExtremity1Orientation'),			
					('long_4','string','CCLightSleepStatus'),	
					('long_5','string','TF_TrainOperatingMode'),			
					('long_6','string','CCRealStopAreaState'),	
					('long_7','string','CCSpeed'),					
					('long_8','string','CCServiceID'),	
					('long_9','string','SkipArea_PB'),
        			('TrainNumber','i4','1')])}
    		<PropertyList name="TrackPortion">
				<ListElem index="1" dt="string">TI_<xsl:value-of select="@Name"/>.TrackPortion</ListElem>
        	</PropertyList>
        	${multilingualvalue("$ttnm")}
		</Properties>	
	</Object>    
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>
