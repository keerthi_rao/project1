<%inherit file="/Object_tem.mako"/>
<%! class_ = "AlarmOnLowRateOfChange" %>

<xsl:variable name="servers" select="if (//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server') then (//Signalisation_Area[Area_Type='ATS_Local_Server' and not(contains(Localisation_Type,'Depot'))]/@ID) else $SERVER_ID"/>
<xsl:variable name="sig_file" select="//Signalisation_Area[@ID=$servers]"/>

<%block name="classes">
	<Class name="AlarmOnLowRateOfChange">
	     <Objects>	
			<xsl:apply-templates select="//ATC_Equipment[(ATC_Equipment_Type='LC' and @ID=$sig_file/sys:lc/@id) or (ATC_Equipment_Type='ZC' and @ID=$sig_file/sys:zc/@id)]"/>
			<xsl:apply-templates select="//CBI[@ID=$sig_file/sys:cbi/@id]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="ATC_Equipment|CBI">
	<xsl:variable name="name" select="@Name"/>
	<xsl:variable name="ty" select="if (ATC_Equipment_Type) then (ATC_Equipment_Type) else 'CBI'"/>
	<xsl:variable name="lc_zc_cbi" select="if ($ty='LC') then ('LCS_') else if ($ty='ZC') then ('ZCS_') else ('CBIS_')"/>
	<xsl:variable name="llTresholdExceed" select="if ($ty='LC') then (125) else if ($ty='ZC') then (238) else (240)"/>
	<xsl:variable name="period" select="if ($ty='LC') then (600) else if ($ty='ZC') then (60) else (60)"/>
	<xsl:variable name="sig_nm" select="substring-after($name,'_')"/>
	%for v in ["A","B"]:
		%for w in ["A","B"]:
			<xsl:variable name="nameGroup" select="concat('OPCClient_UA_Group_FEP_',$sig_nm,'_Maintenance')"/>
			<xsl:variable name="soi_opcClient" select="concat('OPCClient_UA_FEP_',$sig_nm,'_Maintenance')"/>
			<xsl:variable name="objnm" select="concat($lc_zc_cbi,@ID,'.FEP_${v}.Status_Link${w}')"/>
			<xsl:variable name="fep" select="if ('${v}'='A') then 'FEP1' else 'FEP2'"/>
			<xsl:variable name="counter" select="if ('${w}'='A') then 'Counter_Blue' else 'Counter_Red'"/>
			<xsl:variable name="address" select="concat('[2:FEP_NS_2]&lt;Organizes&gt;2:Counters&lt;Organizes&gt;2:',$fep,'&lt;Organizes&gt;2:',$name,'&lt;HasComponent&gt;2:',$counter)"/>
			${obj()}
		%endfor
	%endfor
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

<%def name="obj()">
	<Object name="{$objnm}" rules="update_or_create">
		<Properties>					
			<Property name="AddressInLevel1" dt="string"><xsl:value-of select="$address"/></Property>		
			<Property name="LLThresholdValue" dt="i4"><xsl:value-of select="$llTresholdExceed"/></Property>		
			<Property name="Period" dt="i4"><xsl:value-of select="$period"/></Property>		
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="$soi_opcClient"/></Property>				
			${multilingualvalue("$nameGroup",'Name_OPCGroup')}	
		</Properties>
	</Object>
</%def>
