<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMITrainAlarmMonitored" %>



<%block name="classes">
	<Class name="HMITrainAlarmMonitored">
		<Objects>
			<!-- Call template for generating object-->
			<xsl:apply-templates select="//Train_Unit"/>
		</Objects>
	</Class>
</%block>

<!-- Template for generating object-->
<xsl:template match="Train_Unit">
		<xsl:variable name="id" select="@ID"/>
		<xsl:variable name="tcid" select="Train_Unit_Characteristics_ID"/>
		<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
			<xsl:if test="number(@ID)&lt;=number('30')">
				<xsl:for-each select="//HMITrainAlarmMonitored/Alarm[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
					<Object name="{concat($TrainName,@Name)}" rules="update_or_create">
						<Properties>
							<Property name="TrainIdentifier" dt="string"><xsl:value-of select="$id"/></Property>
							<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="concat($TrainName,@Name)"/></Property>
							<Property name="AcceptedKeys" dt="string"><xsl:value-of select="Key"/></Property>
							<Property name="MimicList" dt="string"><xsl:value-of select="if(current()/MMS='S') then ('View_Submit_Service_Request') else (null)"/></Property>
							<xsl:variable name="CustomLabel01"><xsl:value-of select="if(Localisation != '0') then concat('2',format-number($id,'000'),Localisation) else '_'"/></xsl:variable>
							<xsl:variable name="customlbl02"><xsl:value-of select="if(O_M_B='0') then ('-') else (O_M_B)"/></xsl:variable>
							<xsl:variable name="customlbl04"><xsl:value-of select="Avalanche"/></xsl:variable>
							<xsl:variable name="customlbl05"><xsl:value-of select="VALUE"/></xsl:variable>
							${multilingualvalue('$TrainName')}
							${multilingualvalue('$CustomLabel01', 'CustomLabel01')}
							${multilingualvalue('$customlbl02', 'CustomLabel02')}
							${multilingualvalue("'-'", 'CustomLabel03')}
							${multilingualvalue('$customlbl04', 'CustomLabel04')}
							${multilingualvalue('$customlbl05', 'CustomLabel05')}
						</Properties> 
					</Object>	
				</xsl:for-each>
			</xsl:if>
			<xsl:for-each select="//HMITrainAlarmMonitored_Loco/Alarm_Loco[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
				<xsl:variable name="mimi" select="if(current()/MMS='S') then ('View_Submit_Service_Request') else (null)"/>
				<xsl:if test="//Train_Unit_Characteristics[@ID=$tcid]/@Name='TRUC_LOCO'"> 
				<xsl:variable name="trainAlarmName" select="concat($TrainName,@Name)"/>
					<Object name="{concat($TrainName,@Name)}" rules="update_or_create">
						<Properties>
							<Property name="TrainIdentifier" dt="string"><xsl:value-of select="$id"/></Property>
							<Property name="AcceptedKeys" dt="string"><xsl:value-of select="@Key"/></Property>
							<Property name="MimicList" dt="string"><xsl:value-of select="$mimi"/></Property>
					    	<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="concat($TrainName,@Name)"/></Property>	
							<xsl:variable name="CustomLabel01"><xsl:value-of select="if(Localisation != '0') then concat('L',format-number($id,'000'),Localisation) else '_'"/></xsl:variable>
							<xsl:variable name="customlbl02"><xsl:value-of select="if(O_M_B='0') then ('-') else (O_M_B)"/></xsl:variable>
							<xsl:variable name="customlbl04"><xsl:value-of select="Avalanche"/></xsl:variable>
							<xsl:variable name="customlbl05"><xsl:value-of select="VALUE"/></xsl:variable>
							${multilingualvalue('$trainAlarmName')}
							${multilingualvalue('$CustomLabel01', 'CustomLabel01')}
							${multilingualvalue('$customlbl02', 'CustomLabel02')}
							${multilingualvalue("'-'", 'CustomLabel03')}
							${multilingualvalue('$customlbl04', 'CustomLabel04')}
							${multilingualvalue('$customlbl05', 'CustomLabel05')}
						</Properties> 
					</Object>
				</xsl:if>	 		
			</xsl:for-each>		
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>
