<%inherit file="/SigRules_tem.mako"/>
<xsl:variable name="apptps" select="//Signalisation_Area[@ID = $SERVER_ID]/Block_ID_List/Block_ID"/>
<%block name="main">
	<!-- Template RouteSettingObject -->
	<xsl:apply-templates select="//RouteSettingObjects/RouteSettingObject[TrackPortions_List/TrackPortion_ID[1] = $apptps]"/>
</%block>

<!-- Template RouteSettingObject -->
<xsl:template match="RouteSettingObject">
	<!-- Variables for <States> equations-->
	<xsl:variable name="routes" select="//TWPRoute[@Name=//Route[sys:sigarea/@id=$SERVER_ID and @ID= current()/@ID]/@Name]"/>
	<xsl:variable name="rseteqn" select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then (for $item in ValueSet_List/ValueSet return concat('RSO2_',@Name,'==',$item)) 
										 else (if (OPCServer_StateElement != '') then (for $item in ValueSet_List/ValueSet return concat(OPCServer_StateElement,'==',$item)) else null)"/>
	<xsl:variable name="rnotseteqn" select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then (for $item in ValueNotSet_List/ValueNotSet return concat('RSO2_',@Name,'==',$item)) 
										 else (if (OPCServer_StateElement != '') then (for $item in ValueNotSet_List/ValueNotSet return concat(OPCServer_StateElement,'==',$item)) else null)"/>
	<xsl:variable name="stateeqn_set" select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then concat('(??RSO2_',@Name,')',' &amp;&amp; (',replace($rseteqn,' ',' + '),')') else $rseteqn"/>
	<xsl:variable name="stateeqn_notset" select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then concat('(??RSO2_',@Name,')',' &amp;&amp; (',replace($rnotseteqn,' ',' + '),')') else $rseteqn"/>
	<Equipment name="{@Name}" type="RouteSettingObject" flavor="Detection" CommandTimeOut="{CommandResultTimeout}" ImmediatsTimeOut="{ImmediateResultTimeout}">
		<xsl:if test="OPCServer_ControlElement != ''">
			<Requisites>
				<Equation><xsl:value-of select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then concat('RSOC2_',@Name) else OPCServer_ControlElement"/>[RD]::1</Equation>
				<Equation><xsl:value-of select="if (OPCServer_ProgID != '' and OPCServer_StateElement != '') then concat('RSOC2_',@Name) else OPCServer_ControlElement"/>[RC]::<xsl:value-of select="if (OPCServer_ProgID != '' and OPCServer_RequisiteElement != '') then concat('(','RSOREQ2_',@Name,'==1',')') else concat('(',OPCServer_RequisiteElement,'==1',')')"/> <xsl:value-of select="if($routes/@ID!='') then (concat(' &amp;&amp; ','(',$routes[1]/@Plug_Requesite,'=true',')')) else null"/></Equation>
			</Requisites>
		</xsl:if>
	</Equipment>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="lib_tem_tel.mako" import="Equipment"/>