<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
			<xsl:apply-templates select="//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID and (ATS_Equipment_Type='Central Server' or ATS_Equipment_Type='Local Server')]"/>
  		</Objects>
  	</Class>
</%block>

<xsl:template match="ATS_Equipment">
	<xsl:variable name="cname" select="@Name"/>
	<xsl:if test="//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Central_Server']/Localisation_Type='Mainline'">
		<xsl:variable name="nm" select="@Name"/>
		${obj('.NMS_Alarm_LinkA','CATS Server %s1 - NMS Link A Failure')}
		${obj('.NMS_Alarm_LinkB','CATS Server %s1 - NMS Link B Failure')}
		${obj('.NMS_Alarm_TotalComFail','CATS Server %s1 - NMS Total Communication Failure')}
	</xsl:if>
	<xsl:if test="contains(//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/Localisation_Type,'Depot')">
		<xsl:variable name="nm" select="@Name"/>
		${obj('.NMS_Alarm_LinkA','Sector %s1: DATS Server %s2 - NMS Link A Failure')}
		${obj('.NMS_Alarm_LinkB','Sector %s1: DATS Server %s2 - NMS Link B Failure')}
		${obj('.NMS_Alarm_TotalComFail','DATS Server %s1 - NMS Total Communication Failure')}
	</xsl:if>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>



<%def name="obj(typ,alarmlabel)">
	<Object name="{$nm}${typ}" rules="update_or_create">
		<Properties>
				<xsl:variable name="varCnt" select="count(tokenize('${alarmlabel}','%s'))-1"/>
				<xsl:variable name="opv1link" select="if('${typ}'='.NMS_Alarm_TotalComFail') then (if(ends-with($cname,'A')) then 'IntOutputPlug_1' else 'IntOutputPlug_2') else (if(ends-with($cname,'A')) then (if(ends-with('${typ}','A')) then 'Connected11' else 'Connected21') else if(ends-with($cname,'B')) then (if(ends-with('${typ}','B')) then 'Connected11' else 'Connected21') else null)"/> 
				
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${alarmlabel}'"/></Property>
				  <xsl:if test="$varCnt = 1">
						<xsl:variable name="bstrp1" select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/>
						<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrp1"/></Property>
						<xsl:variable name="eqid" select="concat('MDD','/SIG/ATS/CATS_',$bstrp1)"/>
						${multilingualvalue('$eqid','EquipmentID')}
				 </xsl:if>
				  <xsl:if test="$varCnt = 2">
						<xsl:variable name="bstrp2" select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/>
				  		<Property name="BstrParam1" dt="string">SEC_<xsl:value-of select="tokenize(//Signalisation_Area[@ID=$SERVER_ID]/@Name, '_')[last()]"/></Property>
						<Property name="BstrParam2" dt="string"><xsl:value-of select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/></Property>
						<xsl:variable name="eqid" select="concat('MDD','/SIG/ATS/DATS_',$bstrp2)"/>
						${multilingualvalue('$eqid','EquipmentID')}
				</xsl:if>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="if('${typ}'='.NMS_Alarm_TotalComFail') then ('OPCClient_UA_2_NMS_DCS_OCC_1.MergeConnected12Y') else 'OPCClient_UA_2_NMS_DCS_OCC_1'"/></Property>				
				<!-- <Property name="Out_PV_Trig" dt="string"><xsl:value-of select="if(ends-with($cname,'A')) then (if(ends-with('${typ}','A')) then 'Connected11' else 'Connected21') else if(ends-with($cname,'B')) then (if(ends-with('${typ}','B')) then 'Connected11' else 'Connected21') else null"/></Property> -->
				<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$opv1link"/></Property>
				<Property name="Severity" dt="i4">2</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>
				<xsl:variable name="OMB" select="'B'"/>
				${multilingualvalue('$OMB','OMB')}
				<xsl:variable name="valon" select="'Fault'"/>
				${multilingualvalue('$valon','Value_on')}
				<xsl:variable name="valoff" select="'Normal'"/>
				${multilingualvalue('$valoff','Value_off')}				
		</Properties>
	</Object>
</%def>