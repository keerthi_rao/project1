<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>
			<xsl:apply-templates select="$sysdb//Aspect_Indicator[Direction_Indicator_ID=//Direction_Indicator[Direction_Indicator_Type='ATS Indicator' and sys:sigarea/@id=$SERVER_ID]/@ID]" mode="tt1"/>	
			<xsl:apply-templates select="$sysdb//Aspect_Indicator[Direction_Indicator_ID=//Direction_Indicator[Direction_Indicator_Type='Route Indicator' and sys:sigarea/@id=$SERVER_ID]/@ID]" mode="tt2"/>	
		 </Objects>
	</Class>
</%block>

<xsl:template match="Aspect_Indicator" mode="tt1">
		<xsl:variable name="TTName" select= "concat(@Name,'.Alarms_Gen')"/>
		<xsl:variable name="nm" select= "@Name"/>
		<xsl:variable name="uname" select="concat(substring-after(//Direction_Indicator[@ID=current()/Direction_Indicator_ID]/sys:terr/@name,'Territory_'),'/SIG/CBI/',//Direction_Indicator[@ID=current()/Direction_Indicator_ID]/@Name)"/>
${obj('{$TTName}',[('PAID','Stencil Indicator %s1: 50% or more LEDs Failed','0')])}
</xsl:template>

<xsl:template match="Aspect_Indicator" mode="tt2">
		<xsl:variable name="TTName" select= "concat(@Name,'.Alarms_Gen')"/>
		<xsl:variable name="nm" select= "@Name"/>
		<xsl:variable name="uname" select="concat(substring-after(//Direction_Indicator[@ID=current()/Direction_Indicator_ID]/sys:terr/@name,'Territory_'),'/SIG/CBI/',//Direction_Indicator[@ID=current()/Direction_Indicator_ID]/@Name)"/>
${obj('{$TTName}',[('PAID','SWPD %s1: 50% or more LEDs Failed','0')])}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->


<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,stpv in list:
		<Object name="${nm}.${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>			
				<!-- ${prop('AlarmInhib','1','boolean')}		 -->	
				${multilingualvalue("'-'",'Avalanche')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>			
				${multilingualvalue('$nm','Name')}			
				${multilingualvalue('$uname','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}		
				<Property name="Name_PV_Trig" dt="string">${key}<xsl:value-of select="concat('_',$TTName)"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${stpv}"/></Property>			
				<Property name="Operator" dt="string"></Property>			
				${multilingualvalue("'M'",'OMB')}			
				<Property name="Severity" dt="i4">2</Property>		
				${multilingualvalue("'-'",'Value')}
			</Properties>
		</Object>
	% endfor
</%def>
