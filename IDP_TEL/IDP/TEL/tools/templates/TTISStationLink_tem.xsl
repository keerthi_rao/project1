<%inherit file="/Object_tem.mako"/>
<%! class_ = "TTISStationLink" %>

<%block name="classes">
  	<Class name="TTISStationLink">
  		<Objects>
  			<!-- Generate the items of Stopping_Areas table -->
  			<xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id = $SERVER_ID and sys:stoppoint='yes']"/>
  		</Objects>
  	</Class>
</%block>

<!-- Template to match the Platform table -->
<xsl:template match="Stopping_Area">
	<xsl:variable name="sta" select="concat('TTISLink_', @Name)"/>
	<Object name="{$sta}" rules="update_or_create">
		<Properties>
			<Property name="StopPointName" dt="string"><xsl:value-of select="concat(@Name,'.ATR')"/></Property>
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,props"/>