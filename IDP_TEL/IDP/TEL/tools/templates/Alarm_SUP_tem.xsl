<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>
<xsl:variable name="mlv" select="//MLV"/>
<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>
			<xsl:variable name="sig" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
			<xsl:variable name="us" select="//Urbalis_Sector[ATS_Local_Server_Area_ID_List/Signalisation_Area_ID=$SERVER_ID]/@Name"/>
			<xsl:variable name="s2kalarms_gen">
			<Objs>
			<xsl:for-each select="//ATS_Equipment[(*/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID)]">
				<xsl:variable name="ty" select="ATS_Equipment_Type"/>
				<xsl:variable name="p" select="substring-after(@Name,'SRV')"/>
				<xsl:if test="$ty='Central Server'">
					<Object Alarm_Label="CATS Server %s1 - ISCS Total Communication Failure" BstrParam1="SRV_ATS_{$p}" EquipmentID="MDD/SIG/ATS/CATS_SRV_ATS_{$p}" OMB="B" Name="{@Name}" Severity="1" State_PV_Trig="4" Name_PV_Trig="Links_Status_EXT.ISCS.Merge" Out_PV_Trig="IntOutputPlug_1" Name_AO="{@Name}.CATS_Server_ISCS_Total_Communication_Failure" Value_off="Operational" Value_on="Not operational" Avalanche="N"/>
					<Object Alarm_Label="CATS Server %s1 - MMS Total Communication Failure" BstrParam1="SRV_ATS_{$p}" EquipmentID="MDD/SIG/ATS/CATS_SRV_ATS_{$p}" OMB="B" Name="{@Name}" Severity="1" State_PV_Trig="4" Name_PV_Trig="Links_Status_EXT.MMS.Merge" Out_PV_Trig="IntOutputPlug_1" Name_AO="{@Name}.CATS_Server_MMS_Total_Communication_Failure" Value_off="Operational" Value_on="Not operational" Avalanche="N"/>
				</xsl:if>
				<xsl:if test="$ty='Local Server' and $sig[contains(Localisation_Type,'Depot')]">
					<Object Alarm_Label="%s1: DATS Server %s2 - ISCS Total Communication Failure" BstrParam1="MDD" BstrParam2="SRV_ATS_{$p}" EquipmentID="MDD/SIG/ATS/DATS_SRV_ATS_{$p}" OMB="B" Name="{@Name}" Severity="1" State_PV_Trig="4" Name_PV_Trig="Links_Status_EXT.ISCS.Merge" Out_PV_Trig="IntOutputPlug_1" Name_AO="{@Name}.DATS_Server_ISCS_Total_Communication_Failure" Value_off="Operational" Value_on="Not operational" Avalanche="N"/>
					<Object Alarm_Label="%s1: DATS Server %s2 - MMS Total Communication Failure" BstrParam1="MDD" BstrParam2="SRV_ATS_{$p}" EquipmentID="MDD/SIG/ATS/DATS_SRV_ATS_{$p}" OMB="B" Name="{@Name}" Severity="1" State_PV_Trig="4" Name_PV_Trig="Links_Status_EXT.MMS.Merge" Out_PV_Trig="IntOutputPlug_1" Name_AO="{@Name}.DATS_Server_MMS_Total_Communication_Failure" Value_off="Operational" Value_on="Not operational" Avalanche="N"/>
				</xsl:if>
				<xsl:if test="$ty='Local Server' and $sig[contains(Localisation_Type,'Mainline')]">
					<xsl:variable name="loc" select="substring-after(//Technical_Room[@ID=current()/Technical_Room_ID]/@Name,'SER_')"/>
					<Object Alarm_Label="Sector %s1: LATS Server %s2 - ISCS Total Communication Failure" BstrParam1="{$us}" BstrParam2="SRV_ATS_{$p}" EquipmentID="{$loc}/SIG/ATS/SRV_ATS_{$p}" OMB="B" Name="{@Name}" Severity="1" State_PV_Trig="4" Name_PV_Trig="Links_Status_EXT.ISCS.Merge" Out_PV_Trig="IntOutputPlug_1" Name_AO="{@Name}.LATS_Server_ISCS_Total_Communication_Failure" Value_off="Operational" Value_on="Not operational" Avalanche="N"/>
				</xsl:if>
			</xsl:for-each>
			</Objs>
			</xsl:variable>
			<xsl:apply-templates select="$s2kalarms_gen/Objs/Object"/>
			<!-- OLD CODE -->
		 	<xsl:if test="contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot') and //ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='MDDDSERSRVA'">
				<xsl:apply-templates select="//ATS_ALARM_OPE_DATS_A/S2KAlarms_Gen"/>
			</xsl:if>
		 	<xsl:if test="contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot') and //ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='MDDDSERSRVB'">
				<xsl:apply-templates select="//ATS_ALARM_OPE_DATS_B/S2KAlarms_Gen"/>
			</xsl:if>			
		 	<xsl:if test="//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server'">
				<xsl:apply-templates select="//ATS_ALARM_OPE_CATS_A/S2KAlarms_Gen"/>
				<xsl:apply-templates select="//ATS_ALARM_OPE_CATS_B/S2KAlarms_Gen"/>
			</xsl:if>
			<xsl:if test="//ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='TE02MSERSRVA'">
			 	<xsl:apply-templates select="//ATS_ALARM_OPE_TE02_A/S2KAlarms_Gen"/>
		    </xsl:if>	
			<xsl:if test="//ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='TE02MSERSRVB'">
			 	<xsl:apply-templates select="//ATS_ALARM_OPE_TE02_B/S2KAlarms_Gen"/>
		    </xsl:if>			
			<xsl:if test="//ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='TE03MSERSRVA'">
			 	<xsl:apply-templates select="//ATS_ALARM_OPE_TE03_A/S2KAlarms_Gen"/>
		    </xsl:if>		
			<xsl:if test="//ATS_Equipment[Signalisation_Area_ID=$SERVER_ID]/@Name='TE03MSERSRVB'">
			 	<xsl:apply-templates select="//ATS_ALARM_OPE_TE03_B/S2KAlarms_Gen"/>
		    </xsl:if>			
		 </Objects>
	</Class>
</%block>

<xsl:template match="S2KAlarms_Gen">
		<xsl:variable name="TTName" select= "@Name_AO"/>
		<xsl:variable name="nm" select= "@Name"/>
		<xsl:variable name="uname" select="@EquipmentID"/>
		<xsl:variable name="o_m_b" select="@OMB"/>
		<xsl:variable name="Value_off" select="@Value_off"/>
		<xsl:variable name="Value_on" select="@Value_on"/>
		<Object name="{$TTName}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="@Alarm_Label"/></Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="@BstrParam1"/></Property>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="@BstrParam2"/></Property>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="@Name_PV_Trig"/></Property>
				<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="@Out_PV_Trig"/></Property>
				<Property name="Severity" dt="i4"><xsl:value-of select="@Severity"/></Property>				
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="@Stated_PV_Trig"/></Property>		
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$uname','EquipmentID')}		
				${multilingualvalue('$o_m_b','OMB')}			
				${multilingualvalue('$Value_off','Value_off')}			
				${multilingualvalue('$Value_on','Value_on')}			
			</Properties>
		</Object>			
</xsl:template>

<xsl:template match="Object">
	${obj()}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

<%def name="mulval(value_Var,value_nm)">
           <MultiLingualProperty name="${value_nm}">
			<xsl:for-each select="$mlv">
             <MultiLingualValue roleId="{../@RID}" localeId="{@LCID}"><xsl:value-of select="${value_Var}"/></MultiLingualValue>
			</xsl:for-each>
           </MultiLingualProperty>
</%def>

<%def name="obj()">
<xsl:variable name="TTName" select= "@Name_AO"/>
	<xsl:variable name="nm" select= "@Name"/>
	<xsl:variable name="uname" select="@EquipmentID"/>
	<xsl:variable name="o_m_b" select="@OMB"/>
	<xsl:variable name="Value_off" select="@Value_off"/>
	<xsl:variable name="Value_on" select="@Value_on"/>
	<xsl:variable name="Avalanche" select="@Avalanche"/>
	<Object name="{$TTName}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="@Alarm_Label"/></Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="@BstrParam1"/></Property>
			<xsl:if test="@BstrParam2"><Property name="BstrParam2" dt="string"><xsl:value-of select="@BstrParam2"/></Property></xsl:if>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="@Name_PV_Trig"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="@Out_PV_Trig"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="@Severity"/></Property>				
			<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="@State_PV_Trig"/></Property>		
			${mulval('$nm','Name')}
			${mulval('$uname','EquipmentID')}		
			${mulval('$o_m_b','OMB')}			
			${mulval('$Value_off','Value_off')}			
			${mulval('$Value_on','Value_on')}			
			${mulval('$Avalanche','Avalanche')}			
		</Properties>
	</Object>
</%def>