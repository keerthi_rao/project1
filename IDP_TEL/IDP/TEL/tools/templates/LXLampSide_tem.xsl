<%inherit file="/Object_tem.mako"/>
<%! class_ = "U400Equipment" %>
 
<%block name="classes">
	<Class name="U400Equipment">
	     <Objects>
			 <xsl:apply-templates select="//LX[contains(@Name,'LX3') and LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="LX">
	% for nm in ['LXAI_Lamp_Side_A','LXAI_Lamp_Side_B','LXLP_Lamp_Side_A', 'LXLP_Lamp_Side_B']:	
		 <xsl:variable name="name" select="concat(@Name,'.' ,'${nm}')"/>
		 <Object name="{$name}" rules="update_or_create">
			  <Properties>
					<Property name="BasicObject" dt="string"><xsl:value-of select="$name"/></Property>
					<Property name="Flavor" dt="string">LXLampSide</Property>
					<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$name"/></Property>
					<!--Property name="States" dt="string"></Property-->
			 </Properties>
		  </Object>
	% endfor	  
</xsl:template>
