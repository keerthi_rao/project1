<%inherit file="/Object_tem.mako"/>
<%! class_ = "MMSDat" %>

<%block name="classes">
	<Class name="MMSDat">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="atsid" select="//Signalisation_Area[@ID = $SERVER_ID]/sys:ats/@id"/>
	<xsl:variable name="ID" select="@ID"/>
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:for-each select="$atsid">
	  <Object name="{$TrainName}.MMSDat.{current()}" rules="update_or_create">
		  <Properties>
				<xsl:variable name="rsmevent" select="concat(current(),'.RSMEvents_',format-number($ID,'00'))"/>
				${prop('Shared_RSMEvents','<xsl:value-of select="$rsmevent"/>')}
				${prop('ValidityBit','LSB')}
		  </Properties>
	  </Object>	
	</xsl:for-each>

</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
