<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
	<xsl:apply-templates select="//Platforms/Platform[@ID = $sysdb//Stopping_Area[sys:blockid/@id = $sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]/Block_ID]/Original_Area_List/Original_Area[@Original_Area_Type = 'Platform']/@ID]"/>
</%block>

<!-- Template for generating equations from Platforms Table-->
<xsl:template match="Platform">
	<xsl:variable name="sablocks" select="//Stopping_Area[Original_Area_List/Original_Area/@ID = current()/@ID and Original_Area_List/Original_Area/@Original_Area_Type = 'Platform']/sys:blockid/@id"/>
	<xsl:variable name="spks" select="//SPKS[Block_ID=$sablocks]"/>
	<Equipment name="{@Name}.SPKSSummary" type="SPKSSummary" flavor="ESP">
			<States>
				<xsl:if test="$spks[contains(@Name, 'ESP')]">
				<Equation>ESP_ACTIVATED[0]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'ESP')] return(concat('(',$spk/@Name,'.Status.Value == 0)'))" separator=" || "/>)</Equation>
				<Equation>ESP_NOT_ACTIVATED[1]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'ESP')] return(concat('(',$spk/@Name,'.Status.Value == 1)'))" separator=" &amp;&amp; "/>)</Equation>
				</xsl:if>
				<Equation>ESP_UNKNOWN[2]::1</Equation>
			</States>
	</Equipment>
	<Equipment name="{@Name}.SPKSSummary" type="SPKSSummary" flavor="ESS">
			<States>
				<xsl:if test="$spks[contains(@Name, 'ESS')]">
				<Equation>ESS_ACTIVATED[0]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'ESS')] return(concat('(',$spk/@Name,'.Status.Value == 0)'))" separator=" || "/>)</Equation>
				<Equation>ESS_NOT_ACTIVATED[1]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'ESS')] return(concat('(',$spk/@Name,'.Status.Value == 1)'))" separator=" &amp;&amp; "/>)</Equation>
				</xsl:if>
				<Equation>ESS_UNKNOWN[2]::1</Equation>
			</States>
	</Equipment>
	
	<Equipment name="{@Name}.SPKSSummary" type="SPKSSummary" flavor="SPKS">
			<States>
				<xsl:if test="$spks[contains(@Name, 'HW')]">
				<Equation>SPKS_ACTIVATED[0]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'HW')] return(concat('(',$spk/@Name,'.Status.Value == 0)'))" separator=" || "/>)</Equation>
				<Equation>SPKS_NOT_ACTIVATED[1]::(<xsl:value-of select="for $spk in $spks[contains(@Name, 'HW')]return(concat('(',$spk/@Name,'.Status.Value == 1)'))" separator=" &amp;&amp; "/>)</Equation>
				</xsl:if>
				<Equation>SPKS_UNKNOWN[2]::1</Equation>
			</States>
	</Equipment>	   
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="Equipment(type, flavor, list)" >
  <Equipment name="{concat('ATS_',$SERVER_ID,'.OTHERATSTEL.ATS_',@ID)}" type="${type}" flavor="${flavor}">
	<States>
	% for name,equation in list:
     <Equation>${name}::${equation}</Equation>
	% endfor
	</States>
  </Equipment>
</%def>