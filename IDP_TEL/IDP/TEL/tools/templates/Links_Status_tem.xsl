<%inherit file="/Object_tem.mako"/>
<%! class_ = "Links_Status" %>

<%block name="classes">
  	<Class name="Links_Status">
  		<Objects>
			<xsl:if test="$SERVER_LEVEL='_LV1'">
				<xsl:apply-templates select="//ZCs/ZC[@ID = //Signalisation_Area[@ID = $SERVER_ID]/sys:zc/@id]"/>	
				<xsl:apply-templates select="//CBIs/CBI[@ID = //Signalisation_Area[@ID = $SERVER_ID]//sys:cbi/@id]"/>	
				<xsl:apply-templates select="//ATC_Equipments/ATC_Equipment[ATC_Equipment_Type = 'LC' and @ID = (//Signalisation_Area[@ID = $SERVER_ID]/sys:lc/@id)]"/>	
			</xsl:if>
			<xsl:if test="$SERVER_LEVEL='_LV2'">	
 	  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
			</xsl:if>
		</Objects>
  	</Class>
</%block>

<xsl:template match="ZC|CBI|ATC_Equipment">
	<xsl:variable name="locname" select="if(local-name()='ATC_Equipment') then ('LC') else (local-name())"/>
	<xsl:variable name="nm" select="concat($locname,'S_',@ID,'.Links')"/>
	<Object name="{$nm}" rules="update_or_create">
		<Properties>
			<Property name="AdressInLevel1_A" dt="string">[<xsl:value-of select="@ID"/>:FEP_NS_<xsl:value-of select="@ID"/>]&lt;Organizes&gt;<xsl:value-of select="@ID"/>:OperatingMode&lt;Organizes&gt;<xsl:value-of select="@ID"/>:Diagnostic1&lt;HasComponent&gt;<xsl:value-of select="@ID"/>:State</Property>
			<Property name="AdressInLevel1_B" dt="string">[<xsl:value-of select="@ID"/>:FEP_NS_<xsl:value-of select="@ID"/>]&lt;Organizes&gt;<xsl:value-of select="@ID"/>:OperatingMode&lt;Organizes&gt;<xsl:value-of select="@ID"/>:Diagnostic2&lt;HasComponent&gt;<xsl:value-of select="@ID"/>:State</Property>
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('OPCClient_',$locname,'S_',@ID)"/></Property>
		</Properties>
	</Object>	
</xsl:template>

<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="sig" select="//Function[@Signalisation_Area_ID=//Signalisation_Area[sys:sigarea/@id=$SERVER_ID and @ID=current()/sys:sigarea/@id]/@ID and @Function_Type='FEP']"/>
	<Object name="{@Name}.State" rules="update_or_create">
		<Properties>
			<Property name="AdressInLevel1_A" dt="string">[33:FEP_NS_33]&lt;Organizes&gt;33:<xsl:value-of select="@Name"/>&lt;Organizes&gt;33:OperatingMode&lt;HasComponent&gt;33:iState</Property>
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('PSDOPCClient_PSDSERVER_',$sig[1]/@ID)"/></Property>
		</Properties>
	</Object>
</xsl:template>
