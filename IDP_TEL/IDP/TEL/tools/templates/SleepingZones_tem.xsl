<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:param name="SERVER_ID"/>

<xsl:template match="/">
	<SleepingZones > 
		<AutomaticSleepZones>
			<xsl:apply-templates select="//Stopping_Area[Original_Area_List/Original_Area/@Original_Area_Type='Stabling' and sys:sigarea/@id=$SERVER_ID]"/>
		</AutomaticSleepZones>
	</SleepingZones>
</xsl:template>

<xsl:template match="Stopping_Area">
	<Zone Name="{@Name}" TOM="20"></Zone>
</xsl:template>



<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="versions_xml"/>