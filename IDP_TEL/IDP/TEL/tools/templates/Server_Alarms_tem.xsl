<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen"%>

<xsl:param name="AREA_TYPE"/>
<xsl:variable name="sig" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
<xsl:variable name="atseqs" select="//ATS_Equipment[(contains(ATS_Equipment_Type,'Local Server') or contains(ATS_Equipment_Type,'Central Server')) and (Signalisation_Area_ID=$SERVER_ID or */Signalisation_Area_ID=$SERVER_ID)]"/>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
			<xsl:apply-templates select="$atseqs"/>
  		</Objects>
  	</Class>
</%block>

<xsl:template match="ATS_Equipment">
	<xsl:variable name="objnm" select="concat(@Name,'.Alarm_Server')"/>
	<xsl:variable name="eee" select="@Name"/>
	<xsl:variable name="nmpv" select="concat('SRV.',@Name,'.State')"/>
	<xsl:variable name="loc" select="substring-after(//Technical_Room[@ID=current()/Technical_Room_ID]/@Name,'_')"/>
	<xsl:variable name="eqid_suff_2" select="if (@Name=$atseqs[1]/@Name) then '_A' else '_B'"/>
	<xsl:variable name="eqid_suff_1" select="if ($AREA_TYPE='ATS_Central_Server') then ('CATS_SRV_ATS') else if ($sig[contains(Localisation_Type,'Depot')]) then ('DATS_SRV_ATS') else ('SRV_ATS')"/>
	<xsl:variable name="eqid" select="concat($loc,'/SIG/ATS/',$eqid_suff_1,$eqid_suff_2)"/>
	<Object name="{$objnm}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string">ATS Server %s1 Failed</Property>
			<Property name="BstrParam1" dt="string">SRV_ATS<xsl:value-of select="$eqid_suff_2"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string">StateEqpState</Property>
			<Property name="Severity" dt="i4">2</Property>
			<Property name="State_PV_Trig" dt="string">0</Property>
			${multilingualvalue('$eee','Name')}
			${multilingualvalue('$eqid','EquipmentID')}
			${multilingualvalue("'B'",'OMB')}		
			${multilingualvalue("'Fault'",'Value_on')}	
			${multilingualvalue("'Normal'",'Value_off')}	
			${multilingualvalue("'N'",'Avalanche')}	
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>


