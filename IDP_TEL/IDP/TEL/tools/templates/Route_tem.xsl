<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
  	<Class name="S2KEvents_Merge">
  		<Objects>
  			<xsl:apply-templates select="//Route[sys:cbi/@id=$CBI_ID]"/>
  		</Objects>
  	</Class>	
</%block>	

<xsl:template match="Route">
	<xsl:apply-templates select="//Events/Event[Type='Route']">
		<xsl:with-param name="parentname" select = "@Name" />
		<xsl:with-param name="block" select = "Block_ID_List/Block_ID" />
		<xsl:with-param name="terrname" select = "sys:terr/@name" />
		<xsl:with-param name="bstrp1" select = "Origin_Signal_ID" />
		<xsl:with-param name="bstrp2" select = "Destination_Signal_ID" />
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "parentname" />
	<xsl:param name = "trackid" />
	<xsl:param name = "terrname" />
	<xsl:param name = "bstrp1" />
	<xsl:param name = "bstrp2" />
	<xsl:param name = "block" />
	<xsl:variable name="te" select="substring-after(//Technical_Room[@ID=//Signal[@ID=$bstrp1]/Technical_Room_ID]/@Name,'_')"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then replace(Name_PV_Trig,'--Route.Name--',$parentname) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$parentname),'xxxx',$te)"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="//Signal[@ID=$bstrp1]/@Name"/></Property>
			<Property name="BstrParam2" dt="string"><xsl:value-of select="//Signal[@ID=$bstrp2]/@Name"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname,'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue('$parentname','Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

