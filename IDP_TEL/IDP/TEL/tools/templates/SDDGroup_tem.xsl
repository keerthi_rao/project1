<%inherit file="/Object_tem.mako"/>
<%! class_ = "SDDGroup" %>

<%block name="classes">
	<Class name="SDDGroup">
		<Objects>
		<xsl:apply-templates select="//SDD_Group_In_Operation[sys:zc/@id = $ZC_ID]"/>
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="SDD_Group_In_Operation">
		<xsl:variable name="SDDID" select="@ID"/>
		<xsl:variable name="uid" select="//Urbalis_Sector[@ID=//ZC[@ID=$ZC_ID]/Urbalis_Sector_ID]/@ID"/>
		<xsl:variable name="cl01" select="if($uid='1' or $uid='2') then (concat('MDD_',$uid,'/SIG/CBI'))
											else if($uid='17' or $uid='18') then (concat('ECID_',$uid,'/SIG/CBI'))
											else (concat('TE',$uid,'/SIG/CBI'))"></xsl:variable>	
		<xsl:variable name="CustomLabel02">O</xsl:variable>													
			<Object name="{@Name}" rules="update_or_create">
				<Properties>
					${multilingualvalue("$cl01","CustomLabel01")}
					${multilingualvalue("$CustomLabel02","CustomLabel02")}
					<Property name="Severity" dt="i4">3</Property>
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

