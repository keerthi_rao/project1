<%inherit file="/Object_tem.mako"/>
<%! class_ = "SPKSSummary" %>

<%block name="classes">
<xsl:variable name="filegeneration" select="if ($sysdb//Platforms/Platform) then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="SPKSSummary_{$SERVER_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="SPKSSummary">
	     <Objects>
			 <xsl:apply-templates select="//Platforms/Platform[@ID = $sysdb//Stopping_Area[sys:blockid/@id = $sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]/Block_ID]/Original_Area_List/Original_Area[@Original_Area_Type = 'Platform']/@ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Platform">
	<xsl:variable name="nm" select="@Name"/>
	<Object name="{@Name}.SPKSSummary" rules="update_or_create">
		  <Properties>
				<Property name="ID" dt="string"><xsl:value-of select="concat(@Name,'.SPKSSummary')"/></Property>
				<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="concat(@Name,'.SPKSSummary')"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean"><xsl:value-of select="1"/></Property>	
				${multilingualvalue("$nm")}
		  </Properties>
	  </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>