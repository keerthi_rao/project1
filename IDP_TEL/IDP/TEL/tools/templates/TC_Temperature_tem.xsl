<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
				<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and contains(@Name,'TEMP_TC')]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Generic_ATS_IO">
			<xsl:variable name="TTName" select="substring-after(@Name,'GAIO_TEMP_TC')"/>
			<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
			<xsl:variable name="uname" select="concat(tokenize(@Name,'_')[last()],'/SIG/CBI/',substring-before(substring-after(@Name,'GAIO_TEMP_'),'_'))"/>
			<Object name="{$objname}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string">SDD Cubicle %s Over Temperature Detected</Property>			
					${prop('AlarmInhib','1','boolean')}			
					<Property name="BstrParam1" dt="string"><xsl:value-of select="@Name"/></Property>							
					${multilingualvalue('$TTName','Name')}						
					${multilingualvalue('$uname','EquipmentID')}	
					${multilingualvalue("'-'",'MMS')}						
					<Property name="Name_PV_Trig" dt="string">GATSM_<xsl:value-of select="@Name"/></Property>				
					<Property name="Out_PV_Trig" dt="string">Value</Property>	
					<Property name="State_PV_Trig" dt="i4">1</Property>	
					<Property name="Operator" dt="string"><xsl:value-of select="//Signalisation_Area[@ID=current()/ATS_Signalisation_Area_ID][1]/sys:terr/@name"/></Property>			
			    	${multilingualvalue("'O'",'OMB')}					
			    	<Property name="Severity" dt="i4">2</Property>		
					${multilingualvalue("'Alarm'",'Value_on')}			
					${multilingualvalue("'Normal'",'Value_off')}					
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
