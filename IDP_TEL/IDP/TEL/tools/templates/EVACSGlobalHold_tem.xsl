<%inherit file="/Object_tem.mako"/>
<%! class_ = "EVACSGlobalHold" %>



<%block name="classes">
	<Class name="EVACSGlobalHold">
		<Objects>
			<xsl:apply-templates select="//Evacuation_Zones_Sec/Evacuation_Zone_Sec"/>
		</Objects>
	</Class>
</%block>

<!-- Template for generating object-->
<xsl:template match="Evacuation_Zone_Sec">
	<xsl:variable name="id" select="@ID"/>
	<Object name="{@Name}.GlobalHold" rules="update_or_create">
			<Properties>
				<Property name="EVACS_Plug" dt="string"><xsl:value-of select="concat(@Name,'.Secure.Template.iEqpState')"/></Property>
				<Property name="SharedOPCGroup" dt="string">Split_<xsl:value-of select="$SERVER_ID"/></Property>			
				<xsl:variable name="stoppingareaname"><xsl:value-of select="//Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform' and (sys:main_depot/@name=//Mainlines/Mainline/@Name)]/@Name" separator=";"/></xsl:variable>			
				<Property name="PlatformList" dt="string"><xsl:value-of select="$stoppingareaname"/></Property>				
			</Properties>
		</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
