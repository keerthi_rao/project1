<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
  			<xsl:apply-templates select="$sysdb//Secondary_Detection_Device[Secondary_Detection_Device_Characteristics_ID=//Secondary_Detection_Devices_Characteristic[not(Checkable='false')]/@ID and sys:sigarea/@id=$SERVER_ID]"/>
  		</Objects>
  	</Class>
</%block>


<xsl:template match="Secondary_Detection_Device">
		<xsl:variable name="TTName" select= "concat(@Name,'.Alarms_Gen')"/>
		<xsl:variable name="nm" select= "@Name"/>
		<xsl:variable name="sector_name" select="//Tunnels/Tunnel[SDD_ID_List/SDD_ID=current()/@Name]/@Name"/>
		<xsl:variable name="uname" select="concat($sector_name,'/SIG/CBI/',@Name)"/>
		<Object name="{$TTName}" rules="update_or_create">
			<Properties>
				${multilingualvalue("'N'",'Avalanche')}
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$uname','EquipmentID')}
				${multilingualvalue("'B'",'OMB')}
				<Property name="Alarm_Label" dt="string">ZC %s1: SDD %s2 Out Of Operation</Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="sys:zc/@name"/></Property>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="$nm"/></Property>
				<Property name="Name_PV_Trig" dt="string">SOS_<xsl:value-of select="@Name"/></Property>
				<Property name="Severity" dt="i4">2</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				${multilingualvalue("'Normal'",'Value_off')}
				${multilingualvalue("'Fault'",'Value_on')}
			</Properties>
		</Object>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>