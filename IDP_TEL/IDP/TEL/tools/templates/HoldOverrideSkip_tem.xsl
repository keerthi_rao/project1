<%inherit file="/Object_tem.mako"/>
<%! class_ = "HoldOverrideSkip" %>

<%block name="classes">
	<Class name="HoldOverrideSkip">
	     <Objects>
			 <xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform' and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Stopping_Area">
	<Object name="{@Name}.HOS" rules="update_or_create">
	  <Properties>
			<Property name="PlatformName" dt="string"><xsl:value-of select="@Name"/></Property>
	  </Properties>
	</Object>
</xsl:template>
