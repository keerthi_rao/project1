<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KIOOPCServerMonitor" %>

<%block name="classes">
	<Class name="S2KIOOPCServerMonitor">
		<Objects>
			<xsl:variable name="SigArea" select="//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID and Localisation_Type='Mainline']"/>
			<xsl:variable name="sigar1" select="if($SigArea/Area_Type='ATS_Central_Server') then //Signalisation_Area[Block_ID_List/Block_ID = $SigArea/*/Block_ID and Area_Type='ATS_Local_Server' and Localisation_Type='Mainline' and not(@ID=$SigArea/@ID)] else $SigArea"/>
			<xsl:apply-templates select="//Function[@Signalisation_Area_ID=$sigar1/@ID and @Function_Type='Level_1']"/>
		</Objects>
	</Class>
</%block>


<xsl:template match="Function">
	<xsl:variable name="ats_eqps" select="//ATS_Equipment[Function_ID_List/Function_ID=current()/@ID]/@Name"/>
	<Object name="OPCClient_ATS_{@ID}" rules="update_or_create">
		<Properties>
			${prop("ConnectAttemptsPeriod","10","i4")}
			${prop("DoubleLinks",1,"boolean")}
			${prop("MonitoringPeriod","30","i4")}
			${prop("MultiActive","1","boolean")}
			${prop("ServerNodeName1",'<xsl:value-of select="$ats_eqps[1]"/>')}
			${prop("ServerNodeName2",'<xsl:value-of select="$ats_eqps[2]"/>')}
			${prop("ServerProgID1","S2K.OpcServer.1")}
			${prop("ServerProgID2","S2K.OpcServer.1")}
			${prop("SharedOnIdentifierDefinition","1","boolean")}
			${prop("SharedOnIdentifier",'OPCClient_ATS_<xsl:value-of select="@ID"/>')}
		</Properties>
	</Object>
</xsl:template>




## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>

