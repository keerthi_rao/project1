<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
	<xsl:apply-templates select="$sysdb//Evacuation_Zone_Sec[sys:zc/@id = $ZC_ID]"/>
</%block>

<xsl:template match="Evacuation_Zone_Sec">
	<xsl:variable name="name" select="concat(@Name,'.EvacCause')"/>
	<xsl:variable name="eqn"><xsl:value-of select="if(SPKS_ID_List/SPKS_ID !='0') then for $item in SPKS_ID_List/SPKS_ID return (concat('(',//SPKS[@ID = $item]/@Name,'.Status.Value==0)')) else null"/></xsl:variable>
	<xsl:variable name="spks_eqn" select="if($eqn) then replace($eqn,' ','+') else null"/>
	<Equipment name="{$name}" type="EvacuationZoneSecStatus" flavor="Cause">
		<States>
			<Equation>EVAC_BY_SPKS[0]::<xsl:value-of select="if ($spks_eqn) then concat($spks_eqn,'&amp;&amp;') else null"/>(<xsl:value-of select="$name"/>.Train1.HMITETrain.longPlug_1 == 0 || <xsl:value-of select="$name"/>.Train1.HMITETrain.longPlug_1 == 1)&amp;&amp;(<xsl:value-of select="$name"/>.Train2.HMITETrain.longPlug_1 == 0 || <xsl:value-of select="$name"/>.Train2.HMITETrain.longPlug_1 == 1)&amp;&amp;(<xsl:value-of select="$name"/>.Train3.HMITETrain.longPlug_1 == 0 || <xsl:value-of select="$name"/>.Train3.HMITETrain.longPlug_1 == 1)</Equation>
			<Equation>EVAC_BY_TRAIN[1]::<xsl:value-of select="if ($eqn != '') then concat($eqn,'&amp;&amp;') else null"/>(<xsl:value-of select="$name"/>.Train1.HMITETrain.longPlug_1 == 2 || <xsl:value-of select="$name"/>.Train2.HMITETrain.longPlug_1 == 2 || <xsl:value-of select="$name"/>.Train3.HMITETrain.longPlug_1 == 2)</Equation>
			<Equation>EVAC_BY_BOTH[2]::<xsl:value-of select="if ($spks_eqn) then concat($spks_eqn,'||') else null"/> <xsl:value-of select="$name"/>.Train1.HMITETrain.longPlug_1 == 2 || <xsl:value-of select="$name"/>.Train2.HMITETrain.longPlug_1 == 2 || <xsl:value-of select="$name"/>.Train3.HMITETrain.longPlug_1 == 2</Equation>
			<Equation>NO_EVAC[3]::1</Equation>
		</States>
	</Equipment>
</xsl:template>