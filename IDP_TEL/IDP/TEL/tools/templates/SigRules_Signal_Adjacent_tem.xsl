<%inherit file="/SigRules_tem.mako"/>
<%!
exclude_result_prefixes=False
%>
<%block name="main">
	<xsl:variable name="sigArea" select="//Signalisation_Area[sys:cbi/@id = $CBI_ID and (Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server')]"/>
	<xsl:variable name="Rts" select="//Signal[not(sys:sigarea/@id=$sigArea/@ID or sys:cbi/@id=$CBI_ID or @ID = //Exit_Gate/Exit_Gate_Signal_ID) and (@ID = //Route[sys:cbi/@id=$CBI_ID]/Destination_Signal_ID)]"/>
	<xsl:variable name="filegenerationcond" select="if ($Rts) then 'Yes' else 'No'"/>
	<xsl:if test="$filegenerationcond = 'No'">
		<xsl:result-document href="SigRules_Signal_Adjacent_{$CBI_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>	
    <xsl:apply-templates select="$Rts"/>
</%block>

<xsl:template match="Signal">
<%
  tt = "<xsl:value-of select='@Name'/>"
%>
  <xsl:variable name="failure_timer" select="Signal_Lamp_Type/Signal_Failure_Timer/text()"/>

  <xsl:if test="LCSRES or LCSPER or LCSANN or LCSMOD">
	  ${eqt('Detection')}
	  	  <Equation>SBD_U400_NOTSET[1]:: <xsl:value-of select="if(LCSRES) then concat('LCSRES_',@Name,' == 0') 
else for $v in (LCSPER|LCSANN|CSMOD) return concat('(', local-name($v), '_', @Name, ' == 1)')" separator=' || '/></Equation>

	  	  <Equation>SBD_U400_SET[0]:: <xsl:value-of select="if(LCSRES) then concat('LCSRES_',@Name,' == 1')
else for $v in (LCSPER|LCSANN|LCSMOD) return concat('(', local-name($v), '_', @Name, ' == 0)')" separator=' &amp;&amp; '/></Equation>
	  ${end_sig()} 
  </xsl:if>

  ${simple_eq_set('HMIApproachLocking', 'SAL')}

  <xsl:if test="SB or SDB">
	  ${eqt('HMIBlocking')}
		  <xsl:if test="SB and SDB">
    		  <Equation>SB_ALLSET[0]:: SB_${tt} == 0 &amp;&amp; SDB_${tt} == 0</Equation>
          </xsl:if>
		  <Equation>SB_NOTSET[1]:: ${term('SB', 1, 1)} &amp;&amp; ${term('SDB', 1, 1)}</Equation>
		  <Equation>SB_ORIGINSET[2]:: ${term('SB', 0, 1)} &amp;&amp; ${term('SDB', 1, 1)}</Equation>
		  <Equation>SB_DESTINATIONSET[3]:: ${term('SB', 1, 1)} &amp;&amp; ${term('SDB', 0, 1)}</Equation>
	  ${end_sig()}
  </xsl:if>

  ${simple_eq_set_inv('HMICallOnStatus', 'COA')}
  <xsl:if test="Signal_Lamp_Type_ID and (LPSRES or LPSPER or LPSOVD or LPSANN or LPSDEV or LPSMOD or PLSPL)">
      ${rule_mask('HMIFilament', 'LP')}
  </xsl:if>
  ${simple_eq_lit('HMILampProvedOverridden', 'LPS_OVERRIDDEN', 'LPSOVD')}
  ${simple_eq_lit('HMILampProvedPermissive', 'LPS_PERMISSIVE', 'LPSPER')}
  ${simple_eq_lit('HMILampProvedRestrictive', 'LPS_RESTRICTIVE', 'LPSRES')}
  ${simple_eq_lit('HMILampProvedAnnonce', 'LPS_ANNONCE', 'LPSANN')}
  ${simple_eq_lit('HMILampProvedDeviated', 'LPS_DEVIATED', 'LPSDEV')}
  ${simple_eq_lit('HMILampProvedMOD', 'LPS_MOD', 'LPSMOD')}
  ${simple_eq_lit('HMILampProvedSight', 'LPS_SIGHT', 'LPSPL')}
  ${rule_mask('HMILampCommand', 'LC')}

  <xsl:if test="LCSOVD">
    ${equipment('HMILampCommandOverridden')}
    ${timeout()}
   <States>
	      <Equation>LCS_OVERRIDDEN_NOLIT[0]:: LCSOVD_${tt} == 0</Equation>
    	  <Equation>LCS_OVERRIDDEN_LIT[1]:: LCSOVD_${tt} == 1</Equation>
   </States>
      <DifferedAlarms>
         <Equation>LCS_OVERRIDDEN_ERROR[LCS_OVERRIDDEN_ERROR_END]:: LCSOVD_${tt} == 1 &amp;&amp; (${term('LPSOVD', 0, 0)})</Equation>
      </DifferedAlarms>
   </Equipment>
  </xsl:if>

  <xsl:if test="LCSPER or LCSMOD">
	  ${equipment('HMILampCommandPermissive')}
	    ${timeout()}
	    <States>
	  	<xsl:choose>
	  		<xsl:when test="Signal_Lamp_Type/Lamp_List[Lamp_Name/text()='SPER'] and Signal_Lamp_Type/Flashing_Lamp_List[Lamp_Name='SPER']">
	  			<Equation>LCS_PERMISSIVE_NOLIT[0]:: ${term('LCSPER', 0, 1)} &amp;&amp; ${term('LCSMOD', 0, 1)}</Equation>
	  			<Equation>LCS_PERMISSIVE_LIT[1]:: ${term('LCSPER', 1, 0)} || ${term('LCSMOD', 1, 0)}</Equation>
	  		</xsl:when>
  		  	<xsl:otherwise>
		  		<Equation>LCS_PERMISSIVE_NOLIT[0]:: LCSPER_${tt} == 0</Equation>
		  		<Equation>LCS_PERMISSIVE_LIT[1]:: LCSPER_${tt} == 1</Equation>
  			</xsl:otherwise>
  		</xsl:choose>
      </States>
      <xsl:if test="LCSPER">
          <DifferedAlarms>
             <Equation>LCS_PERMISSIVE_ERROR[LCS_PERMISSIVE_ERROR_END]:: LCSPER_${tt} == 1 &amp;&amp; (${term('LPSPER', 0, 0)})</Equation>
          </DifferedAlarms>
      </xsl:if>
  	  </Equipment>
  </xsl:if>

  <xsl:if test="LCSRES or LCSPER or LCSOVD or LCSANN or LCSDEV or LCSMOD or LCSPL">
	  ${equipment('HMILampCommandRestrictive')}
	  ${timeout()}
	  <States>
	  	<xsl:choose>
	  		<xsl:when test="Signal_Lamp_Type/Lamp_List[Lamp_Name/text()='SPER'] and Signal_Lamp_Type/Flashing_Lamp_List[Lamp_Name='SPER']">
	  			<xsl:choose>
			  		<xsl:when test="LCSRES">
			  			<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSRES_${tt} == 0</Equation>
			  			<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSRES_${tt} == 1</Equation>
			  		</xsl:when>
			  		<xsl:when test="not(LCSRES) and LCSPER and LCSANN and LCSMOD">
			  			<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSPER_${tt} == 1 || LCSMOD_${tt} == 1 || LCSMOD_${tt} == 1</Equation>
			  			<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSPER_${tt} == 0 &amp;&amp; LCSMOD_${tt} == 0 &amp;&amp; LCSMOD_${tt} == 0</Equation>
		  			</xsl:when>
		  			<xsl:when test="not(LCSRES) and LCSPER and LCSMOD">
		  				<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSPER_${tt} == 1 || LCSMOD_${tt} == 1</Equation>
		  				<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSPER_${tt} == 0 &amp;&amp; LCSMOD_${tt} == 0</Equation>
		  			</xsl:when>
		  			<xsl:when test="not(LCSRES) and LCSANN and LCSMOD">
		  				<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSANN_${tt} == 1 || LCSMOD_${tt} == 1</Equation>
		  				<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSANN_${tt} == 0 &amp;&amp; LCSMOD_${tt} == 0</Equation>
		  			</xsl:when>
		  			<xsl:when test="not(LCSRES) and LCSPER">
		  				<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSPER_${tt} == 1</Equation>
		  				<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSPER_${tt} == 0</Equation>
		  			</xsl:when>
					<xsl:when test="not(LCSRES) and LCSANN">
		  				<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSANN_${tt} == 1</Equation>
		  				<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSANN_${tt} == 0</Equation>
		  			</xsl:when>
	  			</xsl:choose>
	  		</xsl:when>
  		  	<xsl:otherwise>
		  		<Equation>LCS_RESTRICTIVE_NOLIT[0]:: LCSRES_${tt} == 0</Equation>
		  		<Equation>LCS_RESTRICTIVE_LIT[1]:: LCSRES_${tt} == 1</Equation>
  			</xsl:otherwise>
		</xsl:choose>
      </States>
      <xsl:if test="LCSRES">
          <DifferedAlarms>
             <Equation>LCS_RESTRICTIVE_ERROR[LCS_RESTRICTIVE_ERROR_END]:: <xsl:choose>
                <xsl:when test="LCSRES">LCSRES_${tt} == 1 &amp;&amp; (${term('LPSRES', 0, 0)})</xsl:when>
                <xsl:otherwise><xsl:value-of select="for $v in (LCSPER|LCSANN|LCSMOD) return concat('(', local-name($v), '_', @Name, ' == 0)')" separator=' &amp;&amp; '/></xsl:otherwise>
            </xsl:choose></Equation>
          </DifferedAlarms>
      </xsl:if>
  	  </Equipment>
  </xsl:if>

  <xsl:if test="LCSANN">
	  ${equipment('HMILampCommandAnnonce')}
	  ${timeout()}
	  <xsl:if test="LCSANN">
	  <States>
	  	<xsl:choose>
	  		<xsl:when test="Signal_Lamp_Type/Lamp_List[Lamp_Name/text()='SANN'] and Signal_Lamp_Type/Flashing_Lamp_List[Lamp_Name='SANN']">
	  			<Equation>LCS_ANNONCE_NOLIT[0]:: ${term('LCSANN', 0, 1)} &amp;&amp; ${term('LCSMOD', 0, 1)}</Equation>
	  			<Equation>LCS_ANNONCE_LIT[1]:: ${term('LCSANN', 1, 0)} || ${term('LCSMOD', 1, 0)}</Equation>
		  	</xsl:when>
	  		<xsl:otherwise>
		  		<Equation>LCS_ANNONCE_NOLIT[0]:: LCSANN_${tt} == 0</Equation>
		  		<Equation>LCS_ANNONCE_LIT[1]:: LCSANN_${tt} == 1</Equation>
  			</xsl:otherwise>
	  	</xsl:choose>
        </States>
      <xsl:if test="LCSANN">
          <DifferedAlarms>
             <Equation>LCS_ANNONCE_ERROR[LCS_ANNONCE_ERROR_END]:: LCSANN_${tt} == 1 &amp;&amp; (${term('LPSANN', 0, 0)})</Equation>
          </DifferedAlarms>
      </xsl:if>
      </xsl:if>
    </Equipment>
  </xsl:if>

  ${simple_eq_lit_alarm('HMILampCommandDeviated', 'DEVIATED', 'DEV')}
  ${simple_eq_lit_alarm('HMILampCommandMOD', 'MOD', 'MOD')}
  ${simple_eq_lit_alarm('HMILampCommandSight', 'SIGHT', 'PL')}

  ${decl_var_route('AR_Route', 'AR')}
  ${decl_var_route('RL_Route', 'RL')}
  ${decl_var_route('R_Route', 'R')}


  <xsl:if test="SOVD">
	  ${eqt('HMIOverridden')}
	  	  <Equation>SOVD_NOTSET[0]:: <xsl:value-of select="if(./Signal_Overridden_For_CBTC_Train/text() = 'true') then concat('SOVD_',@Name,' == 0') else '0'"/></Equation>
			  <Equation>SOVD_SET[1]:: <xsl:value-of select="if(./Signal_Overridden_For_CBTC_Train/text() = 'true') then concat('SOVD_',@Name,' == 1') else '0'"/></Equation>
			  <Equation>SOVD_NOTCONFIG[2]:: 1</Equation>
	  ${end_sig()} 
  </xsl:if>
 

  	  ${simple_eq_set_inv('HMIRouteSignal', 'SPER')}

  <xsl:if test="CSID">
	  ${eqt('Crossing_Detections')}
	  	  <Equation>CROSSING_SIGNAL[0]:: CSID_${tt} == 1</Equation>
		  <Equation>NOCROSSING_SIGNAL[1]:: CSID_${tt} == 0</Equation>
	  ${end_sig()} 
  </xsl:if>
 
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="equipment(equip_name)">
   <Equipment name="{@Name}.${equip_name}" type="SignalEquipment" flavor="SignalEqpt" sys:ID="{@ID}">
</%def>

<%def name="eqt(equip_name)">
   ${equipment(equip_name)}
   <States>
</%def>

<%def name="end_sig()">
   </States>
   </Equipment>
</%def>

<%def name='Pi(node_set, value)'>${list(node_set, value, " &amp;&amp; ")}</%def>
<%def name='Sigma(node_set, value)'>${list(node_set, value, " || ")}</%def>
<%def name="list(node_set, value, separator)"><xsl:value-of select="\
    if (count(${node_set}) > 1) then
        for $t in (${node_set}) return concat('(', $t/@Prefix,'_',$t/@Name,'==', ${value}, ')')\
    else
        concat(${node_set}[1]/@Prefix,'_',${node_set}[1]/@Name,'==', ${value})" separator="${separator}"/></%def>

<%def name="simple_eq_set(type, nom_var)">
<xsl:if test="${nom_var}">
	${eqt(type)}
	  	  <Equation>${nom_var}_SET[0]:: ${nom_var}_${tt} == 0</Equation>
		  <Equation>${nom_var}_NOTSET[1]:: ${nom_var}_${tt} == 1</Equation>
  	${end_sig()}
</xsl:if>
</%def>

<%def name="simple_eq_set_inv(type, nom_var)">
<xsl:if test="${nom_var}">
	${eqt(type)}
		  <Equation>${nom_var}_NOTSET[0]:: ${nom_var}_${tt} == 0</Equation>
	  	  <Equation>${nom_var}_SET[1]:: ${nom_var}_${tt} == 1</Equation>
  	${end_sig()}
</xsl:if>
</%def>

<%def name="simple_eq_lit(type, prefix, nom_var)">
  <xsl:if test="${nom_var}">
    ${equipment(type)}
    <States>
          <Equation>${prefix}_NOLIT[0]:: ${nom_var}_${tt} == 0</Equation>
          <Equation>${prefix}_LIT[1]:: ${nom_var}_${tt} == 1</Equation>
    ${end_sig()}
  </xsl:if>
</%def>

<%def name="simple_eq_lit_alarm(type, prefix, nom_var)">
  <xsl:if test="LCS${nom_var}">
    ${equipment(type)}
    ${timeout()}
    <States>
          <Equation>LCS_${prefix}_NOLIT[0]:: LCS${nom_var}_${tt} == 0</Equation>
          <Equation>LCS_${prefix}_LIT[1]:: LCS${nom_var}_${tt} == 1</Equation>
    </States>
    <DifferedAlarms>
        <Equation>LCS_${prefix}_ERROR[LCS_${prefix}_ERROR_END]:: LCS${nom_var}_${tt} == 1 &amp;&amp; (${term('LCP' + nom_var, 0, 0)})</Equation>
    </DifferedAlarms>
    </Equipment>
  </xsl:if>
</%def>

<%def name="decl_var_route(varname, prefix)">
      <xsl:variable name="${varname}" select="./Routes_Variables_list/Route_Variable[@Prefix='${prefix}']"/>
</%def>

<%def name="timeout()">
	<xsl:attribute name="AlarmTimeOut" select="if ($failure_timer) then (number($failure_timer) div 1000) else 5"/>
</%def>

<%def name="rule_mask(elt_name, pref)">
  <xsl:variable name="${pref}Sn" select="${elt_name}/${pref}Sn"/>
  <xsl:if test="$${pref}Sn">
	  ${eqt(elt_name)}
	  	  <Equation>${pref}S_STATUS[0xFC]:: <xsl:text/>
	  	        <xsl:text/><xsl:value-of select="for $elt in ($${pref}Sn) return \
	  	                if ($elt/@n = 0) then \
	  	                     concat('(',$elt,'_', @Name,' &amp; 0x0001)')\
	  	                else\
	  	                    concat('((',$elt,'_', @Name,' &amp; 0x0001) &lt;&lt; ', $elt/@n ,')')\
                             \
	  	                     " separator=" + "/> </Equation>
		  <Equation>${pref}S_NOTSET[0]:: 1</Equation>
	  ${end_sig()} 
  </xsl:if>
</%def>

<%def name="term(name, test_value, default_value)"><xsl:value-of select="if (${name}) then concat('${name}_',@Name,' == ${test_value}') else '${default_value}'"/></%def>

