<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>




<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="//MSS_SMIO/SMIO[Signalisation_Area_ID=$SERVER_ID and (contains(@Name,'ATC') or contains(@Name,'CBI'))]"/>
		</Objects>	
	</Class>
</%block>


<xsl:template match="SMIO">
	<xsl:variable name="name_sm" select="@Name"/>
%for i in range(1,49):
	<xsl:variable name="terr" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="smiott" select="concat($name_sm,'.Alarm.LRU',${i},'Failed')"/>
	<xsl:variable name="alarmlabel" select="if(contains($name_sm,'ATC')) then('Trackside ATC SMIO - %d1 LRU Failed') else if(contains($name_sm,'CBI')) then ('CBI SMIO - %d1 LRU Failed') else null"/>
	<xsl:variable name="nmpv" select="concat($name_sm,'.Status.LRU',${i},'Failed','.PV_Status')"/>
	<xsl:variable name="nmpv_dy" select="concat($name_sm,'.Status.LRU',${i},'Descr','.PV_Status_String')"/>
	<xsl:variable name="opv" select="'Value'"/>
	<xsl:variable name="Severity" select="'2'"/>
	<xsl:variable name="Stpv" select="'1'"/>
	<xsl:variable name="nm" select="$name_sm"/>
	<xsl:variable name="omb" select="'M'"/>
	<xsl:variable name="von" select="'Fault'"/>
	<xsl:variable name="voff" select="'Normal'"/>
	<xsl:variable name="EqID" select="concat(Location,if(contains($name_sm,'ATC')) then ('/SIG/ZC/') else ('/SIG/CBI/'),Detail_Code)"/>
${objsmio('{$smiott}')}
%endfor

</xsl:template>



<%def name="objsmio(nmtt)">
		<Object name="${nmtt}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarmlabel"/></Property>			
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>			
					<Property name="Name_PV_Dyn_Bstr_1" dt="string"><xsl:value-of select="$nmpv_dy"/></Property>			
					<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$opv"/></Property>			
					<Property name="Severity" dt="i4"><xsl:value-of select="$Severity"/></Property>			
					<Property name="State_PV_Trig" dt="i4">1</Property>	
			
					${multilingualvalue("'N'",'Avalanche')}						
					${multilingualvalue("'-'",'Operator')}						
					${multilingualvalue('$nm','Name')}						
					${multilingualvalue('$EqID','EquipmentID')}						
					${multilingualvalue('$omb','OMB')}						
					${multilingualvalue('$von','Value_on')}						
					${multilingualvalue('$voff','Value_off')}				
				</Properties>
		</Object>
</%def>

<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>