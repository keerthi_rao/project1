<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
  	<Class name="S2KEvents_Merge">
  		<Objects>
			<xsl:if test="//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type='Mainline'">
					<xsl:apply-templates select="//Event[Type='General_Event' or Type='General_Event_LV2']"/>
			</xsl:if>		
  		</Objects>
  	</Class>	
</%block>	

<xsl:template match="Event">
	<xsl:variable name="objname" select="concat('Mainline',@Name)"/>
	<xsl:variable name="terrname" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="EquipmentID"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="optr" select="'TAS'"/>

	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="BstrParam1"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="Name_PV_Trig"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname[1],'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			<xsl:if test="Type='General_Event_LV2'">
				<Property name="Name_PV_Dyn_Bstr_1" dt="string"><xsl:value-of select="Name_PV_Dynam_1"/></Property>
			</xsl:if>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue("'Mainline'",'Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

