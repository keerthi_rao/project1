<%inherit file="/Object_tem.mako"/>
<%! class_ = "FEP_Status" %>

<%block name="classes">
	<Class name="FEP_Status">
		<Objects>
				<xsl:apply-templates select="//Signalisation_Area[(some $i in (//Signalisation_Area[@ID=$SERVER_ID]/Block_ID_List/Block_ID) satisfies Block_ID_List/Block_ID=$i) and Area_Type='ATS']"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Signalisation_Area">
	<xsl:variable name="ats_eqps" select="//ATS_Equipment[(Signalisation_Area_ID_List/Signalisation_Area_ID=current()/@ID or  Signalisation_Area_ID=current()/@ID) and ATS_Equipment_Type='FEP']/@Name"/>
	<xsl:variable name="tt">
		<xsl:choose>
			<xsl:when test="count($ats_eqps)&gt;number(1)">
					<xsl:for-each select="1 to string-length($ats_eqps[1])">
						<xsl:if test="substring($ats_eqps[1], 1, position())!=substring($ats_eqps[2], 1, position())">
							<xsl:value-of select="(substring($ats_eqps[1], 1, position()-1))" />
						</xsl:if>
					</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($ats_eqps[1],'.Status')" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	
	
	<xsl:variable name="nmclient" select="concat('OPCClient_',$tt,'_Status')"/>
	<xsl:variable name="nmgroup" select="concat('OPCGroup_',$tt,'_Status')"/>
	
	<Object name="{$tt}.Status" rules="update_or_create">
		<Properties>
			${multilingualvalue('$nmclient',"Name_Client")}
			${multilingualvalue('$nmgroup',"Name_Group")}
			${prop("Server1Url",'opc.tcp://<xsl:value-of select="$ats_eqps[1]"/>:50018')}
			${prop("Server2Url",'opc.tcp://<xsl:value-of select="$ats_eqps[2]"/>:50018')}
		</Properties>
	</Object>
</xsl:template>



## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,prop"/>

