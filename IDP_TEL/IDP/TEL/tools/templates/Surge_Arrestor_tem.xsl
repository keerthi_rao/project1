<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
				<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and (contains(@Name,'SAF24V'))]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Generic_ATS_IO">
			<xsl:variable name="TTName" select="substring-after(@Name,'GAIO_SAF24V_')"/>
			<xsl:variable name="objname" select="concat('GATSM_',@Name,'.Alarms_Gen')"/>
			<xsl:variable name="uname" select="concat(tokenize(@Name,'_')[last()],'/SIG/CBI')"/>
			<Object name="{$objname}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string">Surge Arrester Failed</Property>			
					${prop('AlarmInhib','1','boolean')}			
					<Property name="BstrParam1" dt="string">-</Property>							
					${multilingualvalue('$TTName','Name')}						
					${multilingualvalue('$uname','EquipmentID')}	
					${multilingualvalue("'-'",'MMS')}						
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="concat('GATSM_',@Name)"/></Property>				
					<Property name="Out_PV_Trig" dt="string">Value</Property>	
					<Property name="State_PV_Trig" dt="i4">1</Property>	
					<Property name="Operator" dt="string"><xsl:value-of select="//Signalisation_Area[@ID=current()/ATS_Signalisation_Area_ID][1]/sys:terr/@name"/></Property>			
			    	${multilingualvalue("'M'",'OMB')}					
			    	<Property name="Severity" dt="i4">2</Property>		
					${multilingualvalue("'-'",'Value')}					
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
