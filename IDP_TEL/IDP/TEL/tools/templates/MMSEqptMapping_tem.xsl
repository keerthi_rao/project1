<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:template match="/">
		<MMSDAT_ALARM_LOOKUP>
				<BY_OBJECT_NAME>
				<xsl:for-each select="//Train_Unit">
					<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
					<xsl:variable name="id" select="@ID"/>
					<xsl:for-each select="//HMITrainAlarmMonitored/Alarm[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
						<xsl:if test="(contains(@Name,'.RSAlarm.CaNcaAlarm') and (not(contains(@Name,'Inconsistent'))) and (MMS!='0'))">
								<EQPT_CODE OBJECT_NAME="{concat($TrainName,@Name)}" ><xsl:value-of select="concat('2',format-number($id,'000'),Localisation)"/></EQPT_CODE>
						</xsl:if>	
					</xsl:for-each>
				</xsl:for-each>
				</BY_OBJECT_NAME>
				<BY_OBJECT_PATH>
				<xsl:for-each select="//Train_Unit">
					<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
					<xsl:variable name="id" select="@ID"/>
					<xsl:for-each select="//HMITrainAlarmMonitored/Alarm[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
						<xsl:if test="(contains(@Name,'.RSAlarm.CaNcaAlarm') and (not(contains(@Name,'Inconsistent'))) and (string(MMS)!='0'))">
								<ALARM_DETAILS PATH="{concat($TrainName,@Name)}">
								<VALUE_ALARM_DESCRIPTION>1</VALUE_ALARM_DESCRIPTION>
								<VALUE_NORMAL_DESCRIPTION>0</VALUE_NORMAL_DESCRIPTION>
								<FAULT_CODE><xsl:value-of select="FAULT_CODE"/></FAULT_CODE>
								<ALARM_CODE><xsl:value-of select="ALARM_CODE"/></ALARM_CODE>
								</ALARM_DETAILS>
						</xsl:if>	
					</xsl:for-each>
				</xsl:for-each>	
				</BY_OBJECT_PATH>				
		</MMSDAT_ALARM_LOOKUP>		
	</xsl:template>
</xsl:stylesheet>
