<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:param name="DATE"/>
<xsl:param name="SERVER_ID"/>
<xsl:template match="/">
	<STATION_BOUNDARIES Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="StationBoundaryPoint.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<xsl:apply-templates select="//ISCSStation[Geographical_Area=//Geographical_Area[Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID=//Signalisation_Area[@ID=$SERVER_ID]/Area_Boundary_Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID]/@ID]"/>
	</STATION_BOUNDARIES>
</xsl:template>
<xsl:template match="ISCSStation">
	<STATION ID="{@ID}" TOP_POINT_NAME="{@Name}"/>
</xsl:template>
</xsl:stylesheet>
