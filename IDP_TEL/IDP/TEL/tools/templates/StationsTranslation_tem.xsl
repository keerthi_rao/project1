<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
    <xsl:import href="alias.xsl"/>
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
		<xsl:template match="/">
		<Translations>
			<xsl:apply-templates select="//Station"/> 
			<xsl:apply-templates select="//Stopping_Area"/> 
		</Translations>
		</xsl:template>
		<xsl:template match="Station">
			<Station Source="{@Name}" Translation="{HMICustomerStation}"/>
		</xsl:template>
		
		<xsl:template match="Stopping_Area">
			<Stopping_Area Source="{@Name}" Translation="{HMICustomerPlatform }"/>
		</xsl:template>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="versions_xml"/>
