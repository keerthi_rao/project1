<%inherit file="/Object_tem.mako"/>
<%! class_ = "LX_Alarm_Side" %>

<%block name="classes">
	<Class name="LX_Alarm_Side">
	     <Objects>
			 <xsl:apply-templates select="//LX[contains(@Name,'LX3') and LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="LX">
		<!-- check for only none -->
	  <xsl:variable name="terrname" select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID and sys:sigarea/@id = $SERVER_ID]/sys:terr/@name"/>
	  <xsl:variable name="Eqid" select="concat(substring-after($terrname,'Territory_'),'/SIG/CBI/',@Name)"/>	 

	 <xsl:variable name="la" select="//LX_Device[(LX_ID=current()/@ID) and (Type='Aspect Indicator') and (Signal_Lamp_Type_ID = //Signal_Lamp_Type[not(Proved_Lamp_List/Lamp_Name='None')]/@ID)]"/>
	  <Object name="{@Name}.AlarmSide" rules="update_or_create">
		  <Properties>
				<Property name="Name_PV_Trig_LA" dt="string"><xsl:value-of select="$la[contains(@Name,'WL') and contains(@Name,'LA')]/@Name"/></Property>
				<Property name="Name_PV_Trig_LB" dt="string"><xsl:value-of select="$la[contains(@Name,'WL') and contains(@Name,'LB')]/@Name"/></Property>
				<Property name="Name_PV_Trig_RA" dt="string"><xsl:value-of select="$la[contains(@Name,'WL') and contains(@Name,'RA')]/@Name"/></Property>
				<Property name="Name_PV_Trig_RB" dt="string"><xsl:value-of select="$la[contains(@Name,'WL') and contains(@Name,'RB')]/@Name"/></Property>
	    	    <Property name="Severity" dt="i4">2</Property>		
				<!-- <Property name="Operator" dt="string"><xsl:value-of select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID  and sys:sigarea/@id = $SERVER_ID]/sys:terr/@name"/></Property> -->
				${multilingualvalue('$Eqid','EquipmentID')}	
				${multilingualvalue("'-'",'Operator')}	
				${multilingualvalue("'B'",'OMB')}	
				${multilingualvalue("'-'",'Avalanche')}	
				${multilingualvalue("'-'",'Value')}	
		 </Properties>
	  </Object>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
