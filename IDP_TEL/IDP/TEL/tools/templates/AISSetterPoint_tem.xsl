<%inherit file="/Object_tem.mako"/>
<%! class_ = "AISSetterPoint" %>

<%block name="classes">
<xsl:variable name="filegeneration" select="if ($sysdb//Route[sys:sigarea/@id=$SERVER_ID]) then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="AISSetterPoint_{$SERVER_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="AISSetterPoint">
	     <Objects>
			 <xsl:apply-templates select="//Route[sys:sigarea/@id=$SERVER_ID and @ID = //Aspect_Indicator/Route_ID_List/Route_ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Route">
	<xsl:variable name="aspectIndicator" select="//Aspect_Indicator[Route_ID_List/Route_ID = current()/@ID and (contains(@Name, '_A') or contains(@Name, '_B'))]"/>
	<xsl:if test="$aspectIndicator[contains(@Name, '_A')] and $aspectIndicator[contains(@Name, '_B')]">
		<Object name="{@Name}.AISSetterPoint" rules="update_or_create">	
		<xsl:variable name="orignRouteSignal" select="Origin_Signal_ID"/>
		<xsl:variable name="nm" select="//Signal[@ID=$orignRouteSignal]/@Name"/>
			  <Properties>
					<Property name="RouteSettingPointName" dt="string"><xsl:value-of select="concat('APPROACH_',$nm)"/></Property>
					<Property name="RouteSettingObjectName" dt="string"><xsl:value-of select="concat(@Name,'.Routesettingobject')" /></Property>
					<Property name="AspectIndicatorA" dt="string"><xsl:value-of select="$aspectIndicator[contains(@Name, '_A')][1]/@Name"/></Property>
					<Property name="AspectIndicatorB" dt="string"><xsl:value-of select="$aspectIndicator[contains(@Name, '_B')][1]/@Name"/></Property>
					<Property name="Shared_StatusAspect" dt="string">APPROACH_<xsl:value-of select="$nm"/>.StatusAspect</Property>
			</Properties>
		 </Object>
	 </xsl:if>
</xsl:template>

