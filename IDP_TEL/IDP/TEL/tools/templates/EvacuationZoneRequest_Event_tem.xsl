<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<xsl:variable name="filegeneration" select="if ($sysdb//Evacuation_Zone_Req[sys:zc/@id=$ZC_ID]) then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="EvacuationZoneRequest_Event_{$ZC_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="S2KEvents_Merge">
	     <Objects>
			<xsl:apply-templates select="$sysdb//Evacuation_Zone_Req[sys:zc/@id=$ZC_ID]"/>	
		 </Objects>
	</Class>
</%block>

<xsl:template match="Evacuation_Zone_Req">
<xsl:variable name="nm" select="@Name"/>
			<xsl:variable name="TTName" select= "concat(@Name,'.Event.')"/>
			<xsl:variable name="id" select="@ID"/>
			<xsl:variable name="Eqid" select="concat(@Name,'/SIG/ATC')"/>
			<xsl:variable name="optr" select="sys:terr/@name"/>
			${obj('{$TTName}',[('MI','Evacuation Request Zone %s1: Monitoring Inhibited','1','.Status'),
				   ('MA','Evacuation Request Zone %s1: Monitoring Activated','0','.Status')
				   ])}
	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

<%def name="obj(nm,list)">
	% for key, description,pv,bl in list:
		<Object name="${nm}${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$Eqid','EquipmentID')}
				${multilingualvalue("'-'",'MMS')}
				${multilingualvalue("'-'",'Value')}
				${multilingualvalue("'O'",'OMB')}
				${prop('AlarmInhib','1','boolean')}
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>
				<Property name="Name_PV_Trig" dt="string">EZRMIS_<xsl:value-of select="$nm"/></Property>
				<Property name="Operator" dt="string"><xsl:value-of select="$optr"/>.TAS</Property>
				<Property name="Severity" dt="i4">-</Property>
				<Property name="State_PV_Trig" dt="i4">${pv}</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
			</Properties>
		</Object>
	% endfor
</%def>
