<%inherit file="/Object_tem.mako"/>
<%! class_ = "Lamp_Alarm" %>

<%block name="classes">
	<Class name="Lamp_Alarm">
	     <Objects>
			<xsl:apply-templates select="$sysdb//Direction_Indicator[@ID=//Aspect_Indicator/Direction_Indicator_ID and sys:sigarea/@id=$SERVER_ID and Signal_Lamp_Type_ID=//Signal_Lamp_Type[@Name='SLTY_SWPD_SIG']/@ID]" mode="tt1"/>	
			<xsl:apply-templates select="$sysdb//Direction_Indicator[@ID=//Aspect_Indicator/Direction_Indicator_ID and sys:sigarea/@id=$SERVER_ID and Signal_Lamp_Type_ID=//Signal_Lamp_Type[@Name='SLTY_RI']/@ID]" mode="tt2"/>	
			<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID and Lamp_Proved_Signal='true' and Signal_Type_Position='Depot' and Track_ID=//Track[not(contains(@Name,'RT')) and  Track_Type!='Test track']/@ID]" mode="sig1"/>		
			<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID and Lamp_Proved_Signal='true']" mode="sig2"/>	
		    <xsl:apply-templates select="$sysdb//LX_Device[LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID and (Type='Aspect Indicator') and (Signal_Lamp_Type_ID = //Signal_Lamp_Type[not(Proved_Lamp_List/Lamp_Name='None')]/@ID)]"  mode="tt5"/>
		    <xsl:apply-templates select="$sysdb//LX_Device[LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID and contains(@Name,'LX3')]"  mode="tt6"/>
			<xsl:apply-templates select="$sysdb//Trains_Unit/Train_Unit"/>
			<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID]" mode="RedAlarm"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TTName" select= "concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="Eqid" select="concat((number(@ID+2000)*10),'/SIG/ATC')"/>
	<xsl:variable name="bstrp1" select="if (Train_Unit_Characteristics_ID='1') then concat('PV',format-number(@ID,'000')) 
	                                    else concat('PV',format-number(number(@ID)-150,'000'))"/>
	<Object name="{$TTName}.Alarm_EB_Request" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string">Train %s1: Emergency Brake Release Request Failed</Property>				
			<Property name="Name_PV_A" dt="string"><xsl:value-of select="$TTName"/>.ProjAttributes1.HMITETrain.boolPlug_1</Property>
			<Property name="Name_PV_B" dt="string"><xsl:value-of select="$TTName"/>.ProjAttributes1.HMITETrain.boolPlug_2</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrp1"/></Property>	
			<Property name="Severity" dt="i4">1</Property>
			<Property name="Value_A_Trig" dt="string">1</Property>
			<Property name="Value_B_Trig" dt="string">1</Property>
			${multilingualvalue('$Eqid','EquipmentID')}		
			${multilingualvalue("'-'",'Avalanche')}
			${multilingualvalue("'O'",'OMB')}			
			${multilingualvalue("'Fault'",'Value_on')}			
			${multilingualvalue("'Normal'",'Value_off')}	
		</Properties>
	</Object>	
</xsl:template>

<xsl:template match="LX_Device" mode="tt5">
	<xsl:variable name="TTnamett5" select="@Name"/>
	<xsl:variable name="nm" select= "@Name"/>
	<xsl:variable name="terrname" select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=//LX[@ID=current()/LX_ID]/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID and sys:sigarea/@id = $SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="uname" select="concat(substring-after($terrname,'Territory_'),'/SIG/CBI/',@Name)"/>
	${obj1('{$TTnamett5}',[('.Alarm','Level Crossing %s1 : At Least One Lamp Failed','LXAI','.LXLP')])}
</xsl:template>


<xsl:template match="LX_Device" mode="tt6">
	<xsl:variable name="TTnamett6" select="@Name"/>
	<xsl:variable name="nm" select= "@Name"/>
	<xsl:variable name="terrname" select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=//LX[@ID=current()/LX_ID]/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID and sys:sigarea/@id = $SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="uname" select="concat(substring-after($terrname,'Territory_'),'/SIG/CBI/',@Name)"/>
	${objLX('{$TTnamett6}',[('.AlarmSideA','Level Crossing %s1: All Lamp Of Side ','A Failed','.LXAI_Lamp_Side_A','.LXLP_Lamp_Side_A'),
								('.AlarmSideB','Level Crossing %s1: All Lamp Of Side ','B Failed','.LXAI_Lamp_Side_B','.LXLP_Lamp_Side_B')])}
</xsl:template>

<xsl:template match="Direction_Indicator" mode="tt1">
	<xsl:variable name="AIname" select="//Aspect_Indicator[Direction_Indicator_ID=current()/@ID][1]/@Name"/>
	<xsl:variable name="TTName" select= "concat(@Name,'.Alarm')"/>
	<xsl:variable name="uname" select="concat(substring-after(sys:terr/@name,'Territory_'),'/SIG/CBI/',@Name)"/>
	<xsl:variable name="nme" select="@Name"/>
		${obj('{$TTName}',[('AISCPS','SWPD %s1: 50% or more LEDs Failed','0','AILPS','Lamp Proved Lit','Lamp Not Proved Lit')])}
</xsl:template>

<xsl:template match="Direction_Indicator" mode="tt2">
	<xsl:variable name="AIname" select="//Aspect_Indicator[Direction_Indicator_ID=current()/@ID][1]/@Name"/>
	<xsl:variable name="TTName" select= "concat(@Name,'.Alarm')"/>
	<xsl:variable name="uname" select="concat(substring-after(sys:terr/@name,'Territory_'),'/SIG/CBI/',@Name)"/>
	<xsl:variable name="nme" select="@Name"/>
		${obj('{$TTName}',[('AISCPS','Stencil Indicator %s1: 50% or more LEDs Failed','0','AILPS','Lamp Proved Lit','Lamp Not Proved Lit')])}
</xsl:template>

<xsl:template match="Signal" mode="sig1">
	<xsl:variable name="TTName" select= "concat(@Name,'.Yellow_Alarm')"/>
	<xsl:variable name="sector_name" select="//Tunnels/Tunnel[Signal_ID_List/Signal_ID=current()/@Name]/@Name"/>
	<xsl:variable name="uname" select="concat($sector_name,'/SIG/CBI/',@Name)"/>
	<xsl:variable name="nme" select="@Name"/>
	<xsl:variable name="AIname" select="@Name"/>

		${obj('{$TTName}',[('LCSPER','Yellow Signal %s1 : 50% or more LEDs Failed','0','LPSPER','Fault','Normal')],'N')}
</xsl:template>

<xsl:template match="Signal" mode="sig2">
		<xsl:variable name="TTName" select= "concat(@Name,'.White_Alarm')"/>
		<xsl:variable name="sector_name" select="//Tunnels/Tunnel[Signal_ID_List/Signal_ID=current()/@Name]/@Name"/>
		<xsl:variable name="uname" select="concat($sector_name,'/SIG/CBI/',@Name)"/>
		<xsl:variable name="nme" select="@Name"/>
		<xsl:variable name="AIname" select="@Name"/>
		<xsl:variable name="tr" select="//Track[@ID=current()/Track_ID]"/>

		<xsl:choose>
			<xsl:when test="(Signal_Type_Position!='Depot') or (Signal_Type_Position='Depot' and ($tr/Track_Type='Test track' or contains($tr/@Name,'RT')))"> 
				${obj('{$TTName}',[('LCSPER','White Signal %s1 : 50% or more LEDs Failed','0','LPSPER','Fault','Normal')],'N')}
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>



<xsl:template match="Signal" mode="RedAlarm">
		<xsl:variable name="TTName" select= "concat(@Name,'.Red_Alarm')"/>
		<xsl:variable name="sector_name" select="//Tunnels/Tunnel[Signal_ID_List/Signal_ID=current()/@Name]/@Name"/>
		<xsl:variable name="uname" select="concat($sector_name,'/SIG/CBI/',@Name)"/>		
		<xsl:variable name="AIname" select="@Name"/>
		<xsl:variable name="nme" select="@Name"/>

		${obj('{$TTName}',[('LCSPER','Red Signal %s1 : 50% or more LEDs Failed','0','LPSPER','Fault','Normal')],'N')}
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list,avalanche_value='-')">
	% for key, description,stpv,nm2,von,voff in list:
		<Object name="${nm}" rules="update_or_create">
		<xsl:variable name="avl" select="'${avalanche_value}'"/>
		<xsl:variable name="valon" select="'${von}'"/>
		<xsl:variable name="valoff" select="'${voff}'"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${description}'"/></Property>				
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nme"/></Property>					
				${multilingualvalue('$uname','EquipmentID')}					
				<Property name="Name_PV_A" dt="string">${key}<xsl:value-of select="concat('_',$AIname)"/></Property>
				<Property name="Name_PV_B" dt="string">${nm2}<xsl:value-of select="concat('_',$AIname)"/></Property>			
				${multilingualvalue("'M'",'OMB')}			
				${multilingualvalue("$avl",'Avalanche')}		
				${multilingualvalue('$valon','Value_on')}			
				${multilingualvalue('$valoff','Value_off')}			
				<Property name="Severity" dt="i4">2</Property>		
			</Properties>
		</Object>
	% endfor
</%def>


<%def name="objLX(nm,list)">
	% for key, description1,description2,npv1,npv2 in list:
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${description1}'"/><xsl:value-of select="'${description2}'"/></Property>				
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>					
				${multilingualvalue('$uname','EquipmentID')}				
				<Property name="Name_PV_A" dt="string"><xsl:value-of select="$nm"/>${npv1}</Property>
				<Property name="Name_PV_B" dt="string"><xsl:value-of select="$nm"/>${npv2}</Property>
				<Property name="Out_PV_Trig" dt="string">-</Property>
				<Property name="State_PV_Trig" dt="i4">-</Property>			
				${multilingualvalue("'B'",'OMB')}			
				<Property name="Severity" dt="i4">2</Property>		
				${multilingualvalue("'Fault'",'Value_on')}			
				${multilingualvalue("'Normal'",'Value_off')}	
			</Properties>
		</Object>
	% endfor
</%def>

<%def name="obj1(nm,list)">
	% for key, description1,npv1,npv2 in list:
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${description1}'"/></Property>				
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>					
				${multilingualvalue('$uname','EquipmentID')}				
				<Property name="Name_PV_A" dt="string">${npv1}_<xsl:value-of select="$nm"/></Property>
				<Property name="Name_PV_B" dt="string">${npv2}_<xsl:value-of select="$nm"/></Property>
				<Property name="Out_PV_Trig" dt="string">-</Property>
				<Property name="State_PV_Trig" dt="i4">-</Property>			
				${multilingualvalue("'B'",'OMB')}			
				<Property name="Severity" dt="i4">3</Property>		
				${multilingualvalue("'Fault'",'Value_on')}			
				${multilingualvalue("'Normal'",'Value_off')}	
			</Properties>
		</Object>
	% endfor
</%def>
