<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "ATSSupervisor"
%>
<%block name="classes">
	<Class name="ATSSupervisor">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signalisation_Area[(Area_Type='ATS_Central_Server'  and $SERVER_TYPE='ATS_Local_Server' and sys:sigarea/@id=$SERVER_ID) or (Area_Type='ATS_Local_Server' and $SERVER_TYPE='ATS_Central_Server' and sys:sigarea/@id=$SERVER_ID)]"/>
			<xsl:apply-templates select="$sysdb//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area">
	<xsl:variable name="servertype" select="if(Area_Type='ATS_Central_Server') then 'Central Server' else 'Local Server'"/>
	<xsl:variable name="atseqpmt" select="$sysdb//ATS_Equipment[(Signalisation_Area_ID=current()/@ID or Signalisation_Area_ID_List/Signalisation_Area_ID=current()/@ID) and ATS_Equipment_Type=$servertype]"/>
	<Object name="{concat('ATS_',$SERVER_ID,'.OTHERATSTEL.ATS_',@ID)}" rules="update_or_create">
		<Properties>
			<Property name="Host1Name" dt="string"><xsl:value-of select="$atseqpmt[1]/@Name"/></Property>
			<Property name="Host2Name" dt="string"><xsl:value-of select="$atseqpmt[2]/@Name"/></Property>
			<Property name="EnableRedundancyServer" dt="boolean">0</Property>
			<Property name="EnableRedundantClient" dt="boolean">0</Property>
			<Property name="ClientConnexion_EnableInstance" dt="boolean">0</Property>
			<Property name="Ping_EnableInstance" dt="boolean">0</Property>
			<Property name="topMgrVersion_EnableInstance" dt="boolean">0</Property>
			<Property name="OPCClientID" dt="string"><xsl:value-of select="concat('OPCClient_ATS_',@ID)"/></Property>
			<Property name="PVSupervisorStateOPCTag" dt="string">SVR.<xsl:value-of select="$atseqpmt[1]/@Name"/>.State.iEqpState</Property>
			<Property name="PVSupervisorRedundancyStatusOPCTag" dt="string">SVR.<xsl:value-of select="$atseqpmt[2]/@Name"/>.State.iEqpState</Property> 
			<xsl:variable name="nm" select="concat('ATS_', @ID)"/>
			${multilingualvalue('$nm')}
		</Properties>
	</Object>
</xsl:template>	
	
<xsl:template match="ATS_Equipment">
		<xsl:variable name="atseqtype" select="if(ATS_Equipment_Type='Local Server' or ATS_Equipment_Type='Central Server') then ('.SVR.')
												else if(ATS_Equipment_Type='Gateway') then ('.GTW.')
												else if(ATS_Equipment_Type='Workstation') then ('.WKS.') else null"/>
		<Object name="ATS_{$SERVER_ID}{$atseqtype}{@Name}" rules="update_or_create">
			<Properties>
				<Property name="States_EnableInstance" dt="boolean">1</Property>
				<Property name="PVSupervisorStateOPCTag" dt="string">ATS_<xsl:value-of select="$SERVER_ID"/>.<xsl:value-of select="current()/ATS_Equipment_Type[1]"/>.<xsl:value-of select="@Name"/>.State.iEqpState</Property> 
				<Property name="Link_EnableInstance" dt="boolean">1</Property>
				<Property name="PVSupervisorLinkOPCTag" dt="string">ATS_<xsl:value-of select="$SERVER_ID"/>.<xsl:value-of select="current()/ATS_Equipment_Type[1]"/>.<xsl:value-of select="@Name"/>.Link.iEqpState</Property>	
				<Property name="Host1Name" dt="string"><xsl:value-of select="if (ATS_Equipment_Type='Gateway') then (concat('ATS_',$FUNCTION_ID,'.GTW.',@Name)) else null"/></Property>	
				<Property name="OPCClientID" dt="string">OPCClient_ATS_<xsl:value-of select="$FUNCTION_ID"/></Property>	
			</Properties>
		</Object>

</xsl:template>	


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
