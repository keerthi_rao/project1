<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
		<xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
</%block>

<xsl:template match="Train_Unit">
<xsl:variable name="TTName" select= "concat('Train',format-number(@ID,'000'),'.MaxDwellTimeActivation')"/>
  <%
    tt = "<xsl:value-of select='$TTName'/>"
  %>

<Equipment name="{$TTName}" type="HMITrainMaxDwellTimeActivation" flavor="RSCommand">
	<States>
		<Equation>STOP_SENDING_ACTIVATION[0]::(MainTELLV1.MaxDwellTimeActivation !=0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.longPlug_1 != 0)||(MainTELLV1.MaxDwellTimeActivation == 0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.longPlug_1==0) &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.boolPlug_1 !=0</Equation>
		<Equation>SET_ACTIVATION[2]::MainTELLV1.MaxDwellTimeActivation != 0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.longPlug_1==0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.boolPlug_1 !=0</Equation>
		<Equation>SET_DESACTIVATION[3]::MainTELLV1.MaxDwellTimeActivation == 0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.longPlug_1 != 0 &amp;&amp; ${tt}.HMITrainAttributes.HMITETrain.boolPlug_1 !=0</Equation>
		<Equation>ACTIVATION_UNKNOWN[10]::1</Equation>
	</States>
</Equipment>	  
</xsl:template>
