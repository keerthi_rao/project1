<%inherit file="/Object_tem.mako"/>
<%! class_ = "MergeConnected12Y" %>

<%block name="classes">
	<xsl:variable name="sigAreas" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
	<xsl:variable name="adjma" select="//Adjacent_Monitoring_Area[ATS_Local_Server_Area_ID = $SERVER_ID or ATS_Central_Server_Area_ID = $SERVER_ID]"/>
	<xsl:variable name="adjSig" select="//Signalisation_Area[@ID=$adjma/sys:sigarea/@id or @ID=$sigAreas/*/Signalisation_Area_ID]"/>
  	<Class name="MergeOPCDAConnected12Y">
  		<Objects>
			<xsl:apply-templates select="$adjSig[Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server']">
				<xsl:with-param name="atstype" select = "'ATS'" />
			</xsl:apply-templates>
  		</Objects>
  	</Class>
	<xsl:if test="$sigAreas[Localisation_Type='Mainline']">
	<Class name="MergeOPCUAConnected12Y">
		<Objects>
			<xsl:apply-templates select="//CBI[@ID=$adjSig[(Area_Type='ATS' and Localisation_Type='Mainline')]/sys:cbi/@id]">
				<xsl:with-param name="atstype" select = "'CBIS'" />
			</xsl:apply-templates>
		</Objects>
  	</Class>
	</xsl:if>
</%block>

<xsl:template match="Signalisation_Area|CBI">
	<xsl:param name="atstype" />
	<Object name="{concat('OPCClient_', $atstype,'_',@ID, '.MergeConnected12Y')}" rules="update_or_create">
		<Properties>
		    <xsl:if test="$atstype='ATS'">
				<Property name="SharedOnIdentifierOPCDAClient" dt="string"><xsl:value-of select="concat('OPCClient_ATS_',@ID)"/></Property>	
			</xsl:if>
			<xsl:if test="$atstype='CBIS'">
				<Property name="SharedOnIdentifierOPCUAClient" dt="string"><xsl:value-of select="concat('OPCClient_CBIS_',@ID)"/></Property>
			</xsl:if>
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>