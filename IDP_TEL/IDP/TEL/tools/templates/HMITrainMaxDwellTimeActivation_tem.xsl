<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMITrainMaxDwellTimeActivation" %>

<%block name="classes">
	<Class name="HMITrainMaxDwellTimeActivation">
	     <Objects>
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TTName" select= "concat('Train',format-number(@ID,'000'),'.MaxDwellTimeActivation')"/>
	<xsl:variable name="id" select="concat('Train',format-number(@ID,'000'))"/>
	<Object name="{$TTName}" rules="update_or_create">
		<Properties>
			<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$TTName"/></Property>		
			<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>		
			<Property name="long_1" dt="string">MAX_DWELL_TIME_ACTIVATED_STATUS</Property>		
			<Property name="bool_1" dt="string">LocalizeID</Property>		
         	        <Property name="TrainName" dt="string"><xsl:value-of select="$id"/></Property>		
			<Property name="RSCommand1" dt="string">%RollingStockID%|<xsl:value-of select="$id"/>|XmlRSMCommand|&lt;GenericSettingRequest&gt;&lt;Field name="MAX_DWELL_TIME_ACTIVATION" value="0" DEMUX="1"/&gt; &lt;Field name="MAX_DWELL_TIME_DESACTIVATION" value="0" DEMUX="1"/&gt;&lt;/GenericSettingRequest&gt;</Property>		
			<Property name="RSCommand2" dt="string">%RollingStockID%|<xsl:value-of select="$id"/>|XmlRSMCommand|&lt;GenericSettingRequest&gt;&lt;Field name="MAX_DWELL_TIME_ACTIVATION" value="1" DEMUX="1"/&gt;&lt;/GenericSettingRequest&gt;</Property>		
			<Property name="RSCommand3" dt="string">%RollingStockID%|<xsl:value-of select="$id"/>|XmlRSMCommand|&lt;GenericSettingRequest&gt;&lt;Field name=&quot;MAX_DWELL_TIME_DESACTIVATION&quot; value=&quot;1&quot; DEMUX=&quot;1&quot;/&gt;&lt;/GenericSettingRequest&gt;</Property>	
			${multilingualvalue('$id','Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>




