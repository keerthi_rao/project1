<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<RouteSettingObjects>
    <xsl:apply-templates select="$sysdb//Route"/>
</RouteSettingObjects>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>


<xsl:template match="Route">
	<xsl:variable name="self" select="."/>
    <xsl:variable name="route_id" select="@ID"/>
    <xsl:variable name="block_ids" select="Block_ID_List/Block_ID"/>
    <xsl:variable name="track_ids" select="distinct-values($sysdb//Block[@ID=$block_ids]/Track_ID)"/>
    <xsl:variable name="mainline_names" select="//Mainlines/Mainline/Track_ID_List/Track_ID[text()=$track_ids]/../../@Name"/>
    <xsl:variable name="depot_names" select="//Depots/Depot/Track_ID_List/Track_ID[.=$track_ids]/../../@Name"/>
    <xsl:variable name="md_name" select="if ($mainline_names) then $mainline_names else $depot_names"/>
    <xsl:variable name="isOnMainline" select="$sysdb/Mainlines/Mainline/@Name = $md_name"/>
    <xsl:variable name="signal_id" select="Origin_Signal_ID"/>
    <xsl:variable name="signal" select="$sysdb//Signal[@ID=$signal_id]"/>
    <xsl:variable name="cross_trigger" select="$sysdb//Cross_Trigger[Signal_ID=$signal_id]"/>
    <xsl:variable name="spec_route" select="$sysdb//Specific_Route[(Type='Call_On' or Type='HILC') and Specific_Route_ID_List/Route_ID = $route_id]"/>
    <xsl:variable name="sigareaid" select="$sysdb//Signalisation_Area[(every $b_id in $block_ids satisfies $b_id = Block_ID_List/Block_ID)]/@ID"/>
	<xsl:variable name="funcs" select="$sysdb//Function[@Signalisation_Area_ID=$sigareaid and @Function_Type='Level_2']"/>
    <xsl:variable name="cbi" select="distinct-values($sysdb/Blocks/Block[@ID = current()/Block_ID_List/Block_ID]/sys:cbi/@id)"/>
	<xsl:variable name="cbis" select="if (count($cbi) &gt; 1) then '2' else '1'"/>
	<xsl:variable name="sc" select="$sysdb//System_Constraints_Constant"/>
    <xsl:if test="$cross_trigger and not($spec_route)">
		<xsl:variable name="rsps" select="//RouteSettingPoint[sys:signal=$signal_id]/@ID"/>
		<xsl:for-each select="$rsps">
			<xsl:variable name="s" select="."/>
			<xsl:variable name="pos" select="position()"/>
			<xsl:variable name="regPT" select="//RegulationPoint[@ID=//RouteSettingPoint[@ID=$s]/RelatedRegulationPointID]/@Name"/>
			<xsl:variable name="nm" select="if (position()=1) then (concat($self/@Name, '.Routesettingobject')) else (concat($self/@Name,'_',$regPT,'.Routesettingobject'))"/>
			<xsl:variable name="id" select="if (position()=1) then ($self/@ID) else ((position()-1)*10000 + number($self/@ID))"/>
			<RouteSettingObject ID="{$id}" Name="{$nm}">
				<xsl:variable name="block_ids" select="$self/Block_ID_List/Block_ID"/>
				${node("RouteSettingPoint_ID", "$rsps[$pos]")}
				<Check_TPs_Free>false</Check_TPs_Free>
				<TrackPortions_List>
					<xsl:for-each select="$self/Block_ID_List/Block_ID">
						<xsl:variable name="block_id" select="."/>
						<xsl:variable name="tp_id" select="//TrackPortion[sys:block_id=$block_id]"/>
						<TrackPortion_ID><xsl:value-of select="$tp_id/@ID"/></TrackPortion_ID>
					</xsl:for-each>
				</TrackPortions_List>
				<OPCServer_ProgID><xsl:if test="count($funcs) > 0">S2K.OpcServer.1</xsl:if></OPCServer_ProgID>
				${node("OPCServer_StateElement", "concat($self/@Name, '.Detection.Template.iEqpState')")}
				<ValueSet_List>
					<ValueSet>0</ValueSet>
				</ValueSet_List>
				<ValueNotSet_List>
					<ValueNotSet>1</ValueNotSet>
				</ValueNotSet_List>    
				${node("OPCServer_RequisiteElement", "concat($self/@Name, '.Requesite.Template.iEqpState')")}
				${node("OPCServer_ControlElement", "concat($self/@Name, '.Detection.Template.iCommand')")}
				${node("OPCServer_ImmediateResultElement", "concat($self/@Name, '.ImmediateResult.Template.iEqpState')")}
				${node("ImmediateResultTimeout", "if ($cbis = '1') then $sc/T_ATS_CBI_Reply_Timeout\
												  else $sc/T_ATS_2CBIs_Reply_Timeout")}
				${node("OPCServer_CommandResultElement", "concat($self/@Name, '.CommandResult.Template.iEqpState')")}
				<!-- Case:  there are some Track_ID in MainLine and others in Depot. 
				In this case we consider the Route on MainLine.-->
				<xsl:variable name="t_r_cancel" select="if ($isOnMainline) then $sc/T_Route_Cancel_Mainline_Exec_Timeout else $sc/T_Route_Cancel_Depot_Exec_Timeout "/>
				<xsl:variable name="t_cbis" select="$sc/T_2CBIs_Route_Control_Exec_Timeout"/>
				${node("CommandResultTimeout", "if (number($t_r_cancel) &gt; number($t_cbis)) then $t_r_cancel else $t_cbis")}
			</RouteSettingObject>
		</xsl:for-each>
    </xsl:if>

</xsl:template>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>

<%namespace file="lib_tem_tel.mako" import="node"/>