<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
  	<Class name="S2KEvents_Merge">
  		<Objects>
  			<xsl:apply-templates select="//Traffic_Locking[Origin_CBI_ID=$CBI_ID]"/>
  		</Objects>
  	</Class>	
</%block>	

<xsl:template match="Traffic_Locking">
	<xsl:apply-templates select="//Events/Event[Type='Traffic_Locking']">
		<xsl:with-param name="parentname" select = "@Name" />
		<xsl:with-param name="signalid" select = "Destination_Signal_ID" />
		<xsl:with-param name="oppTlid" select = "Opposite_TL_ID" />
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "parentname" />
	<xsl:param name = "signalid" />
	<xsl:param name = "oppTlid" />
	<xsl:variable name="yyyy" select="concat('TL_',replace(replace(substring-after($parentname,'TLA_'),'IXL1','01'),'IXL2','02'))"/>
	<xsl:variable name="detailCode" select="substring-before($yyyy,concat('_',tokenize($yyyy,'_')[last()]))"/>
	<xsl:variable name="opsig" select="//Signal[@ID=//Traffic_Locking[@ID=$oppTlid]/Destination_Signal_ID]"/>
	<xsl:variable name="sect" select="substring-after(//Technical_Room[@ID=$opsig/Technical_Room_ID]/@Name,'_')"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (replace(Name_PV_Trig,'--Traffic_Locking.Name--',$parentname)) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$detailCode),'xxxx',$sect)"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="tracktype" select="//Track[@ID=$opsig/Track_ID]/Track_Type"/>
	<xsl:variable name="optr" select="if($tracktype='Depot') then ('MDD') else if($tracktype='Test track') then ('TTCU') else if($tracktype='Mainline') then ('TAS') else null"/>
	<xsl:variable name="terrname" select="$opsig/sys:terr/@name"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="if(ends-with($parentname,'CB')) then ('Changi Bound') else if(ends-with($parentname,'WB')) then ('Woodlands Bound') else null"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname,'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue('$parentname','Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

