<%inherit file="/Object_tem.mako"/>
<%! class_ = "OPCClient" %>
<%!
LIST=[('ISCS','OPCClient_ISCS_LV1','RDD.1.RDDTELOPCMDBGTW','_LV1','localhost','localhost'),
      ('PIM','OPCClient_PIMModule','RDD.1.RDDPIMCOM', '_LV2','localhost','localhost'),
	  ('MMS','OPCClient_MMSModule','RDD.1.RDDMMSCOM', '_LV1','localhost','localhost'),
	  ('TWP','OPCClient_TWP','ModbusProtocol.1.T281OPCMDBGTW', '_LV2','MDDDSERSRVA','MDDDSERSRVB'),
	  ('GTW','OPCClient_GTW','REDUNDANCY.FEP_redundancy_configuration.1', '_LV1','OCCOCCERGTWA','OCCOCCERGTWB')]
 %>
 <xsl:param name="IS_BOTH_LEVEL"/>
<%block name="classes">
	<Class name="S2KIOOPCServerMonitor">
		<Objects>
		 	% for typ, nam, opctag, lv ,snn1,snn2 in LIST:
				<xsl:if test="'${lv}'=$SERVER_LEVEL">
					<Object name="${nam}" rules="update_or_create">
						<Properties>
							${prop("ConnectAttemptsPeriod","10","i4")}
							${prop("DoubleLinks","0","boolean")}
							${prop("MonitoringPeriod","30")}
							${prop("MultiActive","1","boolean")}
							<Property name="ServerNodeName1" dt="string">${snn1}</Property>
							<Property name="ServerNodeName2" dt="string">${snn2}</Property>							
							<Property name="ServerProgID1" dt="string">${opctag}</Property>
							<Property name="ServerProgID2" dt="string">${opctag}</Property>
							${prop("SharedOnIdentifierDefinition","1","boolean")}
							<Property name="SharedOnIdentifier" dt="string">${nam}</Property>
						</Properties>
					</Object>
				</xsl:if>
			% endfor
		</Objects>
	</Class>
</%block>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>