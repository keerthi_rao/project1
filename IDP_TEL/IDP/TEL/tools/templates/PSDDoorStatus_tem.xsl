<%inherit file="/Object_tem.mako"/>
<%! class_ = "PSDDoorStatus" %>

<%block name="classes">
  	<Class name="PSDDoorStatus">
  		<Objects>
  			<!-- Generate the items of Platform_Screen_Doors table -->
  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
  		</Objects>
  	</Class>
</%block>

<!-- Template to match the Platform table -->
<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="rol" select="//Role"/>
	<xsl:for-each select="1 to 20">
		<xsl:variable name="sta" select="concat($nm, '.PSDStatus', '.Door',format-number(position(),'#'))"/>
		<xsl:variable name="stai" select="concat($nm, '.Door',format-number(position(),'#'))"/>
		<Object name="{$sta}" rules="update_or_create">
			<Properties>
				<!-- MAKO to generate properties -->
				${props([('ID', 'string' ,'<xsl:value-of select="$sta"/>'),
						 ('SharedOnIdentifier', 'string', '<xsl:value-of select="$sta"/>'), ('SharedOnIdentifierDefinition', 'boolean', '1')])}
					<xsl:apply-templates select="$rol"><xsl:with-param name="nm1" select="$stai"/></xsl:apply-templates>
			</Properties>
		</Object>
	</xsl:for-each>
</xsl:template>

<!-- Template to match the Platform table -->
<xsl:template match="Role">
<xsl:param name="nm1"/>
	<xsl:variable name="rid" select="@RID"/>
	<MultiLingualProperty name="Name">
		<xsl:for-each select="MLV">
			<MultiLingualValue roleId="{$rid}" localeId="{@LCID}"><xsl:value-of select="$nm1"/></MultiLingualValue>
		</xsl:for-each>
	</MultiLingualProperty>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>