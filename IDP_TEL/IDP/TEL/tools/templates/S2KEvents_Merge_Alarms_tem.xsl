<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge_Alarms" %>



<%block name="classes">
	<Class name="S2KEvents_Merge_Alarms">
		<Objects>
			<!-- Call template for generating object-->
			<xsl:apply-templates select="//Train_Unit"/>
		</Objects>
	</Class>
</%block>

<!-- Template for generating object-->
<xsl:template match="Train_Unit">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="tcid" select="Train_Unit_Characteristics_ID"/>
	<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
	<xsl:if test="number(@ID)&lt;=number('30')"> 	
	<xsl:for-each select="//HMITrainAlarmMonitored/Alarm[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
			<Object name="{concat($TrainName,@Name)}_Events" rules="update_or_create">
 				<Properties>
					<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
					<Property name="AlarmInhib" dt="boolean">1</Property>
					<Property name="Out_PV_Trig" dt="string">MainAlarmState</Property>
	<!-- 				<Property name="State_PV_Trig" dt="i4">1</Property> -->
					<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
					<xsl:variable name="EquipmentID"><xsl:value-of select="if(Localisation != '0') then concat('2',format-number($id,'000'),Localisation) else '-'"/></xsl:variable>
					<xsl:variable name="MMS"><xsl:value-of select="MMS"/></xsl:variable>
					<xsl:variable name="Avalanche"><xsl:value-of select="Avalanche"/></xsl:variable>
					<xsl:variable name="Value"><xsl:value-of select="VALUE"/></xsl:variable>
					<xsl:variable name="O_M_B"><xsl:value-of select="if(O_M_B='0') then ('-') else (O_M_B)"/></xsl:variable>
					<xsl:variable name="Value_off"><xsl:value-of select="VALUE_off"/></xsl:variable>
<!-- 					<xsl:variable name="Name_PV_Trig"><xsl:value-of select="concat($TrainName,@Name,'.TrainMonitoredAlarm.S2KMonitoredAlarm')"/></xsl:variable>
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$Name_PV_Trig"/></Property> -->
					<Property name="BstrParam1" dt="string"></Property>
<!-- 					<Property name="Operator" dt="string">.TAS</Property> -->
						<Property name="Shared_TrainAlarm" dt="string"><xsl:value-of select="concat($TrainName,@Name)"/></Property> 
					${multilingualvalue('$EquipmentID', 'EquipmentID')}
					${multilingualvalue('$MMS', 'MMS')}
					${multilingualvalue('$Avalanche', 'Avalanche')}
					${multilingualvalue('$O_M_B', 'O_M_B')}
					${multilingualvalue('$Value', 'Value')}
					${multilingualvalue('$Value_off', 'Value_off')}
				</Properties> 
			</Object>	
		</xsl:for-each>
		</xsl:if>
		<xsl:for-each select="//HMITrainAlarmMonitored_Loco/Alarm_Loco[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
			<xsl:if test="//Train_Unit_Characteristics[@ID=$tcid]/@Name='TRUC_LOCO'"> 
			<xsl:variable name="trainAlarmName" select="concat($TrainName,@Name)"/>
			<Object name="{concat($TrainName,@Name)}_Events" rules="update_or_create">
 				<Properties>
					<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
					<Property name="AlarmInhib" dt="boolean">1</Property>
					<Property name="Out_PV_Trig" dt="string">MainAlarmState</Property>
					<Property name="State_PV_Trig" dt="i4">1</Property>
					<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
					<xsl:variable name="EquipmentID"><xsl:value-of select="if(Localisation != '0') then concat('2',format-number($id,'000'),Localisation) else '-'"/></xsl:variable>
					<xsl:variable name="MMS"><xsl:value-of select="MMS"/></xsl:variable>
					<xsl:variable name="Avalanche"><xsl:value-of select="Avalanche"/></xsl:variable>
					<xsl:variable name="Value"><xsl:value-of select="VALUE"/></xsl:variable>
					<xsl:variable name="O_M_B"><xsl:value-of select="if(O_M_B='0') then ('-') else (O_M_B)"/></xsl:variable>
					<xsl:variable name="Value_off"><xsl:value-of select="VALUE_off"/></xsl:variable>
					<xsl:variable name="Name_PV_Trig"><xsl:value-of select="concat($TrainName,@Name,'.TrainMonitoredAlarm.S2KMonitoredAlarm')"/></xsl:variable>
					<Property name="BstrParam1" dt="string"><xsl:value-of select="$TrainName"/></Property>
					<Property name="Operator" dt="string">.TAS</Property>
					${multilingualvalue('$EquipmentID', 'EquipmentID')}
					${multilingualvalue('$MMS', 'MMS')}
					${multilingualvalue('$Avalanche', 'Avalanche')}
					${multilingualvalue('$O_M_B', 'O_M_B')}
					${multilingualvalue('$Value', 'Value')}
					${multilingualvalue('$Value_off', 'Value_off')}
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$Name_PV_Trig"/></Property>
					<Property name="Shared_TrainAlarm" dt="string"><xsl:value-of select="concat($TrainName,@Name)"/></Property> 					
				</Properties> 
			</Object>
			</xsl:if>	 		
		</xsl:for-each>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>
