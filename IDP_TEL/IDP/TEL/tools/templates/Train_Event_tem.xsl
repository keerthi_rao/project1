<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
	     <Objects>
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit[number(@ID)&lt;=30]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
<xsl:if test="$SERVER_LEVEL='_LV1'">
	<xsl:apply-templates select="//Events/Event[Type='Train_Event']">
	<xsl:with-param name="parentname" select = "concat('Train',format-number(@ID,'000'))" />
	<xsl:with-param name="id" select = "@ID" />
	</xsl:apply-templates>
</xsl:if>

<xsl:if test="$SERVER_LEVEL='_LV2'">
	<xsl:apply-templates select="//Events/Event[Type='Train_Event_LV2']">
	<xsl:with-param name="parentname" select = "concat('Train',format-number(@ID,'000'))" />
	<xsl:with-param name="id" select = "@ID" />
	</xsl:apply-templates>
</xsl:if>

</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "parentname"/>
	<xsl:param name = "id"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (replace(Name_PV_Trig,'--TrainName--',$parentname)) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="EquipmentID"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="equip" select="replace(EquipmentID,'zzzz',concat('Train',format-number(number($id+2000),'000')))"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$parentname"/></Property>
			<Property name="BstrParam2" dt="string"><xsl:value-of select="BstrParam2"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="'-'"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			<xsl:if test="not(Name_PV_Dynam_1='-')">
				<Property name="Name_PV_Dynam_1" dt="string"><xsl:value-of select="replace(Name_PV_Dynam_1,'--TrainName--',$parentname)"/></Property>
			</xsl:if>
			<xsl:if test="not(Name_PV_Dynam_2='-')">
				<Property name="Name_PV_Dynam_2" dt="string"><xsl:value-of select="replace(Name_PV_Dynam_2,'--TrainName--',$parentname)"/></Property>
			</xsl:if>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$equip','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue('$parentname','Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
