<%inherit file="/Object_tem.mako"/>
<%! class_ = "OPCAnalog" %>

<%block name="classes">
	<Class name="OPCAnalog">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID and not(contains(@Name,'VS')) and Signal_Type_Function!='Virtual']"/>	
			<xsl:apply-templates select="$sysdb//MSSAlarm/lruState[Signalisation_Area_ID=$SERVER_ID and Type='MS']"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Signal">
	<xsl:variable name="nm" select="concat(@Name,'.Status_Restrictive_Counter')"/>\
	<Object name="{$nm}" rules="update_or_create">
		<Properties>
			<Property name="OPCClientRef" dt="string">OPCClient_SW_RT<xsl:value-of select="$SERVER_ID"/>_Status</Property>					
			<Property name="OPCTag" dt="string"></Property>					
			<Property name="HHThresholdValue" dt="i4">63000000</Property>					
		</Properties>	
	</Object>
</xsl:template>
<xsl:template match="lruState">
	<xsl:variable name="nm" select="concat(@Name,'Status_Capacity_Limit')"/>\
	<Object name="{$nm}" rules="update_or_create">
		<Properties>
			<Property name="OPCClientRef" dt="string">OPCClient_SW_RT<xsl:value-of select="$SERVER_ID"/>_Status</Property>					
			<Property name="OPCTag" dt="string">[2:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;2:cpqhost&lt;Organizes&gt;2:<xsl:value-of select="@Name"/>&lt;Organizes&gt;2:cpqHostOs&lt;Organizes&gt;2:cpqHoComponent&lt;Organizes&gt;2:cpqHoFileSys&lt;Organizes&gt;2:cpqHoFileSysTable&lt;Organizes&gt;2:cpqHoFileSysEntry&lt;HasComponent&gt;2:2cpqHoFileSysPercentSpaceUsed</Property>					
			<Property name="LThresholdValue" dt="i4">63000000</Property>					
		</Properties>	
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

