<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "Interlocking"
	name="Interlocking"
 %>
<xsl:variable name="fepopc_interface" select="$sysdb//Project[@ID = '1']/FEP_ATS_OPCUA_Interface"/>
<xsl:variable name="other_cbi_id" select="//Signal[not(sys:cbi/@id=$CBI_ID and @ID = //Exit_Gate/Exit_Gate_Signal_ID) and (@ID = //Route[sys:cbi/@id=$CBI_ID]/Destination_Signal_ID)]/sys:cbi/@id"/>
<%block name="classes">
	<xsl:variable name="sigArea" select="//Signalisation_Area[sys:cbi/@id = $CBI_ID and (Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server')]"/>
	<xsl:variable name="x" select="//Signal[not(sys:sigarea/@id=$sigArea/@ID)]"/>
	<xsl:variable name="Rts" select="//Signal[not(sys:sigarea/@id=$sigArea/@ID or sys:cbi/@id=$CBI_ID or @ID = //Exit_Gate/Exit_Gate_Signal_ID) and (@ID = //Route[sys:cbi/@id=$CBI_ID]/Destination_Signal_ID)]"/>
	
	<xsl:variable name="togenerate" select="if ($Rts) then 'Yes' else 'No'"/>
	<!--t><xsl:value-of select="$sigArea/@ID"/>, <xsl:value-of select="$x/sys:sigarea/@id"/></t-->
	<xsl:if test="$togenerate = 'No'">
		<xsl:result-document href="Interlocking_Adjacent_{$CBI_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	
	<xsl:variable name="interlocking_variable" select="if ($fepopc_interface = 'true') then 'Interlocking' else 'Interlocking'"/>
	<xsl:if test="not($fepopc_interface) or $fepopc_interface != 'true'">
		<Class name="S2KIOOPCServerMonitor">
			<Objects>
				<xsl:apply-templates select="$sysdb/CBIs/CBI[@ID = $Rts/sys:cbi/@id]" mode="S2KIOOPCServerMonitor"/>
			</Objects>
		</Class>
	</xsl:if>
	<xsl:if test="$fepopc_interface = 'true'">
		<Class name="S2KOPCUAClient.IOClient">
			<Objects>
				<xsl:apply-templates select="$sysdb/CBIs/CBI[@ID = $Rts/sys:cbi/@id]" mode="S2KIOOPCUAClient"/>
			</Objects>
		</Class>
	</xsl:if>
	<Class name="{$interlocking_variable}">
		<Objects>
			<xsl:apply-templates select="$sysdb/CBIs/CBI[@ID = $Rts/sys:cbi/@id]" mode="Interlocking"/>
		</Objects>
	</Class>
	<Class name="Variable">
		<Objects>
			<xsl:apply-templates select="$Rts" mode="Variable"/>
		</Objects>
	</Class>
</%block>

<xsl:key name="vmmi_cbi" match="//CBI_ATS/@CBI_Emitter_ID" use="parent::*/CBI_ATS_Object[@Type_Of_Information = 'VMMI Activation']/@Object_Name"/>
<xsl:key name="genericio_cbi" match="//CBI/@ID" use="parent::*/CBI_Area_ID"/>

<!-- Template for S2KIOOPCServerMonitor-->
<xsl:template match="CBI" mode="S2KIOOPCServerMonitor">
	<xsl:variable name="name" select="concat('OPCClient_CBIS_',@ID)"/>
	<xsl:variable name="serverprogid" select="if ($sysdb//Signalisation_Area[Area_Type = 'ATS' and (Server_Type = 'Level_1' or Server_Type = 'Both_Levels') and sys:cbi/@id = $CBI_ID]) then concat('CBIS.1.CBIS_',@ID) else concat('RDD.1.RDDCBIS_',@ID)"/>
	<xsl:variable name="shouldRequestsBeDuplicated" select="0"/>
	<Object name="{$name}" rules="update_or_create">
		<Properties>
			${props([('ConnectAttemptsPeriod','i4','10'),('DoubleLinks','boolean','0'),('MonitoringPeriod','i4','30'),('MultiActive','boolean','1'),
					 ('ShouldRequestsBeDuplicated','boolean','<xsl:value-of select="$shouldRequestsBeDuplicated"/>'),('ServerNodeName1','string','localhost'),
					 ('ServerNodeName2','string','localhost'),('ServerProgID1','string','<xsl:value-of select="$serverprogid"/>'),
					 ('ServerProgID2','string','<xsl:value-of select="$serverprogid"/>'),('SharedOnIdentifierDefinition','boolean','1'),
					 ('SharedOnIdentifier','string','<xsl:value-of select="$name"/>')])}
		</Properties>
	</Object>
</xsl:template>

<!-- Template for S2KIOOPCUAClient-->
<xsl:template match="CBI" mode="S2KIOOPCUAClient">
	<xsl:variable name="name" select="concat('OPCClient_CBIS_',@ID)"/>
	<xsl:variable name="techid" select="Technical_Room_ID"/>
	<xsl:variable name="portnumber" select="concat('51',format-number(SSID,'00'),'8')"/>
	<Object name="{$name}" rules="update_or_create">
		<Properties>
			<Property name="ConnectAttemptsPeriod" dt="i4">10</Property>
			<Property name="Server1Url" dt="string">opc.tcp://<xsl:value-of select="$sysdb//ATS_Equipment[ATS_Equipment_Type = 'FEP' and Technical_Room_ID = $techid][1]/@Name"/>:<xsl:value-of select="$portnumber"/></Property>
			<Property name="Server2Url" dt="string">opc.tcp://<xsl:value-of select="$sysdb//ATS_Equipment[ATS_Equipment_Type = 'FEP' and Technical_Room_ID = $techid][2]/@Name"/>:<xsl:value-of select="$portnumber"/></Property>
			<Property name="DuplicateControls" dt="boolean">1</Property>
			<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
			<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$name"/></Property>
		</Properties>
	</Object>
</xsl:template>

<!-- Template for Interlocking/InterlockingUA-->
<xsl:template match="CBI" mode="Interlocking">
	<xsl:variable name="name" select="concat('CBIS_',@ID)"/>
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="mname" select="@Name"/>
	<Object name="{$name}" rules="update_or_create">
		<Properties>
			<Property name="OPCUAInterface" dt="boolean"><xsl:value-of select="if ($fepopc_interface = 'true') then 1 else 0"/></Property>
			<Property name="ATSPresenceMonitoringAvailable" dt="boolean"><xsl:value-of select="if (CBI_Monitoring_ATS = 'true') then 1 else 0"/></Property>
			<Property name="OPCClientID" dt="string"><xsl:value-of select="concat('OPCClient_CBIS_',@ID)"/></Property>
			<Property name="LinkAlarm_EnableInstance" dt="boolean">0</Property>
			<Property name="MAPS_PV" dt="string"><xsl:value-of select="if (MAPS/@Bit_Position or $sysdb/CBIs_Variable/CBI[@ID = $id]/MAPS/@Bit_Position) then concat('MAPS_',@Name) else null"/></Property>
			<Property name="MAP_PV" dt="string"><xsl:value-of select="if (MAP/@Bit_Position or $sysdb/CBIs_Variable/CBI[@ID = $id]/MAP/@Bit_Position) then concat('MAP_',@Name) else null"/></Property>
			<Property name="IMAP_PV" dt="string"><xsl:value-of select="if (IMAP/@Bit_Position or $sysdb/CBIs_Variable/CBI[@ID = $id]/IMAP/@Bit_Position) then concat('IMAP_',@Name) else null"/></Property>
			<xsl:if test="$fepopc_interface = 'true'">
				<Property name="OpreatingModeStatePVOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:OperatingMode&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:iState</Property>
				<Property name="OperatingModeStatePVSharedOnIdentifier" dt="string"><xsl:value-of select="concat('CBIS_',@ID,'.OperatingModeState')"/></Property>
				<Property name="PV_Diag1LinkAOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics1&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:LinkA</Property>
				<Property name="PV_Diag1LinkBOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics1&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:LinkB</Property>
				<Property name="PV_Diag1MasterOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics1&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:Master</Property>
				<Property name="PV_Diag1RddOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics1&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:RedundantMode</Property>
				<Property name="PV_Diag1SlaveOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics1&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:Slave</Property>
				<Property name="PV_Diag2LinkAOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics2&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:LinkA</Property>
				<Property name="PV_Diag2LinkBOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics2&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:LinkB</Property>
				<Property name="PV_Diag2MasterOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics2&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:Master</Property>
				<Property name="PV_Diag2RddOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics2&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:RedundantMode</Property>
				<Property name="PV_Diag2SlaveOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Diagnostics2&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:Slave</Property>
				<Property name="PV_RddEnableOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Redundancy&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:Enable</Property>
				<Property name="S2KStringPVOPCTag" dt="string">[<xsl:value-of select="$CBI_ID"/>:FEP_NS_<xsl:value-of select="$CBI_ID"/>]&lt;Organizes&gt;<xsl:value-of select="$CBI_ID"/>:Traces&lt;HasComponent&gt;<xsl:value-of select="$CBI_ID"/>:AlarmEventInfo</Property>
			</xsl:if>
			${multilingualvalue('$mname','Name')}
		</Properties>
	</Object>
</xsl:template>

<xsl:template match="Signal" mode="Variable">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="sigcbi" select="sys:cbi[1]/@id"/>
	<xsl:variable name="check" select="if (local-name()='Aspect_Indicator' or local-name()='Direction_Indicator') then (if (//Urbalis_Sector[@ID=//CBI[@ID=$CBI_ID]/Urbalis_Sector_ID]/Urbalis_Sector_Option/@Direction_Indicator_Presence='true') then true() else false()) else if (local-name()='Traffic_Locking') then (if (//Urbalis_Sector[@ID=//CBI[@ID=$CBI_ID]/Urbalis_Sector_ID]/Urbalis_Sector_Option/@Traffic_Locking_Presence='true') then true() else false()) else true()"/>
	<xsl:variable name="parent" select="concat(../local-name(),'_Variable')"/>
	<xsl:variable name="var_path" select="$sysdb/*[local-name() = $parent]/*[@ID = $id]"/>
	<!-- Condition for Route 'RC' Variable :: For each Route with variable 'Route Control' is available in a ATSxxx_CBIyyy_Data_Interface file and no 'Route Control Preparation' in this same file. -->
	<xsl:variable name="hasBoth" select="if (local-name()='Route') then (if ($var_path/*[@Type_Of_Information='Route Control'] and $var_path/*[@Type_Of_Information='Route Control Preparation']) then true() else false()) else false()"/>
	<xsl:for-each select="$var_path/*[(@type = 'CBI_ATS_Object' or @type = 'ATS_CBI_Object')]">
	
		<xsl:if test="(name()='SDBSE' or name()='SDBSCA' or name()='SDBCP' or name()='SDBCC' or name()='LPSMOD' or name()='LCSMOD' or name()='SDB' or name()='SDBSC' or name()='SDBSS' or name()='SDBCCP' or name()='SDBCCC' or name()='SDBSSV' or name()='SDBSCV' or name()='LCSRES' or name()='LCSPER' or name()='LCSANN' or name()='LCSMOD' or name()='LPSPER' or name()='SAL' or name()='SB' or name()='LPSRES' or name()='SPER')">
		<xsl:variable name="objname" select="@Variable_Name"/>
			<xsl:if test="$check and (if ($hasBoth) then (@Type_Of_Information!='Route Control') else true())">
				<Object name="{$objname}" rules="update_or_create">
					<Properties>
						<Property name="Address" dt="i4">0</Property>
						<Property name="InterlockingID" dt="string"><xsl:value-of select="concat('CBIS_',$sigcbi)"/></Property>
						<Property name="OPCGroup" dt="string">MyGroup</Property>
						<Property name="OPCTag" dt="string"><xsl:value-of select="if(//Project[@ID='1']/FEP_ATS_OPCUA_Interface = 'true') then replace(replace(@OPCTag,'Index', $sigcbi),'NameSpace',concat('FEP_NS_',$sigcbi)) else @OPCTag"/></Property>
						<Property name="OPCType" dt="string"><xsl:value-of select="if (@OPCType = 'HILC:String') then 'VT_BSTR' else (if (@OPCType = 'HILC:I2' or @OPCType = 'I2') then 'VT_I2' else (if (@OPCType = 'HILC:I4' or @OPCType = 'I4') then 'VT_I4' else 'VT_BOOL'))"/></Property>
						<Property name="Type" dt="string"><xsl:value-of select="@RM_RC"/></Property>
					</Properties>
				</Object>
			</xsl:if>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="props,multilingualvalue"/>
