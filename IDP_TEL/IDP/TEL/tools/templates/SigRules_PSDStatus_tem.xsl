<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
  	<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
</%block>

<!-- Template for generating equations from Platforms Table-->
<xsl:template match="Platform_Screen_Doors">
        <xsl:variable name="psdnodefualt" select="replace('((DOORSTATUS1_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS2_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS3_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS4_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS5_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS6_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS7_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS8_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS9_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS10_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS11_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS12_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS13_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS14_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS15_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS16_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS17_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS18_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS19_tt.S2KNumericProcessVariable &amp; 4)==0) &amp;&amp; ((DOORSTATUS20_tt.S2KNumericProcessVariable &amp; 4)==0)', 'tt', @Name)"/>
		<xsl:variable name="psddefualt" select="replace('((DOORSTATUS1_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS2_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS3_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS4_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS5_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS6_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS7_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS8_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS9_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS10_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS11_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS12_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS13_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS14_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS15_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS16_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS17_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS18_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS19_tt.S2KNumericProcessVariable &amp; 4) &gt; 0) || ((DOORSTATUS20_tt.S2KNumericProcessVariable &amp; 4) &gt; 0)', 'tt', @Name)"/>
		<xsl:variable name="sa" select="//Stopping_Area[*/*[@Original_Area_Type='Platform']/@ID=//Platform[PSD_ID=current()/@ID]/@ID]"/>
		<%
			xx = '<xsl:value-of select="@Name"/>.S2KNumericProcessVariable'
		%>
		
		${Equipment('PSDStatus','ClosedAndLocked',[('PSD_NOT_LOCKED[0]','(PSDSTATUS1_%s &amp; 1) == 0'%xx),
			   ('PSD_LOCKED[1]','(PSDSTATUS1_%s &amp; 1) &gt; 0'%xx),
			   ('PSD_LOCKED_UNKNOWN[2]','1')], [], 'N', 'Y')}
			   
		${Equipment('PSDStatus','ManualStatus',[('PSD_AUTO_ENABLED[0]','(PSDSTATUS2_%s &amp; 224) == 0'%xx),
			   ('PSD_MANUAL_ENABLED[1]','(PSDSTATUS2_%s &amp; 224) == 1'%xx), ('PSD_MANUAL_ENABLED_OPEN[2]','(PSDSTATUS2_%s &amp; 224) == 3'%xx), 
			   ('PSD_MANUAL_ENABLED_CLOSED[3]','(PSDSTATUS2_%s &amp; 224) == 5'%xx),('PSD_MANUAL_INCONSISTENT[4]','1')],
			   [('PSD_MANUAL_ENABLED[1]', '(PSDSTATUS2_%s &amp; 32) &gt; 0'%xx),
			   ('PSD_MANUAL_ENABLED_OPEN[2]', '(PSDSTATUS2_%s &amp; 96) &gt; 32'%xx),
			   ('PSD_MANUAL_ENABLED_CLOSED[3]', '(PSDSTATUS2_%s &amp; 160) &gt; 32'%xx)], 'N', 'Y')}
		
		${Equipment('PSDStatus','OpenStatus',[('PSD_CLOSED[0]','(PSDSTATUS1_%s &amp; 2) == 0'%xx),
			   ('PSD_OPENED[1]','(PSDSTATUS1_%s &amp; 2) &gt; 0'%xx),
			   ('PSD_OPEN_UNKNOWN[2]','1')],[], 'N', 'Y')}
			   			   
		${Equipment('PSDStatus','ACEarthLeakageStatus',[('PSD_NO_ACLEAK[0]','(PSDSTATUS3_%s &amp; 4) == 0'%xx),
			   ('PSD_ACLEAK[1]','(PSDSTATUS3_%s &amp; 4) &gt; 0'%xx),('PSD_ACLEAK_UNKNOWN[2]','1')],
			   [('PSD_ACLEAK_ALARM[0]', '(PSDSTATUS3_%s &amp; 4) &gt; 0'%xx)], 'N', 'Y')}
			   
		${Equipment('PSDStatus','DCEarthLeakageStatus',[('PSD_NO_DCLEAK[0]','(PSDSTATUS3_%s &amp; 8) == 0'%xx),
			   ('PSD_DCLEAK[1]','(PSDSTATUS3_%s &amp; 8) &gt; 0'%xx),('PSD_DCLEAK_UNKNOWN[2]','1')],
			   [('PSD_DCLEAK_ALARM[0]', '(PSDSTATUS3_%s &amp; 8) &gt; 0'%xx)], 'N', 'Y')}
			   
		${Equipment('PSDStatus','DCUSummary',[('PSD_NO_DCUFAULT[0]','<xsl:value-of select="$psdnodefualt"/>'),
			   ('PSD_DCUFAULT[1]','<xsl:value-of select="$psddefualt"/>'),('PSD_DCUFAULT_UNKNOWN[2]','1')],
			   [], 'N', 'Y')}
			   
		${Equipment('PSDStatus','PDCDCDCConverterStatus',[('PSD_NO_PDCDCDCFAULT[0]','(PSDSTATUS3_%s &amp; 2) == 0'%xx),
			   ('PSD_PDCDCDCFAULT[1]','(PSDSTATUS3_%s &amp; 2) &gt; 0'%xx),('PSD_PDCDCDCFAULT_UNKNOWN[2]','1')],
			   [('PSD_PDCDCDC_ALARM[0]', '(PSDSTATUS3_%s &amp; 2) &gt; 0'%xx)], 'N', 'Y')}
			   
		${Equipment('PSDStatus','PDCRectifierStatus',[('PSD_NO_RECTIFIERFAULT[0]','(PSDSTATUS3_%s &amp; 1) == 0'%xx),
			   ('PSD_RECTIFIERFAULT[1]','(PSDSTATUS3_%s &amp; 1) &gt; 0'%xx),('PSD_RECTIFIERFAULT_UNKNOWN[2]','1')],
			   [('PSD_RECTIFIERFAULT_ALARM[0]', '(PSDSTATUS3_%s &amp; 1) &gt; 0'%xx)], 'N', 'Y')}	
			   
		${Equipment('PSDStatus','SYSSummary',[('PSD_NO_SYSFAULT[0]','(PSDSTATUS1_%s &amp; 32) == 0'%xx),
			   ('PSD_SYSFAULT[1]','(PSDSTATUS1_%s &amp; 32) &gt; 0'%xx),('PSD_SYSFAULT_UNKNOWN[2]','1')],
			   [('PSD_SYS_FAILURE_ALARM[0]', '(PSDSTATUS1_%s &amp; 32) &gt; 0'%xx)], 'N', 'Y')}	
		<xsl:if test="$sa/Bound='WB'">
			${Equipment('PSDStatus','UPSSupplySourceStatus',[('PSD_NO_UPSFAULT[0]','(PSDSTATUS3_%s &amp; 128) == 0'%xx),
				   ('PSD_UPSFAULT[1]','(PSDSTATUS3_%s &amp; 128) &gt; 0'%xx),('PSD_UPSFAULT_UNKNOWN[2]','1')],
				   [('PSD_UPSSUPPLYALARM[0]', '(PSDSTATUS3_%s &amp; 128) &gt; 0'%xx)], 'N', 'Y')}
		</xsl:if>
		<xsl:if test="$sa/Bound='CB'">
			${Equipment('PSDStatus','NormalSupplySourceStatus',[('PSD_NO_NORMALFAULT[0]','(PSDSTATUS3_%s &amp; 128) == 0'%xx),
				   ('PSD_NORMALFAULT[1]','(PSDSTATUS3_%s &amp; 128) &gt; 0'%xx),('PSD_NORMALFAULT_UNKNOWN[2]','1')],
				   [('PSD_NORMALSUPPLYALARM[0]', '(PSDSTATUS3_%s &amp; 128) &gt; 0'%xx)], 'N', 'Y')}
		</xsl:if>
		${Equipment('PSDStatus','AnyPSDFailedToClose',[('PSD_S_PSD_FAILED_CLOSE[1]','(PSDSTATUS1_%s &amp; 8) &gt; 0'%xx)],[('PSD_PSD_FAILED_CLOSE[0]', '(PSDSTATUS1_%s &amp; 8) &gt; 0'%xx)], 'N', 'Y')}
		${Equipment('PSDStatus','AnyPSDFailedToOpen',[('PSD_S_PSD_FAILED_OPEN[1]','(PSDSTATUS1_%s &amp; 4) &gt; 0'%xx)],[('PSD_PSD_FAILED_OPEN[0]', '(PSDSTATUS1_%s &amp; 4) &gt; 0'%xx)], 'N', 'Y')}
		${Equipment('PSDStatus','SIGOpenCommandFault',[('PSD_S_SIG_OPEN_CMD_NO_FAULT[0]','(PSDSTATUS1_%s &amp; 64) == 0'%xx),
		('PSD_S_SIG_OPEN_CMD_FAULT[1]','(PSDSTATUS1_%s &amp; 64) &gt; 0'%xx),('PSD_S_SIG_OPEN_CMD_FAULT_UNKOWN[2]','1')],
		[('PSD_SIG_OPEN_CMD_FAULT[0]', '(PSDSTATUS1_%s &amp; 64) &gt; 0'%xx)], 'N', 'Y')}		
		${Equipment('PSDStatus','SIGCloseCommandFault',[('PSD_S_SIG_CLOSED_CMD_NO_FAULT[0]','(PSDSTATUS1_%s &amp; 128) == 0'%xx),
		('PSD_S_SIG_CLOSED_CMD_FAULT[1]','(PSDSTATUS1_%s &amp; 128) &gt; 0'%xx),('PSD_S_SIG_CLOSED_CMD_FAULT_UNKOWN[2]','1')],
		[('PSD_SIG_CLOSED_CMD_FAULT[0]', '(PSDSTATUS1_%s &amp; 128) &gt; 0'%xx)], 'N', 'Y')}	
		${Equipment('PSDStatus','ClosedAndLockedLoopFault',[('PSD_S_NO_CLOSED_LOCKED_FAULT[0]','(PSDSTATUS1_%s &amp; 512) == 0'%xx),
		('PSD_S_CLOSED_LOCKED_FAULT[1]','(PSDSTATUS1_%s &amp; 512) &gt; 0'%xx),('PSD_S_CLOSED_LOCKED_UNKNOWN[2]','1')],
		[('PSD_CLOSED_LOCKED_FAULT[0]', '(PSDSTATUS1_%s &amp; 512) &gt; 0'%xx)], 'N', 'Y')}		
		
		${Equipment('PSDStatus','PSDDCVoltageLowAlarm',[('PSD_S_PDC_VOLT_NO_LOW[0]','(PSDSTATUS3_%s &amp; 16) == 0'%xx),
		('PSD_S_PDC_VOLT_LOW[1]','(PSDSTATUS3_%s &amp; 16) &gt; 0'%xx),('PSD_S_PDC_VOLT_LOW_UNKNOWN[2]','1')],
		[('PSD_PDC_VOLT_LOW[0]', '(PSDSTATUS3_%s &amp; 16) &gt; 0'%xx)], 'N', 'Y')}		
		
		${Equipment('PSDStatus','PDCDCVoltageHighAlarm',[('PSD_S_PDC_VOLT_NO_HIGH[0]','(PSDSTATUS3_%s &amp; 32) == 0'%xx),
		('PSD_S_PDC_VOLT_HIGH[1]','(PSDSTATUS3_%s &amp; 32) &gt; 0'%xx),('PSD_S_PDC_VOLT_HIGH_UNKNOWN[2]','1')],
		[('PSD_PDC_VOLT_HIGH[0]', '(PSDSTATUS3_%s &amp; 32) &gt; 0'%xx)], 'N', 'Y')}
		
		${Equipment('PSDStatus','MCBTrippedInPDC',[('PSD_S_NO_MCB_TRIPPED[0]','(PSDSTATUS3_%s &amp; 64) == 0'%xx),
		('PSD_S_MCB_TRIPPED[1]','(PSDSTATUS3_%s &amp; 64) &gt; 0'%xx),('PSD_S_MCB_UNKNOWN[2]','1')],
		[('PSD_MCB_TRIPPED[0]', '(PSDSTATUS3_%s &amp; 64) &gt; 0'%xx)], 'N', 'Y')}	
		
		${Equipment('PSDStatus','TransformerFault',[('PSD_S_NO_TRANSFORMERFAULT[0]','(PSDSTATUS3_%s &amp; 256) == 0'%xx),
		('PSD_S_TRANSFORMERFAULT[1]','(PSDSTATUS3_%s &amp; 256) &gt; 0'%xx),('PSD_S_TRANSFORMERFAULT_UNKNOWN[2]','1')],
		[('PSD_TRANSFORMERFAULT[0]', '(PSDSTATUS3_%s &amp; 256) &gt; 0'%xx)], 'N', 'Y')}		
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="Equipment(type, flavor, list, diflist, isdiff, isstate)" >
  <Equipment name="{@Name}.PSDStatus" type="${type}" flavor="${flavor}">
	% if isdiff == 'Y':
		<xsl:attribute name="AlarmTimeOut">0</xsl:attribute>
	% endif
	% if isstate == 'Y':
	<States>
	% for name,equation in list:
     <Equation>${name}::${equation}</Equation>
	% endfor
	</States>
	% endif
	% if isdiff == 'Y':
	<DifferedAlarms>
		% for name,equation in diflist:
		 <Equation>${name}::${equation}</Equation>
		% endfor
	</DifferedAlarms>
	% endif
  </Equipment>
</%def>