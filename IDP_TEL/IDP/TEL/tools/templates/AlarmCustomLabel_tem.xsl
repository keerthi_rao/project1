<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarming.S2KAlarmServerSettings" %>

<%block name="classes">
	<Class name="S2KAlarming.S2KAlarmServerSettings">
	     <Objects>
			<Object name="AlarmCustomLabel" rules="update_or_create">
				<Properties>
						<Property name="Identifier" dt="string">AlarmCustomLabel</Property>
						${multilingualvalue("'Date/Time'","AlarmTimeOfTransition")}
						${multilingualvalue("'Equipment ID'","AlarmCustomLabel01")}
						${multilingualvalue("'T'","AlarmCustomLabel02")}
						${multilingualvalue("'Operator'","AlarmCustomLabel03")}
						${multilingualvalue("'A'","AlarmCustomLabel04")}
						${multilingualvalue("'Value'","AlarmCustomLabel05")}
						${multilingualvalue("'Description'","AlarmLabel")}
						${multilingualvalue("'M'","AlarmUserName")}
						${multilingualvalue("'Area Group'","AlarmAreaName")}
						${multilingualvalue("'Function Group'","AlarmGroupName")}
						${multilingualvalue("'Description'","EventMessage")}
						${multilingualvalue("'Function Group'","EventGroupName")}
						${multilingualvalue("'M'","EventUserName")}
						${multilingualvalue("'Area Group'","EventAreaName")}
						${multilingualvalue("'Sev'","AlarmSeverity")}
						${multilingualvalue("'Sev'","EventSeverity")}	
						${multilingualvalue("'Date/Time'","EventTimestamp")}	
				</Properties>
			</Object>
		 </Objects>
	</Class>
</%block>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>