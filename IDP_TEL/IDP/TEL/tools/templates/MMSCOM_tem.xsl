<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  
<xsl:param name="DATE"/>
<xsl:param name="MACHINE_NAME"/>
<xsl:param name="SIG_AREA_TYPE"/>

<xsl:template match="/">
	<ROOT>
		<MMSCOM>
			<xsl:variable name="sig" select="//ATS_Equipment[@Name=$MACHINE_NAME]"/>
			<xsl:variable name="ip1var" select="//ATS_Equipment[$sig/Signalisation_Area_ID=Signalisation_Area_ID and ATS_Equipment_Type='Gateway']"/>
			<Location Name="OCC" IP1="{$ip1var[1]/IP_Address_Light_Grey}" IP2="{$ip1var[2]/IP_Address_Light_Grey}" Port="8080"/>
			<Location Name="IOCC" IP1="{$ip1var[1]/IP_Address_Light_Grey}" IP2="{$ip1var[2]/IP_Address_Light_Grey}" Port="8080"/>
		</MMSCOM>
	</ROOT>
</xsl:template>
</xsl:stylesheet>
