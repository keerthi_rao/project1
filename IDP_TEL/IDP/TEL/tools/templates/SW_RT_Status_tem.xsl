<%inherit file="/Object_tem.mako"/>
<%! class_ = "SW_RT_Status" %>
<xsl:param name="AREA_TYPE"/>

<xsl:variable name="servers" select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="servers_noDATS" select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID and not(contains(Localisation_Type,'Depot'))]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="locs" select="distinct-values((for $i in (//Signalisation_Area[@ID=$servers_noDATS]/sys:geo/@name) return (substring-after($i,'ISCS_'))))"/>
		 	 
<%block name="classes">
	<Class name="SW_RT_Status">
		<Objects>
			<% mms_type_list=['Switch','Router','Server','Firewall','GTW','WKS HMI','WKS SIMU','WKS DEV','WKS Overview','Printer','Datalogger','FEP','Simulator Computer'] %>
			%for typename in mms_type_list:
				<xsl:apply-templates select="//MSS_lruState/lruState[Type='${typename}' and Signalisation_Area_ID=$servers]"/>				
			%endfor
				<xsl:apply-templates select="//MSS_lruState/lruState[Type='Firewall' and Signalisation_Area_ID=$SERVER_ID]" mode="firewall"/>
				<xsl:apply-templates select="//SMIO[Signalisation_Area_ID=$servers]"/>
				<xsl:apply-templates select="//MSS_lruState/lruState[(Type='GTW' or Type='Server') and Signalisation_Area_ID=$servers]" mode="Power"/>
			<xsl:for-each select="//Signalisation_Area[@ID=$servers and (contains(Localisation_Type,'Depot') or Area_Type='ATS_Central_Server')]">
				<xsl:apply-templates select="//PDC[(contains(Equipment,MDD) or contains(Equipment,OCC)) and not(contains(Equipment,';'))]"><xsl:with-param name="sid" select="@ID"/></xsl:apply-templates>
			</xsl:for-each>
			<xsl:apply-templates select="//Train_Unit[number(@ID) &lt;= 30]"><xsl:with-param name="servers" select="$servers"/></xsl:apply-templates>
			<xsl:apply-templates select="//CBI[@ID=//Signalisation_Area[@ID=$servers_noDATS]/sys:cbi/@id]"/>
			<xsl:for-each select="//Signalisation_Area[@ID=$servers_noDATS and not(contains(Localisation_Type,'Depot'))]">
				<xsl:apply-templates select="//PDC[contains(Equipment,';')]" mode="TE"><xsl:with-param name="sid" select="@ID"/></xsl:apply-templates>
			</xsl:for-each>
		</Objects>
	</Class>
</%block>

<% scu_list=[('.OverTemperature_Status','1lruState','9lruState'),('.LRU_Status','6lruState','16lruState'),('.MMILRU_Status','2lruState','10lruState'),('.UpperFAN_Status','3lruState','11lruState'),('.LowerFAN_Status','4lruState','12lruState')] %>

<xsl:template match="CBI">
	<xsl:variable name="cbinm" select="@Name"/>
	%for suff,v1,v2 in scu_list:
		%for dir in ['.SCULeft','.SCURight']:
			<xsl:variable name="objnm" select="concat($cbinm,'${dir}','${suff}')"/>
			<xsl:variable name="add_suff" select="if ('${dir}'='.SCULeft') then '${v1}' else '${v2}'"/>
			<xsl:variable name="address" select="concat('[3:WEB_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:',$cbinm,'&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',$add_suff)"/>
			<Object name="{$objnm}" rules="update_or_create">
				<Properties>
					<Property name="SOI_OPCClient" dt="string">Connect_SNMP_Webprotocol_<xsl:value-of select="$SERVER_ID"/></Property>
					<Property name="AdressInLevel1" dt="string"><xsl:value-of select="$address"/></Property>
				</Properties>
			</Object>
		%endfor
	%endfor
</xsl:template>

<xsl:template match="Train_Unit">
	<xsl:param name="servers"/>
	<xsl:variable name="tid" select="format-number(@ID,'000')"/>
	<xsl:variable name="train" select="concat('Train',$tid)"/>
	<xsl:for-each select="//CC[Type='Train_Alarm']">
		<xsl:variable name="cc" select="."/>
		<xsl:for-each select="$servers">
			<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:CC_PV',$tid,'&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',$cc/RMCID,'rmcValue')"/>
			<Object name="{$train}{$cc/@Name}.{current()}" rules="update_or_create">
			  <Properties>
				<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',.)"/></Property>	
				${prop('AdressInLevel1','<xsl:value-of select="$AdrLv1"/>')}
			  </Properties>
			</Object>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="PDC">
	<xsl:param name="sid"/>
	<xsl:variable name="tt" select="@Name"/>
	<xsl:variable name="eqp" select="Equipment"/>
		<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:barionet&lt;Organizes&gt;3:',$eqp,'&lt;Organizes&gt;3:barionet&lt;Organizes&gt;3:bariInputTable&lt;Organizes&gt;3:bariInputEntry&lt;HasComponent&gt;3:',ID_Baril,'bariInputValue')"/>
		<Object name="{$tt}" rules="update_or_create">
		  <Properties>
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
			${prop('AdressInLevel1','<xsl:value-of select="$AdrLv1"/>')}
		  </Properties>
	  </Object>
</xsl:template>

<xsl:template match="PDC" mode="TE">
	<xsl:param name="sid"/>
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="name_pdc" select="@Name"/>
	<xsl:variable name="ID_Baril" select="ID_Baril"/>
	<xsl:for-each select="tokenize(Equipment,';')">
		<xsl:variable name="eqp" select="."/>
		<xsl:variable name="cond" select="if (tokenize($eqp,'_')[1]=$locs) then true() else false()"/>
		<xsl:variable name="tt10" select="concat($name_pdc,'_',substring-before($eqp,'_'))"/>
		<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:barionet&lt;Organizes&gt;3:',$eqp,'&lt;Organizes&gt;3:barionet&lt;Organizes&gt;3:bariInputTable&lt;Organizes&gt;3:bariInputEntry&lt;HasComponent&gt;3:',$ID_Baril,'bariInputValue')"/>
		<xsl:if test="$cond">
		<Object name="{$tt10}" rules="update_or_create">
			<Properties>
				<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
				${prop('AdressInLevel1','<xsl:value-of select="$AdrLv1"/>')}
			</Properties>
		</Object>
		</xsl:if>		
	</xsl:for-each>
</xsl:template>

<xsl:template match="SMIO">
	<xsl:variable name="nme" select="@Name"/>
	<xsl:variable name="sid" select="Signalisation_Area_ID"/>
	%for i in range(1,48):
		<xsl:variable name="tt3" select="concat(@Name,'.Status.LRU',${i},'Failed')"/>
		<xsl:variable name="tt4" select="concat(@Name,'.Status.LRU',${i},'Descr')"/>
		

		<xsl:variable name="AdrLv12" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:',$nme,'&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',${i},'lruDescr')"/>
		<xsl:variable name="AdrLv11" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:',$nme,'&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',${i},'lruState')"/>
		
		<Object name="{$tt3}" rules="update_or_create">
			<Properties>
				<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
				${prop('AdressInLevel1','<xsl:value-of select="$AdrLv11"/>')}
				
			</Properties>
		</Object>
		<Object name="{$tt4}" rules="update_or_create">
			<Properties>
				<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
				${prop('AdressInLevel1','<xsl:value-of select="$AdrLv12"/>')}
			</Properties>
		</Object>		
	%endfor
</xsl:template>

<xsl:template match="lruState">
	<xsl:variable name="tt" select="concat(@Name,'.Status')"/>
	<xsl:variable name="sid" select="Signalisation_Area_ID"/>
	<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:Local&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',@ID,'lruState')"/>
		<Object name="{$tt}" rules="update_or_create">
		  <Properties>
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
			${prop('AdressInLevel1','<xsl:value-of select="$AdrLv1"/>')}
		  </Properties>
	  </Object>
</xsl:template>

<xsl:template match="lruState" mode="Power">
	<xsl:variable name="tt" select="if(Type='Server') then (concat(@Name,'.Status_Power_Supply.PV_Status')) else if(Type='GTW') then (@Name,'.Status_Over_Temp.PV_Status') else null"/>
	<xsl:variable name="sid" select="Signalisation_Area_ID"/>
	<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:cpqhlth&lt;Organizes&gt;3:',@Name,'&lt;Organizes&gt;3:cpqHealth&lt;Organizes&gt;3:cpqHeComponent&lt;Organizes&gt;3:')"/>
	<xsl:variable name="Adr2" select="if(Type='Server') then ('cpqHealth&lt;Organizes&gt;3:cpqHeComponent&lt;Organizes&gt;3:cpqHeFltTolPwrSupply&lt;HasComponent&gt;3:cpqHeFltTolPwrSupplyCondition') else if(Type='GTW') then ('cpqHeThermal&lt;HasComponent&gt;3:cpqHeThermalTempStatus') else null"/>
		<Object name="{$tt}" rules="update_or_create">
		  <Properties>
			<Property name="SOI_OPCClient" dt="string"><xsl:value-of select="concat('Connect_SNMP_',$sid)"/></Property>	
			<Property name="AdressInLevel1" dt="string"><xsl:value-of select="concat($AdrLv1,$Adr2)"/></Property>	
		  </Properties>
	  </Object>
</xsl:template>

<xsl:template match="lruState" mode="firewall">
	<xsl:variable name="sig" select="Signalisation_Area_ID"/>
	<xsl:variable name="svrtype" select="if (Area_Type='ATS_Central_Server') then ('Central Server') else if (Area_Type='ATS_Local_Server') then ('Local Server') else null"/>
	<xsl:variable name="name_end" select="substring-after(@Name,'FRW')"/>
	<xsl:variable name="atseq" select="//ATS_Equipment[contains(ATS_Equipment_Type,$svrtype) and ends-with(@Name,$name_end) and (Signalisation_Area_ID=$sig or */Signalisation_Area_ID=$sig)]"/>
	<xsl:variable name="tt" select="concat($atseq/@Name,'.Status.Firewall_LRU_Failed.',@Name)"/>
	<xsl:variable name="AdrLv1" select="concat('[3:SNMP_OPC_GTW_NS_3]&lt;Organizes&gt;3:URBALIS&lt;Organizes&gt;3:Local&lt;Organizes&gt;3:lru&lt;Organizes&gt;3:lruTable&lt;Organizes&gt;3:lruEntry&lt;HasComponent&gt;3:',@ID,'lruState')"/>
	<xsl:variable name="SOI_Client" select="concat('Connect_SNMP_',Signalisation_Area_ID)"/>
	<xsl:if test="$atseq">
	  <Object name="{$tt}" rules="update_or_create">
		  <Properties>
				${prop('SOI_OPCClient','<xsl:value-of select="$SOI_Client"/>')}
				${prop('AdressInLevel1','<xsl:value-of select="$AdrLv1"/>')}
		  </Properties>
	  </Object>
	</xsl:if>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>

<%def name="mulval(value_nm,value_Var)">
           <MultiLingualProperty name="${value_nm}">
			<xsl:for-each select="$mlv">
             <MultiLingualValue roleId="{../@RID}" localeId="{@LCID}"><xsl:value-of select="${value_Var}"/></MultiLingualValue>
			</xsl:for-each>
           </MultiLingualProperty>
</%def>
