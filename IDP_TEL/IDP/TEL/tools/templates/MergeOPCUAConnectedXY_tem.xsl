<%inherit file="/Object_tem.mako"/>
<%! class_ = "MergeOPCUAConnectedXY" %>

<%block name="classes">

	<xsl:variable name="filegeneration" select="if (//Signalisation_Area[@ID=$SERVER_ID and not(contains(Localisation_Type,'Depot'))]/Area_Type='ATS')  then 'Yes' else 'No'"/>
		<xsl:if test="$filegeneration = 'No'">
			<xsl:result-document href="MergeOPCUAConnectedXY_{$FUNCTION_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
		</xsl:if>
	<Class name="MergeOPCUAConnectedXY">
		<Objects>
			<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID and @Signalisation_Area_ID=//Signalisation_Area[not(Localisation_Type='Depot')]/@ID]"/>	
		</Objects>
	</Class>
</%block>	
	
<xsl:template match="Function">					
	<Object name="PSDOPCClient_PSDSERVER_{$FUNCTION_ID}.MergeConnectedXY.Merge.IntOutpoutPlug_1" rules="update_or_create">
		<Properties>
			<Property name="SharedOnIdentifierOPCUAClient" dt="string">PSDOPCClient_PSDSERVER_<xsl:value-of select="$FUNCTION_ID"/></Property>
		</Properties>
	</Object>
</xsl:template>

