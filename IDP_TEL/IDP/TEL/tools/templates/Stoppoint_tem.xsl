<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>                 
  <xsl:template match="/">
    <xsl:apply-templates select="/docs/ATS_SYSTEM_PARAMETERS"/>
  </xsl:template>
  <xsl:template match="Stopping_Area" mode="sta">
	<xsl:variable name="blocks" select="sys:blockid/@id"/>
	<xsl:variable name="bcnt" select="count(sys:blockid)"/>
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="coa" select="Original_Area_List/Original_Area/@Original_Area_Type"/>
	<xsl:variable name="ooa" select="//Stopping_Area[not(@ID =$id) and (every $bid in $blocks satisfies $bid = sys:blockid/@id)]/Original_Area_List/Original_Area/@Original_Area_Type"/>
	<xsl:choose>
		<xsl:when test="$ooa">
		  <sys:stoppoint><xsl:value-of select="if($coa = 'Platform') then 'yes' else if($ooa = 'Platform') then 'no' else if($coa='Stabling') then 'yes' else if($ooa='Stabling') then 'no' else if($coa='Transfer_Track') then 'yes' else if ($ooa='Transfer_Track') then 'no' else if ($coa='Washing') then 'yes' else if($ooa='Washing') then 'no' else if ($coa='Isolation_Area') then 'yes' else if($ooa='Isolation_Area') then 'no'else if($coa = 'Coupling_Uncoupling') then 'yes' else if($ooa = 'Coupling_Uncoupling') then 'no' else if($coa = 'Change_Of_Direction') then 'yes' else 'no'"/></sys:stoppoint> 
		</xsl:when>
		<xsl:otherwise>
			<sys:stoppoint>yes</sys:stoppoint> 
		</xsl:otherwise>
	</xsl:choose>
  </xsl:template>
  
    <xsl:template match="@* | node()"  mode="sta"/>
    <xsl:template match="@* | node()">
        <xsl:copy>
           <xsl:apply-templates select="@* | node()"/>
           <xsl:apply-templates select="." mode="sta"/>
	    </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
