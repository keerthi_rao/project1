<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:param name="DATE"/>
<xsl:param name="SERVER_ID"/>
<xsl:template match="/">
	<PIMParams Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="PIMPlatforms.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<Platforms>
			<xsl:apply-templates select="//PIMPlatform[PlatformID=//Platform[@ID=//Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']/Original_Area_List/Original_Area/@ID]/@ID]"/>
		</Platforms>
		<Sectors>
			<xsl:apply-templates select="//Signalisation_Area[Area_Type='ATS_Local_Server' and @ID=$SERVER_ID]"/>
		</Sectors>
	</PIMParams>
</xsl:template>
<xsl:template match="PIMPlatform">
	<xsl:variable name="pid" select="if(DefaultDirection='1') then '2' else if(DefaultDirection='2') then '1' else null"/>
	<Platform Name="{@Name}" Station_Id="{StationID}" Platform_Id="{$pid}" LeftInterstation="{if(LeftInterstation and LeftInterstation !='') then concat('PIM_', StationID, '_', PlatformID, '_Left') else NULL}" RightInterstation="{if(RightInterstation and RightInterstation != '') then concat('PIM_', StationID, '_', PlatformID, '_Right') else NULL}">
		<xsl:apply-templates select="LeftInterstation/Block_ID"><xsl:with-param name="lblock" select="LeftInterstation/Block_ID[last()]"/><xsl:with-param name="fblock" select="LeftInterstation/Block_ID[1]"/></xsl:apply-templates>
		<xsl:apply-templates select="RightInterstation/Block_ID"><xsl:with-param name="lblock" select="RightInterstation/Block_ID[last()]"/><xsl:with-param name="fblock" select="RightInterstation/Block_ID[1]"/></xsl:apply-templates>
	</Platform>
</xsl:template>
<xsl:template match="Signalisation_Area">
	<Sector ObjectName="{concat('LATSStatus_',@ID, '.PV')}">
		<xsl:apply-templates select="//PIMStation[Signalisation_Area=$SERVER_ID]"/>
	</Sector>
</xsl:template>
<xsl:template match="PIMStation">
	<Station Name="{@Name}"/>
</xsl:template>

<xsl:template match="Block_ID">
	<xsl:param name="lblock"/>
	<xsl:param name="fblock"/>
	<xsl:variable name="kp1" select="number(//Block[@ID=$lblock]/Kp_End)-number(//Block[@ID=$fblock]/Kp_Begin)"/>
	<xsl:variable name="kp2" select="number(//Block[@ID=current()]/Kp_Begin)-number(//Block[@ID=$fblock]/Kp_Begin)"/>
	<xsl:variable name="kp3" select="number(//Block[@ID=current()]/Kp_End)-number(//Block[@ID=$fblock]/Kp_Begin)"/>
	
	<xsl:variable name="strtperc" select="(number($kp2) div number($kp1))*100"/>
	<xsl:variable name="endperc" select="(number($kp3) div number($kp1))*100"/>
	<TrackPortion Name="{concat('TI_', //Block[@ID=current()/text()][1]/@Name, '.TrackPortion')}" StartPercentage="{format-number($strtperc, '00.00')}" EndPercentage="{format-number($endperc,'00.00')}"/>
</xsl:template>

</xsl:stylesheet>
