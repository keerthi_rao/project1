<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
		<Class name="S2KAlarms_Gen">
			<Objects>
				<xsl:apply-templates select="//ATS_Equipment[ATS_Equipment_Type='Maintenance Server' and (Function_ID_List/Function_ID=$FUNCTION_ID or Signalisation_Area_ID=$SERVER_ID)]"/>
			</Objects>
		</Class>
</%block>


<xsl:template match="ATS_Equipment">
<xsl:variable name="nm" select="@Name"/>
	<xsl:for-each select="//Urbalis_Sector[ATS_Area_ID_List/Signalisation_Area_ID!='']">
		<xsl:variable name="tt1" select="concat($nm,'.FEP_',current()/@Name,'.TotalCom_Failure')"/>
		<xsl:variable name="Name_PV" select="concat($nm,'.FEP_',current()/@Name)"/>
		<xsl:variable name="nme" select="concat($nm,'.FEP_',current()/@Name)"/>
		<xsl:variable name="plug" select="if (ends-with($nm,'A')) then 'A' else 'B'"/>
		<xsl:variable name="uname" select="concat(substring-after(current()/@Name,'_'),'/SIG/ATS/MS_SRV_',$plug)"/>
	


		${obj('{$nm}',[('.TotalCom_Failure','Sector %s1: OCC MS Server %s2 - FEP Total Communication Failure','.TotalCom','1'),
						('.Alarm_LinkA','Sector %s1: OCC MS Server %s2 - FEP %s3 Link Failure','.LinkA','2'),
						('.Alarm_LinkB','Sector %s1: OCC MS Server %s2 - FEP %s3 Link Failure','.LinkB','2')])}


	</xsl:for-each>
</xsl:template>

<%def name="obj(nm,list)">
	% for key, alarm_labl,nmpv,svty in list:
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>
				<xsl:variable name="bstr3" select="'${key}'"/>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$nme','Name')}
				${multilingualvalue('$uname','EquipmentID')}
				${multilingualvalue("'B'",'OMB')}
				${multilingualvalue("'Fault'",'Value_on')}
				${multilingualvalue("'Normal'",'Value_off')}
				<Property name="Alarm_Label" dt="string">${alarm_labl}</Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="current()/@Name"/></Property>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="concat('MS_',$plug)"/></Property>
				<xsl:if test="contains($bstr3,'_Link')"><Property name="BstrParam3" dt="string"><xsl:value-of select="concat('FEP_',$plug)"/></Property></xsl:if>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$Name_PV"/>${nmpv}</Property>
				<Property name="Severity" dt="i4">${svty}</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
			</Properties>
		</Object>
	% endfor
</%def>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

