<%inherit file="/Object_tem.mako"/>
<%! 
    class_ = "SAHMEmerStopPointLink"
    exclude_result_prefixes = False
%>
<%block name="classes">
	<Class name="SAHMEmerStopPointLink">
		 <Objects>
			<xsl:apply-templates select="$sysdb//Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']"/>
		 </Objects>
	</Class>
</%block>
	
<xsl:template match="Stopping_Area">
	<Object name="SAHM_EmerStopPointLink_{@Name}" rules="update_or_create">
		<Properties>
			<Property name="GenericAreaID" dt="string"><xsl:value-of select="@Name"/></Property>
		</Properties>
	</Object>
</xsl:template>