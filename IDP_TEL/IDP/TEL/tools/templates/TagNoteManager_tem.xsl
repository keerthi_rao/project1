<%inherit file="/Object_tem.mako"/>
<%! class_ = "TagNoteManager" %>

<%block name="classes">
	<Class name="TagNoteManager">
		<Objects>
			%for eqip in ["Secondary_Detection_Device","Block","Point","Signal","Evacuation_Zone_Sec","Evacuation_Zone_Req","GAMA_Zone"]:
				<xsl:apply-templates select="//${eqip}[sys:sigarea/@id=$SERVER_ID]"/>
			%endfor
				<xsl:apply-templates select="$sysdb//Train_Unit"/>
				<xsl:apply-templates select="$sysdb//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id=$SERVER_ID]/@ID]/@ID]/@ID]"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Secondary_Detection_Device|Block|Point|Signal|Evacuation_Zone_Sec|Evacuation_Zone_Req|GAMA_Zone">
	<Object name="{@Name}.NoteTag" rules="update_or_create">
	  <Properties>
		${prop('PERFilePath','D:\DataPrep\Note\PersistenceFile.txt')}
	  </Properties>
	</Object>
</xsl:template>

<xsl:template match="Train_Unit">
	<xsl:variable name="nm" select="concat('Train', format-number(@ID, '000'))"/>
	<Object name="{$nm}.NoteTag" rules="update_or_create">
	  <Properties>
		${prop('PERFilePath','D:\DataPrep\Note\PersistenceFile.txt')}
	  </Properties>
	</Object>
</xsl:template> 

<xsl:template match="LX">
	<Object name="{@Name}.NoteTag" rules="update_or_create">
	  <Properties>
		${prop('PERFilePath','D:\DataPrep\Note\PersistenceFile.txt')}
	  </Properties>
	</Object>
</xsl:template> 

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>
