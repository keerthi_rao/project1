<%inherit file="/Object_tem.mako"/>
<%! class_ = "Interlocking" %>

<%block name="classes">
	<Class name="Interlocking">
		<Objects>
			<xsl:apply-templates select="$sysdb//CBI[@ID=$CBI_ID]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="CBI">
	<xsl:variable name="CBIID" select="@ID"/>
	<xsl:variable name="nm" select="$sysdb//CBI[@ID=$CBIID]/@Name"/>
	<xsl:variable name="uid" select="//Urbalis_Sector[@ID=//CBI[@ID=$CBIID]/Urbalis_Sector_ID]/@ID"/>
	<xsl:variable name="omsa" select="if($uid='1' or $uid='2')
										then (concat('Sector ','SEC_MDD_',$uid,':Depot ATS FEP','- CBIs_',@ID ,' Total Communication Failure'))
									  else if($uid='17' or $uid='18')
										then (concat('Sector ','SEC_ECID_',$uid,':Local ATS FEP','- CBIs_',@ID ,' Total Communication Failure'))
									  else(concat('Sector ','SEC_',$uid,':Local ATS FEP','- CBIs_',@ID ,' Total Communication Failure'))"/>
	<xsl:variable name="cl01" select="if($uid='1' or $uid='2') then (concat('MDD_',$uid,'/SIG/ATS'))
										else if($uid='17' or $uid='18') then (concat('ECID_',$uid,'/SIG/ATS'))
										else (concat('TE',$uid,'/SIG/ATS'))"></xsl:variable>	
	<xsl:variable name="CustomLabel02"><xsl:value-of select="'B'"/></xsl:variable>													
		<Object name="{concat('CBIS_',$CBIID)}" rules="update_or_create">
			<Properties>
				${multilingualvalue("$omsa","OperatingModeStateAlarmLabel")}
				${multilingualvalue("$cl01","CustomLabel01")}
				${multilingualvalue("$CustomLabel02","CustomLabel02")}
			</Properties>
		</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

