<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "CATSStatus"
%>
<%block name="classes">
	<Class name="CATSStatus">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signalisation_Area[Area_Type='ATS_Central_Server' and sys:sigarea/@id=$SERVER_ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area">
	<Object name="{concat('CATSStatus_',@ID)}" rules="update_or_create">
		<Properties>
			<Property name="OPCClientRef" dt="string"><xsl:value-of select="concat('Split_',$SERVER_ID)"/></Property>
			<Property name="OPCTag" dt="string"><xsl:value-of select="concat('OTHERATSTEL.ATS_',@ID,'.State.iEqpState')"/></Property>
		</Properties>
	</Object>
</xsl:template>
