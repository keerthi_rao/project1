<%inherit file="/Object_tem.mako"/>
<%! class_ = "TELLevelCrossing" %>

<%block name="classes">
	<Class name="TELLevelCrossing">
	     <Objects>
		    <xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:cbi/@id = $CBI_ID]/@ID]/@ID]/@ID]"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="LX">
	  <xsl:variable name="nm" select="@Name"/>
	  <xsl:variable name="vv" select="if(//Generic_ATS_IO/@Name=replace($nm,substring-before(@Name, '_'),'GAIO_LXGB')) then ('1') else ('0')"/>
	  <Object name="{@Name}.TEL" rules="update_or_create">
		  <Properties>
				<Property name="OverriddenPresence" dt="i4"><xsl:value-of select="$vv"/></Property>
				${multilingualvalue('$nm','Name')}
		 </Properties>
	  </Object>	
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>





