<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
		<xsl:apply-templates select="//LX[contains(@Name,'LX3') and LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]"/>
</%block>

<xsl:template match="LX">

	% for nm, var1, var2, var3 in [('LXAI', 'LA','RA','_Lamp_Side_A'),('LXAI', 'LB','RB','_Lamp_Side_B'),('LXLP','LA','RA','_Lamp_Side_A'),('LXLP','LB','RB','_Lamp_Side_B')]:	
		  <xsl:variable name="pv1" select="//LX_Device[(LX_ID=current()/@ID) and (Type='Aspect Indicator') and Signal_Lamp_Type_ID = //Signal_Lamp_Type[not(Proved_Lamp_List/Lamp_Name='None')]/@ID and contains(@Name, '${var1}')]"/>
		  <xsl:variable name="pv2" select="//LX_Device[(LX_ID=current()/@ID) and (Type='Aspect Indicator') and Signal_Lamp_Type_ID = //Signal_Lamp_Type[not(Proved_Lamp_List/Lamp_Name='None')]/@ID and contains(@Name, '${var2}')]"/>
		 <xsl:variable name="name" select="concat(@Name,'.' ,'${nm}','${var3}')"/>
		  	<Equipment name="{$name}" type="U400Equipment" flavor="LXLampSide">
				<States>
					<Equation>LX_SIDE_OFF[0]:: <xsl:if test="$pv1">${nm}_<xsl:value-of select="$pv1/@Name"/>==0</xsl:if><xsl:if test="$pv2"><xsl:if test="$pv1"> &amp;&amp; </xsl:if>${nm}_<xsl:value-of select="$pv2/@Name"/>==0</xsl:if></Equation>
					<Equation>LX_SIDE_ON[1]:: <xsl:if test="$pv1">${nm}_<xsl:value-of select="$pv1/@Name"/>==1</xsl:if><xsl:if test="$pv2"><xsl:if test="$pv1"> &amp;&amp; </xsl:if>${nm}_<xsl:value-of select="$pv2/@Name"/>==1</xsl:if></Equation>
					<Equation>LX_SIDE_UNKNOWN[2]::1</Equation>
				</States>
			</Equipment>
	% endfor	  
</xsl:template>
