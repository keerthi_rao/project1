<%inherit file="/Group_tem.mako"/>

<%block name="classes">
	<Class name="HMITrainAlarmMonitored" rules="update" traces="error">
		<Objects>
			<xsl:apply-templates select="//Train_Unit"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="tcid" select="Train_Unit_Characteristics_ID"/>
	<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
	<xsl:if test="number(@ID)&lt;=number('30')">
			<xsl:for-each select="//HMITrainAlarmMonitored/Alarm[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
				<Object name="{concat($TrainName,current()/@Name)}" rules="update" traces="error">
					<Properties>
						<Property name="UserGroup" dt="string"><xsl:value-of select="if(current()/MMS = 'A') then 'User/A' else if(current()/MMS = 'S') then 'User/S' else null"/></Property>				
						<Property name="AreaGroup" dt="string">Area/Trains/Train_<xsl:value-of select="format-number($id,'000')"/></Property>							
					</Properties>
				</Object>
			</xsl:for-each>
	</xsl:if>	
<!--FOR LOCO Alarms-->
	<xsl:for-each select="//HMITrainAlarmMonitored_Loco/Alarm_Loco[number(IDMin) &lt;= number($id) and number(IDMax) &gt;= number($id)]">
		<xsl:if test="//Train_Unit_Characteristics[@ID=$tcid]/@Name='TRUC_LOCO'"> 
				<Object name="{concat($TrainName,current()/@Name)}" rules="update" traces="error">
					<Properties>
						<Property name="UserGroup" dt="string"><xsl:value-of select="if(current()/MMS = 'A') then 'User/A' else if(current()/MMS = 'S') then 'User/S' else null"/></Property>				
						<Property name="AreaGroup" dt="string">Area/Trains/Train_<xsl:value-of select="format-number($id,'000')"/></Property>							
					</Properties>
				</Object>
		</xsl:if>	 		
	</xsl:for-each>	
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_group_tem.mako" import="group_object_template"/>