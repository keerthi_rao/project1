<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
  			<xsl:apply-templates select="//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID and (ATS_Equipment_Type ='Central Server' or ATS_Equipment_Type ='Local Server')]"/>
  		</Objects>
  	</Class>
</%block>

<xsl:param name="var" select="/docs/Alarms"/>

<xsl:template match="ATS_Equipment">
	<xsl:variable name="cobj" select="."/>
	<xsl:variable name="sigAreas" select="//Signalisation_Area[@ID=$SERVER_ID]"/>

	<xsl:variable name="adjma" select="//Adjacent_Monitoring_Area[ATS_Local_Server_Area_ID = $SERVER_ID or ATS_Central_Server_Area_ID = $SERVER_ID]"/>
	<xsl:variable name="adjSig" select="//Signalisation_Area[@ID=$adjma/sys:sigarea/@id or @ID=$sigAreas/*/Signalisation_Area_ID]"/>
	<xsl:apply-templates select="//ATS_Equipment[(*/Signalisation_Area_ID=$adjSig/@ID or Signalisation_Area_ID = $adjSig/@ID) and not(Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID = $SERVER_ID) and (ATS_Equipment_Type ='Central Server' or (ATS_Equipment_Type ='FEP' and $sigAreas/Localisation_Type='Mainline') or ATS_Equipment_Type ='Local Server')]" mode="child">
	<xsl:with-param name="cname" select = "@Name" />
	<xsl:with-param name="atstype" select = "ATS_Equipment_Type" />
	<xsl:with-param name="csigArea" select="$sigAreas"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="$adjSig[Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server' or (Area_Type='ATS' and Localisation_Type='Mainline')]" mode="child">
	<xsl:with-param name="cname" select = "@Name" />
	<xsl:with-param name="atstype" select = "$cobj" />
	<xsl:with-param name="csigArea" select="$sigAreas"/>
	</xsl:apply-templates>

</xsl:template>

<xsl:template match="Signalisation_Area" mode="child">
	<xsl:param name = "cname" />
	<xsl:param name="atstype" />
	<xsl:param name="csigArea" />
	
	<xsl:variable name="lsigArea" select="."/>
	<xsl:variable name="ctype" select="if(contains($csigArea/Localisation_Type, 'Depot')) then 'DATS' else (if($atstype/ATS_Equipment_Type='Central Server') then 'CATS' else if($atstype/ATS_Equipment_Type='Local Server') then 'LATS'  else 'FEP')"/>
	<xsl:variable name="ltype" select="if(current()/Localisation_Type='Mainline') then (if(Area_Type='ATS_Central_Server') then 'CATS' else if(Area_Type='ATS_Local_Server') then 'LATS'  else 'FEP') else 'DATS'"/>
	<xsl:if test="not($ctype='DATS' and Area_Type='ATS')">
		<xsl:variable name="bstrb2" select="//Urbalis_Sector[*/Signalisation_Area_ID=$SERVER_ID or */Signalisation_Area_ID=current()/@ID]"/>
		<xsl:variable name="bstrb1" select="if(($ctype='DATS' or ($ctype='CATS' and $ltype='DATS'))) then 'MDD' else if($ctype = 'CATS') then $bstrb2/@Name else $bstrb2[*/Signalisation_Area_ID=$SERVER_ID]/@Name"/>
		<xsl:variable name="lname" select="if($ltype='LATS' or $ltype='FEP') then concat($ltype, '_',$bstrb2[*/Signalisation_Area_ID=current()/@ID]/@Name) else if($ltype='DATS') then concat($ltype,replace(@Name, 'SIA_DATS', '')) else $ltype"/>
		<xsl:variable name="nm" select="concat($cname, '.Alarm_TotalComFail.',$lname)"/>
		<xsl:variable name="link" select="concat($ctype, '_',$ltype)"/>
		<xsl:variable name="Out_PV_Trig" select="if(ends-with($cname,'A')) then 'IntOutputPlug_1' else 'IntOutputPlug_2'"/>
		<xsl:variable name="lastParam" select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/>
		${obj('Alarm_TotalComFail','1')}
	</xsl:if>
</xsl:template>

<xsl:template match="ATS_Equipment" mode="child">
	<xsl:param name = "cname" />
	<xsl:param name="atstype" />
	<xsl:param name="csigArea" />
	<xsl:variable name="lsigArea" select="//Signalisation_Area[@ID=current()/*/Signalisation_Area_ID or @ID=current()/Signalisation_Area_ID]"/>
	
	<xsl:variable name="ctype" select="if(contains($csigArea/Localisation_Type, 'Depot')) then 'DATS' else (if($atstype='Central Server') then 'CATS' else if($atstype='Local Server') then 'LATS'  else 'FEP')"/>
	<xsl:variable name="ltype" select="if($lsigArea/Localisation_Type='Mainline') then (if(ATS_Equipment_Type='Central Server') then 'CATS' else if(ATS_Equipment_Type='Local Server') then 'LATS'  else 'FEP') else 'DATS'"/>
	<xsl:if test="not($ltype='DATS' and ATS_Equipment_Type='FEP')">
		<xsl:variable name="bstrb2" select="//Urbalis_Sector[*/Signalisation_Area_ID=$SERVER_ID or */Signalisation_Area_ID=$lsigArea/@ID]"/>
		<xsl:variable name="bstrb1" select="if($ctype='DATS' or ($ctype='CATS' and $ltype='DATS')) then 'MDD' else if($ctype = 'CATS') then $bstrb2/@Name else $bstrb2[*/Signalisation_Area_ID=$SERVER_ID]/@Name"/>
		<xsl:variable name="nm" select="concat($cname, '.Alarm_Link.',@Name)"/>
		<xsl:variable name="link" select="concat($ctype, '_',$ltype)"/>
		<xsl:variable name="Out_PV_Trig" select="if(ends-with($cname,'A')) then (if(ends-with(@Name,'A')) then 'Connected11' else 'Connected12') else (if(ends-with(@Name,'A')) then 'Connected21' else 'Connected22')"/>
		<xsl:variable name="lastParam" select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/>
		${obj('Alarm_Link','2')}
	</xsl:if>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>

<%def name="obj(typ,severity)">
	<xsl:variable name="alarms" select="$var/Alarm[@variable=$link and @type='${typ}']"/>
	<xsl:variable name="fep_sig" select="//Signalisation_Area[Area_Type='ATS' and @ID=//Signalisation_Area[@ID=$SERVER_ID]/sys:sigarea/@id]/@ID"/>
	<xsl:variable name="cond" select="if ($alarms[@variable='LATS_FEP' and @type='Alarm_Link']) then (if (//ATS_Equipment[@Name=current()/@Name and (Signalisation_Area_ID=$fep_sig or */Signalisation_Area_ID=$fep_sig)]) then true() else false()) else true()"/>
	<xsl:if test="$cond">
	<Object name="{$nm}" rules="update_or_create">
		<Properties>
			<xsl:variable name="varCnt" select="count(tokenize($alarms/@message,'%s'))-1"/>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarms/@message"/></Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrb1"/></Property>
			<xsl:variable name="troom" select="//Technical_Room[@ID=//ATS_Equipment[@Name=$cname]/Technical_Room_ID]"/>
			<xsl:variable name="loc" select="substring-after($troom/@Name,'SER_')"/>
			<xsl:choose>
			  <xsl:when test="$varCnt = 4">
				<Property name="BstrParam2" dt="string"><xsl:value-of select="if($ctype='LATS' and $ltype='LATS') then $bstrb2[*/Signalisation_Area_ID=$lsigArea/@ID]/@Name else if($ltype='DATS') then 'MDD' else $bstrb2/@Name"/></Property>
				<Property name="BstrParam3" dt="string"><xsl:value-of select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/></Property>
				<Property name="BstrParam4" dt="string"><xsl:value-of select="if(ends-with(@Name,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/></Property>
			  </xsl:when>
			  <xsl:when test="$varCnt = 2">
					<Property name="BstrParam2" dt="string"><xsl:value-of select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/></Property>
			  </xsl:when>
			  <xsl:otherwise>
				<xsl:variable name="bsparam2" select="if(ends-with($cname,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B'"/>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="if('${typ}' = 'Alarm_TotalComFail') then (if($ltype='DATS') then 'MDD' else $bstrb2[*/Signalisation_Area_ID=current()/@ID]/@Name) else $bsparam2"/></Property>
				<xsl:variable name="fep" select="if(ends-with(@Name,'A')) then 'A' else 'B'"/>
				<Property name="BstrParam3" dt="string"><xsl:value-of select="if (contains($alarms/@message,'FEP %s3')) then ($fep) else (if('${typ}' = 'Alarm_TotalComFail') then $bsparam2 else if(ends-with(@Name,'A')) then 'SRV_ATS_A' else 'SRV_ATS_B')"/></Property>
			  </xsl:otherwise>
			</xsl:choose>
			<xsl:variable name="typa" select="if('${typ}' = 'Alarm_TotalComFail') then '.MergeConnected12Y' else ''"/>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="if($ltype='FEP') then concat('OPCClient_CBIS_', $lsigArea/sys:cbi[1]/@id,$typa) else concat('OPCClient_ATS_', $lsigArea[1]/@ID,$typa)"/></Property>				
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$Out_PV_Trig"/></Property>
			<Property name="Severity" dt="i4">${severity}</Property>
			<Property name="State_PV_Trig" dt="i4">0</Property>
			<xsl:variable name="nm1" select="$alarms/@Name"/>
			${multilingualvalue('$nm1','Name')}
			<xsl:variable name="EquipmentID" select="concat((if($ctype ='DATS' or $ctype ='CATS') then 'MDD' else if($ctype ='LATS') then $loc else 'ECID'),'/SIG/ATS/',(if($ctype='LATS') then '' else concat($ctype, '_')),$lastParam)"/>
			${multilingualvalue('$EquipmentID','EquipmentID')}
			<xsl:variable name="OMB" select="'B'"/>
			${multilingualvalue('$OMB','OMB')}
			<xsl:variable name="valon" select="'Fault'"/>
			${multilingualvalue('$valon','Value_on')}
			<xsl:variable name="valoff" select="'Normal'"/>
			${multilingualvalue('$valoff','Value_off')}	
			<xsl:variable name="avalanche" select="'N'"/>
			${multilingualvalue('$avalanche','Avalanche')}
		</Properties>
	</Object>
	</xsl:if>
</%def>