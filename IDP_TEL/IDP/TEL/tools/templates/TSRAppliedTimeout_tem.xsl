<%inherit file="/Object_tem.mako"/>
<%! 
    class_ = "TSRAppliedTimeout"
    exclude_result_prefixes = False
%>
<xsl:param name="URBALIS_ID"/>
<%block name="classes">
	<Class name="TSRAppliedTimeout">
		 <Objects>
			<xsl:apply-templates select="//Blocks/Block[Track_ID=//Urbalis_Sector[@ID=$URBALIS_ID]/Track_ID_List/Track_ID]"/>
		 </Objects>
	</Class>
</%block>
	
<xsl:template match="Block">
	<Object name="{@Name}.TSRAppliedTimeout" rules="update_or_create">
		<Properties>
			<Property name="TSRAppliedObject" dt="string"><xsl:value-of select="concat(@Name, '.TSRApplied')"/></Property>
			<Property name="LCSafetyTimeout" dt="i4"><xsl:value-of select="//Urbalis_Sector[@ID=$URBALIS_ID]/LC_Change_Timer"/></Property>
		</Properties>
	</Object>
</xsl:template>