<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<xsl:param name="AREA_TYPE"/>
<xsl:variable name="servers" select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="servers_noDATS" select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID and not(contains(Localisation_Type,'Depot'))]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="locs" select="distinct-values((for $i in (//Signalisation_Area[@ID=$servers_noDATS]/sys:geo/@name) return (substring-after($i,'ISCS_'))))"/>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<% mms_type_list=['MS','Switch','Router','Server','Firewall','GTW','WKS HMI','WKS SIMU','WKS DEV','WKS Overview','Printer','Datalogger','FEP','WKS Diag','WKS MTN','WKS Diag','WKS MTN','Simulator Computer'] %>
				%for typename in mms_type_list:
					<xsl:apply-templates select="//MSS_lruState/lruState[Type='${typename}' and Signalisation_Area_ID=$SERVER_ID]"/>				
				%endfor
					<xsl:apply-templates select="//MSS_lruState/lruState[(Type='GTW' or Type='Server' or Type='FEP') and Signalisation_Area_ID=$SERVER_ID]" mode="Power"/>
				<xsl:for-each select="//Signalisation_Area[@ID=$servers and (contains(Localisation_Type,'Depot') or Area_Type='ATS_Central_Server')]">
					<xsl:apply-templates select="//PDC[(contains(Equipment,MDD) or contains(Equipment,OCC)) and not(contains(Equipment,';')) and Alarm_Event='Alarm']"/>
				</xsl:for-each>
				<xsl:apply-templates select="//MSS_lruState/lruState[Type='MS' and Signalisation_Area_ID=$SERVER_ID]" mode="Capacity"/>
				<xsl:apply-templates select="//CBI[@ID=//Signalisation_Area[@ID=$servers_noDATS]/sys:cbi/@id]"/>
				<xsl:for-each select="//Signalisation_Area[@ID=$servers_noDATS and not(contains(Localisation_Type,'Depot'))]">
					<xsl:apply-templates select="//PDC[contains(Equipment,';') and Alarm_Event='Alarm']" mode="TE"/>
				</xsl:for-each>
		</Objects>	
	</Class>
</%block>


<xsl:template match="lruState" mode="Capacity">
	<xsl:variable name="terr" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>

	<xsl:variable name="pnm" select="concat(@Name,'.Alarm_Capacity_Limit')"/>
	<xsl:variable name="alarmlabel" select="'Maintenance System Server Reached An Operator Configured Capacity Limit'"/>
	<xsl:variable name="nmpv" select="concat(@Name,'.Status_Capacity_Limit.PV_Status')"/>
	<xsl:variable name="opv" select="'LTresholdExceed'"/>
	<xsl:variable name="Severity" select="'2'"/>
	<xsl:variable name="st_pv" select="'1'"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="omb" select="'M'"/>
	<xsl:variable name="von" select="'Yes'"/>
	<xsl:variable name="voff" select="'No'"/>
	<xsl:variable name="yy" select="substring(@Name, string-length(@Name), 1)"/>
	<xsl:variable name="EqID" select="concat(substring-after($terr[1],'Territory_'),'/SIG/MS/MS_SRV_',$yy)"/>	
	${Genx('{$pnm}','$alarmlabel','$nmpv','$st_pv','$opv')}
	
</xsl:template>

<xsl:template match="PDC">
	<xsl:variable name="loc" select="'MDD'"/>
	<xsl:variable name="pdcnm" select="concat(@Name,'.Alarm')"/>
	<xsl:variable name="alarmlabel" select="Alarm"/>
	<xsl:variable name="nmpv" select="concat(@Name,'.PV_Status')"/>
	<xsl:variable name="opv" select="'Value'"/>
	<xsl:variable name="Severity" select="'2'"/>
	<xsl:variable name="st_pv" select="'1'"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="omb" select="'M'"/>
	<xsl:variable name="von" select="Value_on"/>
	<xsl:variable name="voff" select="Value_off"/>
	<xsl:variable name="EqID" select="concat($loc,'/SIG/PDC/',Detail_Code)"/>
${obj('{$pdcnm}')}
</xsl:template>


<xsl:template match="PDC" mode="TE">
	<xsl:variable name="name_pdc" select="@Name"/>
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="alarmval" select="Alarm"/>
	<xsl:variable name="detail" select="Detail_Code"/>
	<xsl:variable name="v_von" select="Value_on"/>
	<xsl:variable name="v_voff" select="Value_off"/>
	<xsl:for-each select="tokenize(Equipment,';')">
		<xsl:variable name="eqp" select="."/>
		<xsl:variable name="loc" select="tokenize($eqp,'_')[1]"/>
		<xsl:variable name="cond" select="if ($loc=$locs) then true() else false()"/>
		<xsl:variable name="tt7" select="concat($name_pdc,'_',$loc,'.Alarm')"/>
		<xsl:variable name="EqID" select="concat($loc,'/SIG/PDC/',$detail)"/>
		<xsl:if test="$cond">
		<Object name="{$tt7}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarmval"/></Property>			
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="concat($name_pdc,'_',$loc,'.PV_Status')"/></Property>			
				<Property name="Out_PV_Trig" dt="string">Value</Property>			
				<Property name="Severity" dt="i4">2</Property>			
				<Property name="State_PV_Trig" dt="i4">1</Property>									
				${mulval('EquipmentID','$EqID')}	
				${mulval('Name','$name_pdc')}	
				${mulval('OMB',"'M'")}	
				${mulval('Value_on','$v_von')}	
				${mulval('Value_off','$v_voff')}	
			</Properties>
		</Object>	
		</xsl:if>
     </xsl:for-each>
</xsl:template>

<xsl:template match="lruState">
	<xsl:variable name="terr" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="lbl" select="//Label[Type=current()/Type]"/>
	<xsl:variable name="lrunam" select="concat(@Name,'.Alarm')"/>
	<xsl:variable name="alarmlabel" select="if(Type='MS') then ('Maintenance System Server Over Temperature Detected') else( $lbl/@Name)"/>
	<xsl:variable name="nmpv" select="concat(@Name,'.Status.PV_Status')"/>
	<xsl:variable name="opv" select="'Value'"/>
	<xsl:variable name="Severity" select="if(Type='MS') then ('2') else ($lbl/Severity)"/>
	<xsl:variable name="nm" select="concat(@Name,'.LRU_Failed')"/>
	<xsl:variable name="st_pv" select="'4'"/>
	<xsl:variable name="omb" select="$lbl/OMB"/>
	<xsl:variable name="von" select="'Alarm'"/>
	<xsl:variable name="voff" select="'Normal'"/>
	<xsl:variable name="EqID" select="concat(Location,'/SIG/ATS/',Detail_Code)"/>
${obj('{$lrunam}')}
</xsl:template>


<xsl:template match="lruState" mode="Power">
	<xsl:variable name="sa" select="current()"/>
	<xsl:variable name="terr" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="lbl" select="//Label[Type=current()/Type]"/>
	<xsl:variable name="pnm" select="concat(@Name,'.Alarm_Power_Supply')"/>
	<xsl:variable name="pnm2" select="concat(@Name,'.Alarm_Over_Temp')"/>
	<xsl:variable name="alarmlabel" select="if(Type='Server') then ('ATS Server Power Supply Failed') else if(Type='GTW') then ('Gateway Server Power Supply Failed') else if(Type='FEP') then('FEP Server Power Supply Failed') else null"/>
	<xsl:variable name="alarmlabel2" select="if(Type='Server') then ('ATS Server Over Temperature Detected') else if(Type='GTW') then ('Gateway Server Over Temperature Detected') else if(Type='FEP') then('FEP Server Over Temperature Detected') else null"/>
	<xsl:variable name="nmpv" select="concat(@Name,'.Status_Power_Supply.PV_Status')"/>
	<xsl:variable name="nmpv2" select="concat(@Name,'.Status_Over_Temp.PV_Status')"/>
	<xsl:variable name="opv" select="'Value'"/>
	<xsl:variable name="Severity" select="$lbl/Severity"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="omb" select="$lbl/OMB"/>
	<xsl:variable name="von" select="if(Type='Server') then ('Alarm') else if(Type='GTW') then ('Fault') else null"/>
	<xsl:variable name="voff" select="'Normal'"/>
	<xsl:variable name="yy" select="if(Type='FEP') then (if(contains(Sector,'DATS')) then (concat(Sector,Type,substring(@Name, string-length(@Name), 1))) else if(contains(Sector,'LATS')) then (concat(Type,substring(@Name, string-length(@Name), 1))) else null ) else (@Name)"/>
	<xsl:variable name="EqID" select="concat(substring-after($terr[1],'Territory_'),'/SIG/ATS/',$yy)"/>	
	${Genx('{$pnm}','$alarmlabel','$nmpv','4','$opv')}
	${Genx('{$pnm2}','$alarmlabel2','$nmpv2','4','$opv')}
</xsl:template>

<% scu_list=[('Sector %s1: CBI MooN SCU %s2 LRU Failed', '.LRU_Alarm', '.LRU_Status.PV_Status', '-SCU_xx', '-SCU_xx'),
             ('Sector %s1: CBI MooN SCU %s2 Over Temperature Detected', '.OverTemperature_Alarm', '.OverTemperature_Status.PV_Status', '-SCU_xx', '-SCU_xx'),
             ('Sector %s1: CBI MooN SCU %s2 MMI LRU Failed', '.MMILRU_Alarm', '.MMILRU_Status.PV_Status', '-SCU_xx', '-MMI_xx'),
             ('Sector %s1: CBI MooN SCU %s2 SCU_Upper_Fan_Tray LRU Failed', '.UpperFAN_Alarm', '.UpperFAN_Status.PV_Status', '-SCU_xx', '-SCU_xx-FAN_T'),
             ('Sector %s1: CBI MooN SCU %s2 SCU_Lower_Fan_Tray LRU Failed', '.LowerFAN_Alarm', '.LowerFAN_Status.PV_Status', '-SCU_xx', '-SCU_xx-FAN_B')] %>

<xsl:template match="CBI">
	<xsl:variable name="cbinm" select="@Name"/>
	%for alarmlabel, suff, nmpv_suff, nm_suff, eqid_suff in scu_list:
		%for dir in ['.SCULeft','.SCURight']:
			<xsl:variable name="objnm" select="concat($cbinm,'${dir}','${suff}')"/>
			<xsl:variable name="alarmlabel" select="'${alarmlabel}'"/>
			<xsl:variable name="bstrp1" select="//Urbalis_Sector[@ID=current()/Urbalis_Sector_ID]/@Name"/>
			<xsl:variable name="us_num" select="substring-after($bstrp1,'SEC_')"/>
			<xsl:variable name="bstrp2" select="if (contains('${dir}','Left')) then 'SCU_left' else 'SCU_right'"/>
			<xsl:variable name="nmpv" select="concat($cbinm,'${dir}','${nmpv_suff}')"/>
			<xsl:variable name="xx" select="if (contains('${dir}','Left')) then 'L' else 'R'"/>
			<xsl:variable name="nm" select="concat($cbinm,replace('${nm_suff}','xx',$xx))"/>
			<xsl:variable name="eqid_cbi_nm" select="if (contains($bstrp1,'MDD1')) then ('CBI_MDD_01') else if (contains($bstrp1,'MDD2')) then ('CBI_MDD_02') else ($cbinm)"/>
			<xsl:variable name="eqid_pre" select="if (contains($bstrp1,'MDD')) then 'MDD' else concat('TE',format-number(number($us_num)+1,'00'))"/>
			<xsl:variable name="EqID" select="concat($eqid_pre,'/SIG/CBI/',$eqid_cbi_nm,replace('${eqid_suff}','xx',$xx))"/>
			${objscu()}
		%endfor
	%endfor
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
<%def name="obj(nmtt)">
		<Object name="${nmtt}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarmlabel"/></Property>			
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>			
					<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$opv"/></Property>			
					<Property name="Severity" dt="i4"><xsl:value-of select="$Severity"/></Property>			
					<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="$st_pv"/></Property>					
					${multilingualvalue('$nm','Name')}						
					${multilingualvalue('$EqID','EquipmentID')}						
					${multilingualvalue('$omb','OMB')}						
					${multilingualvalue('$von','Value_on')}						
					${multilingualvalue('$voff','Value_off')}			
					${multilingualvalue("'N'",'Avalanche')}			
				</Properties>
		</Object>
</%def>

<%def name="objscu()">
	<Object name="{$objnm}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarmlabel"/></Property>			
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>			
				<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="'Value'"/></Property>			
				<Property name="Severity" dt="i4">2</Property>			
				<Property name="State_PV_Trig" dt="i4">2</Property>	
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrp1"/></Property>	
				<Property name="BstrParam2" dt="string"><xsl:value-of select="$bstrp2"/></Property>					
				${multilingualvalue('$nm','Name')}						
				${multilingualvalue('$EqID','EquipmentID')}						
				${multilingualvalue("'M'",'OMB')}						
				${multilingualvalue("'Alarm'",'Value_on')}						
				${multilingualvalue("'Normal'",'Value_off')}				
				${multilingualvalue("'N'",'Avalanche')}				
			</Properties>
	</Object>
</%def>

<%def name="mulval(value_nm,value_Var)">
           <MultiLingualProperty name="${value_nm}">
			<xsl:for-each select="$mlv">
             <MultiLingualValue roleId="{../@RID}" localeId="{@LCID}"><xsl:value-of select="${value_Var}"/></MultiLingualValue>
			</xsl:for-each>
           </MultiLingualProperty>
</%def>

<%def name="Genx(value_nm,value_Var,nmpv,stpv='4',out_pv='Value')">
		<Object name="${value_nm}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarmlabel"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="${nmpv}"/></Property>						
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="${out_pv}"/></Property>			
			<Property name="Severity" dt="i4"><xsl:value-of select="$Severity"/></Property>			
			<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${stpv}"/></Property>					
			${multilingualvalue('$nm','Name')}						
			${multilingualvalue('$EqID','EquipmentID')}						
			${multilingualvalue('$omb','OMB')}						
			${multilingualvalue('$von','Value_on')}						
			${multilingualvalue('$voff','Value_off')}	
		</Properties>
		</Object>		
</%def>