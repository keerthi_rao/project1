<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:param name="DATE"/>
<xsl:template match="/">
	<TRACKPORTIONS Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="TrackPortionToTrackID.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<xsl:apply-templates select="//Block"/>
	</TRACKPORTIONS>
</xsl:template>
<xsl:template match="Block">
	<TRACKPORTION ID="TI_{@Name}.TrackPortion" TRACKID="{Track_ID}"/>
</xsl:template>
</xsl:stylesheet>
