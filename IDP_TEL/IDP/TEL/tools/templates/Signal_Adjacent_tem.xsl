<%inherit file="/Object_tem.mako"/>
<%!
    class_ = "Signal"
    xs_ = True
    add_alias_function = True
    exclude_result_prefixes = False
%>

<%block name="classes">

	<xsl:variable name="sigArea" select="//Signalisation_Area[sys:cbi/@id = $CBI_ID and (Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server')]"/>
	<xsl:variable name="Rts" select="//Signal[not(sys:sigarea/@id=$sigArea/@ID or sys:cbi/@id=$CBI_ID or @ID = //Exit_Gate/Exit_Gate_Signal_ID) and (@ID = //Route[sys:cbi/@id=$CBI_ID]/Destination_Signal_ID)]"/>
	
	<xsl:variable name="filegenerationcond" select="if ($Rts) then 'Yes' else 'No' "/>
	<xsl:if test="$filegenerationcond = 'No'">
		<xsl:result-document href="Signal_Adjacent_{$CBI_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>	
    <Class name="Signal">
      <Objects>
        <xsl:apply-templates select="$Rts" mode="none"/>
      </Objects>
    </Class>
    <Class name="SignalEquipment">
      <Objects>
        <xsl:apply-templates select="$Rts" mode="SEqt"/>
      </Objects>
    </Class>
    <xsl:if test="$Rts[SBA]">
    <Class name="SignalHILC">
      <Objects>
        <xsl:apply-templates select="$Rts" mode="hilcDB"/>
      </Objects>
    </Class>
    </xsl:if>
</%block>

<xsl:template match="Signal" mode="none">
  <xsl:variable name="sid" select="@ID"/>
  <xsl:variable name="tid" select="Track_ID"/>
  <xsl:variable name="cbid" select="sys:cbi/@id"/>
  <xsl:variable name="slid" select="Signal_Lamp_Type_ID"/>
  <xsl:variable name="sddid" select="Secondary_Detection_Device_ID"/>
        <Object name="{@Name}" rules="update_or_create">
          <Properties>
            <Property name="SharedOnIdentifier" dt="boolean"><xsl:value-of select="@Name"/></Property>
            <Property name="HILCAvailable" dt="boolean"><xsl:value-of select="if(Signal_Type_Function='Virtual') then 'false' else Signal_Blocked_By_ATS_Operator/text()"/></Property>
  <xsl:variable name="val1" select="if(Destination_Signal_Blocked_By_ATS_Operator='true') then if($sysdb//Exit_Gate/Destination_Signal_ID=$sid) then '2' else '1' else '0'"/>
            <Property name="HILCDestinationBlockingType" dt="i4"><xsl:value-of select="$val1"/></Property>
            <Property name="HILCExitGateType" dt="i4"><xsl:value-of select="$val1"/></Property>
            <Property name="HILCExitGateWithConfirmation" dt="i4"><xsl:value-of select="if($sysdb//Urbalis_Sector[Track_ID_List/Track_ID/text()=$tid and Urbalis_Sector_Option/@DestinationSignal_PrepVerif='true']) then '0' else '1'"/></Property>
            <Property name="ApproachLockingAvailable" dt="boolean"><xsl:value-of select="if(Distance_Approach_Locking='0' or contains(Signal_Type_Function/text(),'Virtual') or //Signal_Lamp_Type[@ID=$slid]/ @Name='1_Aspect') then 'false' else 'true'"/></Property>
            <Property name="OverriddenAvailable" dt="boolean"><xsl:value-of select="if(Signal_Type_Function='Virtual') then 'false' else Signal_Overridden_For_CBTC_Train"/></Property>
            <Property name="CallOnSignalStatusAvailable" dt="boolean"><xsl:value-of select="if(Signal_Type_Function='Virtual') then 'false' else Call_on_Signal"/></Property>
            <Property name="AutoRouteAvailable" dt="boolean"><xsl:value-of select="if($sysdb//Routes/Route[Origin_Signal_ID=$sid and Auto_Route='true']) then 'True' else 'False'"/></Property>
            <Property name="Interlocking" dt="string">CBIS_<xsl:value-of select="current()/sys:cbi[1]/@id"/></Property>
            <xsl:if test="not(Lamp_Proved_Signal='true' and not(Signal_Lamp_Type_ID))">
              <Property name="LampProvedTypeAvailable" dt="i4"><xsl:value-of select="if(Lamp_Proved_Signal='false') then '0' else Signal_Lamp_Type_ID"/></Property>
            </xsl:if>
            <!--xsl:if test="$sysdb//Signal_Lamp_Type[@ID=$slid]/Lamp_List/Lamp_Name/text()='SRES' and not($sysdb//Crossing_Detection/Signal_Crossing_ID/text()=$sid)">
              <Property name="SPAD" dt="string"><xsl:value-of select="$sysdb//Secondary_Detection_Device[@ID=$sddid]/@Name"/></Property>
            </xsl:if-->
            <Property name="KpValue" dt="i4"><xsl:value-of select="xs:integer(Kp/@Value)+xs:integer(Kp/@Corrected_Gap_Value)+xs:integer(Kp/@Corrected_Trolley_Value)"/></Property>
            <Property name="TrackID" dt="i4"><xsl:value-of select="Track_ID"/></Property>
            <Property name="ManagedKeys" dt="string">CMN_HILC_BAD_OPERATOR;CMN_HILC_CANCEL_FAILED;CMN_HILC_CANCEL_SESSION_NOT_WAITED;CMN_HILC_CANCEL_SESSION_SIZE_ERROR;CMN_HILC_CANCEL_SUCCEEDED;CMN_HILC_CONFIRMATION_MSG_NOT_EXPECTED;CMN_HILC_CONFIRMATION_NOT_WAITED;CMN_HILC_DECODE_ENTERSESSION_TAG_VALUE_FAILED;CMN_HILC_ENTER_SESSION_NOT_WAITED;CMN_HILC_ENTER_SESSION_SIZE_ERROR;CMN_HILC_MSG_NOT_WAITED;CMN_HILC_PREPARATION_BAD_WORKSTATION;CMN_HILC_PREPARATION_MSG_NOT_EXPECTED;CMN_HILC_PREPARATION_NOT_WAITED;CMN_HILC_STEP1_FAILED;CMN_HILC_STEP2_FAILED;CMN_HILC_STEP4_FAILED;<xsl:if test="COA">COA_NOTSET;COA_SET;</xsl:if>HILC_CONFIRMATION_ACKNOWLEDGE;HILC_CONFIRMATION_ACKNOWLEDGE_TIMEOUT;HILC_CONFIRMATION_SENT;HILC_CONFIRMATION_TIMEOUT;HILC_OPEN_SESSION;HILC_PREPARATION_ACKNOWLEDGE;HILC_PREPARATION_ACKNOWLEDGE_TIMEOUT;HILC_PREPARATION_SENT;HILC_PREPARATION_TIMEOUT<xsl:if test="LCSANN">;LCS_ANNONCE_ERROR;LCS_ANNONCE_ERROR_END<xsl:if test="LCSMOD">;LCS_ANNONCE_LIT;LCS_ANNONCE_NOLIT</xsl:if></xsl:if>;LCS_NOTSET<xsl:if test="LCSPER">;LCS_PERMISSIVE_ERROR;LCS_PERMISSIVE_ERROR_END<xsl:if test="LCSMOD">;LCS_PERMISSIVE_LIT;LCS_PERMISSIVE_NOLIT</xsl:if></xsl:if><xsl:if test="LCSRES or LCSPER or LCSANN or LCSMOD">;LCS_RESTRICTIVE_ERROR;LCS_RESTRICTIVE_ERROR_END;LCS_RESTRICTIVE_LIT;LCS_RESTRICTIVE_NOLIT</xsl:if>;LCS_STATUS<xsl:if test="LPSANN">;LPS_ANNONCE_LIT;LPS_ANNONCE_NOLIT</xsl:if><xsl:if test="LPSDEV">;LPS_DEVIATED_LIT;LPS_DEVIATED_NOLIT</xsl:if><xsl:if test="LPSMOD">;LPS_MOD_LIT;LPS_MOD_NOLIT</xsl:if>;LPS_NOTSET<xsl:if test="LPSOVD">;LPS_OVERRIDDEN_LIT;LPS_OVERRIDDEN_NOLIT</xsl:if><xsl:if test="LPSPER">;LPS_PERMISSIVE_LIT;LPS_PERMISSIVE_NOLIT</xsl:if><xsl:if test="LPSRES">;LPS_RESTRICTIVE_LIT;LPS_RESTRICTIVE_NOLIT</xsl:if><xsl:if test="LPSPL">;LPS_SIGHT_LIT;LPS_SIGHT_NOLIT</xsl:if>;LPS_STATUS;SAL_NOTSET;SAL_SET<xsl:if test="LCSRES or LCSPER or LCSANN">;SBD_U400_NOTSET;SBD_U400_SET</xsl:if><xsl:if test="SOVD">;SOVD_NOTSET;SOVD_SET;SOVD_NOTCONFIG</xsl:if><xsl:if test="SPER">;SPER_NOTSET;SPER_SET</xsl:if><xsl:if test="./Routes_Variables_list/Route_Variable[@Prefix='AR']">;SRP_INPROGRESS;SRP_NOTSET;SRP_SET</xsl:if><xsl:if test="./Routes_Variables_list/Route_Variable[@Prefix='RL']">;SRN_INPROGRESS;SRN_CALLONINPROGRESS;SRN_NOTSET;SRN_SET;SRN_CALLONSET;SRR_NOTSET;SRR_SET;SRR_INPROGRESS</xsl:if><xsl:if test="LCSOVD">;LCS_OVERRIDDEN_NOLIT;LCS_OVERRIDDEN_LIT;LCS_OVERRIDDEN_ERROR;LCS_OVERRIDDEN_ERROR_END</xsl:if><xsl:if test="LCSDEV">;LCS_DEVIATED_NOLIT;LCS_DEVIATED_LIT;LCS_DEVIATED_ERROR;LCS_DEVIATED_ERROR_END</xsl:if><xsl:if test="LCSMOD">;LCS_MOD_NOLIT;LCS_MOD_LIT;LCS_MOD_ERROR;LCS_MOD_ERROR_END</xsl:if><xsl:if test="LCSOVD">;LCS_SIGHT_NOLIT;LCS_SIGHT_LIT;LCS_SIGHT_ERROR;LCS_SIGHT_ERROR_END</xsl:if><xsl:if test="SB or SDB">;SB_ALLSET;SB_NOTSET;SB_ORIGINSET;SB_DESTINATIONSET</xsl:if><xsl:if test="CSID">;CROSSING_SIGNAL;NOCROSSING_SIGNAL</xsl:if></Property>
  <xsl:variable name="route" select="$sysdb//Route[Origin_Signal_ID=$sid]"/>
  <xsl:if test="$route/Destination_Signal_ID">
            <xsl:variable name="mlv_value">&lt;Signals&gt;<xsl:apply-templates select="$route" mode="MLV"/>&lt;/Signals&gt;</xsl:variable>
            ${multilingualvalue("$mlv_value", prop_name='DestinationSignals')}
  </xsl:if>
            <xsl:variable name="self" select="."/>
            ${multilingualvalue("sys:alias_name($self)")} 
  <xsl:variable name="track" select="$sysdb//Track[@ID=$tid]"/>
            ${multilingualvalue("sys:alias_name($track)", "TrackName")}
          </Properties>
        </Object>
</xsl:template>

<xsl:template match="Exit_Gate" >
    <xsl:variable name="dsid" select="Destination_Signal_ID/text()"/>
    <xsl:text>&lt;ExitGate ID="</xsl:text><xsl:value-of select='@Name'/>"&lt;Destination_Signal ID="<xsl:value-of select='$sysdb//Signal[@ID=$dsid]/@Name'/>"&lt;Name="<xsl:value-of select='sys:alias_name(//Signal[@ID=$dsid])'/>"&lt;/ExitGate&gt;</xsl:template>

<xsl:template match="Route" mode="MLV">
  <xsl:variable name="rid" select="@ID"/>
  <xsl:variable name="rco" select="$sysdb//Specific_Route[Type='Call_On' and Specific_Route_ID_List/Route_ID=$rid]"/>
  <xsl:variable name="dsid" select="Destination_Signal_ID"/>
  <xsl:variable name="osid" select="Opposite_Signal_ID"/>  
  <xsl:variable name="fcr" select="../Route[First_Conjugated_Route_ID/text()=$rid]"/>
  <xsl:variable name="fcrdsid" select="$fcr/Destination_Signal_ID"/>
  <xsl:variable name="fcrid" select="$fcr/@ID"/>
  <xsl:variable name="fcrco" select="$sysdb//Specific_Route[Type='Call_On' and Specific_Route_ID_List/Route_ID=$fcrid]"/>&lt;Signal Name="<xsl:value-of select="sys:alias_name(//Signal[@ID=$dsid])"/>" ID="<xsl:value-of select="if($fcrdsid) then $sysdb//Signal[@ID=$fcrdsid]/@Name else $sysdb//Signal[@ID=$dsid]/@Name"/>" RouteID="<xsl:value-of select="if($fcr) then $fcr/@Name else @Name"/>" CallOn="<xsl:value-of select="if($fcr) then if($fcrco) then '1' else '0' else if($rco) then '1' else '0'"/>" Auto="<xsl:value-of select="if($fcr) then if($fcr/Auto_Route='false') then '0' else '1' else if(Auto_Route='false') then '0' else '1'"/>" OppositeName="<xsl:value-of select="sys:alias_name(//Signal[@ID=$osid])"/>" OppositeID="<xsl:value-of select="if($osid) then $sysdb//Signal[@ID=$osid]/@Name else '0'"/>"&gt;<xsl:apply-templates select="$sysdb//Cycle[Route_Sequence_List/Route_ID=$rid]"/>&lt;/Signal&gt;</xsl:template>

<xsl:template match="Cycle">&lt;AdditionalEqpt ID="<xsl:value-of select="@ID"/>" Name="<xsl:value-of select="@Name"/>"/&gt;</xsl:template>

<xsl:template match="Signal" mode="SEqt">
  <xsl:variable name="sid" select="@ID"/>
  <xsl:variable name="csname" select="sys:alias_name(.)"/>  
  <xsl:if test="LCSRES or LCSPER or LCSANN or LCSMOD">${sig_eqt("Detection")}</xsl:if>
  <xsl:if test="SAL">${sig_eqt("HMIApproachLocking")}</xsl:if>
  <xsl:if test="SB or SDB">${sig_eqt("HMIBlocking")}</xsl:if>
  <xsl:if test="COA">${sig_eqt("HMICallOnStatus")}</xsl:if>
  <xsl:if test="LPSRES or LPSPER or LPSOVD or LPSANN or LPSDEV or LPSMOD or LPSPL">${sig_eqt("HMIFilament")}</xsl:if>
  <xsl:if test="LPSOVD">${sig_eqt("HMILampProvedOverridden")}</xsl:if>
  <xsl:if test="LPSPER">${sig_eqt("HMILampProvedPermissive")}</xsl:if>
  <xsl:if test="LPSRES">${sig_eqt("HMILampProvedRestrictive")}</xsl:if>
  <xsl:if test="LPSANN">${sig_eqt("HMILampProvedAnnonce")}</xsl:if>
  <xsl:if test="LPSDEV">${sig_eqt("HMILampProvedDeviated")}</xsl:if>
  <xsl:if test="LPSMOD">${sig_eqt("HMILampProvedMOD")}</xsl:if>
  <xsl:if test="LPSPL">${sig_eqt("HMILampProvedSight")}</xsl:if>
  <xsl:if test="LCSRES or LCSPER or LCSOVD or LCSANN or LCSDEV or LCSMOD or LCSPL">${sig_eqt("HMILampCommand")}</xsl:if>
  <xsl:if test="LCSOVD">${sig_eqt("HMILampCommandOverridden")}</xsl:if>
  <xsl:if test="LCSPER or LCSMOD">${sig_eqt("HMILampCommandPermissive")}</xsl:if>
  <xsl:if test="LCSRES or LCSPER or LCSANN or LCSMOD">${sig_eqt("HMILampCommandRestrictive")}</xsl:if>
  <xsl:if test="LCSANN">${sig_eqt("HMILampCommandAnnonce")}</xsl:if>
  <xsl:if test="LCSDEV">${sig_eqt("HMILampCommandDeviated")}</xsl:if>
  <xsl:if test="LCSMOD">${sig_eqt("HMILampCommandMOD")}</xsl:if>
  <xsl:if test="LCSPL">${sig_eqt("HMILampCommandSight")}</xsl:if>
  <xsl:if test="$sysdb//Routes/Route[Origin_Signal_ID=$sid]">${sig_eqt("HMINormalRoute")}</xsl:if>
  <xsl:if test="SOVD">${sig_eqt("HMIOverridden")}</xsl:if>
  <xsl:if test="$sysdb//Routes/Route[Origin_Signal_ID=$sid and AR]">${sig_eqt("HMIPermanentRoute")}</xsl:if>
  <xsl:if test="$sysdb//Routes/Route[Origin_Signal_ID=$sid]">${sig_eqt("HMIRouteRelease")}</xsl:if>
  <xsl:if test="SPER">${sig_eqt("HMIRouteSignal")}</xsl:if>
  <xsl:if test="CSID">${sig_eqt("Crossing_Detections")}</xsl:if>
</xsl:template>

<xsl:template match="Signal" mode="hilcDB">
        <Object name="{@Name}.HILCDestinationBlocking" rules="update_or_create" sys:ID="{@ID}">
          <Properties>
            ${props([("HILCEnterSession_PV","SDBSE"),("HILCCancelSession_PV","SDBSCA"),
                     ("HILCCurrentStep_PV","SDBSC"),("HILCSessionState_PV","SDBSS"),
                     ("HILCPreparation_PV","SDBCP"),("HILCPreparationCode_PV","SDBCCP"),
                     ("HILCConfirmation_PV","SDBCC"),("HILCConfirmationCode_PV","SDBCCC"),
                     ("HILCSessionStateValue_PV","SDBSSV"),("HILCCurrentStepValue_PV","SDBSCV")])}
            <Property name="HILCWithConfirmation" dt="i4"><xsl:value-of select="if($sysdb//Urbalis_Sector_Option[@DestinationSignal_PrepVerif='true']) then '0' else '1'"/></Property>
            <Property name="SignalID" dt="string"><xsl:value-of select="@Name"/></Property>
            <Property name="Interlocking" dt="string">CBIS_<xsl:value-of select="current()/sys:cbi[1]/@id"/></Property>
  <xsl:variable name="csname" select="sys:alias_name(.)"/>  
            ${multilingualvalue("$csname")}
          </Properties>
        </Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

<%def name="sig_eqt(type)" >
        <Object name="{@Name}.${type}" rules="update_or_create">
          <Properties>
            <Property name="Flavor" dt="string">${type}</Property>
            <Property name="SignalID" dt="string"><xsl:value-of select="@Name"/></Property>
            ${multilingualvalue("$csname")}
          </Properties>
        </Object>
</%def>

<%def name="props(list)" >
% for name,prefix in list:
    <Property name="${name}" dt="string">${prefix}_<xsl:value-of select="@Name"/></Property>
% endfor
</%def>
