<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  
<xsl:param name="DATE"/>
<xsl:param name="MACHINE_NAME"/>
<xsl:param name="ATS_EQUIP_TYPE"/>

<xsl:variable name="sigArea" select="if (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID) then (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID) else (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID_List/Signalisation_Area_ID)"/>
<xsl:variable name="SIG_AREA_TYPE" select="//Signalisation_Area[@ID=$sigArea]/Area_Type"/>
<xsl:template match="/">
<xml Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="OPCMDBGTW.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:if test="not($ATS_EQUIP_TYPE = 'FEP')">
		<OPCMODBUSCFG HookEncoding="NOZERO_REGULAR" OPCVariableProfile="~.!.value" DiscreteInputsFaultMode="Fallback" CoilsFaultMode="Fallback" InputRegistersFaultMode="Freeze" HoldingRegistersFaultMode="Freeze" Mode="Normal">
			<Server Name="TELOPCMDBGTW" WithCRC="1" MaxPayloadSize="12000" Type="LocalModbusSlave" IPAddress="{//ATS_Equipment[@Name=$MACHINE_NAME]/IP_Address_Light_Grey[1]}" Port="10255" UnitIdentifier="255" ConnectionTimeout="10000" TransactionTimeout="300" ReconnectionPeriod="10000" ReadPeriod="500" WritePeriod="500">
				<xsl:for-each select="//ISCSStation[Signalisation_Area=$sigArea]/ExternalModbusMasters/ExternalModbusMaster,//ISCSCentral[Signalisation_Area=$sigArea]/ExternalModbusMasters/ExternalModbusMaster">
					<ExternalModbusMaster IPAddress="{.}"/>
				</xsl:for-each>
				<Encryption Type="1" SymKey="{//ISCSSettings/ISCSSetting/EncryptionKey/text()}"/>
				<InputRegisters Size="8000">
					<xsl:apply-templates select="//ISCSStation[Signalisation_Area=$sigArea],//ISCSCentral[Signalisation_Area=$sigArea]" mode="trainData"/>
				</InputRegisters>
				<HoldingRegisters Size="24000">
					<xsl:apply-templates select="//ISCSStation[Signalisation_Area=$sigArea],//ISCSCentral[Signalisation_Area=$sigArea]" mode="trainAlarm"/>
				</HoldingRegisters>
			</Server>
		</OPCMODBUSCFG>
	</xsl:if>
	<xsl:if test="$ATS_EQUIP_TYPE = 'FEP'">
		<OPCMODBUSCFG OPCVariableProfile="~.!.value" DiscreteInputsFaultMode="Fallback" CoilsFaultMode="Fallback" InputRegistersFaultMode="Freeze" HoldingRegistersFaultMode="Freeze" Mode="Normal">
		<!--FEP Components -->
			<xsl:apply-templates select="//Platform_Screen_Doors[@ID=//Server/PSD_ID_List/PSD_ID]" mode="PSD"/>
		</OPCMODBUSCFG>
	</xsl:if>
	

</xml>
</xsl:template>
<xsl:template match="ISCSStation|ISCSCentral" mode="trainData">
	<DataBlock Name="{concat('TrainData',position())}" Type="STRING" IsFlexible="1" StringSize="{if ($SIG_AREA_TYPE ='ATS_Central_Server') then '1586' else '182'}" CharSizeInBytes="2" Direction="Output" Size="1" StartingAddress="{TrainDataStartAddress}" PhysicalAddress="1" Comment="">
		<Channel Name="{concat('TrainData',position())}" Rank="1" Comment=""/>
	</DataBlock>
</xsl:template>
<xsl:template match="ISCSStation|ISCSCentral" mode="trainAlarm">
	<DataBlock Name="{concat('TrainAlarm',position())}" Type="STRING" IsFlexible="1" StringSize="{if ($SIG_AREA_TYPE='ATS_Central_Server') then '4928' else '560'}" CharSizeInBytes="2" Direction="Input" Size="1" StartingAddress="{AlarmDataStartAddress}" PhysicalAddress="1" Comment="">
		<Channel Name="{concat('TrainAlarm',position())}" Rank="1" Comment=""/>
	</DataBlock>
</xsl:template>

<xsl:template match="Platform_Screen_Doors" mode="PSD">
	<Server Name="{@Name}" Type="Generic1" IPAddress="{//ATS_Equipment[@Name=$MACHINE_NAME]/IP_Address_Light_Grey[1]}" Port="10255" ConnectionTimeout="10000" TransactionTimeout="300" ReconnectionPeriod="10000" ReadPeriod="500" WritePeriod="500">
		<InputRegisters Size="1324">
			<DataBlock Name="PSDEqptStatus" Type="INT" Direction="Input" Size="22" StartingAddress="600" PhysicalAddress="1" Comment="PSD Equipment Status">
				<Channel Name="Word600" Rank="1" Comment="bit1: DoorProvenClosedAndLocked, bit2: DoorOpenStatus ... bit16: nothin"/>
			</DataBlock>
		
			<DataBlock Name="PSDIsolationStatus" Type="StringBitToChar" Direction="Input" Size="1" StringSize="20" StartingAddress="660" PhysicalAddress="1" Comment="PSD IsolationStatus">
				<Channel Name="IsolationArray" Rank="1" Comment="Array of bits to show the isolation status of the PSD"/>
			</DataBlock>
		</InputRegisters>
		<HoldingRegisters Size="1352">
			<DataBlock Name="TrainDoorStatus" Type="StringBitToChar" StringSize="20" Direction="Output" Size="1" StartingAddress="665" PhysicalAddress="1" Comment="Train Door Status">
				<Channel Name="DoorArray" Rank="1" Comment="Array of bits to show the isolation status of the Train Door"/>
			</DataBlock>
			<DataBlock Name="TimeSync" Type="INT" Direction="Output" Size="4" StartingAddress="670" PhysicalAddress="1" Comment="Time Sync">
				<Channel Name="Day_Month" Rank="1" Comment="Day_Month"/>
				<Channel Name="Year1_Year2" Rank="2" Comment="Year1_Year2"/>
				<Channel Name="Hour_Minute" Rank="3" Comment="Hour_Minute"/>
				<Channel Name="Second_None" Rank="4" Comment="Second_None"/>
			</DataBlock>
			<DataBlock Name="TimeCheck" Type="INT" Direction="Output" Size="1" StartingAddress="674" PhysicalAddress="1" Comment="Time Check Flag">
				<Channel Name="Flag" Rank="1" Comment="Write this flag to trigger a time set"/>
			</DataBlock>
		 </HoldingRegisters>
	</Server>
</xsl:template>

</xsl:stylesheet>
