<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs fn">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sys="http://www.systerel.fr/Alstom/IDP">
<RouteSettingPoints>
    <xsl:variable name="signals" select="$sysdb//Signal"/>
    <xsl:variable name="cross_trigger" select="$sysdb//Cross_Trigger"/>
    <xsl:apply-templates select="$signals[@ID=$cross_trigger/Signal_ID]"/>
</RouteSettingPoints>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>

<xsl:template match="Signal">
	<xsl:variable name="regObj" select="//RegulationPoint"/>
    <xsl:variable name="sid" select="@ID"/>
    <xsl:variable name="sname" select="@Name"/>
    <xsl:variable name="dir" select="Direction"/>
    <xsl:variable name="cross_trigger" select="$sysdb//Cross_Trigger[Signal_ID=$sid]"/>
    <xsl:variable name="departure_trigger" select="$cross_trigger/Departure_Trigger_For_Route_Setting"/>
    <xsl:variable name="delay_route" select="$cross_trigger/Delay_For_Route_Setting"/>
    
    <!-- Compute TP List -->
    <xsl:variable name="tid" select="Track_ID"/>
    <xsl:variable name="ars" select="number($cross_trigger/Signal_ID[text()=$sid]/following-sibling::*[1])"/><!-- Anticipated_Route_Setting_Distance -->
    <xsl:variable name="kp_sig" select="sys:corr_kp(Kp)"/>
    <!-- select the blocks where the signal is located.  -->
    <xsl:variable name="blocks" select="$sysdb//Block[Track_ID = $tid and ((number(Kp_Begin) &lt;= $kp_sig and $kp_sig &lt;= number(Kp_End)) or (number(Kp_End) &lt;= $kp_sig and $kp_sig &lt;= number(Kp_Begin)))]"/>
    
    <xsl:if test="count($blocks) = 0">
        <xsl:message terminate="no">Error : No block found for signal: '<xsl:value-of select="@Name"/>'</xsl:message>
    </xsl:if>
    <xsl:if test="count($blocks) != 0">
    <xsl:variable name="block_id" select="if (count($blocks) = 1) then $blocks/@ID else if ($dir='Up') then $blocks[number(Kp_End)=$kp_sig]/@ID else $blocks[number(Kp_Begin)=$kp_sig]/@ID"/>
    <xsl:variable name="block" select="$sysdb//Block[@ID=$block_id]"/>
    
    <xsl:variable name="tp_list_tmp">
         <xsl:call-template name="collect_tp">
             <xsl:with-param name="curr_block" select="$block"/>
             <xsl:with-param name="dir" select="sys:opposite($dir)"/>
             <xsl:with-param name="kp" select="$kp_sig"/>
             <xsl:with-param name="l" select="$ars"/>
             <xsl:with-param name="regpoint_curent" select="-1"/>
			 <xsl:with-param name="sigarea" select="$blocks[1]/sys:sigarea[contains(@name,'_LATS_')]/@id"/>
			 <xsl:with-param name="sigdir" select="Direction"/>
			 <xsl:with-param name="curr_signal" select="."/>
         </xsl:call-template>
    </xsl:variable>
	
    <xsl:variable name="regulation_point_id" select="$tp_list_tmp/RegulationPoint_ID_Near"/>
    <xsl:variable name="other_regpoints" select="$tp_list_tmp/RegulationPoint_ID"/>
    
	<RouteSettingPoint ID="{@ID}" Name="{concat('APPROACH_', @Name)}">
        ${node("sys:signal", "@ID")}
        ${node("Direction", "if (Direction='Up') then 'Right' else 'Left'")}
        <xsl:if test="$regulation_point_id!=''">
            ${node("RelatedRegulationPointID", "$regulation_point_id")}
        </xsl:if>
        <TrackPortions_List>
           <xsl:copy-of select="reverse($tp_list_tmp/TrackPortion_ID)"/>
        </TrackPortions_List>
        ${node("Departure_Trigger_For_Route_Setting", "if ($departure_trigger) then $departure_trigger else '20'")}
        ${node("Delay_For_Route_Setting", "if ($delay_route) then $delay_route else '1'")}
    </RouteSettingPoint>
	<!-- Generation of RouteSettingPoint APPROACH_SignalName_RegulationPointName-->
	<!-- Create distinct regulation points from other_regpoints-->
    <xsl:variable name="distinct_rps">
		<xsl:for-each select="distinct-values($other_regpoints)">
			<xsl:variable name="self" select="normalize-space(.)"/>
			<xsl:variable name="rp" select="normalize-space($regObj[@ID=$self]/@Name)"/>
			<xsl:if test="$self and not($self = '') and not($self = ' ')">
			<xsl:if test="not(normalize-space($self)=normalize-space($regulation_point_id)) and $rp">
				<RegulationPoint_ID><xsl:value-of select="$self"/></RegulationPoint_ID>
			</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<xsl:for-each select="$distinct_rps/RegulationPoint_ID">
		<xsl:variable name="self" select="normalize-space(.)"/>
    	<xsl:variable name="rp" select="normalize-space($regObj[@ID=$self]/@Name)"/>
		<xsl:variable name="cnt" select="position()*10000"/>
		    <RouteSettingPoint ID="{number($sid)+$cnt}" Name="{concat('APPROACH_',$sname,'_',$rp)}">
    			${node("sys:signal", "$sid")}
        		${node("Direction", "if ($dir='Up') then 'Right' else 'Left'")}
           		${node("RelatedRegulationPointID", "$self")}
        		<TrackPortions_List>
           			<xsl:copy-of select="reverse($tp_list_tmp/TrackPortion_ID)"/>
        		</TrackPortions_List>
        		${node("Departure_Trigger_For_Route_Setting", "if ($departure_trigger) then $departure_trigger else '20'")}
        		${node("Delay_For_Route_Setting", "if ($delay_route) then $delay_route else '1'")}
    		</RouteSettingPoint>
	</xsl:for-each>
    <!--<xsl:for-each select="distinct-values($other_regpoints)">
    	<xsl:variable name="self" select="normalize-space(.)"/>
    	<xsl:variable name="rp" select="normalize-space($regObj[@ID=$self]/@Name)"/>
    	<xsl:if test="$self and not($self = '') and not($self = ' ')">
    	<xsl:if test="not(normalize-space($self)=normalize-space($regulation_point_id)) and $rp">
    		<xsl:variable name="cnt" select="position()*10000"/>
    		<RouteSettingPoint ID="{number($sid)+$cnt}" Name="{concat('APPROACH_',$sname,'_',$rp)}">
    			${node("sys:signal", "$sid")}
        		${node("Direction", "if ($dir='Up') then 'Right' else 'Left'")}
           		${node("RelatedRegulationPointID", "$self")}
        		<TrackPortions_List>
           			<xsl:copy-of select="reverse($tp_list_tmp/TrackPortion_ID)"/>
        		</TrackPortions_List>
        		${node("Departure_Trigger_For_Route_Setting", "if ($departure_trigger) then $departure_trigger else '20'")}
        		${node("Delay_For_Route_Setting", "if ($delay_route) then $delay_route else '1'")}
    		</RouteSettingPoint>
    	</xsl:if>
    	</xsl:if>
    </xsl:for-each>-->
	</xsl:if>
</xsl:template>


<!-- Compute TrackPortion List and RegulationPoint_ID -->
<xsl:template name="collect_tp">
    <xsl:param name="curr_block"/>
    <xsl:param name="dir"/>
    <xsl:param name="kp"/>
    <xsl:param name="l"/><!-- cm -->
    <xsl:param name="regpoint_curent"/>
	<xsl:param name="sigarea"/>
    <xsl:param name="sigdir"/>
	<xsl:param name="curr_signal"/>
    <xsl:choose>
        <xsl:when test="$l>0 and $curr_block">
            <!-- TP id list of tp on curr_block -->
            <xsl:variable name="kp_limit_block" select="if ($dir='Up') then number($curr_block/Kp_End) else number($curr_block/Kp_Begin)"/>
            <xsl:variable name="d_limit_block" select="if ($kp > $kp_limit_block) then number($kp)-number($kp_limit_block) else number($kp_limit_block)-number($kp)"/>
            
            <xsl:if test="$l > $d_limit_block">
                <!-- Collect TP covered by the zone -->
                 <xsl:variable name="tps">
                     <xsl:call-template name="tp_from_block">
                        <xsl:with-param name="curr_block" select="$curr_block"/>
                        <xsl:with-param name="kp1" select="$kp"/>
                        <xsl:with-param name="kp2" select="$kp_limit_block"/>
						<xsl:with-param name="sigarea" select="$sigarea"/>
						<xsl:with-param name="sigdir" select="$sigdir"/>
						<xsl:with-param name="curr_signal" select="$curr_signal"/>
                     </xsl:call-template>
                </xsl:variable>
                <xsl:copy-of select="if ($dir='Up') then $tps else fn:reverse($tps/TrackPortion_ID)"/>
                <!-- call the template -->
                <xsl:variable name="next_normal_id" select="sys:follow($curr_block, $dir, 'Normal')"/>
                <xsl:variable name="next_normal" select="//Block [ $next_normal_id = @ID ]"/>
                <xsl:variable name="next_normal_normal_id" select=" sys:follow( $next_normal, $dir , 'Normal')"/>
                <xsl:variable name="next_normal_dir" select="if ($next_normal_normal_id = $curr_block/@ID) then sys:opposite($dir) else $dir"/>
                <xsl:variable name="kp_limit_block2" select="if ($next_normal_dir='Up') then number($next_normal/Kp_Begin) else number($next_normal/Kp_End)"/>
    
                <xsl:call-template name="collect_tp_2">
                    <xsl:with-param name="curr_block" select="$next_normal"/>
                    <xsl:with-param name="dir" select="$next_normal_dir"/>
                    <xsl:with-param name="kp" select="$kp_limit_block2"/>
                    <xsl:with-param name="l" select="number($l)-number($d_limit_block)"/>
                    <xsl:with-param name="regpoint_curent" select="$regpoint_curent"/>
                    <xsl:with-param name="tps" select="$tps"/>
					<xsl:with-param name="sigarea" select="$sigarea"/>
					<xsl:with-param name="sigdir" select="$sigdir"/>
					<xsl:with-param name="curr_signal" select="$curr_signal"/>
                </xsl:call-template>
            </xsl:if>
            
            <xsl:if test="$l &lt;= $d_limit_block">
                 <xsl:variable name="kp_limit">
                   <xsl:if test="number($curr_block/Kp_Begin) > number($curr_block/Kp_End)">
                       <xsl:value-of select="if ($dir='Up') then number($kp)-number($l) else number($kp)+number($l)"/>
                   </xsl:if>
                   <xsl:if test="number($curr_block/Kp_Begin) &lt;= number($curr_block/Kp_End)">
                       <xsl:value-of select="if ($dir='Up') then number($kp)+number($l) else number($kp)-number($l)"/>
                   </xsl:if>
                 </xsl:variable>
                <!-- Collect TP covered by the zone -->
                <xsl:variable name="tps">
                 <xsl:call-template name="tp_from_block">
                    <xsl:with-param name="curr_block" select="$curr_block"/>
                    <xsl:with-param name="kp1" select="$kp"/>
                    <xsl:with-param name="kp2" select="$kp_limit"/>
					<xsl:with-param name="sigarea" select="$sigarea"/>
					<xsl:with-param name="sigdir" select="$sigdir"/>
					<xsl:with-param name="curr_signal" select="$curr_signal"/>
                 </xsl:call-template>
                </xsl:variable>
                <xsl:copy-of select="if ($dir='Up') then $tps else fn:reverse($tps/TrackPortion_ID)"/>
                <!-- call the template -->
                <xsl:call-template name="collect_tp_2">
                    <xsl:with-param name="curr_block" select="$curr_block"/>
                    <xsl:with-param name="dir" select="$dir"/>
                    <xsl:with-param name="kp" select="$kp_limit"/>
                    <xsl:with-param name="l" select="0"/>
                    <xsl:with-param name="regpoint_curent" select="$regpoint_curent"/>
                    <xsl:with-param name="tps" select="$tps"/>
					<xsl:with-param name="sigarea" select="$sigarea"/>
					<xsl:with-param name="sigdir" select="$sigdir"/>
					<xsl:with-param name="curr_signal" select="$curr_signal"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:when>
        <!-- Returns the nearest regulation point of the signal-->
        <xsl:otherwise>
            <RegulationPoint_ID_Near><xsl:value-of select="$regpoint_curent"/></RegulationPoint_ID_Near>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="collect_tp_2">
    <xsl:param name="curr_block"/>
    <xsl:param name="dir"/>
    <xsl:param name="kp"/>
    <xsl:param name="l"/>
    <xsl:param name="regpoint_curent"/>
    <xsl:param name="tps"/>
	<xsl:param name="sigarea"/>
	<xsl:param name="sigdir"/>
	<xsl:param name="curr_signal"/>
    <!-- Compute the nearest regulation point of the signal-->
    <xsl:variable name="regpoint_idnew" select="if ($regpoint_curent=-1 or $regpoint_curent!=//RegulationPoint[TrackPortions_List/TrackPortion_ID=$tps/TrackPortion_ID]/@ID ) then //RegulationPoint[TrackPortions_List/TrackPortion_ID=$tps/TrackPortion_ID]/@ID else $regpoint_curent"/>
    <xsl:variable name="regpoint" select="//RegulationPoint[@ID=$regpoint_idnew]"/>
    <xsl:variable name="regpoint_id" select="if ($regpoint_curent !=-1) then $regpoint_curent else //RegulationPoint[TrackPortions_List/TrackPortion_ID=$tps/TrackPortion_ID]/@ID"/>
    <RegulationPoint_ID><xsl:value-of select="$regpoint/@ID"/></RegulationPoint_ID>
    <xsl:call-template name="collect_tp">
        <xsl:with-param name="curr_block" select="$curr_block"/>
        <xsl:with-param name="dir" select="$dir"/>
        <xsl:with-param name="kp" select="$kp"/>
        <xsl:with-param name="l" select="$l"/>
        <xsl:with-param name="regpoint_curent" select="$regpoint_id"/>
		<xsl:with-param name="sigarea" select="$sigarea"/>
		<xsl:with-param name="sigdir" select="$sigdir"/>
		<xsl:with-param name="curr_signal" select="$curr_signal"/>
    </xsl:call-template>
    
</xsl:template>
    
<!-- Returns a list of TrackPortion contained into the zone (Block + Kps) -->
<xsl:template name="tp_from_block">
    <xsl:param name="curr_block"/>
    <xsl:param name="kp1"/>
    <xsl:param name="kp2"/>
	<xsl:param name="sigarea"/>
	<xsl:param name="sigdir"/>
	<xsl:param name="curr_signal"/>
    <xsl:for-each select="//(TrackPortion[sys:block_id=$curr_block/@ID and 
                    ((number(KpBegin) > $kp1 and $kp2 > number(KpBegin)) or (number(KpEnd) > $kp1 and $kp2 > number(KpEnd)) or
                     (number(KpBegin) > $kp2 and $kp1 > number(KpBegin)) or (number(KpEnd) > $kp2 and $kp1 > number(KpEnd)) or
                     ($kp1 >= number(KpBegin) and number(KpEnd) >= $kp2) or (number(KpBegin) >= $kp1 and $kp2 >= number(KpEnd)))])">
					 <xsl:variable name="block" select="sys:block_id"/>
					 <xsl:variable name="sigarea_cond" select="if ($sigarea) then (if (//Block[@ID = $block]/sys:sigarea/@id = $sigarea) then 'Yes' else 'No') else 'Yes'"/>
					 <!-- cond2 variable is added for point 31 in U400_Transformer comparison report 21/04/2016-->
					 <!--<xsl:variable name="cond2" select="if ( ($sigdir = 'Up' and (number(KpBegin) = number($kp1)) ) or ($sigdir = 'Down' and (number(KpEnd) = number($kp1)) ) ) then 'No' else 'Yes'"/>-->
					 <xsl:variable name="cond2" select="if ( ($sigdir = 'Up' and sys:track_id=$curr_signal/Track_ID and (number(KpBegin) = number(sys:corr_kp($curr_signal/Kp))) ) or ($sigdir = 'Down' and sys:track_id=$curr_signal/Track_ID and (number(KpEnd) = number(sys:corr_kp($curr_signal/Kp))) ) ) then 'No' else 'Yes'"/>
					 <xsl:if test="$sigarea_cond = 'Yes' and $cond2 = 'Yes'">
						<TrackPortion_ID><xsl:value-of select="@ID"/></TrackPortion_ID>
					 </xsl:if>
    </xsl:for-each>
</xsl:template>


<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
<xsl:include href="../../lib_common/lib_xslt.xsl"/>
</xsl:stylesheet>

<%namespace file="lib_tem_tel.mako" import="node"/>