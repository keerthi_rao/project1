<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KIOOPCUAClient.IOClient" %>


<xsl:variable name="servers" select="if (//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server') then (//Signalisation_Area[Area_Type='ATS_Local_Server' and not(contains(Localisation_Type,'Depot'))]/@ID) else $SERVER_ID"/>
<xsl:variable name="sig_file" select="//Signalisation_Area[@ID=$servers]"/>
<xsl:variable name="sig_all" select="//Signalisation_Area"/>

<%block name="classes">
	<Class name="S2KOPCUAClient.IOClient">
	     <Objects>
			<xsl:apply-templates select="//$sig_all[@ID=$sig_file/sys:sigarea/@id and Area_Type='ATS']"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Signalisation_Area">
	<xsl:variable name="sig" select="@ID"/>
	<xsl:variable name="var" select="substring-after(//ATS[ATS_Area_ID=current()/@ID]/@Name,'ATS_')"/>
	<xsl:variable name="atseq" select="//ATS_Equipment[(Signalisation_Area_ID=$sig or */Signalisation_Area_ID=$sig)]/@Name"/>
	<xsl:variable name="tt" select="concat('OPCClient_UA_FEP_',$var,'_Maintenance')"/>
	<Object name="{$tt}" rules="update_or_create">
		<Properties>
			<Property name="Server1Url" dt="string">opc.tcp://<xsl:value-of select="$atseq[1]"/>:50018</Property>
			<Property name="Server2Url" dt="string">opc.tcp://<xsl:value-of select="$atseq[2]"/>:50018</Property>
			<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$tt"/></Property>
			<Property name="SharedOnIdentifierDefinition" dt="boolean">true</Property>
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>