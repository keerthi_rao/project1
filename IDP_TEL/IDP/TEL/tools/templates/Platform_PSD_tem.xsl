<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
		<Objects>
			<xsl:apply-templates select="$sysdb//Platform[@ID=//Stopping_Area[sys:sigarea/@id=$SERVER_ID  and Original_Area_List/Original_Area/@Original_Area_Type='Platform']/Original_Area_List/Original_Area/@ID]"/>	
		</Objects>		
	</Class>
</%block>	
	
<xsl:template match="Platform">
	<xsl:variable name="id" select="@ID"/>
	<xsl:apply-templates select="//Platform_Screen_Doors[@ID=current()/PSD_ID]"/>
</xsl:template>

<xsl:template match="Platform_Screen_Doors">
			<xsl:variable name="TTName" select="@Name"/>
			<xsl:variable name="bst" select="substring-after(@Name,'PSD_')"/>
 			${obj('{$TTName}',[('.Event.DO','Platform %s1: Doors Opened Status','0','ImmediateDeparture'),
				   ('.Event.DC','Platform %s1: Doors are Closed','1','ImmediateDeparture')
])} 
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,stpv,opv in list:
		<Object name="${nm}${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
		<xsl:variable name="eqid" select="concat($bst,'/SIG/ATS')"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>		
				${prop('AlarmInhib','1','boolean')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$bst"/></Property>	
				<Property name="Name_PV_Trig" dt="string">PSDS_<xsl:value-of select="$TTName"/></Property>
				<Property name="Out_PV_Trig" dt="string">${opv}</Property>			
				<Property name="State_PV_Trig" dt="i4">${stpv}</Property>			
				<Property name="Operator" dt="string"><xsl:value-of select="current()/sys:terr/@name"/>.TAS</Property>		
				<Property name="Severity" dt="i4">-</Property>				
				${multilingualvalue('$TTName','Name')}				
				${multilingualvalue('$eqid','EquipmentID')}			
				${multilingualvalue("'-'",'Avalanche')}			
				${multilingualvalue("'-'",'MMS')}			
				${multilingualvalue("'O'",'OMB')}									
				${multilingualvalue("'-'",'Value')}		
			</Properties>
	
		</Object>
	% endfor
</%def>
