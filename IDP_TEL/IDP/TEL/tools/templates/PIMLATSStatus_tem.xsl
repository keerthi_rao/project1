<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "PIMLATSStatus"
%>
<%block name="classes">
	<Class name="PIMLATSStatus">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signalisation_Area[Area_Type='ATS_Local_Server' and sys:sigarea/@id=$SERVER_ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area">
	<Object name="{concat('LATSStatus_',@ID)}" rules="update_or_create">
		<Properties>
			<Property name="OPCClientRef" dt="string"><xsl:value-of select="concat('Split_',$SERVER_ID)"/></Property>
			<Property name="OPCTag" dt="string"><xsl:value-of select="concat('OTHERATSTEL.ATS_',@ID,'.State.iEqpState')"/></Property>
		</Properties>
	</Object>
</xsl:template>
