<%inherit file="/Object_tem.mako"/>
<%! class_ = "RSTOMRequestBridge" %>

<%block name="classes">
	<Class name="RSTOMRequestBridge">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="trainID" select="@ID"/>
	<Object name="{$TrainName}.RSTOMRequestBridge" rules="update_or_create">
	  <Properties>
		${prop('TrainNumber','<xsl:value-of select="@ID"/>','i4')}
		${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
	  </Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
