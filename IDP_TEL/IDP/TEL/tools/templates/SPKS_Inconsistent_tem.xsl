<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]"/>	
			<xsl:apply-templates select="$sysdb//SPKS_Actuator[Type='Global_Availability' and SPKS_ID_List/SPKS_ID=//SPKS[sys:sigarea/@id=$SERVER_ID]/@ID]"/>
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="SPKS">
	<xsl:variable name="spksvar" select="if(contains(@Name,'ESP')) then ('ESP') else 
										 if(contains(@Name,'ESS')) then ('ESS') else	
										 if(contains(@Name,'CDBD')) then ('CDBD') else null
										"/>
										
	<xsl:variable name="ttsuffix" select="concat(@Name,'.Alarm.SPKS.',$spksvar,'INCONSISTENT')"/>											
	<xsl:variable name="TTName" select= "@Name"/>
	<xsl:variable name="nm_pv_trig" select= "@Name"/>
	<xsl:variable name="terrname" select= "sys:terr/@name"/>
	<xsl:variable name="objname" select= "concat(@Name,'.Alarm.SPKS.',$spksvar,'INCONSISTENT')"/>
    <xsl:variable name="uname" select="concat(substring-after(//Technical_Room[@ID=current()/Technical_Room_ID]/@Name,'_'),'/SIG/CBI/',@Name)"/>
    	
			<xsl:if test="(contains($TTName,'_ESP'))">
					${obj('{$objname}',[('ESP %s1 Circuit Failed (ESP Activated While Corresponding Global SPKS Is Not Activated)','1','0','ConsistencyEqpState')])}
			</xsl:if>
			<xsl:if test="contains($TTName,'_ESS')">
					${obj('{$objname}',[('ESS %s1 Circuit Failed (ESS Activated While Corresponding Global SPKS Is Not Activated)','1','0','ConsistencyEqpState')])}
			</xsl:if>		
			<xsl:if test="contains($TTName,'_CDBD')">
					${obj('{$objname}',[('CDBDKS %s1 Circuit Failed (CDBDKS Activated While Corresponding Global SPKS Is Not Activated)','1','0','ConsistencyEqpState')])}
			</xsl:if>		
			<xsl:if test="not($spksvar or contains(@Name,'SPKS_TT_MDD_ESPB')) and Type='Paired'">
					<xsl:variable name="tt6" select="concat(@Name,'.Alarm.SPKS.SPKSINCONSISTENT')"/>
					${obj('{$tt6}',[('SPKS %s1 Circuit Failed (SPKS Activated While Corresponding Global SPKS Is Not Activated)','1','0','ConsistencyEqpState')])}
			</xsl:if>				
</xsl:template>


<xsl:template match="SPKS_Actuator" >
	<xsl:variable name="spkstrr" select="//SPKS[@ID=//SPKS_Actuator/SPKS_ID_List/SPKS_ID and sys:sigarea/@id=$SERVER_ID]"/>
    <xsl:variable name="uname" select="concat(substring-after(//Technical_Room[@ID=$spkstrr[1]/Technical_Room_ID]/@Name,'_'),'/SIG/CBI/',@Name)"/>
	<xsl:variable name="tt7" select="concat(@Name,'.Alarm.SPKS.SPKSAINCONSISTENT')"/>
	<xsl:variable name="nm_pv_trig" select= "concat(@Name,'.Consistency.Template')"/>
	<xsl:variable name="TTName" select= "@Name"/>
	${obj('{$tt7}',[('Global SPKS %s1 Circuit Failed (Global SPKS Activated While None Of Its Associated SPKS Is Activated)	','1','0','iEqpState')])}
			
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for description,srvty,statepv,outpv in list:
		<Object name="${nm}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${description}'"/></Property>			
				${multilingualvalue("'-'",'Avalanche')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>			
				${multilingualvalue('$TTName','Name')}			
				${multilingualvalue('$uname','EquipmentID')}		
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nm_pv_trig"/></Property>
				<Property name="Out_PV_Trig" dt="string">${outpv}</Property>
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${statepv}"/></Property>					
				${multilingualvalue("'B'",'OMB')}			
				<Property name="Severity" dt="i4"><xsl:value-of select="${srvty}"/></Property>		
				${multilingualvalue("'-'",'Operator')}
				${multilingualvalue("'Activated'",'Value_on')}			
				${multilingualvalue("'Normal'",'Value_off')}	
			</Properties>
		</Object>
	% endfor
</%def>
