<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "TrainRestrictionArea"
%>
<%block name="classes">
	<Class name="TrainRestrictionArea">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signalisation_Area[@ID=$SERVER_ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area">
	<xsl:variable name="tt" select="concat('GenericArea_',@Name,'.TRA')"/>

	
	<Object name="GenericArea_{@Name}.TRA" rules="update_or_create">
		<Properties>
			<Property name="SharedOnIdentifier" dt="string">GenericArea_<xsl:value-of select="@Name"/>.TRA</Property>
			<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
			<Property name="GenericAreaID" dt="string">GenericArea_<xsl:value-of select="@Name"/></Property>
	        ${multilingualvalue("$tt", "Name")}
			<!--TEL has increasing KP-->
			<xsl:variable name="fstblock">
				<xsl:for-each select="Block_ID_List/Block_ID">
					<xsl:sort select="number(//Block[@ID=current() and sys:sigarea/@id=$SERVER_ID]/Kp_Begin)" />
					<xsl:if test="position() = 1">
						<xsl:value-of select="//Block[@ID=current()]/@Name"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable> 
			<xsl:variable name="lastblock">
				<xsl:for-each select="Block_ID_List/Block_ID">
					<xsl:sort select="number(//Block[@ID=current() and sys:sigarea/@id=$SERVER_ID]/Kp_Begin)" order="descending"/>
					<xsl:if test="position() = 1">
						<xsl:value-of select="//Block[@ID=current()]/@Name"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:variable> 
			
		${multilingualvalue("$fstblock", "FirstBlock")}
		${multilingualvalue("$lastblock", "LastBlock")}			
		</Properties>
	</Object>
</xsl:template>	
	



## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
