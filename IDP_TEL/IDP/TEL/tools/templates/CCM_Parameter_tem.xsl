<%inherit file="/Object_tem.mako"/>
<%! class_ = "CCM_Parameter" %>

<%block name="classes">
	<Class name="CCM_Parameter">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="shared" select="concat($TrainName, '.Attributes')"/>
	  <Object name="{$TrainName}.CCM_Param" rules="update_or_create">
		  <Properties>
			  ${prop('Shared','<xsl:value-of select="$shared"/>')}
			  ${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
			  ${multilingualvalue("$shared")} 
		  </Properties>
	  </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
