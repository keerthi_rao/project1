<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
		<Objects>
			<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and contains(@Name,'LXGB')]"/>	
			<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]" />
		   <xsl:apply-templates select="//LX_Device[(starts-with(@Name, 'LX3_WL') or (starts-with(@Name, 'LX3_H'))) and   Type='Aspect Indicator' and  LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID]"/>

		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Generic_ATS_IO">
			<xsl:variable name="TTName" select="substring-after(@Name,'GAIO_LXGB_')"/>
			<xsl:variable name="objname" select="concat(@Name,'.EventBypass')"/>
			<xsl:variable name="uname" select="concat(substring-after(//Urbalis_Sector[ATS_Area_ID_List/Signalisation_Area_ID =current()/ATS_Signalisation_Area_ID]/@Name,'_'),'/SIG/CBI')"/>
			<xsl:variable name="optr" select="//Signalisation_Area[@ID=current()/ATS_Signalisation_Area_ID][1]/sys:terr/@name"/>
			<Object name="{$objname}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string">Level crossing %s1 bypassed</Property>			
					${prop('AlarmInhib','1','boolean')}			
					<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>							
					${multilingualvalue('$TTName','Name')}						
					${multilingualvalue('$uname','EquipmentID')}	
					${multilingualvalue("'-'",'MMS')}						
					<Property name="Name_PV_Trig" dt="string">GATSM_<xsl:value-of select="@Name"/></Property>				
					<Property name="Out_PV_Trig" dt="string">Value</Property>	
					<Property name="State_PV_Trig" dt="i4">1</Property>	
					<Property name="Operator" dt="string"><xsl:value-of select="$optr[1]"/>.TAS</Property>			
			    	${multilingualvalue("'O'",'OMB')}					
			    	<Property name="Severity" dt="i4">-</Property>		
					${multilingualvalue("'-'",'Value')}					
				</Properties>
			</Object>
</xsl:template>
<xsl:template match="LX">
	<xsl:variable name="blkid" select="//Block[sys:sigarea/@id=$SERVER_ID and  @ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID]/@ID"/>
	<xsl:variable name="te" select="//Technical_Room[@ID=//Secondary_Detection_Device[@ID=//Block[@ID=$blkid and  sys:sigarea/@id=$SERVER_ID]/Secondary_Detection_Device_ID and sys:sigarea/@id=$SERVER_ID]/Technical_Room_ID]/@Name"/>
	<xsl:variable name="cls" select="local-name()"/>
	<xsl:apply-templates select="//Events/Event[Type='Level_Crossing']">
		<xsl:with-param name="parentname" select = "@Name" />
		<xsl:with-param name="blockid" select = "$blkid" />
		<xsl:with-param name="te" select="$te" />
		<xsl:with-param name="cls" select="$cls" />
		<xsl:with-param name="bstrparam1" select="@Name" />
	</xsl:apply-templates>
</xsl:template>
<xsl:template match="LX_Device">
	<xsl:variable name="blkid" select="//Block[sys:sigarea/@id=$SERVER_ID and  @ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=//LX[@ID=current()/LX_ID]/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID]/@ID"/>
	<xsl:variable name="te" select="//Technical_Room[@ID=//Secondary_Detection_Device[@ID=//Block[@ID=$blkid and  sys:sigarea/@id=$SERVER_ID]/Secondary_Detection_Device_ID and sys:sigarea/@id=$SERVER_ID]/Technical_Room_ID]/@Name"/>
	<xsl:variable name="cls" select="local-name()"/>
	<xsl:apply-templates select="//Events/Event[Type='Level_Crossing_Device']">
		<xsl:with-param name="parentname" select = "@Name" />
		<xsl:with-param name="blockid" select = "$blkid" />
		<xsl:with-param name="te" select = "$te" />
		<xsl:with-param name="cls" select = "$cls" />
		<xsl:with-param name="bstrparam1" select="//LX[@ID=current()/LX_ID]/@Name"/>
	</xsl:apply-templates>
</xsl:template>


<xsl:template match="Event">
	<xsl:param name = "parentname" />
	<xsl:param name = "blockid" />
	<xsl:param name = "te" />
	<xsl:param name = "cls" />
	<xsl:param name = "bstrparam1" />
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="cond" select="if ($nm='.Event.LCFLNA' or $nm='.Event.LCFLA') then (starts-with($parentname,'LX3_WL')) else if ($nm='.Event.LCAWNA') then (starts-with($parentname,'LX3_H')) else (true())"/>
	<xsl:variable name="clsnam" select="concat('--',$cls,'.Name--')"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (replace(Name_PV_Trig,$clsnam,$parentname)) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$bstrparam1),'xxxx',substring-after($te,'_'))"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="terrname" select="//Block[@ID=$blockid and sys:sigarea/@id=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="tracktype" select="//Track[@ID=//Block[@ID=$blockid and sys:sigarea/@id=$SERVER_ID]/Track_ID]/Track_Type"/>
	<xsl:variable name="optr" select="if($tracktype='Depot') then ('MDD') else if($tracktype='Test track') then ('TTCU') else if($tracktype='Mainline') then ('TAS') else null"/>
	<xsl:if test="$cond">
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrparam1"/></Property>
			<Property name="BstrParam2" dt="string"><xsl:value-of select="BstrParam2"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname[1],'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue('$parentname','Name')}	
		</Properties>
	</Object>
	</xsl:if>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
