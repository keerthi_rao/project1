<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
	     <Objects>
			<xsl:apply-templates select="$sysdb//ZCs/ZC[@ID=$ZC_ID]"/>	
		 </Objects>
	</Class>
</%block>

<xsl:template match="ZC">
			<xsl:variable name="TTName" select= "concat('ZCS_',@ID,'.Event.')"/>
			<xsl:variable name="id" select="concat('ZCS_',@ID)"/>
			<xsl:variable name="Eqid" select="concat($id,'/SIG/ATC')"/>
			<xsl:variable name="optr" select="//Signalisation_Area[@ID=current()/ZC_Area_ID]/sys:terr/@name"/>
			${obj('{$TTName}',[('DMF','Sector %s1: Global GAMA Removed: All Train Movements In AM Or PM Driving Mode Forbidden','0','.Status'),
				   ('DMG','Sector %s1: Global GAMA Authorized: All Train Movements In AM Or PM Driving Mode authorised','1','.Status'),
				   ('NoVDMF','Sector %s1: Global NoV GAMA Removed: All Train Movements In AM Driving Mode Forbidden','0','.NOVStatus'),
				   ('NoVDMG','Sector %s1: Global NoV GAMA Authorized: All Train Movements In AM Driving Mode Granted','1','.NOVStatus')
				   ])}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

<%def name="obj(nm,list)">
	% for key, description,pv,bl in list:
		<Object name="${nm}${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$id','Name')}
				${multilingualvalue('$Eqid','EquipmentID')}
				${multilingualvalue("'-'",'MMS')}
				${multilingualvalue("'-'",'Value')}
				${multilingualvalue("'O'",'OMB')}
				${prop('AlarmInhib','1','boolean')}
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$id"/></Property>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$id"/>${bl}</Property>
				<Property name="Operator" dt="string"><xsl:value-of select="$optr"/>.TAS</Property>
				<Property name="Severity" dt="i4">-</Property>
				<Property name="State_PV_Trig" dt="i4">${pv}</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
			</Properties>
		</Object>
	% endfor
</%def>
