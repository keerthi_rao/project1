<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
		<Class name="S2KAlarms_Gen">
			<Objects>
				<xsl:apply-templates select="//MSS_lruState/lruState[(contains(@Name,'MDDSERSW') or contains(@Name,'SERSW'))  and  Type='Switch' and Signalisation_Area_ID=$SERVER_ID and (contains(//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/Localisation_Type,'Depot') or contains(//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/Localisation_Type,'Mainline'))]"/>
			</Objects>
		</Class>
</%block>


<xsl:template match="lruState">
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="terr" select="//Signalisation_Area[@ID=$SERVER_ID]/sys:terr/@name"/>
	<xsl:variable name="uname" select="concat(substring-after($terr[1],'Territory_'),'/SIG/MS/',@Name)"/>
	<xsl:variable name="tt1" select="$nm"/>
	${obj('{$nm}',[('.Alarm.MS_Server_NetworkAccess','Maintenance System Server Network Access Failed','.Status.PV_Status')])}
</xsl:template>

<%def name="obj(nm,list)">
	% for key, alarm_labl,nmpv in list:
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>
				<xsl:variable name="bstr3" select="'${key}'"/>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$uname','EquipmentID')}
				${multilingualvalue("'M'",'OMB')}
				<Property name="Alarm_Label" dt="string">${alarm_labl}</Property>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nm"/>${nmpv}</Property>
				<Property name="Severity" dt="i4">2</Property>
				<Property name="State_PV_Trig" dt="i4">2</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
			</Properties>
		</Object>
	% endfor
</%def>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
