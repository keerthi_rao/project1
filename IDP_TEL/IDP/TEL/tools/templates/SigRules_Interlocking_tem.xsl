<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
	<xsl:apply-templates select="$sysdb//CBI[@ID=$CBI_ID]"/>
</%block>


<xsl:template match="CBI">
<%
  tt = "<xsl:value-of select='@Name'/>"
%>
	  <Equipment name="CBIS_{$CBI_ID}" type="Interlocking" flavor="Monitor">
	    <Requisites>
	      <Equation>MAP_${tt}#0[NO_MONITREQ]::0</Equation>
	      <Equation>MAP_${tt}[MONITREQ]::1</Equation>
	      <Equation>IMAP_${tt}#0[NO_INHIBITEREQ]::1</Equation>
	      <Equation>IMAP_${tt}[INHIBITEREQ]::0</Equation>
	    </Requisites>
	  </Equipment>	   
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
