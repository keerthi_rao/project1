<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMIStablingLocation" %>

<%block name="classes">
	<Class name="HMIStablingLocation">
	     <Objects>
			 <xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Stabling']"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Stopping_Area">
	  <xsl:variable name="stl" select="//Stabling_Location[@ID= current()/Original_Area_List/*/@ID and Stabling_Type='Depot' and current()/Original_Area_List/*/@Original_Area_Type='Stabling']"/>
	  <Object name="{@Name}.HMIStablingLocation" rules="update_or_create">
		  <Properties>	
				<xsl:variable name="name" select="concat(@Name,'.HMIStablingLocation')"/>
				<xsl:variable name="stbloccustname" select="HMICustomerPlatform"/>		
				<xsl:variable name="stbltcname" select="//Secondary_Detection_Device[@ID=//Block[@ID=current()/sys:Block_ID_List/sys:Block_ID]/Secondary_Detection_Device_ID]/@Name"/>					
				<xsl:variable name="stbllocbname" select="concat('TI_',//Block[@ID=current()/sys:Block_ID_List/sys:Block_ID]/@Name)"/>
				${multilingualvalue('$name')}	
				${multilingualvalue('$stbloccustname','StablingLocationCustomerName')}	
				${multilingualvalue('$stbltcname','StablingLocationTrackCircuitName')}	
				${multilingualvalue('$stbllocbname','StablingLocationBerthName')}	
		  </Properties>
	  </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>