<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KNumericProcessVariable" %>

<%block name="classes">
		<Class name="S2KNumericProcessVariable">
			<Objects>
					<xsl:apply-templates select="//ATS_Equipment[ATS_Equipment_Type='Maintenance Server' and (Function_ID_List/Function_ID=$FUNCTION_ID or Signalisation_Area_ID=$SERVER_ID)]"/>
			</Objects>
		</Class>
</%block>


<xsl:template match="ATS_Equipment">
<xsl:variable name="nm" select="@Name"/>
	<xsl:for-each select="//Urbalis_Sector[ATS_Area_ID_List/Signalisation_Area_ID!='']">
		<xsl:variable name="tt1" select="concat($nm,'.FEP_',current()/@Name,'.TotalCom')"/>
		<xsl:variable name="ttA" select="concat($nm,'.FEP_',current()/@Name,'.LinkA')"/>
		<xsl:variable name="ttB" select="concat($nm,'.FEP_',current()/@Name,'.LinkB')"/>
		<xsl:variable name="plug" select="if (ends-with($nm,'A')) then '1' else '2'"/>
		<xsl:variable name="connected_var" select="SMS.TEL.OPCClient_FEP_.Connected"/>	
		<Object name="{$tt1}" rules="update_or_create">
			<Properties>
				<Property name="AddressInLevel1" dt="string"><xsl:value-of select="concat('SMS.TEL.Merge_OPCDA_FEP_',current()/@Name,'Merge.Merge.IntOutputPlug_',$plug)"/></Property>
				${multilingualvalue('$tt1')}
			</Properties>
		</Object>
		<Object name="{$ttA}" rules="update_or_create">
			<Properties>
				<Property name="AddressInLevel1" dt="string"><xsl:value-of select="$connected_var"/></Property>
				${multilingualvalue('$ttA')}
			</Properties>
		</Object>
		<Object name="{$ttB}" rules="update_or_create">
			<Properties>
				<Property name="AddressInLevel1" dt="string"><xsl:value-of select="$connected_var"/></Property>
				${multilingualvalue('$ttB')}
			</Properties>
		</Object>		
	</xsl:for-each>
</xsl:template>




## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

