<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
  	<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
</%block>

<!-- Template for generating equations from Platforms Table-->
<xsl:template match="Platform_Screen_Doors">
		${Equipment('PSDTimeSyncModule','TimeSyncStatus',[('TIMESYNC_NOTRECIEVED[0]','(PSDSTATUS1_<xsl:value-of select="@Name"/>.S2KNumericProcessVariable &amp; 256) == 0'),
			   ('TIMESYNC_RECEIVED[1]','(PSDSTATUS1_<xsl:value-of select="@Name"/>.S2KNumericProcessVariable &amp; 256) &gt; 0'),
			   ('TIMESYNC_UNKNOWN[2]','1')], [], 'N', 'Y')}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="Equipment(type, flavor, list, diflist, isdiff, isstate)" >
  <Equipment name="PSDTimeSyncModule_{@Name}" type="${type}" flavor="${flavor}">
	% if isdiff == 'Y':
		<xsl:attribute name="AlarmTimeOut">0</xsl:attribute>
	% endif
	% if isstate == 'Y':
	<States>
		% for name,equation in list:
		 <Equation>${name}::${equation}</Equation>
		% endfor
	</States>
	% endif
	% if isdiff == 'Y':
	<DifferedAlarms>
		% for name,equation in diflist:
		 <Equation>${name}::${equation}</Equation>
		% endfor
	</DifferedAlarms>
	% endif
  </Equipment>
</%def>