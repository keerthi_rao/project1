<%inherit file="/Object_tem.mako"/>
<%! class_ = "PSDTimeSyncModule" %>

<%block name="classes">
  	<Class name="PSDTimeSyncModule">
  		<Objects>
  			<!-- Generate the items of Platform_Screen_Doors table -->
  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
  		</Objects>
  	</Class>
</%block>

<!-- Template to match the Platform table -->
<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="sta" select="concat('PSDTimeSyncModule_', @Name)"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="sigAreaType" select="//Signalisation_Area[@ID=$SERVER_ID]/Area_Type"/>
	<xsl:variable name="CentralServer" select="//Signalisation_Area[sys:sigarea/@id=$SERVER_ID and $sigAreaType='ATS_Local_Server' and Area_Type ='ATS_Central_Server']"/>
	<xsl:variable name="CATSStatusPVName" select="if($CentralServer) then concat('CATSStatus_',$CentralServer/@ID) else null"/>
	<xsl:variable name="serType" select="if ($sigAreaType='ATS_Central_Server') then 'CATS' else if ($sigAreaType='ATS_Local_Server') then 'LATS' else null"/>
	<xsl:variable name="pimStationIDs">
		<xsl:apply-templates select="//PIMStation[Signalisation_Area=$SERVER_ID]"><xsl:sort select="@ID"/></xsl:apply-templates>
	</xsl:variable>
	<Object name="{$sta}" rules="update_or_create">
		<Properties>
			<!-- MAKO to generate properties -->
			${props([('DayMonthPVRef', 'string' ,'DAYMONTH_<xsl:value-of select="@Name"/>'),
					 ('FlagPVRef', 'string', 'FLAG_<xsl:value-of select="@Name"/>'),('HourMinutePVRef', 'string', 'HOURMINUTE_<xsl:value-of select="@Name"/>'),
					 ('SecondNonePVRef', 'string', 'SECONDNONE_<xsl:value-of select="@Name"/>'),('Year1Year2PVRef', 'string', 'YEAR1YEAR2_<xsl:value-of select="@Name"/>')])}
			<xsl:if test="$serType='LATS' and $CentralServer">
				${props([('CATSStatusPVName', 'string' ,'<xsl:value-of select="$CATSStatusPVName"/>')])}
			</xsl:if>
			${multilingualvalue('$nm')}
		</Properties>
	</Object>
</xsl:template>
<xsl:template match="PIMStation"><xsl:value-of select="@ID"/>;</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,props"/>