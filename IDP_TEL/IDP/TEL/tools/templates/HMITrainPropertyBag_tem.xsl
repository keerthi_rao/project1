<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMITrainPropertyBag" %>



<%block name="classes">
	<Class name="HMITrainPropertyBag">
		<Objects>
			<!-- Call template for generating object-->
			<xsl:apply-templates select="//Train_Unit"/>
		</Objects>
	</Class>
</%block>

<!-- Template for generating object-->
<xsl:template match="Train_Unit">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="TrainName">Train<xsl:value-of select="format-number(@ID,'000')"/></xsl:variable>
	<Object name="{concat($TrainName,'.CONSISTDESC')}" rules="update_or_create">
			<Properties>
				<Property name="PropertyName" dt="string">CONSISTDESC</Property>
				<Property name="PropertyType" dt="string">bstr</Property>
				<Property name="PropertyValue" dt="string"></Property>				
				<Property name="TrainName" dt="string"><xsl:value-of select="$TrainName"/></Property>				
			</Properties>
	</Object>
	<Object name="{concat($TrainName,'.TrainWashMode')}" rules="update_or_create">
			<Properties>
				<Property name="PropertyName" dt="string">TrainWashMode</Property>
				<Property name="PropertyType" dt="string">long</Property>
				<Property name="PropertyValue" dt="string">0</Property>				
				<Property name="TrainName" dt="string"><xsl:value-of select="$TrainName"/></Property>				
			</Properties>
	</Object>	
	<Object name="{concat($TrainName,'.TrainType')}" rules="update_or_create">
			<Properties>
				<Property name="PropertyName" dt="string">TrainType</Property>
				<Property name="PropertyType" dt="string">bstr</Property>
				<Property name="PropertyValue" dt="string"><xsl:value-of select="if(current()/Train_Unit_Characteristics_ID='1') then ('Train') else if(current()/Train_Unit_Characteristics_ID='2') then ('Loco') else null"/></Property>				
				<Property name="TrainName" dt="string"><xsl:value-of select="$TrainName"/></Property>				
			</Properties>
	</Object>
	<Object name="{concat($TrainName,'.UnsignalledAreaLocation')}" rules="update_or_create">
			<Properties>
				<Property name="PropertyName" dt="string">UnsignalledAreaLocation</Property>
				<Property name="PropertyType" dt="string">bstr</Property>
				<Property name="PropertyValue" dt="string"></Property>				
				<Property name="TrainName" dt="string"><xsl:value-of select="$TrainName"/></Property>				
			</Properties>
	</Object>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>
