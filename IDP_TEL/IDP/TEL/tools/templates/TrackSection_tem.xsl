<%inherit file="/Object_tem.mako"/>
<%! 
  class_ = "TrackSection"
  add_alias_function = True
%>


<%block name="classes">
	<xsl:variable name="togenerate" select="if (($sysdb//Secondary_Detection_Device[sys:cbi/@id=$CBI_ID]) or ($sysdb//Adjacent_Monitoring_Area[ATS_Central_Server_Area_ID=//Signalisation_Area[(Area_Type='ATS_Central_Server') and (sys:cbi/@id=$CBI_ID)]/@ID or ATS_Local_Server_Area_ID=//Signalisation_Area[(Area_Type='ATS_Local_Server') and (sys:cbi/@id=$CBI_ID)]/@ID])) then 'Yes' else 'No'"/>
	<xsl:if test="$togenerate = 'No'">
		<xsl:result-document href="TrackSection_{$CBI_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
    <Class name="TrackSection">
      <Objects>
		<xsl:apply-templates select="$sysdb//Secondary_Detection_Device[sys:cbi/@id=$CBI_ID]"/>
      </Objects>
    </Class>
</%block>


<xsl:template match="Secondary_Detection_Device">
	<xsl:variable name="name" select="@Name"/>
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="sdd_variable" select="$sysdb/Secondary_Detection_Devices_Variable/Secondary_Detection_Device[@ID = current()/@ID]"/>
	<Object name="{$name}" rules="update_or_create">
		<Properties>
		<xsl:variable name="sdd" select="concat('SDD ',@Name,' Set As Out Of Operation')"/>
			${multilingualvalue('$sdd','SDOperationalStatusAlarmLabel')}	
		</Properties>    
    </Object>
</xsl:template>




## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%namespace file="/lib_tem.mako" import="multilingualvalue,prop"/>


