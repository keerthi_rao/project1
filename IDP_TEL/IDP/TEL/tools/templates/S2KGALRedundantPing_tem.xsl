<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KGALRedundantPing" %>

<%block name="classes">
	<xsl:variable name="filegeneration" select="if (//ATS_Equipment[ATS_Equipment_Type='Gateway' and (Function_ID_List/Function_ID=$FUNCTION_ID or Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID)])  then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="S2KGALRedundantPing_{$FUNCTION_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="S2KGALRedundantPing">
		<Objects>
			<xsl:apply-templates select="//ATS_Equipment[ATS_Equipment_Type='Gateway' and (Function_ID_List/Function_ID=$FUNCTION_ID or Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID)]"/>
		</Objects>
	</Class>
</%block>


<xsl:template match="ATS_Equipment">
<xsl:variable name="nm" select="@Name"/>
	<Object name="ATS_{$SERVER_ID}.GTW.{@Name}.RedundantPing" rules="update_or_create">
		<Properties>
			<Property name="Address1" dt="string"><xsl:value-of select="IP_Address_Light_Grey"/></Property>	
			<Property name="Address2" dt="string"><xsl:value-of select="IP_Address_Dark_Grey"/></Property>	
			<Property name="Host1Name" dt="string"><xsl:value-of select="@Name"/></Property>	
			<Property name="WMIClientIdentifier" dt="string"><xsl:value-of select="if(contains(@Name,'A')) then ('S2KGALServer1MainWMIClientIdentifier') else ('S2KGALServer2MainWMIClientIdentifier')"/></Property>	
			${multilingualvalue('$nm','Name')}	
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>