<%inherit file="/Group_tem.mako"/>

<%block name="classes">
	<Class name="LCClient" rules="update" traces="error">
		<Objects>
			<xsl:apply-templates select="//ATC_Equipments/ATC_Equipment[ATC_Equipment_Type = 'LC' and @ID =$LC_ID]"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="ATC_Equipment">
	<xsl:variable name="id" select="@ID"/>
			<Object name="LCS_{$id}" rules="update" traces="error">
				<Properties>
					<Property name="UserGroup" dt="string">User/A</Property>				
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_group_tem.mako" import="group_object_template"/>