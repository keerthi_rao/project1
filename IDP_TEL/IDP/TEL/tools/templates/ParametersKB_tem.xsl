<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sy="http://www.systerel.fr/test" exclude-result-prefixes="sy">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  
<xsl:param name="DATE"/>

<xsl:template match="/">
<VALUES xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="Parameter.xsd" Generation_Date="{$DATE}" name="Parameter" version="1.0.0" Project="{//Project/@Name}" >
    ${versions_xml()}
    <Param name="DisappearedTrainTimer" value="10"  class="MainTracker"/>
	<Param name="CheckOverride" value="Yes"  class="MainTracker"/>
	<Param name="InitializedWithFO" value="Yes"  class="MainTracker"/>
	<Param name="TCCheckOutTimer" value="300"  class="MainTracker"/>
</VALUES>	
</xsl:template>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->




<%namespace file="/lib_tem.mako" import="versions_xml"/>