<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<RegulationPoints>
    <xsl:apply-templates select="//RegulationPoint"/>
</RegulationPoints>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>

 <xsl:function name="sys:eqpt_ord">
    <xsl:param name="e"/>
    <xsl:variable name="origin_types" select="$e/sys:type"/>
    <xsl:choose>
		<xsl:when test="$origin_types='Platform'">1</xsl:when>
		<xsl:when test="$origin_types='Stabling'">2</xsl:when>
		<xsl:when test="$origin_types='Transfer Track'">3</xsl:when>
		<xsl:when test="$origin_types='Washing'">4</xsl:when>
		<xsl:when test="$origin_types='Isolation Area'">5</xsl:when>
		<xsl:when test="$origin_types='Coupling Uncoupling'">6</xsl:when>
		<xsl:when test="$origin_types='Change Of Direction Area'">7</xsl:when>
		<xsl:otherwise>	
		   <xsl:message terminate="yes">Unknown equipment type : '<xsl:value-of select="$origin_types"/>'.</xsl:message>
		</xsl:otherwise>
    </xsl:choose>
 </xsl:function>

<xsl:template match="RegulationPoint">
<xsl:variable name="regul_pt_id" select="@ID"/>
<xsl:variable name="regul_tk_list" select="TrackPortions_List"/>
<xsl:variable name="sa_ordered" as="element(RegulationPoint)+">
	<xsl:perform-sort select="//RegulationPoint[TrackPortions_List=$regul_tk_list]" >
		<xsl:sort select="sys:eqpt_ord(.)"/>
    </xsl:perform-sort>
</xsl:variable>
<xsl:if test="$regul_pt_id=$sa_ordered[1]/@ID">
	<xsl:copy copy-namespaces="no">
		<xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
</xsl:if>	
</xsl:template>

<xsl:template match="sys:type"/>

<xsl:template match="@* | node()">
	<xsl:copy copy-namespaces="no">
		<xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="ori_area_type(param, type)">
    <xsl:choose>
        <xsl:when test="Original_Area_List/Original_Area/@Original_Area_Type='${type}'">
            <${param}>true</${param}>
        </xsl:when>
        <xsl:otherwise>
            <${param}>false</${param}>
        </xsl:otherwise>
    </xsl:choose> 
</%def>

<%namespace file="lib_tem_tel.mako" import="copy"/>
