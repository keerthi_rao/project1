<%inherit file="/Object_tem.mako"/>
<%! class_ = "HoldJustif" %>

<%block name="classes">
	<Class name="HoldJustif">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="trainID" select="@ID"/>
	<Object name="{$TrainName}.HoldJustif" rules="update_or_create">
	  <Properties>
		${prop('Shared','<xsl:value-of select="$TrainName"/>.ProjAttributes1')}
		${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
		<xsl:variable name="mulval" select="concat($TrainName, '.Attributes')"/>
		${multilingualvalue("$mulval")}
	  </Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>
