<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
		<Objects>
			<xsl:apply-templates select="$sysdb//Platform[@ID=//Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']/Original_Area_List/Original_Area/@ID][1]"/>	
		</Objects>		
	</Class>
</%block>	
	
<xsl:template match="Platform">
	<xsl:variable name="id" select="@ID"/>
	<xsl:apply-templates select="//Stopping_Area[sys:sigarea/@id=$SERVER_ID and $id=Original_Area_List/Original_Area/@ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']">
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Stopping_Area">
			<xsl:variable name="TTName" select= "@Name"/>
			<xsl:variable name="psd" select="substring-after(//Platform_Screen_Doors[@ID=//Platform[@ID=current()/Original_Area_List/Original_Area/@ID  and current()/Original_Area_List/Original_Area/@Original_Area_Type='Platform'][1]/PSD_ID]/@Name,'PSD_')"/>
			${obj('{$TTName}',[('.Event.HR','Platform %s1: Platform Hold Released','.HoldSkip.HSMPoint','OperatorHold','0'),
				   ('.Event.HS','Platform %s1: Platform Hold Set','.HoldSkip.HSMPoint','OperatorHold','1'),
				    ('.Event.IDA','Platform %s1: Immediate Departure Applied','.ATR.RegPoint','ImmediateDeparture','1')
])}
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,pv,opv,spv in list:
		<Object name="${nm}${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>		
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>		
				${prop('AlarmInhib','1','boolean')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/>${key}</Property>	
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$TTName"/>${pv}</Property>
				<Property name="Out_PV_Trig" dt="string">${opv}</Property>			
				<Property name="State_PV_Trig" dt="i4">${spv}</Property>			
				<Property name="Operator" dt="string"><xsl:value-of select="current()/sys:terr/@name"/>.TAS</Property>		
				<Property name="Severity" dt="i4">-</Property>				
				${multilingualvalue('$TTName','Name')}				
				${multilingualvalue('$TTName','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}			
				${multilingualvalue("'O'",'OMB')}									
				${multilingualvalue("'-'",'Value')}		
			</Properties>
		</Object>
	% endfor
</%def>
