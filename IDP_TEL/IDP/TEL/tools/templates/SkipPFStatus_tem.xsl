<%inherit file="/Object_tem.mako"/>
<%! class_ = "SkipPFStatus" %>

<%block name="classes">
	<Class name="SkipPFStatus">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Stopping_Area">
	<xsl:variable name="stoppingareaname" select="@Name"/>
	  <Object name="{$stoppingareaname}.HoldSkip.SkipPFStatus" rules="update_or_create">
		  <Properties>
				<xsl:variable name="nm" select="concat($stoppingareaname, '.HoldSkip')"/>
				${prop('Platform_HoldSkip','<xsl:value-of select="$nm"/>')}
				${prop('Platform_Name','<xsl:value-of select="$stoppingareaname"/>')}
		  </Properties>
	  </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>
