<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
<xsl:strip-space elements="*"/>

<xsl:key name="variables" match="CBI_ATS_Object|ATS_CBI_Object|ZC_ATS_Object|ATS_ZC_Object" use="concat(@Type_Of_Information,'+',@Object_Name)"/>

<xsl:template match="/">
	<xsl:apply-templates select="$sysdb"/>
</xsl:template>

<xsl:template match="*[count(ancestor-or-self::*)=4]">
	<xsl:copy>
		<xsl:apply-templates select="@*"/>
		${max_rm_rc('CBI')}
        ${max_rm_rc('ZC')}
		<xsl:apply-templates select="node()"/>
		<xsl:variable name="id" select="@ID"/>
		<xsl:variable name="name" select="@Name"/>
		<xsl:variable name="objname_temp" select="if (local-name() = 'Signal') then (concat(@Name,' ',$sysdb/Exit_Gates/Exit_Gate[Exit_Gate_Signal_ID = $id]/@Name)) 
												  else (if (local-name() = 'Train_Unit') then (@ID) 
												  else (if (local-name() = 'Overlap') then ($sysdb/Signals/Signal[Overlap_ID = $id]/@Name)
												  else @Name))"/>
		<xsl:variable name="localname" select="local-name()"/>
		<xsl:variable name="ats_cbi" select="$var/Interface_Variable[$localname = tokenize(@class,',')]"/>
		<xsl:for-each select="tokenize($objname_temp,' ')">
		<xsl:variable name="objname" select="."/>
		<xsl:variable name="objname_pass1" select="if($localname = 'Signal' and $sysdb/Exit_Gates/Exit_Gate[@Name = $objname]) then ($sysdb/Signals/Signal[@ID = $sysdb/Exit_Gates/Exit_Gate[@Name = $objname]/Exit_Gate_Signal_ID][1]/@Name) else ."/>
			<xsl:for-each select="$ats_cbi">
			<xsl:variable name="abbr" select="@variable"/>
			<xsl:variable name="toi" select="tokenize(@Type_Of_Information,'~')"/>
			<xsl:variable name="ats_cbi_zc_object1" select="if(count($toi)>1) then key('variables',concat($toi[1],'+',$objname)) else key('variables',concat(@Type_Of_Information,'+',$objname))"/>
			
			<xsl:variable name="ats_cbi_zc_object" select="if($ats_cbi_zc_object1) then $ats_cbi_zc_object1 else key('variables',concat($toi[2],'+',$objname))"/>
			<!--xsl:variable name="ats_cbi_zc_object" select="key('variables',concat(@Type_Of_Information,'+',$objname))"/-->
			<xsl:variable name="opctag" select="replace(@OPCTag,'Name',$objname)"/>
			<xsl:variable name="opcua_last" select="tokenize(@OPCTag,'\.')[last()]"/>
			<xsl:variable name="opcua" select="concat('[Index:NameSpace]&lt;Organizes&gt;Index:',substring-before(@OPCTag,concat('.',$opcua_last)))"/>
			<xsl:variable name="opcua_tag_temp" select="replace($opcua,'\.','&lt;Organizes&gt;Index:')"/>
			<xsl:variable name="object_name" select="if ($localname = 'Overlap') then $name else ($objname_pass1)"/>
			<xsl:variable name="object_name_opctag" select="if ($localname = 'Overlap') then $name else (if ($localname = 'Train_Unit') then concat('Train',format-number(number($objname_pass1),'000')) else $objname_pass1)"/>
			<xsl:variable name="opcua_tag" select="if ($sysdb/Projects/Project/FEP_ATS_OPCUA_Interface = 'true') then concat(replace($opcua_tag_temp,'&lt;Organizes&gt;Index:Name&lt;Organizes&gt;Index:',concat('&lt;Organizes&gt;Index:',$object_name_opctag,'&lt;Organizes&gt;Index:')),concat('&lt;HasComponent&gt;Index:',$opcua_last)) else (replace(@OPCTag,'Name',$object_name_opctag))"/>
			<xsl:variable name="chkvariablenm" select="if(@Varaiable_Name='true') then $ats_cbi_zc_object[contains(@Variable_Name, $abbr)] else $ats_cbi_zc_object"/>
			<xsl:if test="$chkvariablenm">
				<xsl:element name="{@variable}">
					<xsl:attribute name="type" select="distinct-values($ats_cbi_zc_object/local-name())"/>
					<xsl:attribute name="Bit_Position" select="if($localname = 'ZC') then $ats_cbi_zc_object[last()]/@Bit_Position else if ($localname = 'Train_Unit') then $ats_cbi_zc_object[1]/@Bit_Position else (if (count($ats_cbi_zc_object) &gt; 1) then ($ats_cbi_zc_object[@Variable_Name = concat($abbr,'_',$objname)]/@Bit_Position) else ($ats_cbi_zc_object/@Bit_Position))"/>
					<xsl:attribute name="Type_Of_Information" select="@Type_Of_Information"/>
					<xsl:attribute name="Variable_Name" select="if ($localname = 'Train_Unit' and ($ats_cbi_zc_object/../@ZC_Emitter_ID or $ats_cbi_zc_object/../@ZC_Receptor_ID)) then concat(@variable,'_','Train',format-number(number($objname),'000')) else concat(@variable,'_',$object_name)"/>
					<xsl:attribute name="Object_Name" select="$objname"/>
					<xsl:attribute name="OPCTag" select="$opcua_tag"/>
					<xsl:attribute name="OPCType" select="@OPCType"/>
					<xsl:attribute name="RM_RC" select="@RM_RC"/>
					<xsl:if test="@ZCType"><xsl:attribute name="ZCType" select="@ZCType"/></xsl:if>
				</xsl:element>
			</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:copy>
</xsl:template>

<xsl:template match="@* | node()">
	<xsl:copy>
		<xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="*[count(ancestor-or-self::*)=3]">
	<xsl:variable name="lcname" select="local-name()"/>
	<xsl:element name="{concat($lcname,'_Variable')}">
		<xsl:apply-templates select="@* | node()"/>
	</xsl:element>
</xsl:template>

<xsl:template match="*[count(ancestor-or-self::*)=5]"/>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
<xsl:param name="var" select="/docs/Interface_Variables"/>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%def name="max_rm_rc(cmp)">

        <xsl:if test="local-name()='${cmp}'">
            <xsl:variable name="${cmp}_id" select="@ID"/>
            <xsl:attribute name="RMMax"><xsl:value-of select="max(//${cmp}_ATS[@${cmp}_Emitter_ID=$${cmp}_id]/${cmp}_ATS_Object/@Bit_Position)"/></xsl:attribute>
            <xsl:attribute name="RCMax"><xsl:value-of select="max(//ATS_${cmp}[@${cmp}_Receptor_ID=$${cmp}_id]/ATS_${cmp}_Object/@Bit_Position)"/></xsl:attribute>
        </xsl:if>

</%def>
