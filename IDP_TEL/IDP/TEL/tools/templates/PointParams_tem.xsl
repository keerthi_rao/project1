<%inherit file="/Object_tem.mako"/>
<%! class_ = "PointParams" %>

<%block name="classes">
<xsl:variable name="filegeneration" select="if ($sysdb//Points/Point[sys:sigarea/@id=$SERVER_ID]) then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="PointParams_{$SERVER_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="PointParams">
	     <Objects>
			 <xsl:apply-templates select="//Points/Point[sys:sigarea/@id=$SERVER_ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Point">
	<xsl:variable name="sw" select="//Switchs/Switch[Convergent_Point_ID_List/Convergent_Point_ID=current()/@ID or Divergent_Point_ID_List/Divergent_Point_ID=current()/@ID ]"/>
	<xsl:variable name="filenm" select="for $cbi in sys:cbi return(for $ats in sys:ats return(concat('CBI_', $cbi/@id, '_ATS_', $ats/@id, '_DATA_INTERFACE.xml')))"/>
	<xsl:variable name="CBI_ATS_File" select="for $fn in $filenm return(document(concat('../inputs/', $fn))//CBI_ATS_Object)"/>
	<xsl:variable name="BlockingPresenceConf" select="if($CBI_ATS_File[@Type_Of_Information='Switch Blocking Control Acknowledgement' and @Object_Name = $sw/@Name]) then '1' else '0'"/>
	<Object name="{@Name}.PointParams" rules="update_or_create">
		  <Properties>
				<Property name="BlockingPresenceConf" dt="i4"><xsl:value-of select="$BlockingPresenceConf"/></Property>
		  </Properties>
	  </Object>
</xsl:template>