<%inherit file="/Object_tem.mako"/>
<%! class_ = "TrackingObjectAE" %>

<%block name="classes">
	<Class name="TrackingObjectAE">
		<Objects>
			<xsl:apply-templates select="$sysdb//Secondary_Detection_Device[sys:sigarea/@id=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Secondary_Detection_Device">
	<Object name="{@Name}.AlarmEvent" rules="update_or_create">
	</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

