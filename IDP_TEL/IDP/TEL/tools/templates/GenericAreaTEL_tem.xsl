<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "GenericArea"
	add_alias_function = True
%>

<% Direction={'Left':2,'Right':1} %>
<%block name="classes">
	<Class name="GenericArea">
		<Objects>
			<xsl:apply-templates select="$sysdb//PIMPoint[Block_ID_List/Block_ID/text()=//Block[sys:sigarea/@id=$SERVER_ID]/@ID]"/>
			<xsl:apply-templates select="$sysdb//ISCSStation[Geographical_Area=//Geographical_Area[Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID=//Signalisation_Area[@ID=$SERVER_ID and $SERVER_LEVEL='_LV1']/Area_Boundary_Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID]/@ID]"/>
			<xsl:apply-templates select="$sysdb//PIMPlatform[(LeftInterstation/Block_ID=//Signalisation_Area[@ID=$SERVER_ID]/Block_ID_List/Block_ID) or (RightInterstation/Block_ID=//Signalisation_Area[@ID=$SERVER_ID]/Block_ID_List/Block_ID)]"/>
			<xsl:apply-templates select="$sysdb//RegulationPointArea[Block_ID_List/Block_ID=//Block[sys:sigarea/@id=$SERVER_ID]/@ID]"/>
			<xsl:apply-templates select="$sysdb//Signalisation_Area[@ID=$SERVER_ID]"/>		
		</Objects>
	</Class>
</%block>
<xsl:template match="PIMPoint|ISCSStation">
	<xsl:variable name="objname" select="@Name"/>
	<xsl:variable name="b1" select="for $blks in //Block[@ID=current()/Block_ID_List/Block_ID and sys:sigarea/@id=$SERVER_ID]/@Name return(concat('TI_',$blks,'.TrackPortion'))"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			${prop('Type','0x0000010')}
			${prop('SharedOnIdentifier','<xsl:value-of select="$objname"/>')}
			${prop('SharedOnIdentifierDefinition','1','boolean')}
			<!--xsl:variable name="tplist" select="if (local-name()='PIMPoint') then concat('TI',$bl,'.TrackPortion') else for $blk in distinct-values(//Block[Secondary_Detection_Device_ID=(//Geographical_Area[@ID=current()/Geographical_Area]/@Name) return concat('TI_',$blk,'.TrackPortion')"/-->
			<xsl:variable name="tplist" select="if (local-name()='PIMPoint') then $b1 else for $blk in distinct-values(//Block[Secondary_Detection_Device_ID=//Geographical_Area[@ID=current()/Geographical_Area]/Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID]/@Name) return(concat('TI_',$blk,'.TrackPortion'))"/>
			<!--Property name="Technical" dt="string"><xsl:if test="$tplist">TrackPortions(<xsl:value-of select="$tplist" separator=";"/>)</xsl:if></Property-->
			<Property name="Technical" dt="string"><xsl:if test="$tplist[1]">TrackPortions(<xsl:value-of select="$tplist" separator=";"/>)</xsl:if></Property>
			<xsl:variable name="dir" select="if(Direction='Left') then 1 else if(Direction='Right') then '2' else '3'"/>
			${prop('Direction','<xsl:value-of select="$dir"/>','i4')}
			<Property name="PropagateAutomaton" dt="i4"><xsl:value-of select="if(local-name()='ISCSStation') then 0 else if($SERVER_LEVEL='_LV1') then 1 else 0"/></Property>
			<xsl:if test="$SERVER_LEVEL='_LV2'">
				<Property name="InAutomaton_PV" dt="string"><xsl:value-of select="if($SERVER_LEVEL='_LV2') then concat('PIA_', $objname) else ''"/></Property>
			</xsl:if>
			${prop('ID','<xsl:value-of select="$objname"/>','string')}			
			<xsl:variable name="self" select="sys:alias_name(.)"/>
        	${multilingualvalue("$self")}
		</Properties>
	</Object>
</xsl:template>

<xsl:template match="PIMPlatform">
	% for dir in Direction:
		<xsl:variable name="st" select="if('${dir}'='Left') then LeftInterstation else RightInterstation"/>
		<xsl:if test="$st">
			<xsl:variable name="objname" select="concat('PIM_',StationID,'_', PlatformID,'_${dir}' )"/>
			<Object name="{$objname}" rules="update_or_create">
				<Properties>
					${prop('Type','0x0000010')}
					${prop('SharedOnIdentifier','<xsl:value-of select="$objname"/>')}
					${prop('SharedOnIdentifierDefinition','1','boolean')}
					<xsl:variable name="tplist" select="for $blk in //Block[@ID=$st/Block_ID and sys:sigarea/@id=$SERVER_ID]/@Name return(concat('TI_',$blk,'.TrackPortion'))"/>
					<Property name="Technical" dt="string"><xsl:if test="$tplist[1]">TrackPortions(<xsl:value-of select="$tplist" separator=";"/>)</xsl:if></Property>
					<Property name="Direction" dt="i4">${Direction[dir]}</Property>
					<Property name="PropagateAutomaton" dt="i4"><xsl:value-of select="if($SERVER_LEVEL='_LV1') then 1 else 0"/></Property>
					<xsl:if test="$SERVER_LEVEL='_LV2'">
						<Property name="InAutomaton_PV" dt="string"><xsl:value-of select="if($SERVER_LEVEL='_LV2') then concat('PIA_', $objname) else ''"/></Property>
					</xsl:if>
					${prop('ID','<xsl:value-of select="$objname"/>','string')}			
					<xsl:variable name="self" select="."/>
					${multilingualvalue("$objname")}
				</Properties>
			</Object>
		</xsl:if>
	% endfor
</xsl:template>

<xsl:template match="RegulationPointArea">
	<xsl:variable name="ttnm" select="concat('SAHM_RegPointArea_', @Name)"/>
	<Object name="{$ttnm}" rules="update_or_create">
	<Properties>
				${prop('Type','0x0000010')}
				<Property name="SharedOnIdentifier" dt="string">SAHM_RegPointArea_<xsl:value-of select="@Name"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
				<xsl:variable name="tplist" select="for $blk in //Block[@ID=current()/Block_ID_List/Block_ID and sys:sigarea/@id=$SERVER_ID]/@Name return(concat('TI_',$blk,'.TrackPortion'))"/>
					<Property name="Technical" dt="string"><xsl:if test="$tplist[1]">TrackPortions(<xsl:value-of select="$tplist" separator=";"/>)</xsl:if></Property>
				<Property name="Direction" dt="i4">3</Property>
				<Property name="PropagateAutomaton" dt="Boolean"><xsl:value-of select="if($SERVER_LEVEL='_LV1') then 1 else 0"/></Property>
				<xsl:if test="$SERVER_LEVEL='_LV2'">
						<Property name="InAutomaton_PV" dt="string"><xsl:value-of select="if($SERVER_LEVEL='_LV2') then concat('SAHM_RegPointArea_', @Name) else ''"/></Property>
				</xsl:if>
				<Property name="ID" dt="string">SAHM_RegPointArea_<xsl:value-of select="@Name"/></Property>
				${multilingualvalue("$ttnm")}
	 </Properties>
	</Object>
</xsl:template>


<xsl:template match="Signalisation_Area">
	<xsl:variable name="ttnm" select="concat('GenericArea_', @Name)"/>
	<Object name="{$ttnm}" rules="update_or_create">
	<Properties>
				${prop('Type','0x0000010')}
				<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$ttnm"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
				<xsl:variable name="tplist" select="for $blk in //Block[@ID=current()/Block_ID_List/Block_ID and sys:sigarea/@id=$SERVER_ID]/@Name return(concat('TI_',$blk,'.TrackPortion'))"/>
					<Property name="Technical" dt="string"><xsl:if test="$tplist[1]">TrackPortions(<xsl:value-of select="$tplist" separator=";"/>)</xsl:if></Property>
				<Property name="Direction" dt="i4">3</Property>
				<Property name="PropagateAutomaton" dt="Boolean"><xsl:value-of select="if($SERVER_LEVEL='_LV1') then 1 else 0"/></Property>
				<xsl:if test="$SERVER_LEVEL='_LV2'">
						<Property name="InAutomaton_PV" dt="string"><xsl:value-of select="if($SERVER_LEVEL='_LV2') then concat('SAHM_RegPointArea_', @Name) else ''"/></Property>
				</xsl:if>
				<Property name="ID" dt="string"><xsl:value-of select="$ttnm"/></Property>
				${multilingualvalue("$ttnm")}
	 </Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,prop"/>