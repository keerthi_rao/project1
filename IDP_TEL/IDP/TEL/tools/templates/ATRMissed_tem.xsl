<%inherit file="/Object_tem.mako"/>
<%! class_ = "ATRMissed" %>

<%block name="classes">
  	<Class name="ATRMissed">
  		<Objects>
  			<!-- Generate the items of Stopping_Areas table -->
  			<xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id = $SERVER_ID and sys:stoppoint='yes']"/>
  		</Objects>
  	</Class>
</%block>

<!-- Template to match the Platform table -->
<xsl:template match="Stopping_Area">
	<xsl:variable name="sta" select="concat(@Name, '.Missed')"/>
	<Object name="{$sta}" rules="update_or_create">
		<Properties>
			<Property name="BlockPlug" dt="string"><xsl:value-of select="concat('TI_',sys:blockid[1]/@name,'.TrainIndicator2.HMITETrain.longPlug_6')"/></Property>
			<Property name="SharedATR" dt="string"><xsl:value-of select="concat(@Name,'.ATR')"/></Property>
			<Property name="SharedOPCGroup" dt="string"><xsl:value-of select="concat('Split_', $SERVER_ID)"/></Property>
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,props"/>