<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:param name="DATE"/>
<xsl:param name="SERVER_ID"/>
<xsl:param name="FUNCTION_ID"/>

<xsl:template match="/">
	<MovementConditions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<Zones>
			<Zone id="TrainRestrictionAreaTEL_{$FUNCTION_ID}" Static="1">
				<MaxSpeed id="0"/>
				<MotionContext id="1"/>
			</Zone>
		</Zones>
	</MovementConditions>
</xsl:template>

<xsl:template match="Signalisation_Area">
	<Zone id="TrainRestrictionArea_{@ID}" Static="1">
		<MaxSpeed id="0"/>
		<MotionContext id="1"/>
	</Zone>
</xsl:template>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="versions_xml"/>