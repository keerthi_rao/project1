<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signal[sys:cbi/@id=$CBI_ID]" mode="Event" />	
		</Objects>
	</Class>
	
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signal[sys:cbi/@id=$CBI_ID and Signal_Type_Function!='Virtual' and not(contains(@Name,'VS'))]" mode="Alarm"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Signal" mode="Event">
			<xsl:variable name="TTName" select= "@Name"/>
			<xsl:variable name="terrname" select= "sys:terr/@name"/>
			<xsl:variable name="uname" select="concat(substring-after(sys:terr/@name,'_'),'/SIG/CBI')"/>
			${obj('{$TTName}',[('SDB','Destination Signal %s1 Is Blocked','0'),
				   ('SDUB','Destination Signal %s1 Unblocked','1'),
				   ('SB','Signal %s1 is Blocked','0'),
				   ('SUB','Signal %s1 is Unblocked','1'),
				   ('IO','Signal %s1 is Set In Operation','1'),
				   ('OO','Signal %s1 is Set Out of Operation','0')
])}
</xsl:template>
<xsl:template match="Signal" mode="Alarm">
			<xsl:variable name="nm" select= "@Name"/>
			${objAlarm('.Alarm_PermissiveCounter',
				 'Permissive Aspect Signal Counter Exceeded Threshold %i',
				 '.PermissiveCntExceed.Value',
				 '.Status_Permissive_Counter')}
			${objAlarm('.Alarm_RestrictiveCounter',
					'Restrictive Aspect Signal Counter Exceeded Threshold %i',
					'.Status_Restrictive_Counter',
					'.RestrictiveCntExceed')}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,stpv in list:
		<Object name="${nm}.${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>			
				${prop('AlarmInhib','1','boolean')}			
				${multilingualvalue("'-'",'Avalanche')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>			
				${multilingualvalue('$TTName','Name')}			
				${multilingualvalue('$uname','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}		
				<Property name="Name_PV_Trig" dt="string">${key}<xsl:value-of select="concat('_',$TTName)"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${stpv}"/></Property>			
				<Property name="Operator" dt="string"><xsl:value-of select="$terrname"/>.TAS</Property>			
				${multilingualvalue("'O'",'OMB')}			
				<Property name="Severity" dt="i4">-</Property>		
				${multilingualvalue("'-'",'Value')}
			</Properties>
		</Object>
	% endfor
</%def>

<%def name="objAlarm(typ,alarmlabel,nmpvdy,nmpv)">
	<Object name="{$nm}${typ}" rules="update_or_create">
		<Properties>
				<xsl:variable name="eqid" select="concat('MDD','/SIG/ATS/',@Name)"/>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${alarmlabel}'"/></Property>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="if('${typ}'='.Alarm_PermissiveCounter') then (concat($nm,'.Status_Permissive_Counter')) else (concat($nm,'.RestrictiveCntExceed'))"/></Property>				
				<Property name="Name_PV_Dynam_1" dt="string"><xsl:value-of select="if('${typ}'='.Alarm_PermissiveCounter') then (concat($nm,'.PermissiveCntExceed.Value')) else (concat($nm,'.Status_Restrictive_Counter'))"/></Property>				
				<Property name="Out_PV_Trig" dt="string">HHThresholdExceed</Property>
				<Property name="Severity" dt="i4">2</Property>
				<Property name="State_PV_Trig" dt="i4">1</Property>
				<xsl:variable name="OMB" select="'M'"/>
				${multilingualvalue('$OMB','OMB')}
				<xsl:variable name="valon" select="'Yes'"/>
				${multilingualvalue('$valon','Value_on')}
				<xsl:variable name="valoff" select="'No'"/>
				${multilingualvalue('$valoff','Value_off')}		
				${multilingualvalue('$eqid','EquipmentID')}				
		</Properties>
	</Object>
</%def>
