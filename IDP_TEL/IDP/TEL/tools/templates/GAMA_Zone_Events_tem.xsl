<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
	     <Objects>
			<xsl:apply-templates select="$sysdb//GAMA_Zone[sys:zc/@id=$ZC_ID]"/>	
		 </Objects>
	</Class>
</%block>

<xsl:template match="GAMA_Zone">
	<xsl:apply-templates select="//Events/Event[Type='GAMA_Zone']">
	<xsl:with-param name="parentname" select = "@Name" />
	<xsl:with-param name="blockID" select = "//Block[sys:zc/@id=$ZC_ID and  @ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/Basic_Multitrack_Zone_ID_List/Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID]/@ID"/>
	<xsl:with-param name="terrname" select = "sys:terr/@name" />
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "parentname" />
	<xsl:param name = "blockID" />
	<xsl:param name = "terrname" />
	<xsl:variable name="te" select="//Technical_Room[@ID=//Secondary_Detection_Device[@ID=//Block[@ID=$blockID and  sys:zc/@id=$ZC_ID]/Secondary_Detection_Device_ID and sys:zc/@id=$ZC_ID]/Technical_Room_ID]/@Name"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (replace(Name_PV_Trig,'--GAMA_Zone.Name--',$parentname)) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$parentname),'xxxx',substring-after($te[1],'_'))"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="tracktype" select="//Track[@ID=//Block[@ID=$blockID]/Track_ID]/Track_Type"></xsl:variable>
	<xsl:variable name="optr" select="if($tracktype='Depot') then ('MDD') else if($tracktype='Test track') then ('TTCU') else if($tracktype='Mainline') then ('TAS') else null"/>
	
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$parentname"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname,'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>


