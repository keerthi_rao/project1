<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sys="http://www.systerel.fr/Alstom/IDP">
<TrackPortions>
    <xsl:variable name="blocks" select="$sysdb//Block"/>
    <xsl:variable name="split_sdd_id" select="$sysdb//TI_Distribution/*/Secondary_Detection_Device_ID"/>
    <xsl:variable name="blocks_split" select="$blocks[Secondary_Detection_Device_ID=$split_sdd_id]"/>
    <xsl:variable name="blocks_split_id" select="$blocks_split/@ID"/>
    <xsl:variable name="blocks_normal" select="$blocks[not(@ID = $blocks_split_id)]"/>
	<xsl:variable name="tp_ids_blocks" select="$blocks_normal/@ID,$blocks_split_id"/>
    <xsl:variable name="sdz_tp_ids_start" select="if($blocks_split) then (10000 + max($tp_ids_blocks)) else max($blocks_normal/@ID)"/>
	<xsl:apply-templates select="$blocks_normal" mode="normal"/>
    <xsl:apply-templates select="$blocks_split" mode="split">
        <xsl:with-param name="pos_init" select="count($blocks_normal)"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="$sysdb//Static_Dark_Zone" mode="track_portion">
        <xsl:with-param name="pos_init" select="100000"/>
    </xsl:apply-templates>
</TrackPortions>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>


<xsl:template match="Block" mode="normal">
    <xsl:variable name="bid" select="@ID"/>
    <xsl:variable name="sector" select="sys:sector($bid)"/>
    ${track_portion("@ID",
                    "$bid",
                    "concat('TI_', @Name)", 
                    "Kp_Begin",
                    "Kp_End", 
                    "$sector",
                    "if ($sector='0') then 'true' else 'false'")}
</xsl:template>

<xsl:template match="Block" mode="split">
    <xsl:param name="pos_init"/>
    <xsl:variable name="block" select="."/>
    <xsl:variable name="bid" select="@ID"/>
    <xsl:variable name="bname" select="@Name"/>
    <xsl:variable name="sector" select="sys:sector($bid)"/>
    <xsl:variable name="sdd_id" select="Secondary_Detection_Device_ID"/>
    <xsl:variable name="ti_distribution" select="$sysdb//TI_Distribution[*/Secondary_Detection_Device_ID=$sdd_id]"/>
    <xsl:variable name="max_length" select="number($ti_distribution/Subdivision_Step)*100"/><!-- in cm -->
    <xsl:variable name="block_length" select="max(Kp_Begin|Kp_End) - min(Kp_Begin|Kp_End)"/>

    <xsl:if test="$block_length >= $max_length">
        <xsl:variable name="modulo" select="$block_length mod $max_length"/>
        <xsl:variable name="nb_block" select="(number($block_length - $modulo) div $max_length)-1"/>
        <xsl:variable name="kp_begin_block" select="Kp_Begin"/>
        <xsl:for-each select="1 to xs:integer($nb_block)">
            <xsl:variable name="indexb" select="."/>
            <xsl:variable name="kp_begin" select="$kp_begin_block + number($indexb - 1)*$max_length"/>
            ${track_portion("'XY'",
                            "$bid",
				            "concat('TI_', $bname, '_', $indexb)", 
				            "format-number($kp_begin, '#')",
				            "format-number($kp_begin + $max_length, '#')", 
				            "$sector",
				            "if ($sector='0') then 'true' else 'false'",
				            splited_index="position()-1",
				            max="$nb_block")}
		    <!-- TODO: faire le next_up_tp_id pour les blocks splites -->
        </xsl:for-each>
        ${track_portion("'XY'",
            "$bid",
	        "concat('TI_', @Name, '_', $nb_block + 1)", 
	        "format-number($kp_begin_block + number($nb_block*$max_length), '#')",
	        "format-number(Kp_End, '#')", 
	        "$sector",
	        "if ($sector='0') then 'true' else 'false'",
	        splited_index="xs:integer($nb_block)",
	        max="$nb_block")}
    </xsl:if>
</xsl:template>

<xsl:template match="Static_Dark_Zone" mode="track_portion">
    <xsl:param name="pos_init"/>
    ${track_portion("'XY'",
                "''",
                "concat('TI_', @Name)", 
                "'0'",
                "'1'", 
                "'0'",
                "'true'",
                sdz_id='@ID')}
</xsl:template>

<xsl:function name="sys:sector">
    <xsl:param name="bid"/>
    <xsl:variable name="sid" select="$sysdb//Signalisation_Area[Area_Type='ZC' and Block_ID_List/Block_ID=$bid]/@ID"/>
    <xsl:variable name="zc" select="$sysdb//ZC[ZC_Area_ID=$sid]"/>
    <xsl:value-of select="if ($zc) then $zc/SSID else '0'"/>
</xsl:function>


<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>

<%namespace file="/lib_tem.mako" import="versions_xml"/>

<%def name="track_portion(tp_id, bid, tp_name, KpBegin, KpEnd, Sector, FixedBlockOnly, sdz_id=None, splited_index=None, max=None)">
<TrackPortion ID="{${tp_id}}" Name="{${tp_name}}.TrackPortion">
    <KpBegin><xsl:value-of select="${KpBegin}"/></KpBegin>
    <KpEnd><xsl:value-of select="${KpEnd}"/></KpEnd>
    <Sector><xsl:value-of select="${Sector}"/></Sector>
    <FixedBlockOnly><xsl:value-of select="${FixedBlockOnly}"/></FixedBlockOnly>
    <sys:block_id><xsl:value-of select="${bid}"/></sys:block_id>
    <sys:track_id><xsl:value-of select="$sysdb//Block[@ID=${bid}]/Track_ID"/></sys:track_id>
    <sys:tp_name><xsl:value-of select="${tp_name}"/></sys:tp_name>
    % if sdz_id is not None:
        <sys:sdz_id><xsl:value-of select="${sdz_id}"/></sys:sdz_id>
    % endif
    % if splited_index is not None:
        <sys:splited_index value="{${splited_index}}" max="{${max}}"/>
    % endif
</TrackPortion>
</%def>
