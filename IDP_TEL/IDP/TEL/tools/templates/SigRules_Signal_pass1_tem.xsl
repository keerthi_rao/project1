<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"  >
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ATS_SYSTEM_PARAMETERS  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:sys="http://www.systerel.fr/Alstom/IDP" >
    <Signals>
    	<xsl:apply-templates select="$sysdb/Signals/Signal"/>
    </Signals>
</ATS_SYSTEM_PARAMETERS>
</xsl:template>

<xsl:key name="routes" match="Route" use="Origin_Signal_ID/text()"/>

<xsl:template match="Signal">
  <xsl:variable name="slt_id" select="./Signal_Lamp_Type_ID/text()"/>
  <xsl:variable name="slt" select="$sysdb/Signal_Lamp_Types/Signal_Lamp_Type[@ID=$slt_id]"/>

  <Signal ID="{@ID}" Name="{@Name}">
    <xsl:apply-templates/>
    <Routes_Variables_list>
		<xsl:apply-templates select="key('routes', @ID)">
		  <xsl:sort select="@Name"/>
		</xsl:apply-templates>
    </Routes_Variables_list>

    % for (elt_name, c) in [('HMILampCommand', 'C'), ('HMIFilament', 'P')] :
    <${elt_name}>
        <xsl:comment> Add L${c}Sn element used to build the equation ${elt_name} </xsl:comment>
        <xsl:if test="L${c}SRES and $slt/*/Lamp_Name/text() = 'SRES'">
            <L${c}Sn n="0">L${c}SRES</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SPER and $slt/*/Lamp_Name/text() = 'SPER'">
            <L${c}Sn n="1">L${c}SPER</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SOVD and Signal_Overridden_For_CBTC_Train/text()='true'">
             <L${c}Sn n="2">L${c}SOVD</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SANN">
            <L${c}Sn n="3">L${c}SANN</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SDEV">
            <L${c}Sn n="4">L${c}SDEV</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SMOD">
            <L${c}Sn n="5">L${c}SMOD</L${c}Sn>
        </xsl:if>
        <xsl:if test="L${c}SPL">
            <L${c}Sn n="6">L${c}SPL</L${c}Sn>
        </xsl:if>
    </${elt_name}>
    % endfor
    <xsl:apply-templates select="$slt"/>
  </Signal>
</xsl:template>

<xsl:key name="call_on" match="Specific_Route[Type/text()='Call_On']" use="Specific_Route_ID_List/Route_ID/text()"/>
<xsl:template match="Route">
	<xsl:if test="AR">
        <Route_Variable Name="{@Name}" Prefix="AR"/>
	</xsl:if>
    <xsl:variable name="call_on_value" select="if (key('call_on', @ID)) then 'True' else 'False'"/>
	<xsl:if test="RL">
        <Route_Variable Name="{@Name}" Prefix="RL" Call_On="{$call_on_value}"/>
	</xsl:if>
	<xsl:if test="R">
        <Route_Variable Name="{@Name}" Prefix="R"  Call_On="{$call_on_value}"/>
	</xsl:if>
</xsl:template>

  <xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
<xsl:include href="copy.xsl"/>
</xsl:stylesheet>

