<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
    <xsl:apply-templates select="$sysdb//Train_Unit"/>
</%block>

<!-- List for RSM commands: [(rsmmode, (rsmkeys), (equation values), (equation status), rsminfo)] -->
<% rsmcommand=[('.RSCommand.ReadinessMode1' ,('RSM_READY_4', 'RSM_READY_5', 'RSM_READY_6', 'RSM_READY_7', 'RSM_READY_8'), (4,5,6,7,8),(4,5,6,7,8),(6,6,6,6,6), 2, ('!=', '!=', '!=','!=', '!='),('==', '==', '==','==', '=='), 'longPlug'),
			   ('.RSCommand.ReadinessMode2' ,('RSM_READY_1', 'NA'), (1, 1),(1,1),(6, 6), 2, ('!=','!='), ('==','=='),'longPlug'),
			   ('.RSCommand.DrivingMode',('RS_CBTC_AUTHORIZED', 'RS_CBTC_FORBIDDEN'), (0,0),(0,0),(6,6), 7, ('!=','=='), ('==','!='), 'boolPlug'),
			   ('.RSCommand.NoCondCtrl',('RSM_PROPULSION_RESET', 'RSM_AUX_BATT_RESET', 'RSM_SMOKE_RESET','PMD_TRANS_REQ'), ('NA','NA','NA','NA'),('NA', 'NA', 'NA', 'NA'),(3,3,5,5), 1, ('==', '==','==','=='), ('==', '==','==','=='),'NA'),
			   ('.RSCommand.Creep', ('RSM_CREEP_ON','RSM_CREEP_OFF'), (0, 0),(0,0),(4, 4), 1, ('==','!='),('!=','=='), 'boolPlug'),
			   ('.RSCommand.EBReset',('RSM_EB_RESET','NA'), (0,0),(0,0),(9,9), 2, ('!=','!='),('==','=='), 'longPlug'),
			   ('.RSCommand.Parking',('RSM_PARKING_ON','NA'), (0,0),(0,0),(8,8), 6, ('==','=='),('!=','!='), 'boolPlug'),
			   ('.RSCommand.TunnelAir',('RSM_TUNNEL_AIR_ON', 'RSM_TUNNEL_AIR_OFF'), (0,0),(0,0),(4,4), 7, ('==','!='),('!=','=='),'boolPlug'),
			   ('.RSCommand.EmerVent',('RSM_EMER_VENT_ON', 'RSM_EMER_VENT_OFF'), (0,0),(0,0),(1,1), 7, ('==','!='), ('!=','=='),'boolPlug'),
			   ('.RSCommand.PreCool',('RSM_PRECOOL_ON', 'RSM_PRECOOL_OFF'),(0,0),(0,0),(9,9),6, ('==','!='),('!=','=='), 'boolPlug'),
			   ('.RSCommand.ExitSign',('RSM_EXIT_DM1_ON', 'RSM_EXIT_DM1_OFF', 'RSM_EXIT_DM2_ON', 'RSM_EXIT_DM2_OFF'),(0,0,0,0),(0,0,0,0),(4,4,5,5), 6, ('==','!=','==','!='),('!=','==','!=','=='),'boolPlug'),
			   ('.RSCommand.Floodlight',('RSM_LIGHT_DM1_ON', 'RSM_LIGHT_DM1_OFF', 'RSM_LIGHT_DM2_ON', 'RSM_LIGHT_DM2_OFF'), (0,0,0,0),(0,0,0,0),(6,6,7,7), 6, ('==','!=','=','!='),('!=','==','!=','=='),'boolPlug')]     
%>

<!-- Template to call Train Units -->
<xsl:template match="Train_Unit">
%	for name, key, value,value2, eqnnum, rsminf, equnsy, equnsy2,type in rsmcommand:
	<xsl:variable name='tt' select="'${name}'"/>
	<xsl:variable name="nm" select="concat('Train', format-number(@ID, '000') , '${name}')"/>
	<xsl:variable name="control" select="concat($nm, 'RSInterposeCmd.IntOutputPlug_1#', position())"/>
	<Equipment name="{$nm}" type="HMITrainRSMCommand" flavor="RSCommand" CommandTimeOut ="6">
		<Requisites>${equation(key, value, eqnnum,"REQUISITE", rsminf, equnsy, type)}</Requisites>
		<CommandResults>${equation(key, value2, eqnnum,"COMMANDRESULT", rsminf, equnsy2, type)}</CommandResults>
	</Equipment>
%	endfor
</xsl:template>   		

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

## Mako code to compute equation for each object
<%def name="equation(ky, vl, eqnnum,el, rsminf, equnsy, type)" >
		% for i in range(len(ky)):
			% if ky[i] != 'NA':
				<xsl:variable name='value' select="'${vl[i]}'"/>
				<xsl:variable name='key' select="'${ky[i]}'"/>
				<xsl:variable name='el' select="'${el}'"/>
				<xsl:variable name='eqnum' select="'${eqnnum[i]}'"/>
				<xsl:variable name='equnsys' select="'${equnsy[i]}'"/>
				<xsl:variable name="eqn" select="if($value='NA') then 1 else concat('Train', format-number(@ID, '000'), '.ProjAttributes','${rsminf}','.HMITETrain.','${type}','_',$eqnum,' ', $equnsys,' ' ,$value)"/>
				<xsl:choose>
					<xsl:when test="$el='REQUISITE'"><Equation><xsl:value-of select="$nm"/>.RSInterposeCmd.IntOutputPlug_1#${i+1}[${ky[i]}]::<xsl:value-of select="$eqn"/></Equation></xsl:when>
					<xsl:otherwise><Equation><xsl:value-of select="$nm"/>.RSInterposeCmd.IntOutputPlug_1#${i+1}[${ky[i]+'_ERR'}]::<xsl:value-of select="$eqn"/></Equation></xsl:otherwise>
				</xsl:choose>
			% endif
		% endfor
</%def>