<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>
		    <xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID and  (@ID= //LX_Device[Type='Safe To Proceed']/LX_ID)]" mode="stp"/>
		    <xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID and  (@ID= //LX_Device[Type='Safe To Proceed']/LX_ID)]" mode="gd"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="LX" mode="stp">
	<xsl:variable name="TTName" select= "concat(@Name,'.Alarm')"/>
	<xsl:variable name="nme" select= "@Name"/>
	  <xsl:variable name="terrname" select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID and sys:sigarea/@id = $SERVER_ID][1]/sys:terr/@name"/>
	<xsl:variable name="uname" select="concat(substring-after($terrname,'Territory_'),'/SIG/CBI/',@Name)"/>
	${obj('{$TTName}',[('LXSTP_','Level Crossing %s1 : Gates Are Unlocked','2','1')])}
</xsl:template>

<xsl:template match="LX" mode="gd">
	<xsl:variable name="TTName" select= "concat(@Name,'.Alarm')"/>
	<xsl:variable name="nme" select= "@Name"/>
	  <xsl:variable name="terrname" select="//Block[@ID=//Elementary_Zone[@ID=//Basic_Multitrack_Zone[@ID=current()/LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID]/Elementary_Zone_ID_List/Elementary_Zone_ID]/Elementary_Zone_Block_ID_List/Block_ID and sys:sigarea/@id = $SERVER_ID][1]/sys:terr/@name"/>
	<xsl:variable name="uname" select="concat(substring-after($terrname,'Territory_'),'/SIG/CBI/',@Name)"/>
	${obj('{$TTName}',[('LXSTP_','Level Crossing %s1 : Gates Are Not Closed And Locked','2','0')])}
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->


<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,svty,state_pv in list:
		<Object name="${nm}" rules="update_or_create">
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="'${description}'"/></Property>				
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nme"/></Property>					
				${multilingualvalue('$uname','EquipmentID')}					
				<Property name="Name_PV_Trig" dt="string">${key}<xsl:value-of select="$nme"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				${multilingualvalue("'O'",'OMB')}			
				<Property name="Severity" dt="i4"><xsl:value-of select="${svty}"/></Property>		
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${state_pv}"/></Property>	
				${multilingualvalue('$nme','Name')}						
			</Properties>
		</Object>
	% endfor
</%def>



