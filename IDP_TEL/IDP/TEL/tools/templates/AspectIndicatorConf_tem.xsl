<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:template match="/">
		<AIS_ASPECT_LABEL xsi:noNamespaceSchemaLocation="AspectIndicatorConf.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<xsl:for-each select="//Stabling_Location[Stabling_Type='Depot']">
				<xsl:variable name="id" select="@ID"/>
					<xsl:variable name="sta" select="//Stopping_Area[Original_Area_List/Original_Area[@ID=$id]/@Original_Area_Type='Stabling']/@Name"/>
					<xsl:variable name="t" select="number(Track_ID)"/>
					<xsl:variable name="aspe" select="if ($t &gt;=23 and $t &lt;= 59) then (if (number(substring(current()/@Name, 6)) mod 2=0) then 'A' else 'B') else
													if ($t &gt;=60 and $t &lt;= 68) then (if (number(substring(current()/@Name, 6)) mod 2=0) then 'B' else 'A') else null"/>
					<STOP_POINT NAME="{$sta}" ASPECT="{$aspe}"></STOP_POINT>
			</xsl:for-each>
		</AIS_ASPECT_LABEL>		
	</xsl:template>
</xsl:stylesheet>
