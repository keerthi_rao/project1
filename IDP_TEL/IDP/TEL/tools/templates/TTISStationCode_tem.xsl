<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:param name="DATE"/>
<xsl:param name="SERVER_ID"/>
<xsl:template match="/">
	<STATIONCODE Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="TTISStationCode.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Platform']"/>
	</STATIONCODE>
</xsl:template>
<xsl:template match="Stopping_Area">
	<xsl:variable name="stationID" select="//Station[@ID=current()/Station_ID]"/>
		<STATION CODE="{$stationID/TTISCode}" TOP_POINT_NAME="{current()/@Name}"/>
</xsl:template>
</xsl:stylesheet>
