<%inherit file="/Object_tem.mako"/>
<%! class_ = "PSDOPCServer" %>
<%!
LIST1=[('PSDIS_','PSDIsolationStatus&lt;HasComponent&gt;33:IsolationArray'), 
	('PSDOIC_','TrainDoorStatus&lt;HasComponent&gt;33:DoorArray')]
	
LIST2=[('PSDSTATUS','PSDEqptStatus&lt;HasComponent&gt;33:PSDStatus', 3), 
	('DOORSTATUS','PSDEqptStatus&lt;HasComponent&gt;33:Door', 20),
	('DAYMONTH','TimeSync&lt;HasComponent&gt;33:Day_Month', 1),
	('YEAR1YEAR2','TimeSync&lt;HasComponent&gt;33:Year1_Year2', 1),
	('HOURMINUTE','TimeSync&lt;HasComponent&gt;33:Hour_Minute', 1),
	('SECONDNONE','TimeSync&lt;HasComponent&gt;33:Second_None', 1),
	('FLAG','TimeSync&lt;HasComponent&gt;33:Flag', 1)]	
 %>
 <xsl:param name="IS_BOTH_LEVEL"/>
<%block name="classes">
	<xsl:variable name="filegeneration" select="if (//Signalisation_Area[@ID=$SERVER_ID and not(contains(Localisation_Type,'Depot'))]/Area_Type='ATS' and
													//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID and 
													sys:sigarea/@id=//ATS_Equipment[Function_ID_List/Function_ID=$FUNCTION_ID
													and ATS_Equipment_Type='FEP']/Signalisation_Area_ID  
													  ])  then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="PSDOPCServer_{$FUNCTION_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="S2KOPCUAClient.IOClient">
		<Objects>
			<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID and @Signalisation_Area_ID=//Signalisation_Area[not(Localisation_Type='Depot')]/@ID]" mode="S2KIOOPCServerMonitor"/>
		</Objects>
	</Class>
	<Class name="OPCGroup">
		<Objects>
			<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID and @Signalisation_Area_ID=//Signalisation_Area[not(Localisation_Type='Depot')]/@ID]" mode="OPCGroup"/>
		</Objects>
	</Class>
	<Class name="OPCStringVariable">
		<Objects>
			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type!='Depot' and sys:sigarea/@id=$SERVER_ID and (sys:sigarea/@id=//ATS_Equipment[Function_ID_List/Function_ID=$FUNCTION_ID and ATS_Equipment_Type='FEP']/Signalisation_Area_ID or sys:sigarea/@id=//ATS_Equipment[Function_ID_List/Function_ID=$FUNCTION_ID and ATS_Equipment_Type='FEP']/Signalisation_Area_ID_List/Signalisation_Area_ID)]" mode="OPCStringVariable"/>
		</Objects>
	</Class>
	<Class name="OPCAnalogVariable">
		<Objects>
			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type!='Depot' and sys:sigarea/@id=$SERVER_ID and (sys:sigarea/@id=//ATS_Equipment[Function_ID_List/Function_ID=$FUNCTION_ID and ATS_Equipment_Type='FEP']/Signalisation_Area_ID or sys:sigarea/@id=//ATS_Equipment[Function_ID_List/Function_ID=$FUNCTION_ID and ATS_Equipment_Type='FEP']/Signalisation_Area_ID_List/Signalisation_Area_ID)]" mode="OPCAnalogVariable"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Function" mode="OPCGroup">
	<Object name="PSDOPCGroup_PSDSERVER_{@ID}" rules="update_or_create">
		<Properties>
		${prop("OPCDAInterface","0","boolean")}
		${prop("OPCUAInterface","1","boolean")}
		${prop("OPCClientRef",'PSDOPCClient_PSDSERVER_<xsl:value-of select="@ID"/>')}
		${prop("SharedOnIdentifier",'PSDOPCGroup_PSDSERVER_<xsl:value-of select="@ID"/>')}
		${prop("SharedOnIdentifierDefinition","1","boolean")}
		</Properties>
	</Object>
</xsl:template>

<xsl:template match="Function" mode="S2KIOOPCServerMonitor">
	<xsl:variable name="ats_eqps" select="//ATS_Equipment[(Function_ID_List/Function_ID = $FUNCTION_ID or Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID ) and ATS_Equipment_Type='FEP']"/>
	<Object name="PSDOPCClient_PSDSERVER_{@ID}" rules="update_or_create">
		<Properties>
		${prop("ConnectAttemptsPeriod","10","i4")}
		<Property name="Server1Url" dt="string">opc.tcp://<xsl:value-of select="$ats_eqps[1]/@Name"/>:51200</Property>
		<Property name="Server2Url" dt="string">opc.tcp://<xsl:value-of select="$ats_eqps[2]/@Name"/>:51200</Property>
		${prop("DuplicateControls","1","boolean")}
		${prop("SharedOnIdentifierDefinition","1","boolean")}
		${prop("SharedOnIdentifier",'PSDOPCClient_PSDSERVER_<xsl:value-of select="@ID"/>')}
		</Properties>
	</Object>
</xsl:template>

<xsl:template match="Platform_Screen_Doors" mode="OPCStringVariable">
	% for var, opc in LIST1:
		<xsl:variable name="nm" select="concat('${var}',@Name)"/>
		<xsl:variable name="opctag" select="concat('[33:FEP_NS_33]&lt;Organizes&gt;33:',@Name,'&lt;Organizes&gt;33:', '${opc}')"/>
		<Object name="{$nm}" rules="update_or_create">
			<Properties>
				<Property name="OPCGroupRef" dt="string"><xsl:value-of select="concat('PSDOPCGroup_PSDSERVER_', $FUNCTION_ID)"/></Property>
				<Property name="OPCTag" dt="string"><xsl:value-of select="$opctag"/></Property>
				<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$nm"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
			</Properties>
		</Object>
	% endfor
</xsl:template>

<xsl:template match="Platform_Screen_Doors" mode="OPCAnalogVariable">
	% for var, opc, cnt in LIST2:
		% for i in range(cnt):
			<xsl:variable name="nm" select="if('${cnt}'='1')then concat('${var}_',@Name) else concat('${var}','${i+1}_',@Name)"/>
			<xsl:variable name="opctag" select="concat('[33:FEP_NS_33]&lt;Organizes&gt;33:',@Name,'&lt;Organizes&gt;33:', '${opc}')"/>
			<xsl:variable name="opctg" select="if('${cnt}'='1')then $opctag else if('${var}'='PSDSTATUS') then concat($opctag, '${i+1}') else concat($opctag, '${i+1}', 'Status')"/>
			<Object name="{$nm}" rules="update_or_create">
				<Properties>
					<Property name="OPCGroupRef" dt="string"><xsl:value-of select="concat('PSDOPCGroup_PSDSERVER_', $FUNCTION_ID)"/></Property>
					<Property name="OPCTag" dt="string"><xsl:value-of select="$opctg"/></Property>
					<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$nm"/></Property>
					<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
				</Properties>
			</Object>
		% endfor
	% endfor
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>