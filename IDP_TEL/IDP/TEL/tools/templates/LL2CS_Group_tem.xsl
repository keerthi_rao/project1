<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
    <LL2CS xsi:noNamespaceSchemaLocation="LL2CS.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	   ${versions_xml()}
	   <Groups>
			<File name="Group\S2KUser.xml"/>			
	   </Groups>
    </LL2CS>
</xsl:template>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="versions_xml"/>
