<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" >
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<!-- Get the server_level and date from command line -->
<xsl:param name="SERVER_LEVEL"/>
<xsl:param name="SERVER_ID"/>
<xsl:param name="DATE"/>
<xsl:param name="SERVER_NAME"/>
<xsl:template match="/">
<VALUES name="ParametersTEL" version="1.0.0" Project="{//Project/@Name}" Generation_Date="{$DATE}"  xsi:noNamespaceSchemaLocation="ParametersTEL.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 	<xsl:variable name="btvvalue" select="if($SERVER_LEVEL ='_LV1') then 'Direction;CCStatusReport;TF_CCItfver;TF_CCNTPTimeDesynchronized;LocalizedID;HeadTrainLocation;FrontTPID;BackTPID;TFCharacteristic;RollingStockID;bstrTPList;iFrontAbscissa;iBackAbscissa;TrackingMode;TF_TrainOperatingMode;TOMChangeReport;CREEP_MODE_STATUS;TUList;TrainUnitCharacteristicID;RSTrainDoorIsolationOnSideA;RSTrainDoorIsolationOnSideB;TRAIN_FIRE_ALARM_MERGED;TRAIN_PEC_ALARM_MERGED;CCRealStopAreaState;TrainOperatingMode;TF_TOM_Transition_in_progress;CCPlatformDoorStatusSideA;CCPlatformDoorStatusSideB;CCTrainDoorStatusSideA;RSTalkative;CCExtremity1Ahead;CCHold;TrainWashMode;CCDrivingMode;TRAIN_READINESS_STATE' else 'CCSettingRequest;CCServiceID_PB:CCServiceID;CCTripID;CCFinalDestinationNumber;CCCrewID;CCDriverNumber;ServiceID;TripID;OriginCode;DestinationCode;Delay;bDelay;bAdvance;bTrainHeld;TrainOutOfRegulation;Associated;TripNumber;ReachableStopPointsWithManeuvers;HoldSkipList;AlarmActivityLV2;TrainCode;TM1StopAreaName;AppliedSpeedProfile;TOMChangeRequest;bTrainStalled;OverallTrainDoorInhibitionOnSideA;OverallTrainDoorInhibitionOnSideB;LastStopPoint;SkipArea_PB;ForbidForUnstabling;ShuttleMode;OperatorTrainDoorInhibitionOnSideA;OperatorTrainDoorInhibitionOnSideB'"/>
	<xsl:variable name="Server1opcclientname" select="if($SERVER_LEVEL ='_LV1') then (concat(substring(substring-before($SERVER_NAME,'_'),1,4),' A')) else (concat(substring(substring-before($SERVER_NAME,'_'),1,4),' B'))"/>
	${params([('bstrTrainEventList','{$btvvalue}' ,'TrainEventDescriber')])}
	${params([("PurgeToBeDeletedFileAge", "8", "S2KGALSimpleApplicationKernel"),
			("ArchiveToBePurgedFileAge", "8", "S2KGALSimpleApplicationKernel"),
			("PlaybackToBePurgedFileAge", "8","S2KGALSimpleApplicationKernel"),
			("ServerDisconnectedFromOPCServer", "Server disconnected","S2KIOOPCClient"),
			("Server1OPCClientName", '{$Server1opcclientname}',"S2KGALSimpleApplicationKernel"),
			("ServerConnectedToOPCServer", "Server connected","S2KIOOPCClient")])}
	<xsl:if test="$SERVER_LEVEL='_LV1'">${params([("AlarmQualityThreshSec","6","ISCSAlarmData"), ("CriticalAlarmInterfaceConf","CriticalAlarmInterface.xml","ISCSAlarmData"), ("LengthOfTrain","{min(//Line/Min_Vehicle_Length)}","ISCSTrainData"),
	
			("StationBoundaryPointsConf","StationBoundaryPoint_{$SERVER_ID}.xml","ISCSTrainData"),
			("TrackPortionToTrackID","TrackPortionToTrackID.xml","ISCSTrainData"),
			("TrainOnFirePropertyName","TRAIN_FIRE_ALARM_MERGED","ISCSTrainData"),
			("TrainStalledTimerSec","30","ISCSTrainData"),
			("PMDAckResetTimeout","2","TrainDataAdapter"),
			("TrainTemperatureCar1PropertyName","DM1_CAR_AMBIENT_TEMPERATURE","ISCSTrainData"),
			("TrainTemperatureCar4PropertyName","DM2_CAR_AMBIENT_TEMPERATURE","ISCSTrainData"),
			("LCSafetyTimeout","45","TSRAppliedTimeout"),	
			("MultipathAlarmConf", "MultipathAlarmConf.xml", "MAMManager"),					
			("MovementConditionsFile", "MovementConditionsTEL_{$SERVER_ID}.xml", "CCMRestrictionManager")])}</xsl:if>
			<xsl:if test="$SERVER_LEVEL = '_LV2'">
				${params([("MaxPredictedDisplayedForTPMPoint","9","RegulationPoint"),("OperatingModeConfiguration","CMMGCentralPIM_{$SERVER_ID}.xml","PIMMMG"),
				("FireAlarmAttribute","TRAIN_FIRE_ALARM_MERGED","EmergencyMgr"),
				("NoMaxDwellOnFirstPlatformRevenuService","-1","TPMA"),
				("PECAlarmAttribute","TRAIN_PEC_ALARM_MERGED","EmergencyMgr"),
				("FireAlarmAttribute","TRAIN_FIRE_ALARM_MERGED","EmergencyPoint"),
				("PECAlarmAttribute","TRAIN_PEC_ALARM_MERGED","EmergencyPoint"),
				("PredictedParameters","0x307","TPMPoint"),
				("AspectIndicatorConf","AspectIndicatorConf.xml","AIS"),
				("GenericAreaReverseAccess","STA_COD_T5117_BOTH_STA_COD_T5701_BOTH","TWPMgr"),
				("GenericAreaTR_RT1","STA_COD_T5101_BOTH","TWPMgr"),
				("GenericAreaTR_RT2","STA_COD_T5103_BOTH","TWPMgr"),
				("GenericAreaTR_RT3","STA_COD_T5501_BOTH","TWPMgr"),
				("GenericAreaWashPlant","STA_WSH_MDD_1","TWPMgr"),
				("GenericAreaWashPlantAccess","STA_COD_T5501_BOTH_STA_WSH_MDD_1","TWPMgr"),
				("GenericAreaWashPlantEnd","STA_COD_T5701_BOTH","TWPMgr"),
				("TWPMgr_EnterWashPlanManeuver","STA_COD_T5101_BOTH;30003;STA_COD_T5103_BOTH;30004;STA_COD_T5501_BOTH;21001","TWPMgr"),
				("TWPMgr_ExitWashPlanManeuver","21111","TWPMgr"),			
				("TWPMgr_TWPtoTransferTrackManeuver","MVT_STA_LOOP_UP_COD_T5101_BOTH_NP","TWPMgr")	
				])}
	</xsl:if>
</VALUES>
</xsl:template>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

## Mako template used for generating <Params/> elements with values passed as list.
<%def name="params(list)" >
% for name,value,dt in list:
	<Param name="${name}" value="${value}" class="${dt}"/>
% endfor
</%def>
