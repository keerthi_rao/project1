<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  
<xsl:param name="DATE"/>
<xsl:param name="MACHINE_NAME"/>
<xsl:param name="SIG_AREA_TYPE"/>

<xsl:template match="/">

<xml Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="PIMCOM.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:variable name="SERVER_ID" select="if (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID) then (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID) else (//ATS_Equipment[@Name=$MACHINE_NAME]/Signalisation_Area_ID_List/Signalisation_Area_ID)"/>
	<xsl:variable name="serverType" select="if ($SIG_AREA_TYPE='ATS_Central_Server') then 'CATS' else if ($SIG_AREA_TYPE='ATS_Local_Server') then 'LATS' else null"/>
	<PIM ServerType="{$serverType}">
		<STIS>
			<M00 Periodicity="500" TimeoutAck="400" NRetries="3"/>
			<M09 />
			<xsl:for-each select="//PIMStation[Signalisation_Area=$SERVER_ID and (STISAddress1/text() or STISAddress2/text())]">
				<xsl:sort select="@ID" data-type="number" order="ascending"/>
				<Location Name="{concat('STISConnection',position())}" StationID="{@Name}" IP1="{STISAddress1}" IP2="{STISAddress2}" M00Port="1801" M09Port="1800"/>
			</xsl:for-each>
		</STIS>
		<PA>
			<M00 Periodicity="1000" TimeoutAck="1000" NRetries="3"/>
			<M09 TimeoutAck="1000" NRetries="3"/>
			<xsl:for-each select="//PIMStation[Signalisation_Area=$SERVER_ID and (PAAddress1/text() or PAAddress2/text())]">
				<xsl:sort select="@ID" data-type="number" order="ascending"/>
				<Location Name="{concat('PAConnection',position())}" StationID="{@Name}" IP1="{PAAddress1}" IP2="{PAAddress2}" M00Port="29000" M09Port="29001"/>
			</xsl:for-each>
		</PA>
		<MAPPING>
			<DIRECTION_DEFAULT>
				<xsl:apply-templates select="//PIMPlatform"><xsl:with-param name="dir" select="'default'"/></xsl:apply-templates>
			</DIRECTION_DEFAULT>
			<DIRECTION_UP>
				<xsl:apply-templates select="//PIMPlatform"><xsl:with-param name="dir" select="'up'"/></xsl:apply-templates>
			</DIRECTION_UP>
			<DIRECTION_DOWN>
				<xsl:apply-templates select="//PIMPlatform"><xsl:with-param name="dir" select="'down'"/></xsl:apply-templates>
			</DIRECTION_DOWN>
		</MAPPING>
	</PIM>
</xml>
</xsl:template>
<xsl:template match="PIMPlatform">
	<xsl:param name="dir"/>
	<xsl:variable name="default" select="DefaultDirection"/>
	<xsl:variable name="upstreamPF" select="UpstreamPlatform"/>
	<xsl:variable name="downstreamPF" select="DownstreamPlatform"/>
	<xsl:if test="$dir='default'">
		<Platform StationID="{StationID}" PlatformID="{PlatformID}"  Direction="{DefaultDirection}"/>
	</xsl:if>
	<xsl:if test="$dir !='default'">
	<Platform StationID="{StationID}" PlatformID="{PlatformID}" UpstreamStationID="{if ($dir='up') then (//PIMPlatform[@ID=$upstreamPF]/StationID) else (//PIMPlatform[@ID=$downstreamPF]/StationID)}" UpstreamPlatformID="{if ($dir='up') then (//PIMPlatform[@ID=$upstreamPF]/PlatformID) else (//PIMPlatform[@ID=$downstreamPF]/PlatformID)}"/>
	</xsl:if>
</xsl:template>
</xsl:stylesheet>
