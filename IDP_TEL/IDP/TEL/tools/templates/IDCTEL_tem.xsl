<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<!-- Get the date from command line -->
<xsl:param name="SERVER_ID"/>
<xsl:param name="SIG_SERVER_ID"/>
<xsl:param name="SERVER_LEVEL"/>
<xsl:param name="IS_BOTH_LEVEL"/>
<xsl:param name="DATE"/>

<xsl:template match="/">
<IDC Project="{//Project/@Name}" Generation_Date="{$DATE}" xsi:noNamespaceSchemaLocation="IDC.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    ${versions_xml()}
	<Enumerates>
		<File name="Enummgnt.xml"/>
	</Enumerates>
	<xsl:variable name="sigare" select="//Signalisation_Area[@ID=$SIG_SERVER_ID]"/>
	<xsl:variable name="CBIs" select="$sigare/sys:cbi"/>
	<xsl:variable name="ZCs" select="$sigare/sys:zc"/>
	<xsl:variable name="areatype" select="$sigare/Area_Type"/>
	<xsl:variable name="otherarea" select="if($sigare[Area_Type='ATS_Central_Server']) then true() else if($sigare[not(sys:sigarea/@id =//Signalisation_Area[Area_Type='ATS_Central_Server']/@ID)]) then true() else false()"/>
	<xsl:variable name="urbalisSector" select="//Urbalis_Sector[ATS_Area_ID_List/Signalisation_Area_ID = $SIG_SERVER_ID or ATS_Area_ID_List/Signalisation_Area_ID=$sigare/sys:sigarea/@id]"/>
	<xsl:variable name="feprefsigid" select="//ATS_Equipment[ATS_Equipment_Type='FEP']/Signalisation_Area_ID"/>

	<Parameters>
		<File name="TEL\OPCClient_TEL_External_{$SERVER_ID}{$SERVER_LEVEL}.xml"/>
		<xsl:if test="$SERVER_LEVEL = '_LV1'">
			${gen_file([('TEL\TPBerthTrainIndicator_{$SERVER_ID}.xml'),
				('TEL\S2KOPCUAClient_{$SERVER_ID}.xml'),				
				('TEL\OPCClient_TEL_MS_Server_{$SERVER_ID}.xml'),					
				('TEL\OPCClient_TEL_IntraServer_{$SERVER_ID}.xml'),			
				('TEL\OPCAnalog_{$SERVER_ID}_Alarms.xml'),
				('TEL\HMITrainAttributes.xml'),
				('TEL\AlarmCustomLabel.xml'),
				('TEL\Train_Event{$SERVER_LEVEL}.xml'),
				('TEL\HMITrainRSMCommand.xml'),
				('TEL\MMSDat_{$SERVER_ID}.xml'),
				('TEL\RSTOMSettingRequest.xml'),
				('TEL\ISCSStation_{$SERVER_ID}.xml'),
				('TEL\CalcHMITrainAttributes.xml'),
				('TEL\ATSSupervisionTEL_{$SERVER_ID}.xml'),
				('TEL\HoldJustif_{$SERVER_ID}.xml'),				
				('TEL\SPKSSummary_{$SERVER_ID}.xml'),
				('TEL\BlockParams_{$SERVER_ID}.xml'),
				('TEL\HMITrainPropertyBag_{$SERVER_ID}.xml'),				
				('TEL\PointParams_{$SERVER_ID}.xml'),
				('TEL\TagNoteManager_{$SERVER_ID}.xml'),
				('TEL\HMIStablingLocation_{$SERVER_ID}.xml'),
				('TEL\FEP_Status_{$SERVER_ID}.xml'),
				('TEL\SW_RT_Status_{$SERVER_ID}.xml'),
				('TEL\PointOut_{$SERVER_ID}.xml'),								
				('TEL\Links_Status_{$SERVER_ID}.xml'),				
				('TEL\Links_Status_EXT_{$SERVER_ID}.xml'),			
				('TEL\TrainRestrictionAreaTEL_{$SERVER_ID}.xml'),				
				('TEL\ServerLinks_{$SERVER_ID}.xml'),				
				('TEL\MergeOPCConnected12Y_{$SERVER_ID}.xml'),				
				('TEL\LX_Alarm_Side_{$SERVER_ID}.xml'),				
				('TEL\TELObjectPassedAtDanger_{$SERVER_ID}.xml'),						
				('TEL\TrackingObjectAE_{$SERVER_ID}.xml'),				
				('TEL\EVSCP_ENB_status_{$SERVER_ID}.xml'),	
				('TEL\EVSCP_NOT_ENB_status_{$SERVER_ID}.xml'),				
				('TEL\Alarm_MS_{$SERVER_ID}.xml'),				
				('TEL\Alarm_SUP_{$SERVER_ID}.xml'),				
				('TEL\TC_Temperature_{$SERVER_ID}.xml')	,			
				('TEL\Surge_Arrestor_{$SERVER_ID}.xml'),										
				('TEL\AspectIndicator_{$SERVER_ID}_Alarms.xml'),							
				('TEL\SPKS_{$SERVER_ID}_Alarms.xml'),					
				('TEL\TrackSection_{$SERVER_ID}_Alarms.xml'),					
				('TEL\LinksAB_{$SERVER_ID}_Alarms.xml'),					
				('TEL\Lamp_Alarm_{$SERVER_ID}_Alarms.xml'),					
				('TEL\Level_Crossing_{$SERVER_ID}_Alarms.xml'),	
				('TEL\Server_{$SERVER_ID}_Alarms.xml'),									
				('TEL\FEP_MS_Links_Alarms_{$SERVER_ID}.xml'),					
				('TEL\MS_Variable_{$SERVER_ID}.xml'),					
				('TEL\MS_Server_Network_FAILURE_{$SERVER_ID}.xml'),					
				('TEL\HMITrainMaxDwellTimeActivation.xml'),
				('TEL\CBI_Points_{$SERVER_ID}_Alarms.xml'),
				('TEL\SPKS_Inconsistent_{$SERVER_ID}_Alarms.xml'),
				('TEL\Equipment_{$SERVER_ID}_Alarms.xml'),
				('TEL\NMS_{$SERVER_ID}_Alarms.xml'),
				('TEL\Connect_UA_{$SERVER_ID}.xml'),
				('TEL\Train_Alarm.xml'),
				('TEL\Level_Crossing_{$SERVER_ID}_Events.xml'),
				('TEL\General_Events_{$SERVER_ID}.xml'),
				('TEL\RateOfChange_{$SERVER_ID}.xml'),
				('TEL\MS_CC_{$SERVER_ID}_Alarms.xml'),
				('TEL\TAS_{$SERVER_ID}_Alarms.xml'),
				('Group\Group_S2KAlarms_Gen_{$SERVER_ID}.xml'),					
				('Group\Group_S2KEvents_Merge_{$SERVER_ID}.xml')
				])}
				<xsl:if test="//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID]/ATS_Equipment_Type='Gateway'">	
					<File name="TEL\S2KGALRedundantPing_{$SERVER_ID}.xml"/>
				</xsl:if>
				<xsl:if test="//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID]/ATS_Equipment_Type='Local Server' and contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot')">	
					<File name="TEL\TOMTTDepotMgr_{$SERVER_ID}.xml"/>
				</xsl:if>				
				<File name="TEL\HMIStringInformation_{$SERVER_ID}.xml"/>
				<File name="TEL\GenericAreaTEL_{$SERVER_ID}{$SERVER_LEVEL}.xml"/>
				<File name="TEL\TELLV1_{$SERVER_ID}.xml"/>
				<xsl:for-each select="//ZCs/ZC[@ID = //Signalisation_Area[@ID = $SIG_SERVER_ID]/sys:zc/@id]">
					<File name="TEL\EvacuationZoneSecCause_{@ID}.xml"/>
					<File name="TEL\SDDGroup_{@ID}.xml"/>
					<File name="TEL\Sector_{@ID}.xml"/>
					<File name="TEL\EvacuationZoneRequest_Event_{@ID}.xml"/>
					<File name="TEL\GAMA_Zone_{@ID}_Events.xml"/>
				</xsl:for-each>
				<xsl:for-each select="//CBIs/CBI[@ID = //Signalisation_Area[@ID = $SIG_SERVER_ID]/sys:cbi/@id]">
					<File name="TEL\CBI_Sector_{@ID}_Events.xml"/>
					<File name="TEL\SPKS_{@ID}.xml"/>
					<File name="TEL\Point_{@ID}_Events.xml"/>
					<File name="TEL\Signal_{@ID}.xml"/>
					<File name="TEL\TELLevelCrossing_{@ID}.xml" />
					<File name="TEL\TELLevelCrossingDevice_{@ID}.xml"/>
					<File name="TEL\Route_{@ID}_Events.xml"/>
					<File name="TEL\TrafficLocking_{@ID}_Events.xml"/>
					
				</xsl:for-each>				
				<xsl:for-each select="$urbalisSector">
					<File name="TEL\TSRAppliedTimeout_{@ID}.xml"/>
				</xsl:for-each>
		</xsl:if>
		<xsl:if test="$SERVER_LEVEL = '_LV2'">
			${gen_file([('TEL\OPCClient_TEL_SRV_{$SERVER_ID}_LV2.xml'),
				('TEL\TELLV2_{$SERVER_ID}.xml'),
				('TEL\PSD_{$SERVER_ID}_Alarms.xml'),
				('TEL\PSDStatus_{$SERVER_ID}.xml'),
				('TEL\PIMMonitor_{$SERVER_ID}.xml'),
				('TEL\AHMLastStopLink_{$SERVER_ID}.xml'),
				('TEL\SkipStatus_{$SERVER_ID}.xml'),
				('TEL\TTISStationLink_{$SERVER_ID}.xml'),
				('TEL\SAHMEmerStopPointLink_{$SERVER_ID}.xml'),
				('TEL\SAHMRegPoint_{$SERVER_ID}.xml'),
				('TEL\MonitoringTrainsZone_{$SERVER_ID}_Alarms.xml'),
				('TEL\CCM_Parameter.xml'),
				('TEL\AlarmCustomLabel.xml'),
				('TEL\ATRMissed_{$SERVER_ID}.xml'),
				('TEL\PSDTimeSyncModule_{$SERVER_ID}.xml'),
				('TEL\PSDDoorStatus_{$SERVER_ID}.xml'), 
				('TEL\AISSetterPoint_{$SERVER_ID}.xml'),
				('TEL\AISStatusAspect_{$SERVER_ID}.xml'),				
				('TEL\EVACSGlobalHold_{$SERVER_ID}.xml'),
				('TEL\Links_Status_EXT_{$SERVER_ID}.xml'),	
				('TEL\Links_Status_{$SERVER_ID}.xml'),				
				('TEL\SkipPFStatus_{$SERVER_ID}.xml'),
				('TEL\HoldSkipPointTEL_{$SERVER_ID}.xml'),
				('TEL\Alarm_PSD_{$SERVER_ID}.xml'),
				('TEL\Train_Event{$SERVER_LEVEL}.xml'),
				('TEL\General_Events_{$SERVER_ID}.xml'),
				('Group\Group_S2KAlarms_Gen_{$SERVER_ID}.xml'),
				('Group\Group_S2KEvents_Merge_{$SERVER_ID}.xml')								
				])}
			<File name="TEL\GenericAreaPropertyBagTEL_{$SERVER_ID}{$SERVER_LEVEL}.xml"/>
			<File name="TEL\GenericAreaTEL_{$SERVER_ID}{$SERVER_LEVEL}.xml"/>
			<xsl:if test="$areatype='ATS_Central_Server'"><File name="TEL\PIMLATSStatus_{$SERVER_ID}.xml"/></xsl:if>
			<xsl:if test="$areatype='ATS_Local_Server'"><File name="TEL\CATSStatus_{$SERVER_ID}.xml"/></xsl:if>
			<xsl:for-each select="//Function[@Signalisation_Area_ID=//Signalisation_Area[Area_Type='ATS' and sys:sigarea/@id=$SIG_SERVER_ID and @ID=$feprefsigid]/@ID]">
				<File name="TEL\PSDOPCServer_{@ID}.xml"/>
				<File name="TEL\MergeOPCUAConnectedXY_{@ID}.xml"/>
				<File name="TEL\S2KOPCUAClient_{@ID}.xml"/>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="$otherarea = true() and $SERVER_LEVEL != '_LV2'">
			${iter_equipment('$CBIs', 'DLC', ['Interlocking_Adjacent','Signal_Adjacent', 'Group_Signal_Adjacent'])}
		</xsl:if>
	</Parameters>
	<SigRules>
		<xsl:if test="$SERVER_LEVEL = '_LV1'">
			<File name="TEL\SigRules_HMITrainRSMCommand.xml" check="N"/>
			<File name="TEL\SigRules_ATSSupervisionTEL_{$SERVER_ID}.xml"/>
			<File name="TEL\SigRules_MaxDwellTimeActivation.xml" check="N"/>
			<xsl:for-each select="//ZCs/ZC[@ID = //Signalisation_Area[@ID = $SIG_SERVER_ID]/sys:zc/@id]">
				${gen_file([('TEL\SigRules_EvacuationZoneSecCause_{@ID}.xml')])}
			</xsl:for-each>
			<xsl:for-each select="//CBIs/CBI[@ID = //Signalisation_Area[@ID = $SIG_SERVER_ID]/sys:cbi/@id]">
				<File name="TEL\SigRules_TELLevelCrossing_{@ID}.xml" check="N"/>
				<File name="TEL\SigRules_TELLevelCrossingDevice_{@ID}.xml" check="N"/>
			</xsl:for-each>				
			<File name="TEL\SigRules_SPKSSummary_{$SERVER_ID}.xml" check="N"/>
		</xsl:if>
		<xsl:if test="$SERVER_LEVEL = '_LV2'">
			${gen_file([('TEL\SigRules_PSDStatus_{$SERVER_ID}.xml')])}
			${gen_file([('TEL\SigRules_PSDDoorStatus_{$SERVER_ID}.xml')])}
			${gen_file([('TEL\SigRules_PSDTimeSyncModule_{$SERVER_ID}.xml')])}
			${gen_file([('TEL\Sigrules_RouteSettingObject_{$SERVER_ID}.xml')])}
		</xsl:if>
		<xsl:if test="$otherarea = true() and $SERVER_LEVEL != '_LV2'">
			${iter_equipment('$CBIs', 'DLC', ['SigRules_Signal_Adjacent'])}
		</xsl:if>
	</SigRules>
	<Export directory="D:\DataPrep\IDC\IconisAppl">
		<ObjectModel FileName="AppObjectsTEL_{$SERVER_ID}{$SERVER_LEVEL}.xml"/>
	</Export>
</IDC>
</xsl:template>
</xsl:stylesheet>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="versions_xml"/>
<%def name="gen_file(list_files)">
	% for item  in list_files :
		<File name="${item}"/> 
	% endfor
</%def>
<%def name="iter_equip(file,equp)">
	<xsl:for-each select="${equp}">
		<File name="${file}_{@id}.xml"/>
	</xsl:for-each>
</%def>
<%def name="iter_equipment(equipment_list, dir, fnames)">
			<xsl:for-each select="${equipment_list}">			
	        % for fname in fnames:
				<xsl:variable name="path">../output/IDC/${dir}/${fname}_<xsl:value-of select="@id"/>.xml</xsl:variable>
				<xsl:variable name="filestatus" select="unparsed-text-available($path)"/>
				<xsl:if test="$filestatus">
					<File name="${dir}\${fname}_{@id}.xml"/>
				</xsl:if>
	        % endfor
			</xsl:for-each>
</%def>