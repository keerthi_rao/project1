<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TTName" select= "concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="Eqid" select="concat((number(@ID+2000)*10),'/SIG/ATC')"/>
	<xsl:variable name="bstrp1" select="if (Train_Unit_Characteristics_ID='1') then concat('PV',format-number(@ID,'000')) 
	                                    else concat('PV',format-number(number(@ID)-150,'000'))"/>
	${obj('{$TTName}',[('.Alarm.DFO','Train %s1: Doors Failed To Open','1','2','.ProjAttributes6.HMITETrain','B','longPlug_7','N','Fault','Normal'),
					   ('.Alarm.DFC','Train %s1: Doors Failed To Close','1','4','.ProjAttributes6.HMITETrain','B','longPlug_7','N','Fault','Normal'),		   
					   ('.Alarm.CCResetRequest','Train %s1: Slave CC Reset Needed','2','3','.ProjAttributes3.HMITETrain','B','longPlug_5','N','-','-')])}		   
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>



<%def name="obj(nm,list)">
	% for key,description,sev,stpv,nm_pv,o_m_b,outpv,avalanche,v_on,v_off in list:
		<xsl:variable name="omb_val" select="'${o_m_b}'"/>
		<xsl:variable name="mulval" select="'${description}'"/>
		<xsl:variable name="avalanche" select="'${avalanche}'"/>
		<xsl:variable name="v_on" select="'${v_on}'"/>
		<xsl:variable name="v_off" select="'${v_off}'"/>
		
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>				
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>	
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$bstrp1"/></Property>		
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$TTName"/>${nm_pv}</Property>
				<Property name="Out_PV_Trig" dt="string">${outpv}</Property>	
				<Property name="Severity" dt="i4">${sev}</Property>
				<Property name="State_PV_Trig" dt="i4">${stpv}</Property>		
				${multilingualvalue('$bstrp1','Name')}			
				${multilingualvalue('$Eqid','EquipmentID')}
				${multilingualvalue('$omb_val','OMB')}
				${multilingualvalue('$avalanche','Avalanche')}
				<Property name="Operator" dt="string">-</Property>			
				${multilingualvalue('$v_on','Value_on')}			
				${multilingualvalue('$v_off','Value_off')}			
			</Properties>
		</Object>
	% endfor
</%def>
