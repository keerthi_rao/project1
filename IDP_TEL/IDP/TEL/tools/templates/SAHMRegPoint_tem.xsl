<%inherit file="/Object_tem.mako"/>
<%! class_ = "SAHMRegPoint" %>

<%block name="classes">

	<Class name="SAHMRegPoint">
	     <Objects>
			 <xsl:apply-templates select="//$sysdb//Monitoring_Trains_Zone[((Inter_Stopping_Area_ID=//Inter_Stopping_Area[sys:block_list/sys:block/@id=//Block[sys:sigarea/@id=$SERVER_ID]/@ID]/@ID) or (Block_ID_List/Block_ID=//Block[sys:sigarea/@id=$SERVER_ID]/@ID)) and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Monitoring_Trains_Zone">
	<xsl:variable name="blockidlist" select="Block_ID_List/Block_ID"/>
	<xsl:variable name="inters" select="Inter_Stopping_Area_ID"/>
	<xsl:variable name="sta" select="//Stopping_Area[@ID=//Inter_Stopping_Area[@ID=current()/Inter_Stopping_Area_ID]/Stopping_Area_Origin_ID]/@Name"/>
	<xsl:variable name="firstBlock" select="Block_ID_List/Block_ID[1]"/>
	<xsl:variable name="secondBlock" select="Block_ID_List/Block_ID[2]"/>
	<xsl:variable name="isa" select="//Inter_Stopping_Area[every $idf in sys:block_list/sys:block/@id satisfies $idf=current()/Block_ID_List/Block_ID]"/>
	<xsl:variable name="isafirst" select="$isa[sys:block_list/sys:block[@id=$firstBlock]/following-sibling::*/@id=$secondBlock]"/>
	<xsl:variable name="sta2" select="//Stopping_Area[@ID=$isafirst/Stopping_Area_Origin_ID]/@Name"/>
	<xsl:variable name="blkevery" select="if (every $blkk in $blockidlist satisfies ($blkk=//Block[sys:sigarea/@id=$SERVER_ID and @ID=$blkk]/@ID)) then 'True' else null"/>	
	<!--xsl:if test="$inters!='' or $blkevery!=''"-->
		<Object name="SAHM_RegPoint_{@Name}" rules="update_or_create">	
		  <Properties>
				<Property name="MonitoringTrainZoneID" dt="string"><xsl:value-of select="@Name"/>.MTZ</Property>
				<Property name="MaxTrainNumberInit" dt="i4"><xsl:value-of select="Max_Nb_Trains" /></Property>
				<Property name="HoldReason" dt="string">SAHM_RegPoint_<xsl:value-of select="@Name" /></Property>
				<Property name="HoldSkipPoint" dt="string"><xsl:value-of select="if($sta!='' and $inters!='') then ($sta) else ($sta2)"/>.HoldSkip</Property>						
				<Property name="VentilationArea" dt="boolean"><xsl:value-of select="if($inters!='') then ('True') else ('False')"/></Property>			
				<Property name="InterstationArea" dt="boolean"><xsl:value-of select="if($inters='') then ('True') else ('False')"/></Property>					
			</Properties>
		</Object>	
	<!--/xsl:if-->	
</xsl:template>

