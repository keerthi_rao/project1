<%inherit file="/Object_tem.mako"/>
<%! class_ = "OPCClient" %>
<%!
LIST=[('ISCSStation','pia','$sysdb//ISCSStation[Geographical_Area=//Geographical_Area[Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID=//Signalisation_Area[@ID=$SERVER_ID]/Area_Boundary_Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID]/@ID]','PIA_','.Point.OutAutomaton', ['NA']),
     ('PIMPoint','pia','$sysdb//PIMPoint[Block_ID_List/Block_ID/text()=//Block[sys:sigarea/@id=$SERVER_ID]/@ID]','PIA_','.Point.OutAutomaton', ['NA']),
	 ('PIMPlatform','pia','$sysdb//PIMPlatform[(LeftInterstation/Block_ID=//Signalisation_Area[@ID=$SERVER_ID]/Block_ID_List/Block_ID) or (RightInterstation/Block_ID=//Signalisation_Area[@ID=$SERVER_ID]/Block_ID_List/Block_ID)]','PIA_','.Point.OutAutomaton', ['Left','Right'])]
 %>
 <xsl:param name="IS_BOTH_LEVEL"/>
<%block name="classes">
	<Class name="S2KIOOPCServerMonitor">
		<Objects>
			<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID]" mode="S2KIOOPCServerMonitor"/>
		</Objects>
	</Class>
	<Class name="OPCGroup">
		<Objects>
			<xsl:apply-templates select="//Function[@ID=$FUNCTION_ID]" mode="OPCGroup"/>
		</Objects>
	</Class>
	<Class name="Variable">
		<Objects>
		 	% for cls, mode, gen, var, opctag, di in LIST:
				<xsl:apply-templates select="${gen}" mode="${mode}"/>
			% endfor
		</Objects>
	</Class>
</%block>
<xsl:template match="Function" mode="OPCGroup">
	<Object name="Split_{@Signalisation_Area_ID}" rules="update_or_create">
		${prop("OPCClientRef",'OPCClient_ATS_<xsl:value-of select="@Signalisation_Area_ID"/>')}
		${prop("SharedOnIdentifier",'Split_<xsl:value-of select="@Signalisation_Area_ID"/>')}
	</Object>
</xsl:template>

<xsl:template match="Function" mode="S2KIOOPCServerMonitor">
	<xsl:variable name="ats_eqps" select="//ATS_Equipment[Function_ID = $FUNCTION_ID or Function_ID_List/Function_ID = $FUNCTION_ID]/@Name"/>
	<Object name="OPCClient_ATS_{@Signalisation_Area_ID}" rules="update_or_create">
		${prop("ConnectAttemptsPeriod","10","i4")}
		${prop("DoubleLinks","0","boolean")}
		${prop("MonitoringPeriod","30")}
		${prop("MultiActive","1","boolean")}
		${prop("ServerNodeName1",'<xsl:value-of select="$ats_eqps[1]"/>')}
		${prop("ServerNodeName2",'<xsl:value-of select="$ats_eqps[2]"/>')}
		${prop("ServerProgID1","S2K.OpcServer.1")}
		${prop("ServerProgID2","S2K.OpcServer.1")}
		${prop("SharedOnIdentifierDefinition","1","boolean")}
		${prop("SharedOnIdentifier",'OPCClient_ATS_<xsl:value-of select="@Signalisation_Area_ID"/>')}
	</Object>
</xsl:template>

% for cls, mode, gen, var, opctag, di in LIST:
	<xsl:template match="${cls}" mode="${mode}">
		% for dir in di:
			<xsl:variable name="st" select="if('${dir}'='NA') then 'True' else if('${dir}'='Left') then LeftInterstation else RightInterstation"/>
			<!--print><xsl:value-of select="$st"/></print-->
			<xsl:if test="$st">
				<xsl:variable name="nm" select="if('${dir}'='NA') then @Name else concat('PIM_',StationID,'_', PlatformID,'_${dir}')"/>
				<xsl:variable name="objname" select="concat('${var}',$nm)"/>
				<Object name="{$objname}" rules="update_or_create">
					<Properties>
						<Property name="Type" dt="string">RM</Property>
						<Property name="OPCType" dt="string">VT_BSTR</Property>
						<Property name="OPCGroupID" dt="string">Split_<xsl:value-of select="//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID"/></Property>
						<Property name="OPCGroup" dt="string">self</Property>
						<Property name="OPCTag" dt="string"><xsl:value-of select="concat($nm,'${opctag}')"/></Property>
						<Property name="Address" dt="i4"/>
					</Properties>
				</Object>
			</xsl:if>
		% endfor
	</xsl:template>
% endfor
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>