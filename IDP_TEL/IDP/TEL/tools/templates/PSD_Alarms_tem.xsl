<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
  		</Objects>
  	</Class>
</%block>


<xsl:template match="Platform_Screen_Doors">

<xsl:variable name="atsname" select="//ATS_Equipment[Function_ID_List/Function_ID=//Function[@Signalisation_Area_ID=//Signalisation_Area[@ID=//ATS[@ID=current()/sys:ats/@id]/ATS_Area_ID and not(contains(Localisation_Type,'Depot'))]/@ID]/@ID and ATS_Equipment_Type='FEP'][1]/@Name"/>
		<xsl:variable name="TTName" select= "concat(@Name,'.Alarms_Gen')"/>
		<xsl:variable name="nm" select= "@Name"/>
		<xsl:variable name="tRoom" select= "//Technical_Room[@ID=current()/Technical_Room_ID]/@Name"/>
		<xsl:variable name="fepnm" select="if (substring($atsname, string-length($atsname)-3) = 'FEPA') then 'FEP_A' else 'FEP_B'"/>
		<xsl:variable name="uname" select="concat(substring-after($tRoom,'SER_'),'/SIG/ATS/',$fepnm)"/>
<xsl:if test="$SERVER_LEVEL='_LV2'">		
		<Object name="{$TTName}" rules="update_or_create">
			<Properties>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$uname','EquipmentID')}
				${multilingualvalue("'B'",'OMB')}
				<!-- ${prop('AlarmInhib','1','boolean')} -->
				<Property name="Alarm_Label" dt="string">Platform %s1: LATS FEP %s2 - PSD Total Communication Failure</Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="substring($atsname, string-length($atsname))"/></Property>
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="@Name"/>.State.PV_Links_A</Property>
				<Property name="Severity" dt="i4">1</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
			</Properties>
		</Object>	
</xsl:if>
<xsl:if test="$SERVER_LEVEL='_LV2'">
	<xsl:variable name="TTName1" select="@Name"/>
				${obj('{$TTName1}',[('.Alarm_LinkA','Platform %s1: LATS FEP %s2 - ','PSD Link A Failure','','IntOutputPlug_2'),
									('.Alarm_LinkB','Platform %s1: LATS FEP %s2 - ','PSD Link B Failure','','IntOutputPlug_3')])}									
</xsl:if>

</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, prop"/>


<%def name="obj(nm,list)">
	% for key, description1,pv,bl,rt in list:
		<Object name="${nm}${key}" rules="update_or_create">
			<Properties>
				${multilingualvalue("'-'",'Avalanche')}
				${multilingualvalue('$nm','Name')}
				${multilingualvalue('$uname','EquipmentID')}
				${multilingualvalue("'B'",'OMB')}
				<!-- ${prop('AlarmInhib','1','boolean')} -->
				<Property name="Alarm_Label" dt="string">${description1}${pv}</Property>
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$nm"/></Property>
				<Property name="BstrParam2" dt="string"><xsl:value-of select="substring($atsname, string-length($atsname))"/></Property>
				<Property name="Name_PV_Trig" dt="string">PSDOPCClient_PSDSERVER_<xsl:value-of select="$SERVER_ID"/>.MergeConnectedXY.Merge</Property>
				<Property name="Severity" dt="i4">2</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>
				<Property name="Out_PV_Trig" dt="string">${rt}</Property>
			</Properties>
		</Object>
	% endfor
</%def>