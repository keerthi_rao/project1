<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
  	<Class name="S2KEvents_Merge">
  		<Objects>
  			<xsl:apply-templates select="//CBI[@ID=$CBI_ID]"/>
  		</Objects>
  	</Class>	
</%block>	

<xsl:template match="CBI">
	<xsl:variable name="te" select="substring-after(//Technical_Room[@ID=current()/Technical_Room_ID]/@Name,'_')"/>
	<xsl:apply-templates select="//Events/Event[Type='CBI_Sector']">
		<xsl:with-param name="parentname" select = "@Name"/>
		<xsl:with-param name="blockid" select = "//Block[sys:cbi/@id=current()/@ID]/@ID"/>
		<xsl:with-param name="urbalissector" select = "//Urbalis_Sector[@ID=current()/Urbalis_Sector_ID]/@Name"/>
		<xsl:with-param name="techroom" select = "$te"/>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "parentname"/>
	<xsl:param name = "blockid"/>
	<xsl:param name = "urbalissector"/>
	<xsl:param name = "techroom"/>
	<xsl:variable name="objname" select="concat($parentname,@Name)"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$parentname),'xxxx',$techroom)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (if(contains(current()/Name_PV_Trig,'xxxx')) then (replace(Name_PV_Trig,'--xxxx--',$techroom)) else    (replace(Name_PV_Trig,'--Urbalis_Sector.Name--',$urbalissector))) else null"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="tracktype" select="//Track[@ID=//Block[@ID=$blockid and sys:cbi/@id=$CBI_ID]/Track_ID]/Track_Type"/>
	<xsl:variable name="optr" select="if($tracktype='Depot') then ('MDD') else if($tracktype='Test track') then ('TTCU') else if($tracktype='Mainline') then ('TAS') else null"/>
	<xsl:variable name="terrname" select="//Block[@ID=$blockid and sys:cbi/@id=$CBI_ID]/sys:terr/@name"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$urbalissector"/></Property>
			<Property name="BstrParam2" dt="string"><xsl:value-of select="BstrParam2"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname[1],'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
			${multilingualvalue('$parentname','Name')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

