<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
	<xsl:apply-templates select="$sysdb//Signalisation_Area[(Area_Type='ATS_Central_Server'  and $SERVER_TYPE='ATS_Local_Server' and sys:sigarea/@id=$SERVER_ID) or (Area_Type='ATS_Local_Server' and $SERVER_TYPE='ATS_Central_Server' and sys:sigarea/@id=$SERVER_ID)]"/>
</%block>

<!-- Template for generating equations from Platforms Table-->
<xsl:template match="Signalisation_Area">
	<xsl:variable name="eqn" select="concat('ATS_', $SERVER_ID,'.OTHERATSTEL.ATS_',@ID, '.')"/>
	${Equipment('ATSSupervisor','State',[('ATSSUPDISTANTSTATE_NOTRUNNING[0]','<xsl:value-of select="$eqn"/>PVSupervisorState.Value==0 &amp;&amp; <xsl:value-of select="$eqn"/>PVSupervisorRedundancyStatus.Value==0'),
			   ('ATSSUPDISTANTSTATE_RUNNING[1]','<xsl:value-of select="$eqn"/>PVSupervisorState.Value==1 || <xsl:value-of select="$eqn"/>PVSupervisorRedundancyStatus.Value==1'), 
			   ('ATSSUPDISTANTSTATE_UNKNOWN[2]','1')])}	 		   
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="Equipment(type, flavor, list)" >
  <Equipment name="{concat('ATS_',$SERVER_ID,'.OTHERATSTEL.ATS_',@ID)}" type="${type}" flavor="${flavor}">
	<States>
	% for name,equation in list:
     <Equation>${name}::${equation}</Equation>
	% endfor
	</States>
  </Equipment>
</%def>