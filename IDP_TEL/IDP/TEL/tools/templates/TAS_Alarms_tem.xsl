<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
	     <Objects>
			<xsl:apply-templates select="$sysdb//Urbalis_Sector[*/Track_ID=//Block[sys:sigarea/@id=$SERVER_ID]/Track_ID]"/>	
		 </Objects>
	</Class>
</%block>

<xsl:template match="Urbalis_Sector">
	<xsl:variable name="secnum" select="substring-after(@Name,'SEC_')"/>
	<xsl:variable name="secnm" select="concat('Territory_',substring-after(@Name,'SEC_'))"/>
	<xsl:variable name="TTName" select= "concat($secnm,'.Alarms_Gen')"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="loc" select="if (contains($secnum,'MDD')) then ('MDD') else concat('TE',format-number(number($secnum)+1,'00'))"/>
	<xsl:variable name="uname" select="concat($loc,'/SIG/ATS/-')"/>
	<Object name="{$TTName}" rules="update_or_create">
		<Properties>
			${multilingualvalue("'No'",'Value_on')}
			${multilingualvalue("'Yes'",'Value_off')}
			${multilingualvalue('$uname','EquipmentID')}
			${multilingualvalue("'O'",'OMB')}
			<Property name="Alarm_Label" dt="string">Territory %s1 Not Assigned</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$secnm"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$secnm"/></Property>
			<Property name="Severity" dt="i4">3</Property>
			<Property name="State_PV_Trig" dt="i4">0</Property>
			<Property name="Out_PV_Trig" dt="string">IsControlled</Property>
		</Properties>
	</Object>			
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
