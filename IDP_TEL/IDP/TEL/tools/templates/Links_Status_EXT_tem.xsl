<%inherit file="/Object_tem.mako"/>
<%! class_ = "Links_Status_EXT" %>

<%!
LIST=[('ISCS','OPCClient_ISCS_LV1','RDD.1.RDDTELOPCMDBGTW','_LV1','localhost','localhost'),
      ('PIM','OPCClient_PIMModule','RDD.1.RDDPIMCOM', '_LV2','localhost','localhost'),
	  ('MMS','OPCClient_MMSModule','RDD.1.RDDMMSCOM', '_LV1','localhost','localhost'),
	  ('TWP','OPCClient_TWP','ModbusProtocol.1.T281OPCMDBGTW', '_LV2','MDDDSERSRVA','MDDDSERSRVB')]
 %>

<%block name="classes">
  	<Class name="Links_Status_EXT">
  		<Objects>
			% for typ, nam, opctag, lv ,snn1,snn2 in LIST:
			<xsl:if test="$SERVER_LEVEL='${lv}'">
			<xsl:variable name="nm" select="concat('Links_Status_EXT.','${typ}')"></xsl:variable>
			<Object name="{$nm}" rules="update_or_create">
				<Properties>
					<Property name="AdressInLevel1_A" dt="string">Diags1.LinkA</Property>
					<Property name="AdressInLevel1_B" dt="string">Diags1.LinkB</Property>
					<Property name="SOI_OPCClient" dt="string">${nam}</Property>
				</Properties>
			</Object>	
			</xsl:if>
			% endfor
  		</Objects>
  	</Class>
</%block>



