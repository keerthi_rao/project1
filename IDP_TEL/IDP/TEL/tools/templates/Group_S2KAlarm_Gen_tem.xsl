<%inherit file="/Group_tem.mako"/>
<%! 
xs_ = True
%>
<xsl:variable name="sig_curr" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
<xsl:variable name="servers" select="if (//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server') then (//Signalisation_Area[Area_Type='ATS_Local_Server' and not(contains(Localisation_Type,'Depot'))]/@ID) else $SERVER_ID"/>
<xsl:variable name="servers1" select="if (//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="servers_noDATS" select="if (//Signalisation_Area[@ID=$SERVER_ID]/Area_Type='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID and not(contains(Localisation_Type,'Depot'))]/@ID) else ($SERVER_ID)"/>
<xsl:variable name="sig_linksab" select="//Signalisation_Area[@ID=$servers]"/>
<xsl:variable name="locs" select="distinct-values((for $i in (//Signalisation_Area[@ID=$servers_noDATS]/sys:geo/@name) return (substring-after($i,'ISCS_'))))"/>
<%block name="classes">
	<Class name="S2KAlarms_Gen" rules="update" traces="error">
		<Objects>
			<xsl:if test="$SERVER_LEVEL='_LV1'">
				<xsl:apply-templates select="//MSS_SMIO/SMIO[Signalisation_Area_ID=$SERVER_ID and (contains(@Name,'ATC') or contains(@Name,'CBI'))]"/>
				<xsl:apply-templates select="$sysdb//Urbalis_Sector[*/Track_ID=//Block[sys:sigarea/@id=$SERVER_ID]/Track_ID]"/>	
				<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and (contains(@Name,'SAF24V'))]" mode="sa"/>	
				<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and contains(@Name,'TEMP_TC')]" mode="tct"/>	
				<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]"/>	
				<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID and contains(@Name,'_ESP')]" mode="depot"/>	
				<xsl:apply-templates select="$sysdb//Signal[sys:cbi/@id=//Signalisation_Area[@ID=$SERVER_ID]/sys:cbi/@id]" mode="sig"/>	
				<xsl:apply-templates select="$sysdb//Secondary_Detection_Device[Secondary_Detection_Device_Characteristics_ID=//Secondary_Detection_Devices_Characteristic[not(Checkable='false')]/@ID and sys:sigarea/@id=$SERVER_ID]"/>
				<xsl:apply-templates select="//Signalisation_Area[@ID=$SERVER_ID and sys:lc/@id!='']"/>
				<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID and  (@ID= //LX_Device[Type='Safe To Proceed']/LX_ID)]" mode="stp"/>
				<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID and  (@ID= //LX_Device[Type='Safe To Proceed']/LX_ID)]" mode="gd"/>
				<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID and  contains(@Name,'LX3')]" mode="u400"/>
				<xsl:apply-templates select="//ATS_Equipment[ATS_Equipment_Type='Maintenance Server' and (Function_ID_List/Function_ID=$FUNCTION_ID or Signalisation_Area_ID=$SERVER_ID)]"/>
		  		<xsl:apply-templates select="//ATS_Equipment[Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID and (ATS_Equipment_Type ='Central Server' or ATS_Equipment_Type ='Local Server')]" mode="ServerLink"/>
				<xsl:apply-templates select="//ATC_Equipment[(ATC_Equipment_Type='LC' and @ID=$sig_curr/sys:lc/@id) or (ATC_Equipment_Type='ZC' and @ID=$sig_curr/sys:zc/@id)]"/>
				<xsl:apply-templates select="//ATC_Equipment[(ATC_Equipment_Type='LC' and @ID=$sig_linksab/sys:lc/@id) or (ATC_Equipment_Type='ZC' and @ID=$sig_linksab/sys:zc/@id)]" mode="linksab"/>
				<xsl:apply-templates select="//CBI[@ID=$sig_curr/sys:cbi/@id]"/>
				<xsl:apply-templates select="//CBI[@ID=$sig_linksab/sys:cbi/@id]" mode="linksab"/>
				<xsl:apply-templates select="$sysdb//Point[sys:cbi/@id=//Signalisation_Area[@ID=$SERVER_ID]/sys:cbi/@id]"/>	
				<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]" mode="INCON"/>	
				<xsl:apply-templates select="$sysdb//SPKS_Actuator[Type='Global_Availability' and SPKS_ID_List/SPKS_ID=//SPKS[sys:sigarea/@id=$SERVER_ID]/@ID]" mode="INCON"/>
				<xsl:apply-templates select="//MSS_lruState/lruState[(contains(@Name,'MDDSERSW') or contains(@Name,'SERSW'))  and  Type='Switch' and Signalisation_Area_ID=$SERVER_ID and (contains(//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/Localisation_Type,'Depot') or contains(//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/Localisation_Type,'Mainline'))]" mode="MSNFailure"/>
			    <xsl:apply-templates select="//Trains_Unit/Train_Unit[number(@ID)&lt;=30]" mode="Train_Alarm"/>
			<!-- MS Alarm -->
				<% mms_type_list=['Switch','Router','Server','Firewall','GTW','WKS HMI','WKS SIMU','WKS DEV','WKS Overview','Printer','Datalogger','FEP'] %>
				 %for typename in mms_type_list:
					<xsl:apply-templates select="//MSS_lruState/lruState[Type='${typename}' and Signalisation_Area_ID=$SERVER_ID]"/>				
				 %endfor
					<xsl:apply-templates select="//MSS_lruState/lruState[(Type='GTW' or Type='Server') and Signalisation_Area_ID=$SERVER_ID]" mode="Power"/>
					<xsl:apply-templates select="//MSS_lruState/lruState[Type='MS' and Signalisation_Area_ID=$SERVER_ID]" mode="Capacity"/>
				<xsl:for-each select="//Signalisation_Area[@ID=$servers1 and (contains(Localisation_Type,'Depot') or Area_Type='ATS_Central_Server')]">
					<xsl:apply-templates select="//PDC[(contains(Equipment,MDD) or contains(Equipment,OCC)) and not(contains(Equipment,';')) and Alarm_Event='Alarm']"><xsl:with-param name="sid" select="@ID"/></xsl:apply-templates>
				</xsl:for-each>
				<xsl:for-each select="//Signalisation_Area[@ID=$servers_noDATS and not(contains(Localisation_Type,'Depot'))]">
					<xsl:apply-templates select="//PDC[contains(Equipment,';') and Alarm_Event='Alarm']" mode="TE"/>
				</xsl:for-each>
				<!-- MS Alarm -->
				<xsl:variable name="sig" select="//Signalisation_Area[@ID=$SERVER_ID]"/>
				<xsl:for-each select="//ATS_Equipment[(*/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID=$SERVER_ID)]">
					<xsl:variable name="ty" select="ATS_Equipment_Type"/>
					<xsl:if test="$ty='Central Server'">
						<xsl:variable name="tt1" select="concat(@Name,'.CATS_Server_ISCS_Total_Communication_Failure')"/>
						${obj('{$tt1}','User/A')}
						<xsl:variable name="tt4" select="concat(@Name,'.CATS_Server_MMS_Total_Communication_Failure')"/>
						${obj('{$tt4}','User/-')}
					</xsl:if>
					<xsl:if test="$ty='Local Server' and $sig[contains(Localisation_Type,'Depot')]">
						<xsl:variable name="tt2" select="concat(@Name,'.DATS_Server_ISCS_Total_Communication_Failure')"/>
						${obj('{$tt2}','User/A')}
						<xsl:variable name="tt5" select="concat(@Name,'.DATS_Server_MMS_Total_Communication_Failure')"/>
						${obj('{$tt5}','User/-')}
					</xsl:if>
					<xsl:if test="$ty='Local Server' and $sig[contains(Localisation_Type,'Mainline')]">
						<xsl:variable name="tt3" select="concat(@Name,'.LATS_Server_ISCS_Total_Communication_Failure')"/>
						${obj('{$tt3}','User/A')}
					</xsl:if>
				</xsl:for-each>
				<xsl:apply-templates select="//CBI[@ID=//Signalisation_Area[@ID=$servers_noDATS]/sys:cbi/@id]" mode="scu"/>
				<xsl:apply-templates select="//ATS_Equipment[(contains(ATS_Equipment_Type,'Local Server') or contains(ATS_Equipment_Type,'Central Server')) and (Signalisation_Area_ID=$SERVER_ID or */Signalisation_Area_ID=$SERVER_ID)]" mode="SLF"/>
			</xsl:if>	
			<xsl:if test="$SERVER_LEVEL='_LV2'">
				<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
				<xsl:apply-templates select="//Station[Platform_ID_List/Platform_ID=//Platform[PSD_ID=//Platform_Screen_Doors[Technical_Room_ID!='1'  and sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]/@ID]/@ID]"/>
			</xsl:if>
		</Objects>
	</Class>
	<Class name="Lamp_Alarm" rules="update" traces="error">
		<Objects>
			<xsl:if test="$SERVER_LEVEL='_LV1'">
				<xsl:apply-templates select="$sysdb//Direction_Indicator[@ID=//Aspect_Indicator/Direction_Indicator_ID and sys:sigarea/@id=$SERVER_ID and Signal_Lamp_Type_ID=//Signal_Lamp_Type[@Name='SLTY_SWPD_SIG']/@ID]" mode="di1"/>	
				<xsl:apply-templates select="$sysdb//Direction_Indicator[@ID=//Aspect_Indicator/Direction_Indicator_ID and sys:sigarea/@id=$SERVER_ID and Signal_Lamp_Type_ID=//Signal_Lamp_Type[@Name='SLTY_RI']/@ID]" mode="di2"/>	
				<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID and Lamp_Proved_Signal='true' and Signal_Type_Position='Depot' and Track_ID=//Track[not(contains(@Name,'RT')) and  Track_Type!='Test track']/@ID]" mode="sig1"/>		
				<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID and Lamp_Proved_Signal='true']" mode="sig2"/>	
				<xsl:apply-templates select="$sysdb//LX_Device[LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID and (Type='Aspect Indicator') and (Signal_Lamp_Type_ID = //Signal_Lamp_Type[not(Proved_Lamp_List/Lamp_Name='None')]/@ID)]"  mode="tt5"/>
				<xsl:apply-templates select="$sysdb//LX_Device[LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID and contains(@Name,'LX3')]"  mode="tt6"/>
				<xsl:apply-templates select="$sysdb//Trains_Unit/Train_Unit"/>
				<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=$SERVER_ID]" mode="RedAlarm"/>
			</xsl:if>
		</Objects>
	</Class>	
</%block>

<xsl:template match="CBI" mode="scu">
	<xsl:variable name="cbinm" select="@Name"/>
	%for suff in ['.LRU_Alarm','.OverTemperature_Alarm','.MMILRU_Alarm','.UpperFAN_Alarm','.LowerFAN_Alarm']:
		%for dir in ['.SCULeft','.SCURight']:
			<xsl:variable name="objnm" select="concat($cbinm,'${dir}','${suff}')"/>
			${obj('{$objnm}','User/A')}
		%endfor
	%endfor
</xsl:template>


<xsl:template match="SMIO">
	%for i in range(1,49):
		<xsl:variable name="tt" select="concat(@Name,'.Alarm.LRU',${i},'Failed')"/>
		${obj('{$tt}','User/A')}
	%endfor
</xsl:template>
<xsl:template match="lruState" mode="MSNFailure">
	<xsl:variable name="tt1" select="concat(@Name,'.Alarm.MS_Server_NetworkAccess')"/>	
	${obj('{$tt1}','User/A')}
</xsl:template>
<xsl:template match="lruState" mode="Capacity">
	<xsl:variable name="tt1" select="concat(@Name,'.Alarm_Capacity_Limit')"/>	
	${obj('{$tt1}','User/A')}
</xsl:template>

<xsl:template match="PDC">
	<xsl:variable name="pdcnm" select="concat(@Name,'.Alarm')"/>
	<xsl:variable name="grp" select="concat('User/',MMS)"/>
	${obj('{$pdcnm}','<xsl:value-of select="$grp"/>')}
</xsl:template>
<xsl:template match="PDC" mode="TE">
	<xsl:variable name="name_pdc" select="@Name"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:for-each select="tokenize(Equipment,';')">
		<xsl:variable name="eqp" select="."/>
		<xsl:variable name="loc" select="tokenize($eqp,'_')[1]"/>
		<xsl:variable name="cond" select="if ($loc=$locs) then true() else false()"/>
		<xsl:variable name="tt7" select="concat($name_pdc,'_',$loc,'.Alarm')"/>
		<xsl:variable name="grp" select="concat('User/',$mms)"/>
		<xsl:if test="$cond">
			${obj('{$tt7}','<xsl:value-of select="$grp"/>')}
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Signal" mode="RedAlarm">
	<xsl:variable name="signamea" select="concat(@Name,'.Red_Alarm')"/>
	<xsl:variable name="urgrp" select="'User/A'"/>
	${obj('{$signamea}','<xsl:value-of select="$urgrp"/>')}
</xsl:template>

<xsl:template match="Point">
	<xsl:variable name="pnm1" select="concat(@Name,'.Alarm.MvtCounterThresholdXceed')"/>
	<xsl:variable name="pnm2" select="concat(@Name,'.Alarm.MvtTimeChangeValue')"/>
	<xsl:variable name="urgrp" select="'User/A'"/>
	${obj('{$pnm1}','<xsl:value-of select="$urgrp"/>')}
	${obj('{$pnm2}','<xsl:value-of select="$urgrp"/>')}
</xsl:template>

<xsl:template match="lruState">
	<xsl:variable name="lrunam" select="concat(@Name,'.Alarm')"/>
	<xsl:variable name="urgrp" select="if(Type='Server' or Type='WKS Overview') then ('User/S') else ('User/A')"/>
	${obj('{$lrunam}','<xsl:value-of select="$urgrp"/>')}
</xsl:template>

<xsl:template match="lruState" mode="Power">
	<xsl:variable name="pnm" select="if(Type='Server') then (concat(@Name,'.Status_Power_Supply.PV_Status')) else if(Type='GTW') then (@Name,'.Status_Over_Temp.PV_Status') else null"/>
	<xsl:variable name="urgrp" select="if(Type='Server' or Type='WKS Overview') then ('User/S') else ('User/A')"/>	
	${obj('{$pnm}','<xsl:value-of select="$urgrp"/>')}
</xsl:template>


<xsl:template match="ATS_Equipment" mode="ServerLink">
	<xsl:variable name="cobj" select="."/>
	<xsl:variable name="sigAreas" select="//Signalisation_Area[@ID=$SERVER_ID]"/>

	<xsl:variable name="adjma" select="//Adjacent_Monitoring_Area[ATS_Local_Server_Area_ID = $SERVER_ID or ATS_Central_Server_Area_ID = $SERVER_ID]"/>
	<xsl:variable name="adjSig" select="//Signalisation_Area[@ID=$adjma/sys:sigarea/@id or @ID=$sigAreas/*/Signalisation_Area_ID]"/>
	<xsl:apply-templates select="//ATS_Equipment[(Signalisation_Area_ID_List/Signalisation_Area_ID=$adjSig/@ID or Signalisation_Area_ID = $adjSig/@ID) and not(Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID or Signalisation_Area_ID = $SERVER_ID) and (ATS_Equipment_Type ='Central Server' or (ATS_Equipment_Type ='FEP' and $sigAreas/Localisation_Type='Mainline') or ATS_Equipment_Type ='Local Server')]" mode="child">
	<xsl:with-param name="cname" select = "@Name" />
	<xsl:with-param name="atstype" select = "ATS_Equipment_Type" />
	<xsl:with-param name="csigArea" select="$sigAreas"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="$adjSig[Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server' or (Area_Type='ATS' and Localisation_Type='Mainline')]" mode="child">
	<xsl:with-param name="cname" select = "@Name" />
	<xsl:with-param name="atstype" select = "$cobj" />
	<xsl:with-param name="csigArea" select="$sigAreas"/>
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Signalisation_Area" mode="child">
	<xsl:param name = "cname" />
	<xsl:param name="atstype" />
	<xsl:param name="csigArea" />
	
	<xsl:variable name="lsigArea" select="."/>
	<xsl:variable name="ctype" select="if(contains($csigArea/Localisation_Type, 'Depot')) then 'DATS' else (if($atstype/ATS_Equipment_Type='Central Server') then 'CATS' else if($atstype/ATS_Equipment_Type='Local Server') then 'LATS'  else 'FEP')"/>
	<xsl:variable name="ltype" select="if(current()/Localisation_Type='Mainline') then (if(Area_Type='ATS_Central_Server') then 'CATS' else if(Area_Type='ATS_Local_Server') then 'LATS'  else 'FEP') else 'DATS'"/>
	<xsl:if test="not($ctype='DATS' and Area_Type='ATS')">
			<xsl:variable name="bstrb2" select="//Urbalis_Sector[*/Signalisation_Area_ID=$SERVER_ID or */Signalisation_Area_ID=current()/@ID]"/>
			<xsl:variable name="lname" select="if($ltype='LATS' or $ltype='FEP') then concat($ltype, '_',$bstrb2[*/Signalisation_Area_ID=current()/@ID]/@Name) else if($ltype='DATS') then concat($ltype,replace(@Name, 'SIA_DATS', '')) else $ltype"/>
			<xsl:variable name="nm" select="concat($cname, '.Alarm_TotalComFail.',$lname)"/>

			${obj('{$nm}','User/A')}
	</xsl:if>
</xsl:template>

<xsl:template match="ATS_Equipment" mode="child">
	<xsl:param name = "cname" />
	<xsl:param name="atstype" />
	<xsl:param name="csigArea" />
	<xsl:variable name="lsigArea" select="//Signalisation_Area[@ID=current()/*/Signalisation_Area_ID or @ID=current()/Signalisation_Area_ID]"/>
	
	<xsl:variable name="ctype" select="if(contains($csigArea/Localisation_Type, 'Depot')) then 'DATS' else (if($atstype='Central Server') then 'CATS' else if($atstype='Local Server') then 'LATS'  else 'FEP')"/>
	<xsl:variable name="ltype" select="if($lsigArea/Localisation_Type='Mainline') then (if(ATS_Equipment_Type='Central Server') then 'CATS' else if(ATS_Equipment_Type='Local Server') then 'LATS'  else 'FEP') else 'DATS'"/>
	<xsl:if test="not($ltype='DATS' and ATS_Equipment_Type='FEP')">
		<xsl:variable name="nm" select="concat($cname, '.Alarm_Link.',@Name)"/>

		${obj('{$nm}','User/A')}
	</xsl:if>
</xsl:template>

<xsl:template match="ATS_Equipment">
	<xsl:variable name="nm" select="@Name"/>
		<xsl:for-each select="//Urbalis_Sector[ATS_Area_ID_List/Signalisation_Area_ID!='']">
			<xsl:variable name="tt1" select="concat($nm,'.FEP_',current()/@Name,'.TotalCom_Failure')"/>
			<xsl:variable name="tt2" select="concat($nm,'.FEP_',current()/@Name,'.Alarm_LinkA')"/>
			<xsl:variable name="tt3" select="concat($nm,'.FEP_',current()/@Name,'.Alarm_LinkB')"/>
			${obj('{$tt1}','User/A')}
			${obj('{$tt2}','User/A')}
			${obj('{$tt3}','User/A')}
		</xsl:for-each>
</xsl:template>
<xsl:template match="ATS_Equipment" mode="SLF">
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="tt1" select="concat($nm,'.Alarm_Server')"/>
	${obj('{$tt1}','User/A')}
</xsl:template>

<xsl:template match="Urbalis_Sector">
	<xsl:variable name="secnm" select="concat('Territory_',substring-after(@Name,'SEC_'))"/>
	<xsl:variable name="objname" select= "concat($secnm,'.Alarms_Gen')"/>
	${obj('{$objname}','User/-')}
</xsl:template>

<xsl:template match="Generic_ATS_IO" mode="sa">
	<xsl:variable name="objname" select="concat('GATSM_',@Name,'.Alarms_Gen')"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="Generic_ATS_IO" mode="tct">
	<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="SPKS">
	<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
			<xsl:if test="not(contains(@Name,'_ESP') or contains(@Name,'_ESS'))">
					${obj('{$objname}','User/-')}
			</xsl:if>
			<xsl:if test="contains(@Name,'_ESP')">
					${obj('{$objname}','User/-')}
			</xsl:if>		
			<xsl:if test="contains(@Name,'_ESS')">
					${obj('{$objname}','User/-')}
			</xsl:if>		
			<xsl:if test="contains(@Name,'_CDBD')">
					${obj('{$objname}','User/-')}
			</xsl:if>				
</xsl:template>


<xsl:template match="SPKS" mode="INCON">
	<xsl:variable name="spksvar" select="if(contains(@Name,'ESP')) then ('ESP') else 
									 if(contains(@Name,'ESS')) then ('ESS') else	
									 if(contains(@Name,'CDBD')) then ('CDBD') else null
									"/>
	<xsl:variable name="objname" select= "concat(@Name,'.Alarm.SPKS.',$spksvar,'INCONSISTENT')"/>	
	${obj('{$objname}','User/-')}	
	<xsl:if test="not($spksvar or contains(@Name,'SPKS_TT_MDD_ESPB')) and Type='Paired'">
		<xsl:variable name="tt6" select="concat(@Name,'.Alarm.SPKS.SPKSINCONSISTENT')"/>
		${obj('{$tt6}','User/-')}
	</xsl:if>												
</xsl:template>
<xsl:template match="SPKS_Actuator" mode="INCON">
	<xsl:variable name="tt7" select="concat(@Name,'.Alarm.SPKS.SPKSAINCONSISTENT')"/>
	${obj('{$tt7}','User/-')}
</xsl:template>
<xsl:template match="SPKS" mode="depot">
		<xsl:if test="//Track[@ID=//Block[@ID=current()/Block_ID and sys:sigarea/@id=$SERVER_ID]/Track_ID]/Track_Type='Test track'">
			<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
				${obj('{$objname}','User/NoMMS')}
		</xsl:if>

</xsl:template>

<xsl:template match="Signal" mode="sig">
	<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
	${obj('{$objname}','User/A')}
</xsl:template>


<xsl:template match="Signal" mode="sig1">
	<xsl:variable name="objname" select="concat(@Name,'.Yellow_Alarm')"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="Signal" mode="sig2">
	<xsl:variable name="objname" select="concat(@Name,'.White_Alarm')"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="Secondary_Detection_Device">
	<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="Platform_Screen_Doors">
		<xsl:variable name="objname1" select="concat(@Name,'.Alarms_Gen')"/>
		${obj('{$objname1}','User/A')}
		<xsl:variable name="objname2" select="concat(@Name,'.Alarm_LinkA')"/>
		<xsl:variable name="objname3" select="concat(@Name,'.Alarm_LinkB')"/>
		${obj('{$objname2}','User/A')}
		${obj('{$objname3}','User/-')}
		<xsl:variable name="psd" select="."/>
		<xsl:variable name="nbDoors" select="number(//Platform_Screen_Doors_Characteristics[@ID=current()/Platform_Screen_Doors_Characteristics_ID]/Number_Of_Doors)"/>
		<xsl:apply-templates select="//PSDDoor_Operational_Alarm">
			<xsl:with-param name="nbDoors" select="$nbDoors"/>
			<xsl:with-param name="psd" select="$psd"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="//PSDDoor_Maintenance_Alarm">
			<xsl:with-param name="nbDoors" select="$nbDoors"/>
			<xsl:with-param name="psd" select="$psd"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="//PSD_Maintenance_Alarm[Type='PF']">
			<xsl:with-param name="psd" select="$psd"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="//PSD_Operational_Alarm[Type='PF']">
			<xsl:with-param name="psd" select="$psd"/>
		</xsl:apply-templates>
		<xsl:variable name="objname4" select="concat('PSDTimeSyncModule_',@Name,'.Alarm_TimeSync')"/>
		${obj('{$objname4}','User/-')}
</xsl:template>

<xsl:template match="PSD_Operational_Alarm">
	<xsl:param name = "psd"/>
	<xsl:variable name="trigger" select="Trigger"/>
	<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.',$trigger,'_',@ID)"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<xsl:template match="PSDDoor_Operational_Alarm|PSDDoor_Maintenance_Alarm">
	<xsl:param name="psd"/>
	<xsl:param name="nbDoors"/>
	<xsl:variable name="loc" select="local-name()"/>
	<xsl:variable name="trigger" select="Trigger"/>
	<xsl:for-each select="1 to xs:integer($nbDoors)">
		<xsl:variable name="id" select="current()"/>
		<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.Door',$id,'.',$trigger)"/>
		<xsl:variable name="grp" select="if ($loc='PSDDoor_Operational_Alarm') then 'User/-' else 'User/A'"/>
		${obj('{$objname}','<xsl:value-of select="$grp"/>')}
	</xsl:for-each>
</xsl:template>

<xsl:template match="PSD_Maintenance_Alarm">
	<xsl:param name="psd"/>
	<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.',Trigger)"/>
	${obj('{$objname}','User/A')}
</xsl:template>

<!--LinksAB-->
<xsl:template match="Signalisation_Area">
		<xsl:for-each select="sys:lc/@id">
			<xsl:variable name="objnamelc" select= "concat('LCS_',current())"/>
			${obj('{$objnamelc}','User/-')}
		</xsl:for-each>
		<xsl:for-each select="sys:zc/@id">
			<xsl:variable name="objnamezc" select= "concat('ZCS_',current())"/>
			${obj('{$objnamezc}','User/A')}
		</xsl:for-each>	
		<xsl:for-each select="sys:cbi/@id">
			<xsl:variable name="objnamecbi" select= "concat('CBIS_',current())"/>
			${obj('{$objnamecbi}','User/A')}
		</xsl:for-each>			
</xsl:template>


<!--EquipmentAlarms-->
<xsl:template match="ATC_Equipment|CBI">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="ty" select="if (ATC_Equipment_Type) then (ATC_Equipment_Type) else 'CBI'"/>
	<xsl:variable name="lc_zc_cbi" select="if ($ty='LC') then ('LCS_') else if ($ty='ZC') then ('ZCS_') else ('CBIS_')"/>
	<xsl:variable name="ssid" select="SSID"/>
	%for v in ["A","B"]:
	<xsl:variable name="objnm1" select="concat($lc_zc_cbi,$id,'.Alarm_TotalCom_FEP_${v}')"/>
	<xsl:choose>
		<xsl:when test="$ty='LC'">${obj('{$objnm1}','User/-')}</xsl:when>
		<xsl:otherwise>${obj('{$objnm1}','User/A')}</xsl:otherwise>
	</xsl:choose>
	%endfor
</xsl:template>

<xsl:template match="ATC_Equipment|CBI" mode="linksab">
	<xsl:variable name="id" select="@ID"/>
	<xsl:variable name="ty" select="if (ATC_Equipment_Type) then (ATC_Equipment_Type) else 'CBI'"/>
	<xsl:variable name="lc_zc_cbi" select="if ($ty='LC') then ('LCS_') else if ($ty='ZC') then ('ZCS_') else ('CBIS_')"/>
	<xsl:variable name="ssid" select="SSID"/>
	%for v in ["A","B"]:
		%for w in ["A","B"]:
			<xsl:variable name="objnm2" select="concat($lc_zc_cbi,@ID,'.FEP_${v}.Alarm_Link${w}')"/>
			<xsl:choose>
				<xsl:when test="$ty='LC'">${obj('{$objnm2}','User/-')}</xsl:when>
				<xsl:otherwise>${obj('{$objnm2}','User/A')}</xsl:otherwise>
			</xsl:choose>
		%endfor
	%endfor
</xsl:template>


<xsl:template match="Direction_Indicator" mode="di1">
	<xsl:variable name="objnamedi" select= "concat(@Name,'.Alarm')"/>
	${obj('{$objnamedi}','User/A')}
</xsl:template>

<xsl:template match="Direction_Indicator" mode="di2">
	<xsl:variable name="objnamedi" select= "concat(@Name,'.Alarm')"/>
	${obj('{$objnamedi}','User/A')}
</xsl:template>

<xsl:template match="LX_Device" mode="tt5">
	<xsl:variable name="objnameLX1" select= "concat(@Name,'.Alarm')"/>
	${obj('{$objnameLX1}','User/S')}
</xsl:template>

<xsl:template match="LX_Device" mode="tt6">
	<xsl:variable name="objnameLX2" select= "concat(@Name,'.AlarmSideA')"/>
	<xsl:variable name="objnameLX3" select= "concat(@Name,'.AlarmSideb')"/>
	${obj('{$objnameLX2}','User/S')}
	${obj('{$objnameLX3}','User/S')}
</xsl:template>

<xsl:template match="Train_Unit">
	<xsl:variable name="objectnametu" select= "concat('Train',format-number(@ID,'000'),'.Alarm_EB_Request')"/>
	${obj('{$objectnametu}','User/-')}
</xsl:template>

<xsl:template match="Train_Unit" mode="Train_Alarm">
	<xsl:variable name="objtu" select= "concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="tu1" select="concat($objtu,'.Alarm.DFO')"/>
	<xsl:variable name="tu2" select="concat($objtu,'.Alarm.DFC')"/>
	<xsl:variable name="tu3" select="concat($objtu,'.Alarm.CCResetRequest')"/>
		${obj('{$tu1}','User/-')}
		${obj('{$tu2}','User/-')}
		${obj('{$tu3}','User/-')}
	<xsl:for-each select="//MSS_CC/CC[Type='Train_Alarm']">
		<xsl:variable name="objname" select="concat($objtu,@Name,'.Alarm')"/>
		${obj('{$objname}','User/A')}
	</xsl:for-each>
</xsl:template>

<xsl:template match="LX" mode="stp">
	<xsl:variable name="objectnamelx1" select= "concat(@Name,'.Alarm')"/>
	${obj('{$objectnamelx1}','User/-')}
</xsl:template>

<xsl:template match="LX" mode="gd">
	<xsl:variable name="objectnamelx2" select= "concat(@Name,'.Alarm')"/>
	${obj('{$objectnamelx2}','User/-')}
</xsl:template>

<xsl:template match="LX" mode="u400">
	<xsl:variable name="objectnamelx3" select= "concat(@Name,'.LXAI_LX_Lamp_Side_A')"/>
	<xsl:variable name="objectnamelx4" select= "concat(@Name,'.LXAI_LX_Lamp_Side_B')"/>
	<xsl:variable name="objectnamelx5" select= "concat(@Name,'.LXLP_LX_Lamp_Side_A')"/>
	<xsl:variable name="objectnamelx6" select= "concat(@Name,'.LXLP_LX_Lamp_Side_B')"/>
	<xsl:variable name="objectnamelx7" select= "concat(@Name,'.AlarmSide')"/>
	${obj('{$objectnamelx3}','User/NoMMS')}
	${obj('{$objectnamelx4}','User/NoMMS')}
	${obj('{$objectnamelx5}','User/NoMMS')}
	${obj('{$objectnamelx6}','User/NoMMS')}
	${obj('{$objectnamelx7}','User/S')}
</xsl:template>

<xsl:template match="Station">
	<xsl:variable name="st" select="."/>
	<xsl:variable name="pfs" select="//Platform[@ID=current()/Platform_ID_List/Platform_ID]"/>
	<xsl:for-each select="//PSD_Maintenance_Alarm[Type='ST']">
		<xsl:variable name="pf" select="if (position()=1) then $pfs[1] else $pfs[2]"/>
		<xsl:variable name="objname" select="concat($st/@Name,'.Alarm','.',Trigger)"></xsl:variable>
		<xsl:if test="$pf">${obj('{$objname}','User/A')}</xsl:if>
	</xsl:for-each>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_group_tem.mako" import="group_object_template,create_path"/>
<%def name="obj(objname,usergroup)">
		<Object name="${objname}" rules="update" traces="error">
			<Properties>
				<Property name="UserGroup" dt="string">${usergroup}</Property>	
			</Properties>
		</Object>
</%def>

