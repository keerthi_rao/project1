<%inherit file="/Group_tem.mako"/>
<%! 
xs_ = True
%>

<xsl:variable name="sig_curr" select="//Signalisation_Area[@ID=$SERVER_ID]"/>

<%block name="classes">
	<Class name="S2KEvents_Merge" rules="update" traces="error">
		<Objects>
			<xsl:if test="$SERVER_LEVEL='_LV1'">
				<xsl:apply-templates select="//CBI[@ID=$sig_curr/sys:cbi/@id]"><xsl:with-param name="type" select="'CBI_Sector'"/></xsl:apply-templates>
				<xsl:apply-templates select="//GAMA_Zone[sys:zc/@id=$sig_curr/sys:zc/@id]"><xsl:with-param name="type" select="'GAMA_Zone'"/></xsl:apply-templates>
				<xsl:apply-templates select="//Point[sys:cbi/@id=$sig_curr/sys:cbi/@id]"><xsl:with-param name="type" select="'Point'"/></xsl:apply-templates>
				<xsl:apply-templates select="//Route[sys:cbi/@id=$sig_curr/sys:cbi/@id]"><xsl:with-param name="type" select="'Route'"/></xsl:apply-templates>
				<xsl:apply-templates select="//Traffic_Locking[Origin_CBI_ID=$sig_curr/sys:cbi/@id]"><xsl:with-param name="type" select="'Traffic_Locking'"/></xsl:apply-templates>
				<xsl:apply-templates select="//Trains_Unit/Train_Unit[number(@ID)&lt;=30]"><xsl:with-param name="type" select="'Train_Event'"/></xsl:apply-templates>
				<xsl:apply-templates select="//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and contains(@Name,'LXGB')]"/>
				<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]">
					<xsl:with-param name="type" select="'Level_Crossing'"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="//LX_Device[(starts-with(@Name, 'LX3_WL') or (starts-with(@Name, 'LX3_H'))) and   Type='Aspect Indicator' and  LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:sigarea/@id = $SERVER_ID]/@ID]/@ID]/@ID]/@ID]">
					<xsl:with-param name="type" select="'Level_Crossing_Device'"/>
				</xsl:apply-templates>
				<xsl:if test="$sig_curr/Localisation_Type='Mainline'">
					<xsl:apply-templates select="//Event[Type='General_Event']"/>
				</xsl:if>
			</xsl:if>
			<xsl:if test="$SERVER_LEVEL='_LV2'">
				<xsl:apply-templates select="//Trains_Unit/Train_Unit[number(@ID)&lt;=30]"><xsl:with-param name="type" select="'Train_Event_LV2'"/></xsl:apply-templates>
				<xsl:if test="$sig_curr/Localisation_Type='Mainline'">
					<xsl:apply-templates select="//Event[Type='General_Event_LV2']"/>
				</xsl:if>
			</xsl:if>
		</Objects>
	</Class>	
</%block>

<xsl:template match="CBI|GAMA_Zone|Point|Route|Traffic_Locking|Train_Unit">
	<xsl:param name="type"/>
	<xsl:variable name="loc" select="local-name()"/>
	<xsl:variable name="parentname" select="if ($loc='Train_Unit') then (concat('Train',format-number(@ID,'000'))) else (@Name)"/>
	<xsl:for-each select="//Events/Event[Type=$type]">
		<xsl:variable name="objname" select="concat($parentname,@Name)"/>
		${obj('$objname','User/-')}
	</xsl:for-each>	
</xsl:template>

<xsl:template match="Event">
	<xsl:variable name="objname" select="concat('Mainline',@Name)"/>
	${obj('$objname','User/-')}
</xsl:template>

<xsl:template match="Generic_ATS_IO">
	<xsl:variable name="objname" select="concat(@Name,'.EventBypass')"/>
	${obj('$objname','User/-')}
</xsl:template>

<xsl:template match="LX|LX_Device">
	<xsl:param name="type"/>
	<xsl:variable name="parentname" select="@Name"/>
	<xsl:for-each select="//Events/Event[Type=$type]">
		<xsl:variable name="nm" select="@Name"/>
		<xsl:variable name="cond" select="if ($nm='.Event.LCFLNA' or $nm='.Event.LCFLA') then (starts-with($parentname,'LX3_WL')) else if ($nm='.Event.LCAWNA') then (starts-with($parentname,'LX3_H')) else (true())"/>
		<xsl:variable name="objname" select="concat($parentname,@Name)"/>
		<xsl:if test="$cond">${obj('$objname','User/-')}</xsl:if>
	</xsl:for-each>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_group_tem.mako" import="group_object_template,create_path"/>
<%def name="obj(objname,usergroup)">
	<Object name="{${objname}}" rules="update" traces="error">
		<Properties>
			<Property name="UserGroup" dt="string">${usergroup}</Property>	
		</Properties>
	</Object>
</%def>

