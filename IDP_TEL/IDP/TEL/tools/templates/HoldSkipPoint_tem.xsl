<%inherit file="/Object_tem.mako"/>
<%! class_ = "HoldSkipPoint" %>

<%block name="classes">
	<Class name="HoldSkipPoint">
	     <Objects>
			 <xsl:apply-templates select="//Stopping_Areas/Stopping_Area[sys:sigarea/@id=$SERVER_ID and Original_Area_List/Original_Area/@Original_Area_Type='Stabling' and contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot')]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Stopping_Area">
	<xsl:variable name="name" select="concat(@Name,'.HoldSkip')"/>
	<Object name="{@Name}.HoldSkip" rules="update_or_create">
	  <Properties>
			<Property name="GenericAreaID" dt="string"><xsl:value-of select="@Name"/></Property>
			<Property name="RegulationPointID" dt="string"><xsl:value-of select="@Name"/>.ATR</Property>
			<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$name"/></Property>
	  </Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
