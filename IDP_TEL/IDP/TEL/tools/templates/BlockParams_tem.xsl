<%inherit file="/Object_tem.mako"/>
<%! class_ = "BlockParams" %>
<xsl:param name="URBALIS_ID"/>
<%block name="classes">
<xsl:variable name="filegeneration" select="if ($sysdb//Blocks/Block[sys:sigarea/@id=$SERVER_ID]) then 'Yes' else 'No'"/>
	<xsl:if test="$filegeneration = 'No'">
		<xsl:result-document href="BlockParams_{$SERVER_ID}_EmptyFileDoNotGenerate.xml" method="xml"/>
	</xsl:if>
	<Class name="BlockParams">
	     <Objects>
			 <xsl:apply-templates select="//Blocks/Block[sys:sigarea/@id=$SERVER_ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Block">
	<xsl:variable name ="Urbalis" select="//Urbalis_Sector[Track_ID_List/Track_ID=current()/Track_ID]/Urbalis_Sector_Option/@Overlap_Presence"/>
	<Object name="{@Name}.BlockParams" rules="update_or_create">
		  <Properties>
				<Property name="OverlapPresenceConf" dt="i4"><xsl:value-of select="if($Urbalis='true') then '1' else '0' "/></Property>
		  </Properties> 
	  </Object>
</xsl:template>