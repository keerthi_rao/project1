<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KIOOPCUAClient.IOClient" %>

<%block name="classes">
	<Class name="S2KOPCUAClient.IOClient">
	     <Objects>
			<xsl:apply-templates select="//ATS_Equipment[ATS_Equipment_Type='FEP' and Signalisation_Area_ID_List/Signalisation_Area_ID=$SERVER_ID][1]"/>
			<xsl:apply-templates select="//DCSAlarm/DCS_Equipment[Signalisation_Area_ID=$SERVER_ID and Type='NMS']"/>
		</Objects>
	</Class>
</%block>



<xsl:template match="ATS_Equipment">
<xsl:variable name="tt" select= "concat('OPCClient_SW_RT',Signalisation_Area_ID,'_Status')"/>
<xsl:variable name="opcgrp" select= "concat('OPCCGroup_SW_RT',Signalisation_Area_ID,'_Status')"/>
<xsl:variable name="ats1" select="//ATS_Equipment[Signalisation_Area_ID=current()/Signalisation_Area_ID]/@Name"/>
<Object name="{$tt}" rules="update_or_create">
			<Properties>
					<Property name="Server1Url" dt="string">opc.tcp://<xsl:value-of select="$ats1[1]"/>:50088</Property>
					<Property name="Server2Url" dt="string">opc.tcp://<xsl:value-of select="$ats1[2]"/>:50088</Property>
					<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$tt"/></Property>
					<Property name="SharedOnIdentifierDefinition" dt="boolean">True</Property>
					${multilingualvalue('$opcgrp')}
			</Properties>
		</Object>
</xsl:template>

<xsl:template match="DCS_Equipment">
<xsl:variable name="tt2" select= "concat('OPCClient_UA_',Signalisation_Area_ID,'_',@Name)"/>
<xsl:variable name="opcgrp" select= "concat('OPCCGroup_',@Name)"/>
<xsl:variable name="ats1" select="//ATS_Equipment[Signalisation_Area_ID=current()/Signalisation_Area_ID]/@Name"/>
<Object name="{$tt2}" rules="update_or_create">
			<Properties>
					<Property name="Server1Url" dt="string">opc.tcp://<xsl:value-of select="IP_Address_Blue"/>:11182/OPCUA/HiVisionUaServer</Property>
					<Property name="Server2Url" dt="string">opc.tcp://<xsl:value-of select="Gateway_Red"/>:11182/OPCUA/HiVisionUaServer</Property>
					<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$tt2"/></Property>
					<Property name="SharedOnIdentifierDefinition" dt="boolean">True</Property>
					${multilingualvalue('$opcgrp')}
			</Properties>
		</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>