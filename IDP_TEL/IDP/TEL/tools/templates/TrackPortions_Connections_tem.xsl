<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sys="http://www.systerel.fr/Alstom/IDP">
<TrackPortions_Connections>
    <xsl:apply-templates select="//TrackPortion"/> 
    <xsl:apply-templates select="$sysdb//Point"/> 
	<xsl:apply-templates select="$sysdb//Static_Dark_Zone"/>
</TrackPortions_Connections>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>

<xsl:template match="Point">
    <!-- Copied from SigRulesPointEnd template -->
    <xsl:variable name="point_id" select="@ID"/>
    <xsl:variable name="common_block" select="//Block[Point_ID = $point_id and (Next_Up_Reverse_Block_ID or Next_Down_Reverse_Block_ID)]"/>
    <xsl:variable name="direction" select="if ($common_block/Next_Up_Reverse_Block_ID) then 'Up' else 'Down'"/>
    <xsl:variable name="common" select="$common_block/@ID"/>
    <xsl:variable name="normal" select="if ($direction = 'Up') then $common_block/Next_Up_Normal_Block_ID else $common_block/Next_Down_Normal_Block_ID"/>
    <xsl:variable name="reverse" select="if ($direction = 'Up') then $common_block/Next_Up_Reverse_Block_ID else $common_block/Next_Down_Reverse_Block_ID"/>
    
    <TrackPortions_Connection ID="XY" Name="{concat(@Name, '.TrackPortionConnection')}">
        ${node("StateElement", "concat(@Name, '.Detection.Template.iEqpState')")}
        <xsl:if test="Orientation = 'Divergent'">
            ${node("Left_Normal_TP", "$common")}
            ${node("Right_Normal_TP", "$normal")}
            ${node("Right_Up_TP", "$reverse")}
        </xsl:if>
        <xsl:if test="Orientation = 'Convergent'">
            ${node("Left_Normal_TP", "$normal")}
            ${node("Right_Normal_TP", "$common")}
            ${node("Left_Up_TP", "$reverse")}
        </xsl:if>
        <sys:point_id><xsl:value-of select="$point_id"/></sys:point_id>
    </TrackPortions_Connection>

</xsl:template>

<xsl:template match="TrackPortion">
    <xsl:variable name="tp1_name" select="sys:tp_name"/>
    <xsl:variable name="tp1_bid" select="sys:block_id"/>
    <!-- Compute Next TP in directions 'Up' and 'Down'  -->
    <xsl:variable name="tp2_up">
        <xsl:call-template name="nextup_tp"><xsl:with-param name="tp1" select="."/></xsl:call-template>
    </xsl:variable>
    <xsl:variable name="tp2_down">
        <xsl:call-template name="nextdown_tp"><xsl:with-param name="tp1" select="."/></xsl:call-template>
    </xsl:variable>
    <!-- Compute A TP in directions 'Up' and 'Down'  -->
    <xsl:variable name="tp2_id" select="if ($tp2_up) then $tp2_up else $tp2_down"/>
    <xsl:variable name="tp2" select="//TrackPortion[@ID=$tp2_id]"/>
    <xsl:variable name="tp2_bid" select="$tp2/sys:block_id"/>
    <xsl:variable name="tp1_block" select="$sysdb//Block[@ID=$tp1_bid]"/>
    <xsl:variable name="tp2_block" select="$sysdb//Block[@ID=$tp2_bid]"/>
    
	<xsl:variable name="sdz_blocks" select="//Block[Secondary_Detection_Device_ID = //Static_Dark_Zone/Adjacent_SDD_ID_List/Adjacent_SDD_ID]"/>
	<!-- Following code generates Hub's with Right_Normal_TP and Right_Up_TP -->
	<xsl:variable name="left_hub" select="$sysdb//Specific_Block_Chaining[($tp1_bid=First_Block_ID and ($tp2_down=Second_Block_ID)) and Specific_Block_Chaining_Type = 'Changing Of Orientation']"/>
	<xsl:variable name="right_hub" select="$sysdb//Specific_Block_Chaining[($tp1_bid=First_Block_ID and ($tp2_up=Second_Block_ID)) and Specific_Block_Chaining_Type = 'Changing Of Orientation']"/>
	<xsl:variable name="LeftHubName" select="concat($tp1_name,'_',//TrackPortion[@ID=$tp2_down]/sys:tp_name)"/>
	<xsl:if test="$left_hub and not($right_hub)">
		     ${tp_connection("concat('Hub_', $LeftHubName)", 
							[('Right_Normal_TP', '@ID'),
                            ('Right_Up_TP', '$tp2_down')])}
	</xsl:if>
	<!-- End of code for adding Hubs with Right_Normal_TP and Right_Up_TP -->
    <xsl:if test="($tp1_block[Point_ID_Not_Defined='0'] or $tp2_block[Point_ID_Not_Defined='0']) or ($tp1_block/Point_ID != $tp2_block/Point_ID)">
        <xsl:variable name="tp2_name" select="//TrackPortion[@ID=$tp2_id]/sys:tp_name"/>
        <xsl:variable name="cod_up" select="$sysdb//Specific_Block_Chaining[($tp1_bid=First_Block_ID and $tp2_bid=Second_Block_ID) and Specific_Block_Chaining_Type = 'Changing Of Orientation']"/>
        <xsl:variable name="cod_down" select="$sysdb//Specific_Block_Chaining[($tp2_bid=First_Block_ID and $tp1_bid=Second_Block_ID) and Specific_Block_Chaining_Type = 'Changing Of Orientation']"/>
        <xsl:variable name="tp1_tp2" select="concat($tp1_name, '_', $tp2_name)"/>
        <xsl:if test="not($cod_down)"> <!-- Avoid to generate a TP Connection in direction 'Down' -->
            <xsl:choose>
               <xsl:when test="$cod_up">
                   ${tp_connection("concat('Hub_', $tp1_tp2)", 
                                  [('Left_Normal_TP', '@ID'),
                                   ('Left_Up_TP', '$tp2_id')])}
               </xsl:when>
               <xsl:when test="not($cod_up) and $tp2_id=''">
                   <xsl:if test="$tp2_up!='' and not(substring-after($tp1_name,'TI_') = $sdz_blocks/@Name)">
                       ${tp_connection("concat('JStart_', $tp1_name)",
                                       [('Right_Normal_TP', '@ID')])}
                   </xsl:if>
                   <xsl:if test="$tp2_down!='' and not(substring-after($tp1_name,'TI_') = $sdz_blocks/@Name)">
                       ${tp_connection("concat('JStart_', $tp1_name)",
                                       [('Left_Normal_TP', '@ID')])}
                   </xsl:if>
               </xsl:when>
               <xsl:otherwise>
                   ${tp_connection("concat('J_', $tp1_tp2)",
                                   [('Left_Normal_TP', '@ID'),
                                    ('Right_Normal_TP', '$tp2_id')])}
               </xsl:otherwise>
            </xsl:choose>
            <!-- Manage TrackPortion without adjacent TP (direction 'Down') -->
            <xsl:if test="$tp1_block/Next_Down_Normal_Block_ID_Not_Defined and not(substring-after($tp1_name,'TI_') = $sdz_blocks/@Name)">
                ${tp_connection("concat('JStart_', $tp1_name)", [('Right_Normal_TP', '@ID')])}
            </xsl:if>
        </xsl:if>
    </xsl:if>
</xsl:template>

<!-- Compute Next TrackPortion in 'UP' direction-->
<xsl:template name="nextup_tp">
  <xsl:param name="tp1"/>
  <xsl:variable name="tp1_id" select="$tp1/@ID"/>
  <xsl:variable name="tp1_bid" select="$tp1/sys:block_id"/>
  <xsl:variable name="tp2_bid" select="$sysdb//Block[@ID=$tp1_bid]/Next_Up_Normal_Block_ID"/>
  <xsl:variable name="tp2" select="//TrackPortion[sys:block_id=$tp2_bid]"/>
  <xsl:choose>
    <!-- Same block, Both TP are splited -->
    <xsl:when test="$tp1/sys:splited_index and $tp1/sys:splited_index[@value != @max]">
        <xsl:value-of select="number($tp1_id)+1"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
            <!-- Next TP comes from a splited Block -->
            <xsl:when test="$tp2/sys:splited_index">
                <xsl:value-of select="$tp2[sys:splited_index/@value='0']/@ID"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$sysdb//Block[@ID=$tp1_bid]/Next_Up_Normal_Block_ID"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- Compute Next TrackPortion in 'DOWN' direction-->
<xsl:template name="nextdown_tp">
  <xsl:param name="tp1"/>
  <xsl:variable name="tp1_id" select="$tp1/@ID"/>
  <xsl:variable name="tp1_bid" select="$tp1/sys:block_id"/>
  <xsl:variable name="tp2_bid" select="$sysdb//Block[@ID=$tp1_bid]/Next_Down_Normal_Block_ID"/>
  <xsl:variable name="tp2" select="//TrackPortion[sys:block_id=$tp2_bid]"/>
  <xsl:choose>
    <!-- Same block, Both TP are splited -->
    <xsl:when test="$tp1/sys:splited_index and $tp1/sys:splited_index[@value != 0]">
        <xsl:value-of select="number($tp1_id)-1"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
            <!-- Next TP comes from a splited Block -->
            <xsl:when test="$tp2/sys:splited_index">
                <xsl:value-of select="$tp2[sys:splited_index/@value='0']/@ID"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$sysdb//Block[@ID=$tp1_bid]/Next_Down_Normal_Block_ID"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
 
 <!-- Trackportions_Connection from StaticDarkZone-->
<xsl:template match="Static_Dark_Zone">
	<xsl:variable name="sdz_name" select="@Name"/>
	<xsl:variable name="sdz" select="."/>
	<xsl:variable name="sdd1" select="Adjacent_SDD_ID_List/Adjacent_SDD_ID[1]"/>
	<xsl:variable name="sdz_tp" select="//TrackPortion[substring-after(substring-before(@Name,'.TrackPortion'),'TI_') = $sdz_name]/@ID"/>
	<xsl:for-each select="Adjacent_SDD_ID_List/Adjacent_SDD_ID">
		<xsl:variable name="sdd_id" select="text()"/>
		<xsl:variable name="sdd" select="$sysdb//Secondary_Detection_Device[@ID = $sdd_id]"/>
		<xsl:variable name="blocks" select="$sysdb//Block[Secondary_Detection_Device_ID = $sdd_id and ( (not(Next_Up_Normal_Block_ID) and not(Next_Up_Reverse_Block_ID)) or (not(Next_Down_Normal_Block_ID) and not(Next_Down_Reverse_Block_ID)) )]"/>
		<xsl:variable name="sdd_tp" select="//TrackPortion[sys:block_id = $blocks/@ID][1]/@ID"/>
		<xsl:variable name="blocks_firstsdd" select="$sysdb//Block[Secondary_Detection_Device_ID = $sdd1]"/>
		<xsl:variable name="dir_first_sd" select="if ($blocks_firstsdd[not(Next_Up_Normal_Block_ID) and not(Next_Up_Reverse_Block_ID)]) then 'Up' else 'Down'"/>
		<xsl:variable name="dir_current" select="if ($blocks[not(Next_Up_Normal_Block_ID) and not(Next_Up_Reverse_Block_ID)]) then 'Up' else 'Down'"/>
		<TrackPortions_Connection ID="XY" Name="{concat('J_',$sdz_name,'_',$sdd/@Name,'.TrackPortionConnection')}">
			${node("StateElement", "''")}
			<xsl:if test="position() = 1 or (position() != 1 and $dir_first_sd != $dir_current)">
			<xsl:if test="$blocks[not(Next_Up_Normal_Block_ID) and not(Next_Up_Reverse_Block_ID)]">
				${node("Left_Normal_TP", "$sdd_tp")}
				${node("Right_Normal_TP", "$sdz_tp")}
			</xsl:if>
			<xsl:if test="$blocks[not(Next_Down_Normal_Block_ID) and not(Next_Down_Reverse_Block_ID)]">
				${node("Left_Normal_TP", "$sdz_tp")}
				${node("Right_Normal_TP", "$sdd_tp")}			
			</xsl:if>
			</xsl:if>
			<xsl:if test="position() != 1 and $dir_first_sd = $dir_current">
				<xsl:if test="$blocks[not(Next_Up_Normal_Block_ID) and not(Next_Up_Reverse_Block_ID)]">
					${node("Left_Up_TP", "$sdd_tp")}
					${node("Left_Normal_TP", "$sdz_tp")}
				</xsl:if>
				<xsl:if test="$blocks[not(Next_Down_Normal_Block_ID) and not(Next_Down_Reverse_Block_ID)]">
					${node("Right_Normal_TP", "$sdz_tp")}
					${node("Right_Up_TP", "$sdd_tp")}			
				</xsl:if>
			</xsl:if>
		</TrackPortions_Connection>
	</xsl:for-each>
</xsl:template>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>
</xsl:stylesheet>

<%namespace file="lib_tem_tel.mako" import="node"/>

<%def name="tp_connection(name, tps)">
    <TrackPortions_Connection ID="XY">
        <xsl:attribute name="Name" select="concat(${name}, '.TrackPortionConnection')"/>
        <StateElement/>
        % for tp in tps:
           ${node(tp[0], tp[1])}
        % endfor
    </TrackPortions_Connection>
</%def>
