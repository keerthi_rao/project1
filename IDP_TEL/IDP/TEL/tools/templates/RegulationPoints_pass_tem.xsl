<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	
	<xsl:template match="/">
		<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<RegulationPoints>
	    		<xsl:apply-templates select="//Stopping_Area"/>
			</RegulationPoints>
		</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
	</xsl:template>
	
  	<xsl:key name="origin" match="Change_Of_Direction_Area|Platform|Stabling_Location|Transfer_Track|Coupling_Uncoupling_Area|Washing_Zone|Isolation_Area|Track_Allocation_Zone" use="@ID"/>
  
<!-- Return the table name corresponding to a Stopping Area Type.-->
	<xsl:function name="sys:table">
	  <xsl:param name="type"/>
	      <xsl:choose>
	          <xsl:when test="$type='Change Of Direction Area'">Change_Of_Direction_Area</xsl:when>
	          <xsl:when test="$type='Platform'">Platform</xsl:when>
	          <xsl:when test="$type='Transfer Track'">Transfer_Track</xsl:when>
	          <xsl:when test="$type='Stabling'">Stabling_Location</xsl:when>
	          <xsl:when test="$type='Coupling Uncoupling'">Coupling_Uncoupling_Area</xsl:when>
	          <xsl:when test="$type='Washing'">Washing_Zone</xsl:when>
	          <xsl:when test="$type='Isolation Area'">Isolation_Area</xsl:when>
	          <xsl:otherwise><xsl:message terminate="yes">Unknown type<xsl:value-of select="$type"/></xsl:message></xsl:otherwise>
	      </xsl:choose>
	</xsl:function>
 

  <xsl:template match="Stopping_Area">
       <xsl:variable name="tps">
            <!-- this variable conatains all the track portion id of all the origin area -->
            <xsl:for-each select="Original_Area_List/Original_Area">
                <xsl:variable name="type" select="@Original_Area_Type"/>
                <xsl:variable name="id" select="@ID"/>
                <xsl:variable name="original_area" select="key('origin', $id)[local-name()=sys:table($type)]"/>
                <xsl:variable name="tid" select="$original_area/Track_ID/text()"/>
        		<xsl:variable name="kpb" select="$original_area/sys:corr_kp(Kp_Begin)"/>
        		<xsl:variable name="kpe" select="$original_area/sys:corr_kp(Kp_End)"/>
        		<xsl:for-each select="//TrackPortion[sys:track_id=$tid and
                                ((number(KpBegin) > $kpb and $kpe > number(KpBegin)) or (number(KpEnd) > $kpb and $kpe > number(KpEnd)) or
                                 (number(KpBegin) > $kpe and $kpb > number(KpBegin)) or (number(KpEnd) > $kpe and $kpb > number(KpEnd)) or
                                 ($kpb >= number(KpBegin) and number(KpEnd) >= $kpe) or (number(KpBegin) >= $kpb and $kpe >= number(KpEnd)) )]">
            		<sys:TP_ID><xsl:value-of select="@ID"/></sys:TP_ID>
        		</xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
		<xsl:variable name="coa" select="Original_Area_List/Original_Area/@Original_Area_Type"/>
		<xsl:variable name="ooa" select="//Stopping_Area[not(@ID = current()/@ID) and (some $bid in current()/sys:blockid/@id satisfies $bid = sys:blockid/@id)]/Original_Area_List/Original_Area/@Original_Area_Type"/>
		<xsl:variable name="stopPoint" select="if (not($ooa)) then 'yes' else 
						(if($coa = 'Platform') then 'yes' else if($ooa = 'Platform') then 'no' else if($coa='Stabling') then 'yes' else if($ooa='Stabling') then 'no' else if($coa='Transfer Track') then 'yes' else if ($ooa='Transfer Track') then 'no' else if ($coa='Washing') then 'yes' else if($ooa='Washing') then 'no' else if ($coa='Isolation Area') then 'yes' else if($ooa='Isolation Area') then 'no'else if($coa = 'Coupling Uncoupling') then 'yes' else if($ooa = 'Coupling Uncoupling') then 'no' else if($coa = 'Change Of Direction Area') then 'yes' else 'no')"/>
		<xsl:if test="$stopPoint = 'yes'">
		<RegulationPoint ID="{@ID}" Name="{@Name}">
	        <TrackPortions_List>
	            <xsl:for-each select="distinct-values($tps/sys:TP_ID/text())">
	                <TrackPortion_ID><xsl:value-of select="."/></TrackPortion_ID>
	            </xsl:for-each>
	        </TrackPortions_List>
	        
			<xsl:for-each select="Original_Area_List/Original_Area">
				<sys:type><xsl:value-of select="@Original_Area_Type"/></sys:type>
			</xsl:for-each>
		    
		    <StationID><xsl:value-of select="Station_ID"/></StationID>
		    ${ori_area_type('Platform', 'Platform')}
		    ${ori_area_type('COD', 'Change Of Direction Area')}
		    ${ori_area_type('Stabling', 'Stabling')}
		    ${ori_area_type('TransferTrack', 'Transfer Track')}
		    ${ori_area_type('Washing', 'Washing')}
			<Depot><xsl:value-of select="if (sys:main_depot/@name = //Depots/Depot/@Name) then 'true' else 'false'"/></Depot>
	</RegulationPoint>
	</xsl:if>
</xsl:template>

<xsl:include href="../../lib_common/lib_xslt.xsl"/>
</xsl:stylesheet>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%def name="ori_area_type(param, type)">
    <${param}><xsl:value-of select="Original_Area_List/Original_Area/@Original_Area_Type='${type}'"/></${param}>
</%def>
