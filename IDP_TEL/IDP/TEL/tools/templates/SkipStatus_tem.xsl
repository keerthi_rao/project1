<%inherit file="/Object_tem.mako"/>
<%! class_ = "SkipStatus" %>

<%block name="classes">
	<Class name="SkipStatus">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	  <Object name="{$TrainName}.SkipStatus" rules="update_or_create">
		  <Properties>
				<xsl:variable name="mulval" select="concat($TrainName, '.AttributesHS')"/>
				${prop('Shared','<xsl:value-of select="$mulval"/>')}
				${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
				${multilingualvalue("$mulval")}
		  </Properties>
	  </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
