<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "ISCSStation"
%>
<%block name="classes">
	<Class name="ISCSStation">
		<Objects>
			<xsl:apply-templates select="$sysdb//ISCSStation[Geographical_Area=//Geographical_Area[Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID=//Signalisation_Area[@ID=$SERVER_ID]/Area_Boundary_Secondary_Detection_Device_ID_List/Secondary_Detection_Device_ID]/@ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="ISCSStation">
	<Object name="{concat('ISCSStation_',@Name)}" rules="update_or_create">
		<Properties>
			<Property name="ISCSGenericAreaID" dt="string"><xsl:value-of select="@Name"/></Property>
		</Properties>
	</Object>
</xsl:template>