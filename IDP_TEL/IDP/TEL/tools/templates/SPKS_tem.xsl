<%inherit file="/Object_tem.mako"/>
<%! class_ = "SPKS" %>

<%block name="classes">
	<Class name="SPKS">
		<Objects>
			<xsl:apply-templates select="//SPKS[sys:cbi/@id = $CBI_ID]"/>
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="SPKS">
		<xsl:variable name="SDDID" select="@ID"/>
		<xsl:variable name="uid" select="//Urbalis_Sector[@ID=//CBI[@ID=$CBI_ID]/Urbalis_Sector_ID]/@ID"/>
		<xsl:variable name="stAlarmLabel" select="concat(if(contains(@Name,'ESS')) then ('ESS ')
														else if(contains(@Name,'ESP')) then 'ESP '
														else 'SPKS ',@Name,' In Activated Position')"/>
		<xsl:variable name="cl01" select="if($uid='1' or $uid='2') then (concat('MDD_',$uid,'/SIG/CBI'))
											else if($uid='17' or $uid='18') then (concat('ECID_',$uid,'/SIG/CBI'))
											else (concat('TE',$uid,'/SIG/CBI'))"></xsl:variable>	
		<xsl:variable name="CustomLabel02">O</xsl:variable>													
			<Object name="{@Name}" rules="update_or_create">
				<Properties>
					${multilingualvalue("$cl01","CustomLabel01")}
					${multilingualvalue("$CustomLabel02","CustomLabel02")}
					${multilingualvalue("$stAlarmLabel","StatusAlarmLabel")}
					<Property name="Severity" dt="i4"><xsl:value-of select="if(contains(@Name,'_ESS') or contains(@Name,'_ESP')) then '1' else '2'"/></Property>
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

