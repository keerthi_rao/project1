<%inherit file="/Object_tem.mako"/>
<%! class_ = "TOMTT2DepotMgr" %>

<%block name="classes">
	<Class name="TOMTT2DepotMgr">
	     <Objects>
			 <xsl:apply-templates select="//ATS_Equipment[Signalisation_Area_ID=$SERVER_ID and ATS_Equipment_Type='Local Server' and //Signalisation_Area[@ID=$SERVER_ID and contains(Localisation_Type,'Depot') and Area_Type='ATS_Local_Server']]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="ATS_Equipment">
 <Object name="MainTELLV1.TOMTT2DepotMgr" rules="update_or_create">

 </Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
