<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
  	<Class name="S2KEvents_Merge">
  		<Objects>
  			<xsl:apply-templates select="//Point[sys:cbi/@id=$CBI_ID]" mode="Event"/>
  		</Objects>
  	</Class>	
</%block>	

<xsl:template match="Point" mode="Event">
	<xsl:apply-templates select="//Events/Event[Type='Point']">
		<xsl:with-param name="pointname" select = "@Name" />
		<xsl:with-param name="trackid" select = "Track_ID" />
		<xsl:with-param name="terrname" select = "sys:terr/@name" />
	</xsl:apply-templates>
</xsl:template>

<xsl:template match="Event">
	<xsl:param name = "pointname" />
	<xsl:param name = "trackid" />
	<xsl:param name = "terrname" />
	<xsl:variable name="te" select="if (contains($terrname,'MDD')) then ('MDD') else if (contains($terrname,'TTCU')) then 'TTCU' else concat('TE',substring-after($terrname,'Territory_'))"/>
	<xsl:variable name="objname" select="concat($pointname,@Name)"/>
	<xsl:variable name="nmpv" select="if (Name_PV_Trig) then (replace(Name_PV_Trig,'--Point.Name--',$pointname)) else null"/>
	<xsl:variable name="avanch" select="Avalanche"/>
	<xsl:variable name="eqid" select="replace(replace(EquipmentID,'yyyy',$pointname),'xxxx',$te)"/>
	<xsl:variable name="mms" select="MMS"/>
	<xsl:variable name="omb" select="OMB"/>
	<xsl:variable name="tracktype" select="//Track[@ID=$trackid]/Track_Type"></xsl:variable>
	<xsl:variable name="optr" select="if($tracktype='Depot') then ('MDD') else if($tracktype='Test track') then ('TTCU') else if($tracktype='Mainline') then ('TAS') else null"/>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="Label"/></Property>
			<Property name="AlarmInhib" dt="boolean">1</Property>
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$pointname"/></Property>
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="Out_PV_Trig"/></Property>
			<Property name="State_PV_Trig" dt="string"><xsl:value-of select="State_PV_Trig"/></Property>
			<Property name="Operator" dt="string"><xsl:value-of select="concat($terrname,'.TAS')"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="Severity"/></Property>
			${multilingualvalue('$avanch','Avalanche')}	
			${multilingualvalue('$eqid','EquipmentID')}	
			${multilingualvalue('$mms','MMS')}	
			${multilingualvalue('$omb','OMB')}	
			${multilingualvalue("'-'",'Value')}	
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

