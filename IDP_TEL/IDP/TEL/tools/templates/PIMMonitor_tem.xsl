<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "PIMMonitor"
%>
<%block name="classes">
	<Class name="PIMMonitor">
		<Objects>
			<xsl:apply-templates select="$sysdb//PIMMonitor[PIMPlatform=//PIMPlatform[PlatformID=//Platform[@ID=//Stopping_Area[sys:sigarea/@id=$SERVER_ID]/*/Original_Area[@Original_Area_Type='Platform']/@ID]/@ID]/@ID]"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="PIMMonitor">
	<xsl:variable name="nm" select="@Name"/>
	<Object name="{@Name}" rules="update_or_create">
	<xsl:variable name="dir1" select="if(AnnouncementPoint_1='') then '0' else if(tokenize(AnnouncementPoint_1,'_')[last()]='Right') then ('2') else if (tokenize(AnnouncementPoint_1,'_')[last()]='Left') then('1') else '0'"/>
	<xsl:variable name="dir2" select="if(AnnouncementPoint_2='') then '0' else if(tokenize(AnnouncementPoint_2,'_')[last()]='Right') then ('2') else if (tokenize(AnnouncementPoint_2,'_')[last()]='Left') then('1') else '0'"/>

		<Properties>
			<Property name="Announcement_1" dt="boolean"><xsl:value-of select="if (AnnouncementPoint_1 != '') then 1 else 0"/></Property>
			<Property name="Announcement_2" dt="boolean"><xsl:value-of select="if (AnnouncementPoint_2 != '') then 1 else 0"/></Property>
			${props([("AnnouncementPoint_1","string",'<xsl:value-of select="AnnouncementPoint_1"/>'),
			         ("AnnouncementPoint_2","string",'<xsl:value-of select="AnnouncementPoint_2"/>'),
			         ("ApproachPoint_1","string",'<xsl:value-of select="ApproachPoint_1"/>'),
			         ("ApproachPoint_2","string",'<xsl:value-of select="ApproachPoint_2"/>'),
			         ("bAnnounceIsOnHL_1","boolean",'<xsl:value-of select="AnnounceIsOnHL_1"/>'),
			         ("bAnnounceIsOnHL_2","boolean",'<xsl:value-of select="AnnounceIsOnHL_2"/>'),
			         ("bApproachIsOnTE_1","boolean",'<xsl:value-of select="ApproachIsOnTE_1"/>'),
			         ("bApproachIsOnTE_2","boolean",'<xsl:value-of select="ApproachIsOnTE_2"/>'),
			         ("ForceAreaID","i4","0"),
			         ("Direction_1","i4",'<xsl:value-of select="$dir1"/>'),
			         ("Direction_2","i4",'<xsl:value-of select="$dir2"/>')])}
			${multilingualvalue('$nm')}
			<Property name="PIMMonitorID" dt="i4"><xsl:value-of select="@ID"/></Property>
			<Property name="pimPlatformPoint" dt="string"><xsl:value-of select="PlatformPoint"/></Property>
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,props"/>