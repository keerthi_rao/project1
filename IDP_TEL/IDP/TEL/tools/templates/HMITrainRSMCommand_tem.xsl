<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMITrainRSMCommand" %>

<!-- List for RSM commands: [(rsmmode, (rsmcommand), (rsmfield), (rsmvalue))] -->
<% rsmcommand=[('.RSCommand.ReadinessMode1' , ('RSCommand1', 'RSCommand2', 'RSCommand3', 'RSCommand4', 'RSCommand5'), ('TRAIN_READINESS_STATE_REQUEST', 'TRAIN_READINESS_STATE_REQUEST', 'TRAIN_READINESS_STATE_REQUEST', 'TRAIN_READINESS_STATE_REQUEST', 'TRAIN_READINESS_STATE_REQUEST'), ('00000100', '00000101', '00000110', '00000111','00001000')), 
('.RSCommand.ReadinessMode2', ('RSCommand1', 'NA'), ('TRAIN_READINESS_STATE_REQUEST', 'NA'), ('00000001', 'NA')),
('.RSCommand.DrivingMode', ('RSCommand1', 'RSCommand2'), ('REMOTE_AUTHORIZATION_OF_CBTC', 'REMOTE_PROHIBITION_OF_CBTC'), ('1', '1')),
('.RSCommand.NoCondCtrl', ('RSCommand1', 'RSCommand2', 'RSCommand3', 'RSCommand4'), ('PROPULSION_RESET', 'AUXILIARY_CONVERTER_BATTERY_CHARGER_RESET', 'FIRE_SMOKE_DETECTION_SYSTEM_RESET','PMD_TRANSMISSION_REQUEST'), ('1', '1', '1','1')),
('.RSCommand.Creep', ('RSCommand1', 'RSCommand2'), ('CREEP_MODE_REQUEST', 'CREEP_MODE_DEACTIVATION'), ('1', '1')),
('.RSCommand.EBReset', ('RSCommand1', 'NA'), ('EB_RESET_REQUEST', ''), ('1', '')),
('.RSCommand.Parking', ('RSCommand1', 'NA'), ('PARKING_BRAKE_ON', ''), ('1', '')),
('.RSCommand.TunnelAir', ('RSCommand1','RSCommand2'), ('TUNNEL_AIR_CUT_OUT_ON', 'TUNNEL_AIR_CUT_OUT_OFF'), ('1', '1')),
('.RSCommand.EmerVent', ('RSCommand1', 'RSCommand2'), ('EMERGENCY_VENTILATION_ON', 'EMERGENCY_VENTILATION_OFF'), ('1', '1')),
('.RSCommand.PreCool', ('RSCommand1', 'RSCommand2'), ('PRE_COOLING_ON', 'PRE_COOLING_OFF'), ('1', '1')),
('.RSCommand.ExitSign', ('RSCommand1', 'RSCommand2', 'RSCommand3', 'RSCommand4'), ('EXIT_SIGNAGE_DM1_ON', 'EXIT_SIGNAGE_DM1_OFF', 'EXIT_SIGNAGE_DM2_ON', 'EXIT_SIGNAGE_DM2_OFF'), ('1', '1', '1','1')),
('.RSCommand.Floodlight', ('RSCommand1','RSCommand2', 'RSCommand3', 'RSCommand4'), ('FLOODLIGHT_DM1_ON', 'FLOODLIGHT_DM1_OFF', 'FLOODLIGHT_DM2_ON', 'FLOODLIGHT_DM2_OFF'), ('1', '1', '1', '1'))] 
%>

<%block name="classes">
	 <Class name='HMITrainRSMCommand'>
	 	<Objects>
	 		<xsl:apply-templates select="$sysdb//Train_Unit"/>
	 	</Objects>
	 </Class>
</%block>

<!-- Template to call Train Units -->
<xsl:template match="Train_Unit">
%	for name, cmmd, field, value in rsmcommand:
	<xsl:variable name='tt' select="'${name}'"/>
	<xsl:variable name="nm" select="concat('Train', format-number(@ID, '000') , '${name}')"/>
	<Object name="{$nm}" rules="update_or_create">
		<Properties>
			${props([('SharedOnIdentifier','string','<xsl:value-of select="$nm"/>'),('SharedOnIdentifierDefinition','boolean','1')])}	
			% for i in range(len(cmmd)):
			    % if cmmd[i] != 'NA':
					<xsl:variable name="rsmcommand">%RollingStockID%|<xsl:value-of select="@ID"/>|XmlRSMCommand|&lt;GenericSettingRequest&gt;&lt;Field name="${field[i]}" value="${value[i]}" DEMUX="1"/&gt;&lt;/GenericSettingRequest&gt;</xsl:variable>
					${prop(cmmd[i], '<xsl:value-of select="$rsmcommand"/>' , 'string')}
				% endif
			% endfor
			<xsl:variable name="trainid" select="concat('Train', format-number(@ID, '000'))"/>
			${multilingualvalue("$trainid")}
		</Properties>
	</Object>
%	endfor
</xsl:template>   		

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->

<%namespace file="/lib_tem.mako" import="props, prop, multilingualvalue"/>
