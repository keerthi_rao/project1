<%inherit file="/s2kfunction_tem.mako"/>

<%block name="classes">
	<Grp GrpName="User">
		<xsl:variable name="grObject" select="'User'"/>
		<xsl:variable name="prpval" select="//Projects/Project/@Name"/>
		${s2kfun('$grObject','$prpval')}
			<Grp GrpObj="User" GrpName="A">
				<xsl:variable name="grObject" select="concat('User/','A')"/>
				<xsl:variable name="prpval" select="'A'"/>
				<xsl:variable name="shared" select="concat('User_','Auto')"/>
				${s2kfun('$grObject','$prpval','$shared','$prpval')}
			</Grp>
			<Grp GrpObj="User" GrpName="S">
				<xsl:variable name="grObject" select="concat('User/','S')"/>
				<xsl:variable name="prpval" select="'S'"/>
				<xsl:variable name="shared" select="concat('User_','Semi')"/>
				${s2kfun('$grObject','$prpval','$shared','$prpval')}
			</Grp>			
	</Grp>
</%block>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="lib_tem_tel.mako" import="s2kfun"/>