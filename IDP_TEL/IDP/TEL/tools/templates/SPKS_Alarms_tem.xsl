<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID]"/>	
			<xsl:apply-templates select="$sysdb//SPKS[sys:sigarea/@id=$SERVER_ID and contains(@Name,'_ESP')]"  mode="tt3"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="SPKS">
	<xsl:variable name="TTName" select= "@Name"/>
	<xsl:variable name="objname" select= "concat(@Name,'.Alarms_Gen')"/>
    <xsl:variable name="uname" select="concat(substring-after(//Technical_Room[@ID=current()/Technical_Room_ID]/@Name,'_'),'/SIG/CBI/',@Name)"/>
    	
			<xsl:if test="not(contains($TTName,'_ESP') or contains(@Name,'_ESS'))">
					${obj('{$objname}',[('SPKS','SPKS %s1 In Activated Position','2')])}
			</xsl:if>
			<xsl:if test="contains($TTName,'_ESP')">
					${obj('{$objname}',[('SPKS','ESP %s1 In Activated Position','1')])}
			</xsl:if>		
			<xsl:if test="contains($TTName,'_ESS')">
					${obj('{$objname}',[('SPKS','ESS %s1 In Activated Position','1')])}
			</xsl:if>		
			<xsl:if test="contains($TTName,'_CDBD')">
					${obj('{$objname}',[('SPKS','CDBDKS %s1 In Activated Position','1')])}
			</xsl:if>				
</xsl:template>


<xsl:template match="SPKS" mode="tt3">
<xsl:variable name="techroomid" select="current()/Technical_Room_ID"/>
	<xsl:if test="//Track[@ID=//Block[@ID=current()/Block_ID and sys:sigarea/@id=$SERVER_ID]/Track_ID]/Track_Type='Test track'">
		<xsl:variable name="TTName" select= "@Name"/>
		<xsl:variable name="objname" select= "concat(@Name,'.Alarms_Gen')"/>
		<xsl:variable name="uname" select="concat(substring-after(//Technical_Room[@ID=$techroomid]/@Name,'_'),'/SIG/CBI/',@Name)"/>
    	
			${obj('{$objname}',[('SPKS','Test Track ESB In Activated Position','2')])}
	</xsl:if>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,srvty in list:
		<Object name="${nm}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>			
			<!-- 	${prop('AlarmInhib','1','boolean')}			 -->
				${multilingualvalue("'-'",'Avalanche')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>			
				${multilingualvalue('$TTName','Name')}			
				${multilingualvalue('$uname','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}		
				<Property name="Name_PV_Trig" dt="string">${key}<xsl:value-of select="concat('_',$TTName)"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				<Property name="State_PV_Trig" dt="i4">0</Property>					
				${multilingualvalue("'O'",'OMB')}			
				<Property name="Severity" dt="i4"><xsl:value-of select="${srvty}"/></Property>		
				${multilingualvalue("'Activated'",'Value_on')}			
				${multilingualvalue("'Normal'",'Value_off')}	
			</Properties>
		</Object>
	% endfor
</%def>
