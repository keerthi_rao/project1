<%inherit file="/Object_tem.mako"/>
<%! class_ = "PointOut" %>

<%block name="classes">
	<Class name="PointOut">
	     <Objects>
			 <xsl:apply-templates select="//Points/Point[sys:sigarea/@id=$SERVER_ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Point">
		<xsl:variable name="sector_name" select="//Tunnels/Tunnel[Point_ID_List/Point_ID=current()/@Name]/@Name"/>
		<Object name="{@Name}.PointOut" rules="update_or_create">
		  <Properties>
				<Property name="Name_PV_APN" dt="string"><xsl:value-of select="concat('APN_',@Name)"/></Property>
				<Property name="Name_PV_APR" dt="string"><xsl:value-of select="concat('APR_',@Name)"/></Property>
				<Property name="SOI_Detect" dt="string"><xsl:value-of select="concat(@Name,'.Detection')"/></Property>				
				<Property name="PointName" dt="string"><xsl:value-of select="@Name"/></Property>	
				<xsl:variable name="eid" select="concat($sector_name,'/SIG/CBI/',@Name)"/>
				${multilingualvalue("$eid","EquipmentID")}
				${multilingualvalue("'B'","OMB")}		
				${multilingualvalue("'Fault'",'Value_on')}
				${multilingualvalue("'Normal'",'Value_off')}				
				${multilingualvalue("'N'",'Avalanche')}				
		  </Properties>
		</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>