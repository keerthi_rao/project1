<%inherit file="/Object_tem.mako"/>
<%! class_ = "HMITrainAttributes" %>

<%block name="classes">
	<Class name="HMITrainAttributes">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="Train_Unit">
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:for-each select="//HMITrainAttributes/Object">
	  <Object name="{$TrainName}.{@Name}" rules="update_or_create">
		  <Properties>
			${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
			<xsl:for-each select="./*">
				${prop('{name(.)}','<xsl:value-of select="."/>')}
			</xsl:for-each>
			${multilingualvalue("$TrainName")}
		  </Properties>
	  </Object>
	</xsl:for-each>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
