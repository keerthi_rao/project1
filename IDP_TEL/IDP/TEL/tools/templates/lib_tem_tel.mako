<%def name="s2kfun(grpobj,PrpValue,sharedval='',otherProp='NA')">
	<Properties GrpObj="{${grpobj}}">
	  <xsl:for-each select="//MLV">
		<Property PrpName="Name" PrpValue="{${PrpValue}}" LocaleID="{@LCID}" RoleID="{../@RID}"/>
	  </xsl:for-each>
	% if otherProp != 'NA':
		<Property PrpName="SharedOnIdentifier" PrpValue="{${sharedval}}"/>
	% endif
	</Properties>
</%def>
<%def name="node(name, select)"><xsl:element name="${name}"><xsl:value-of select="${select}"/></xsl:element></%def>
<%def name="copy(elems)">
% for e in elems:
    <xsl:copy-of select="${e}" copy-namespaces="no"/>
% endfor
</%def>
<%def name="Equipment(type, flavor, list)" >
     <Equipment name="{@Name}" type="${type}" flavor="${flavor}">
		<States>
	% for name,equation in list:
			  <Equation>${name}::${equation}</Equation>
	% endfor
		</States>
     </Equipment>
</%def>
