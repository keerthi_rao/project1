<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
		   <xsl:apply-templates select="//LX_Device[Type='Aspect Indicator' and  LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:cbi/@id = $CBI_ID]/@ID]/@ID]/@ID]/@ID]"/>
</%block>

<xsl:template match="LX_Device">
	<xsl:variable name="lxdvar" select="$sysdb//LX_Devices_Variable/LX_Device[@ID =current()/@ID]"/>
<%
  tt = "<xsl:value-of select='@Name'/>"
%>
<xsl:if test="$lxdvar/LXAI and $lxdvar/LXLP">
	<Equipment name="{@Name}.TEL" type="TELLevelCrossingDevice" flavor="LampStatus">
		<States>
			<Equation>LXAI_RED[0]::LXAI_${tt}==1 &amp;&amp; LXLP_${tt}==1</Equation>
			<Equation>LXAI_RED_BLINKING[1]::LXAI_${tt}==1 &amp;&amp; LXLP_${tt}==0</Equation>
			<Equation>LXAI_WRITE[2]::LXAI_${tt}==0 &amp;&amp; MySelf==0</Equation>
			<Equation>LXAI_WRITE_BLINKING[3]::LXAI_${tt}==0 &amp;&amp; MySelf==1</Equation>
		</States>
	</Equipment>
</xsl:if>
	  
</xsl:template>
