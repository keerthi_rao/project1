<%inherit file="/Object_tem.mako"/>
<%! class_ = "CalcHMITrainAttributes" %>

<%block name="classes">
%	for clsname in ['CalculatedTrainPTI', 'CalculatedTrainTemp','CalculatedCumulatedDist']:
	<Class name="${clsname}">
	     <Objects>
			 <!-- Template for generating Object's from template Train_Unit-->
			 <xsl:apply-templates select="//Trains_Unit/Train_Unit"><xsl:with-param name="classname" select="'${clsname}'"/></xsl:apply-templates>
		 </Objects>
	</Class>
% endfor
</%block>

<xsl:template match="Train_Unit">
	<xsl:param name="classname"/>
	<xsl:variable name="TrainName" select="concat('Train',format-number(@ID,'000'))"/>
	<xsl:variable name="trainID" select="@ID"/>
	<xsl:for-each select="//ComputedTrainAttributes/ObjectClass[@Name=$classname]/Instance">
	  <Object name="{$TrainName}.{Property[@Name='PropertyName']/text()}" rules="update_or_create">
		  <Properties>
		  ${prop('TrainName','<xsl:value-of select="$TrainName"/>')}
		  ${prop('UniqueID','<xsl:value-of select="$trainID"/>', 'i4')}
			  <xsl:for-each select="Property">
				<xsl:variable name="propvl" select="normalize-space(.)"/>
				<Property name="{@Name}" dt="string"><xsl:value-of select="if(contains(@Name, 'TrainAttributeObj') and $propvl) then concat($TrainName, '.', $propvl) else if(contains(@Name, 'TrainAttributeObj') and not($propvl)) then null else $propvl"/></Property>
			  </xsl:for-each>
		  </Properties>
	  </Object>
	</xsl:for-each>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
