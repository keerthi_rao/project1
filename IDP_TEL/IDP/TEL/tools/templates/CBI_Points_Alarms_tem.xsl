<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="$sysdb//Point[sys:cbi/@id=//Signalisation_Area[@ID=$SERVER_ID]/sys:cbi/@id]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Point">
			<xsl:variable name="sector_name" select="//Tunnels/Tunnel[Point_ID_List/Point_ID=current()/@Name]/@Name"/>
			<xsl:variable name="TTName" select= "@Name"/>
			<xsl:variable name="name_pv_Trig1" select="concat(@Name,'.MvtCounterThresholdXceed','.PV_Status')"/>
			<xsl:variable name="name_pv_Trig2" select="concat(@Name,'.MvtTimeChangeValue','.PV_Status')"/>
			<xsl:variable name="uname" select="concat($sector_name,'/SIG/CBI/',@Name)"/>
			${obj('{$TTName}',[('Alarm.MvtCounterThresholdXceed','Point %s1 Movement Counter Exceeded Threshold %s2','10000000','10000001'),
				   ('Alarm.MvtTimeChangeValue','Point %s1 Movement Time','','777')
])}
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,bstr2,stpv in list:
		<Object name="${nm}.${key}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				<xsl:variable name="bstrp2v" select="'${bstr2}'"/>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>		
				${prop('AlarmInhib','1','boolean')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>			
				<xsl:if test="$bstrp2v != ''"><Property name="BstrParam2" dt="string">${bstr2}</Property></xsl:if>			
				${multilingualvalue('$TTName','Name')}				
				${multilingualvalue('$uname','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}			
				<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="if($bstrp2v != '') then ($name_pv_Trig1) else ($name_pv_Trig2)"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>			
				<Property name="State_PV_Trig" dt="i4">${stpv}</Property>					
				${multilingualvalue("'M'",'OMB')}				
				${multilingualvalue("'N'",'Avalanche')}				
				${multilingualvalue("'-'",'Operator')}				
				${multilingualvalue("'Yes'",'Value_on')}				
				${multilingualvalue("'No'",'Value_off')}				
				<Property name="Severity" dt="i4">2</Property>					
	
			</Properties>
		</Object>
	% endfor
</%def>
