<%inherit file="/Object_tem.mako"/>
<%! class_ = "MainTELLV2" %>
<%block name="classes">
	<xsl:variable name="sigarea" select="//Signalisation_Area[Area_Type='ATS_Central_Server' and sys:sigarea/@id=//Signalisation_Area[@ID=$SERVER_ID and Area_Type='ATS_Local_Server']/@ID]"/>
	<xsl:if test="$sigarea">
	<Class name="S2KIOOPCServerMonitor">
		<Objects>
			<xsl:apply-templates select="$sigarea" mode="S2KIOOPCServerMonitor"/>
		</Objects>
	</Class>
	</xsl:if>
	<Class name="MainTELLV2">
		<Objects>
			<Object name="MainTELLV2" rules="update_or_create">
				<xsl:variable name="pimStationIDs">
					<xsl:apply-templates select="//PIMStation[Signalisation_Area=$SERVER_ID]"><xsl:sort select="@ID"/></xsl:apply-templates>
				</xsl:variable>
				<xsl:variable name="iscsStationIDs">
					<xsl:apply-templates select="//ISCSStation[Signalisation_Area=$SERVER_ID]"><xsl:sort select="@ID"/></xsl:apply-templates>
				</xsl:variable>
				<xsl:variable name="sigAreaType" select="//Signalisation_Area[@ID=$SERVER_ID]/Area_Type"/>
				<xsl:variable name="centralSigArea" select="//Signalisation_Area[Area_Type='ATS_Central_Server']"/>
				<xsl:variable name="serType" select="if ($sigAreaType='ATS_Central_Server') then 'CATS' else if ($sigAreaType='ATS_Local_Server') then 'LATS' else null"/>
				<xsl:variable name="PIMDatConnQualityStation1" select="if ($serType='CATS') then (//PIMStation[Signalisation_Area=$SERVER_ID]/@Name) else (//PIMStation[@ID=(tokenize($pimStationIDs,';')[1])]/@Name)"/>
				<xsl:variable name="PIMDatConnQualityStation2" select="if ($serType='CATS') then (null) else (//PIMStation[@ID=(tokenize($pimStationIDs,';')[2])]/@Name)"/>
				<xsl:variable name="PIMDatConnQualityStation3" select="if ($serType='CATS') then (null) else (//PIMStation[@ID=(tokenize($pimStationIDs,';')[last()-1])]/@Name)"/>	
				<xsl:variable name="checkcqs" select="if($PIMDatConnQualityStation3=$PIMDatConnQualityStation1 or $PIMDatConnQualityStation3=$PIMDatConnQualityStation2) then (null) else $PIMDatConnQualityStation3"/>
				<Properties>
					<Property name="ServerType" dt="string"><xsl:value-of select="$serType"/></Property>

					${multilingualvalue("$PIMDatConnQualityStation1", "PIMDatConnQualityStation1")}

					${multilingualvalue("$PIMDatConnQualityStation2", "PIMDatConnQualityStation2")}

					${multilingualvalue("$checkcqs", "PIMDatConnQualityStation3")}
					
					<xsl:variable name="pim" select="//PIMStation[Signalisation_Area=$SERVER_ID]/@Name"/>
					<Property name="PIMDatEnableStation1" dt="boolean"><xsl:value-of select="if($serType='CATS' or $pim) then 1 else 0"/></Property>
					<Property name="PIMDatEnableStation2" dt="boolean"><xsl:value-of select="if($serType='CATS' or not($pim[2])) then 0 else 1"/></Property>
					<Property name="PIMDatEnableStation3" dt="boolean"><xsl:value-of select="if($serType='CATS' or not($pim[last()])) then 0 else 1"/></Property>
					<Property name="BCATSOPCClientSharedIdentifier" dt="string"></Property>
					<Property name="BCATSTrafficInfoOPCTag" dt="string"></Property>
					<Property name="EnableBCATSTrafficInfo" dt="boolean">0</Property>
					<Property name="CATSOPCClientSharedIdentifier" dt="string"><xsl:value-of select="if($centralSigArea) then concat('OPCClient_ATS_', $centralSigArea/@ID, '_LV2') else ''"/></Property>
					<Property name="CATSTrafficInfoOPCTag" dt="string"><xsl:if test="$sigAreaType='ATS_Local_Server' and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/@Name,'DATS'))">MainTELLV2.PIMModule.PIMBridge.OnTrafficInfo</xsl:if></Property>
					<Property name="EnableCATSTrafficInfo" dt="boolean"><xsl:value-of select="if($sigAreaType='ATS_Local_Server' and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/@Name,'DATS'))) then '1' else '0'"/></Property>
					<Property name="SplitOPCGroupName" dt="string">Split_<xsl:value-of select="$SERVER_ID"/></Property>
					<Property name="TWPOPCClientID" dt="string"><xsl:if test="$sigAreaType='ATS_Local_Server'">OPCClient_TWP</xsl:if></Property>					
				</Properties>
			</Object>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area" mode="S2KIOOPCServerMonitor">
	<xsl:variable name="ats_eqps" select="//ATS_Equipment[Signalisation_Area_ID = current()/@ID or Signalisation_Area_ID_List/Signalisation_Area_ID = current()/@ID]/@Name"/>
	<Object name="OPCClient_ATS_{@ID}_LV2" rules="update_or_create">
	<Properties>
		${prop("ConnectAttemptsPeriod","10","i4")}
		${prop("DoubleLinks","0","boolean")}
		${prop("MonitoringPeriod","30", "i4")}
		${prop("MultiActive","1","boolean")}
		${prop("ServerNodeName1",'<xsl:value-of select="$ats_eqps[1]"/>')}
		${prop("ServerNodeName2",'<xsl:value-of select="$ats_eqps[2]"/>')}
		${prop("ServerProgID1","S2K.OpcServer.First.1")}
		${prop("ServerProgID2","S2K.OpcServer.First.1")}
		${prop("SharedOnIdentifier",'OPCClient_ATS_<xsl:value-of select="@ID"/>_LV2')}
	</Properties>
	</Object>
</xsl:template>
<xsl:template match="ISCSStation|PIMStation"><xsl:value-of select="@ID"/>;</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue,prop"/>