<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "GenericAreaPropertyBag"
%>

<% Direction={'Left':'LUP','Right':'RUP'} %>

<xsl:variable name="blocks" select="//Block"/>

<%block name="classes">
	<Class name="GenericAreaPropertyBag">
		<Objects>
			<xsl:apply-templates select="$sysdb//Stopping_Area[Original_Area_List/Original_Area/@Original_Area_Type='Platform' and sys:sigarea/@id=$SERVER_ID and (//Signalisation_Area[@ID=$SERVER_ID and (Localisation_Type='Mainline' and (Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server'))])]"/>
		</Objects>
	</Class>
</%block>

<xsl:template match="Stopping_Area">
	<xsl:variable name="sa" select="current()"/>
	<xsl:variable name="sa_id" select="@ID"/>
	<xsl:variable name="sa_nm" select="@Name"/>
	
	%for dir in Direction:
		<xsl:variable name="direction" select="'${dir}'"/>
		
		<xsl:variable name="obj_name" select="concat($sa_nm,'.${Direction[dir]}')"/>
		
		<xsl:variable name="UpstreamPlatforms">
			<xsl:call-template name="Compute_UpstreamPlatforms">
				<xsl:with-param name="direction" select="$direction"/>
				<xsl:with-param name="curr_sa" select="$sa"/>
				<xsl:with-param name="ori_sa" select="$sa"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name="upstream_platforms"><xsl:value-of select="for $sa in $UpstreamPlatforms/Sure_Upstream_Stopping_Areas return (if (not($sa=$sa_nm)) then $sa else null)" separator=";"/></xsl:variable>
		
		<Object name="{$obj_name}" rules="update_or_create">
			<Properties>
				<Property name="SharedOnIdentifier" dt="string"><xsl:value-of select="$obj_name"/></Property>
				<Property name="SharedOnIdentifierDefinition" dt="boolean">1</Property>
				<Property name="GenericAreaID" dt="string"><xsl:value-of select="$sa_nm"/></Property>
				<Property name="PropertyName" dt="string">${dir}_Upstream_Platform</Property>
				<Property name="PropertyType" dt="string">Bstr</Property>
				<xsl:choose>
					<xsl:when test="$sa_id=//Stopping_Area[@ID=//SAHMPlatform[if ($direction='Left') then (Direction='1') else (Direction='2')]/Stopping_Area_ID]/@ID">
						<Property name="PropertyValue" dt="string"><xsl:value-of select="concat(//SAHMPlatform[(if ($direction='Left') then (Direction='1') else (Direction='2')) and  Stopping_Area_ID=$sa_id]/UpstreamPlatform,';')"/></Property>
					</xsl:when>
					<xsl:otherwise>
				<Property name="PropertyValue" dt="string"><xsl:value-of select="if (normalize-space($upstream_platforms)) then (concat($upstream_platforms,';')) else null"/></Property>
					</xsl:otherwise>
				</xsl:choose>
			
			</Properties>
		</Object>
	%endfor
</xsl:template>

<xsl:template name="Compute_UpstreamPlatforms">
	<xsl:param name="direction"/>
	<xsl:param name="curr_sa"/>
	<xsl:param name="ori_sa"/>
	
	<xsl:variable name="poss_sa">
		<xsl:for-each select="//Inter_Stopping_Area[Stopping_Area_Destination_ID=$curr_sa/@ID and ($direction = sys:getISADirection(.,$blocks))]">
			<Possible_Upstream_Stopping_Areas><xsl:value-of select="Stopping_Area_Origin_ID"/></Possible_Upstream_Stopping_Areas>
		</xsl:for-each>
	</xsl:variable>
	
	<xsl:variable name="sure_sa" select="//Stopping_Area[@ID=$poss_sa/Possible_Upstream_Stopping_Areas and Original_Area_List/Original_Area/@Original_Area_Type='Platform']"/>
	
	<xsl:choose>
		<xsl:when test="not($poss_sa/Possible_Upstream_Stopping_Areas)">
			<xsl:message terminate="no">
+++++-----------------+++++
Warning: No UpstreamPlatforms for <xsl:value-of select="$direction"/> direction
	Stopping Area ID: <xsl:value-of select="$ori_sa/@ID"/>; Name: <xsl:value-of select="$ori_sa/@Name"/>
	In File : GenericAreaPropertyBag_<xsl:value-of select="$FUNCTION_ID"/><xsl:value-of select="$SERVER_LEVEL"/>.xml
+++++-----------------+++++	
			</xsl:message>
		</xsl:when>
		
		<xsl:when test="$sure_sa">
			<xsl:for-each select="$sure_sa">
				<Sure_Upstream_Stopping_Areas><xsl:value-of select="@Name"/></Sure_Upstream_Stopping_Areas>
			</xsl:for-each>
		</xsl:when>
		
		<xsl:when test="$poss_sa/Possible_Upstream_Stopping_Areas and not($sure_sa)">
			<xsl:variable name="next_sa" select="//Stopping_Area[@ID=$poss_sa/Possible_Upstream_Stopping_Areas]"/>
			<xsl:call-template name="Compute_UpstreamPlatforms">
				<xsl:with-param name="direction" select="sys:opposite($direction)"/>
				<xsl:with-param name="curr_sa" select="$next_sa"/>
				<xsl:with-param name="ori_sa" select="$ori_sa"/>
			</xsl:call-template>
		</xsl:when>
		
		<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>

<xsl:function name="sys:getISADirection">
  <xsl:param name="isa"/>
  <xsl:param name="blocks"/>
  <xsl:variable name="b" select="$isa/sys:block_list/sys:block"/>
  <xsl:value-of select="if ($blocks[@ID=$b[last()-1]/@id]/Next_Down_Normal_Block_ID=$b[last()]/@id) then 'Left' else 'Right'"/>
</xsl:function>

<xsl:function name="sys:opposite">
    <xsl:param name="dir"/>
	<xsl:choose>
		<xsl:when test="$dir = 'Left'">Right</xsl:when>
		<xsl:when test="$dir = 'Right'">Left</xsl:when>
		<xsl:otherwise>
			<xsl:message terminate="yes">Invalid direction : '<xsl:value-of select="$dir"/>'</xsl:message>
		</xsl:otherwise>
	</xsl:choose>
 </xsl:function>

