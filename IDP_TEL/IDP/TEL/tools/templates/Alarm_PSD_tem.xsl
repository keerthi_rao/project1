<%inherit file="/Object_tem.mako"/>
<%! 
class_ = "S2KAlarms_Gen"
xs_ = True
%>

<%block name="classes">
  	<Class name="S2KAlarms_Gen">
  		<Objects>
  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[Technical_Room_ID!='1'  and sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]"/>
			<xsl:apply-templates select="//Station[Platform_ID_List/Platform_ID=//Platform[PSD_ID=//Platform_Screen_Doors[Technical_Room_ID!='1'  and sys:sigarea/@id=$SERVER_ID and not(contains(//Signalisation_Area[@ID=$SERVER_ID]/Localisation_Type,'Depot'))]/@ID]/@ID]"/>
		</Objects>
  	</Class>
</%block>


<!-- Template to match the Platform table -->
<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="psd" select="."/>
	<xsl:variable name="nbDoors" select="number(//Platform_Screen_Doors_Characteristics[@ID=current()/Platform_Screen_Doors_Characteristics_ID]/Number_Of_Doors)"/>
	<xsl:apply-templates select="//PSDDoor_Operational_Alarm">
		<xsl:with-param name="nbDoors" select="$nbDoors"/>
		<xsl:with-param name="psd" select="$psd"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="//PSDDoor_Maintenance_Alarm">
		<xsl:with-param name="nbDoors" select="$nbDoors"/>
		<xsl:with-param name="psd" select="$psd"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="//PSD_Maintenance_Alarm[Type='PF']">
		<xsl:with-param name="psd" select="$psd"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="//PSD_Operational_Alarm[Type='PF']">
		<xsl:with-param name="psd" select="$psd"/>
	</xsl:apply-templates>
	<xsl:apply-templates select="$psd" mode="psd_sync"/>
</xsl:template>

<xsl:template match="Platform_Screen_Doors" mode="psd_sync">
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="pf" select="//Platform[PSD_ID=current()/@ID]"/>
	<xsl:variable name="pltform" select="$pf/@Name"/>
	<xsl:variable name="pf_pos" select="if (//Station[Platform_ID_List/Platform_ID[1]=$pf/@ID]) then 'PF-1' else 'PF-2'"/>
	<xsl:variable name="techroomname" select="//Technical_Room[@ID=current()/Technical_Room_ID]"/>
	<xsl:variable name="alarm_lbl" select="concat('Platform ',$pltform,': Time Synchronization Failed')"/>
	<xsl:variable name="spvt" select="'1'"/>
	<xsl:variable name="outpv" select="'Activity'"/>
	<xsl:variable name="o_m_b" select="'M'"/>
	<xsl:variable name="severity" select="'3'"/>
	<xsl:variable name="v_on" select="'-'"/>
	<xsl:variable name="v_off" select="'-'"/>
	<xsl:variable name="eqid" select="concat(substring-after($techroomname/@Name,'SER_'),'/PSD/Door/',$pf_pos)"/>
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="nmpv" select="concat('PSDTimeSyncModule_',@Name,'.SyncAlarm')"/>
	<xsl:variable name="objname" select="concat('PSDTimeSyncModule_',@Name,'.Alarm_TimeSync')"/>
	${obj(avalanche='N')}
</xsl:template>


<xsl:template match="PSD_Operational_Alarm">
	<xsl:param name = "psd"/>
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="pf" select="//Platform[PSD_ID=$psd/@ID]"/>
	<xsl:variable name="pltform" select="$pf/@Name"/>
	<xsl:variable name="techroomname" select="//Technical_Room[@ID=$psd/Technical_Room_ID]"/>
	<xsl:variable name="trigger" select="Trigger"/>
	<xsl:variable name="alarm_lbl" select="@Name"/>
	<xsl:variable name="spvt" select="State_PV_Trig"/>
	<xsl:variable name="outpv" select="Trigger"/>
	<xsl:variable name="o_m_b" select="OMB"/>
	<xsl:variable name="severity" select="Severity"/>
	<xsl:variable name="v_on" select="Value_On"/>
	<xsl:variable name="v_off" select="Value_Off"/>
	<xsl:variable name="eqid" select="concat(substring-after($techroomname/@Name,'SER_'),Location,'/',$pltform)"/>
	<xsl:variable name="nm" select="concat($psd/@Name,'.PSDStatus')"/>
	<xsl:variable name="nmpv" select="concat($psd/@Name,'.PSDStatus')"/>
	<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.',$trigger,'_',@ID)"/>
	${obj(bstrParam1=True,avalanche='N')}
</xsl:template>

<xsl:template match="PSD_Maintenance_Alarm">
	<xsl:param name = "psd"/>
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="pf" select="//Platform[PSD_ID=$psd/@ID]"/>
	<xsl:variable name="techroomname" select="//Technical_Room[@ID=$psd/Technical_Room_ID]"/>
	<xsl:variable name="pltform" select="$pf/@Name"/>
	<xsl:variable name="pfType" select="if ($pf/Platform_Type='Single Right Docking') then ('1') else if ($pf/Platform_Type='Single Left Docking') then ('2') else null"/>
	<xsl:variable name="trigger" select="Trigger"/>
	<xsl:variable name="alarm_lbl" select="@Name"/>
	<xsl:variable name="outpv" select="Trigger"/>
	<xsl:variable name="o_m_b" select="OMB"/>
	<xsl:variable name="severity" select="Severity"/>
	<xsl:variable name="spvt" select="'1'"/>
	<xsl:variable name="v_on" select="Value_On"/>
	<xsl:variable name="v_off" select="Value_Off"/>
	<xsl:variable name="eqid" select="concat(substring-after($techroomname/@Name,'_'),Location,'/',Detail_Code,$pfType)"/>
	<xsl:variable name="nm" select="concat($psd/@Name,'.PSDStatus')"/>
	<xsl:variable name="nmpv" select="concat($psd/@Name,'.PSDStatus')"/>
	<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.',$trigger)"></xsl:variable>
	${obj(bstrParam1=True,avalanche='N')}
</xsl:template>

<xsl:template match="PSDDoor_Operational_Alarm|PSDDoor_Maintenance_Alarm">
	<xsl:param name = "nbDoors"/>
	<xsl:param name = "psd"/>
	<xsl:variable name="loc" select="local-name()"/>
	<xsl:variable name="poa_pma" select="."/>
	<xsl:variable name="mlv" select="//MLV"/>
	<xsl:variable name="pf" select="//Platform[PSD_ID=$psd/@ID]"/>
	<xsl:variable name="techroomname" select="//Technical_Room[@ID=$psd/Technical_Room_ID]"/>
	<xsl:variable name="pltform" select="$pf/@Name"/>
	<xsl:variable name="pfType" select="if ($pf/Platform_Type='Single Right Docking') then ('1') else if ($pf/Platform_Type='Single Left Docking') then ('2') else null"/>
	<xsl:variable name="trigger" select="Trigger"/>
	<xsl:variable name="alarm_lbl" select="@Name"/>
	<xsl:variable name="outpv" select="Trigger"/>
	<xsl:variable name="o_m_b" select="OMB"/>
	<xsl:variable name="severity" select="Severity"/>
	<xsl:variable name="spvt" select="'1'"/>
	<xsl:variable name="v_on" select="Value_On"/>
	<xsl:variable name="v_off" select="Value_Off"/>
	<xsl:for-each select="1 to xs:integer($nbDoors)">
		<xsl:variable name="id" select="current()"/>
		<xsl:variable name="bp2" select="format-number(current(),'00')"/>
		<xsl:variable name="objname" select="concat($psd/@Name,'.Alarm','.Door',$id,'.',$trigger)"></xsl:variable>
		<xsl:variable name="nm" select="concat($psd/@Name,'.PSDStatus','.Door',$id)"></xsl:variable>
		<xsl:variable name="nmpv" select="concat($psd/@Name,'.PSDStatus','.Door',$id)"></xsl:variable>
		<xsl:variable name="eqid" select="if ($loc='PSDDoor_Operational_Alarm') then (concat(substring-after($techroomname/@Name,'_'),$poa_pma/Location,'/',$psd/@Name,'_',$id))
	                                      else (concat(substring-after($techroomname/@Name,'_'),$poa_pma/Location,'/',$poa_pma/Detail_Code,$pfType,$id))"/>
		<xsl:variable name="avalanche" select="if ($loc='PSDDoor_Operational_Alarm') then '-' else 'N'"/>
		${obj(bstrParam1=True,bstrParam2=True,avalanche='<xsl:value-of select="$avalanche"/>')}
	</xsl:for-each>
</xsl:template>

<xsl:template match="Station">
	<xsl:variable name="st" select="."/>
	<xsl:variable name="troom" select="//Technical_Room"/>
	<xsl:variable name="pfs" select="//Platform[@ID=current()/Platform_ID_List/Platform_ID]"/>
	<xsl:variable name="psd" select="//Platform_Screen_Doors"/>
	<xsl:for-each select="//PSD_Maintenance_Alarm[Type='ST']">
		<xsl:variable name="mlv" select="//MLV"/>
		<xsl:variable name="pf" select="if (position()=1) then $pfs[1] else $pfs[2]"/>
		<xsl:variable name="techroomname" select="$troom[@ID=$psd[@ID=$pf/PSD_ID]/Technical_Room_ID]"/>
		<xsl:variable name="pltform" select="$st/@Name"/>
		<xsl:variable name="pfType" select="if ($pf/Platform_Type='Single Right Docking') then ('1') else if ($pf/Platform_Type='Single Left Docking') then ('2') else null"/>
		<xsl:variable name="trigger" select="Trigger"/>
		<xsl:variable name="alarm_lbl" select="@Name"/>
		<xsl:variable name="outpv" select="Trigger"/>
		<xsl:variable name="o_m_b" select="OMB"/>
		<xsl:variable name="severity" select="Severity"/>
		<xsl:variable name="v_on" select="Value_On"/>
		<xsl:variable name="v_off" select="Value_Off"/>
		<xsl:variable name="spvt" select="'1'"/>
		<xsl:variable name="eqid" select="concat(substring-after($techroomname/@Name,'_'),Location,if (position()=1) then ('/PDC-T2') else ('/PDC-T1'))"/>
		<xsl:variable name="nm" select="concat($psd[@ID=$pf/PSD_ID]/@Name,'.PSDStatus')"/>
		<xsl:variable name="nmpv" select="concat($psd[@ID=$pf/PSD_ID]/@Name,'.PSDStatus')"/>
		<xsl:variable name="objname" select="concat($st/@Name,'.Alarm','.',$trigger)"></xsl:variable>
		<xsl:if test="$pf">${obj(bstrParam1=True,avalanche='N')}</xsl:if>
	</xsl:for-each>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>
<%def name="mulval(value_nm,value_Var)">
           <MultiLingualProperty name="${value_nm}">
			<xsl:for-each select="$mlv">
             <MultiLingualValue roleId="{../@RID}" localeId="{@LCID}"><xsl:value-of select="${value_Var}"/></MultiLingualValue>
			</xsl:for-each>
           </MultiLingualProperty>
</%def>
<%def name="obj(avalanche,bstrParam1=False,bstrParam2=False)">
	<xsl:variable name="Avalanche">${avalanche}</xsl:variable>
	<Object name="{$objname}" rules="update_or_create">
		<Properties>
			<Property name="Alarm_Label" dt="string"><xsl:value-of select="$alarm_lbl"/></Property>
			%if bstrParam1:
			<Property name="BstrParam1" dt="string"><xsl:value-of select="$pltform"/></Property>
			%endif
			%if bstrParam2:
			<Property name="BstrParam2" dt="string"><xsl:value-of select="$bp2"/></Property>
			%endif
			<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="$nmpv"/></Property>
			<Property name="Severity" dt="i4"><xsl:value-of select="$severity"/></Property>
			<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="$spvt"/></Property>
			<Property name="Out_PV_Trig" dt="string"><xsl:value-of select="$outpv"/></Property>	
			${mulval('EquipmentID','$eqid')}
			${mulval('OMB','$o_m_b')}
			${mulval('Name','$nm')}
			${mulval('Value_on','$v_on')}
			${mulval('Value_off','$v_off')}
			${mulval('Avalanche','$Avalanche')}
		</Properties>
	</Object>
</%def>


