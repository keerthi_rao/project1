﻿<?xml version="1.0" encoding="utf-8"?>
<!--ProjectVersion 2.02.7.8080-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" >
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
<xsl:template match="/">
<xml xmlns="x-schema:OPCMODBUSCFGschema.xml">
  <OPCMODBUSCFG OPCVariableProfile="~.!.value" DiscreteInputsFaultMode="FallBack" CoilsFaultMode="FallBack" InputRegistersFaultMode="Freeze" HoldingRegistersFaultMode="Freeze" Mode="normal">
    <Server Name="OPCMDBGTW_T281" Type="generic2" IPAddress="2.82.160.10" Port="10281" UnitIdentifier="1" ExtraParameter="" ReadPeriod="1000" WritePeriod="1000" TransactionTimeout="1000" ReconnectionPeriod="10000">
      <DiscreteInputs Size="172">
        <DataBlock Name="T281Link" Type="BOOL" Direction="Input" Size="172" StartingAddress="0" Comment="T281Link: Status of the washing machine">
          <Channel Name="PV_T281_001" Rank="1" Comment="Ready/Not Ready” status: 0-NOT READY,1-READY" />
          <Channel Name="PV_T281_002" Rank="2" Comment="No Wash Mode: 0-UNSELECTED, 1- SELECTED" />
          <Channel Name="PV_T281_003" Rank="3" Comment="" />
          <Channel Name="PV_T281_004" Rank="4" Comment="" />
          <Channel Name="PV_T281_005" Rank="5" Comment="" />
          <Channel Name="PV_T281_006" Rank="6" Comment="" />
          <Channel Name="PV_T281_007_Bit0" Rank="7" Comment="Mode Selector Keyswitch Status: 0-INVALID,1-AUTO,2-MANUAL,3-MAINTENANCE" />
		  <Channel Name="PV_T281_007_Bit1" Rank="8" Comment="Mode Selector Keyswitch Status: 0-INVALID,1-AUTO,2-MANUAL,3-MAINTENANCE" />
          <Channel Name="PV_T281_008" Rank="9" Comment="Washing /Not Washing Status: 0-NOT WASHING,1- WASHING" />
		  <Channel Name="PV_T281_009" Rank="10" Comment="Train Wash Cycle Fully Completed/ Incomplete: 0-INCOMPLETED,1-COMPLETED" />
		  <Channel Name="PV_T281_010" Rank="11" Comment="" />
		  <Channel Name="PV_T281_011" Rank="12" Comment="" />
		  <Channel Name="PV_T281_012_Bit0" Rank="13" Comment="" />
		  <Channel Name="PV_T281_012_Bit1" Rank="14" Comment="" />
		  <Channel Name="PV_T281_013" Rank="15" Comment="" />
		  <Channel Name="PV_T281_014" Rank="16" Comment="" />
		  <Channel Name="PV_T281_015" Rank="17" Comment="" />
		  <Channel Name="PV_T281_016" Rank="18" Comment="" />
		  <Channel Name="PV_T281_017" Rank="19" Comment="" />
		  <Channel Name="PV_T281_018" Rank="20" Comment="" />
		  <Channel Name="PV_T281_019" Rank="21" Comment="" />
		  <Channel Name="PV_T281_020" Rank="22" Comment="" />
		  <Channel Name="PV_T281_021" Rank="23" Comment="" />
		  <Channel Name="PV_T281_022" Rank="24" Comment="" />
		  <Channel Name="PV_T281_023" Rank="25" Comment="" />
		  <Channel Name="PV_T281_024" Rank="26" Comment="" />
		  <Channel Name="PV_T281_025" Rank="27" Comment="" />
		  <Channel Name="PV_T281_026" Rank="28" Comment="" />
		  <Channel Name="PV_T281_027" Rank="29" Comment="" />
		  <Channel Name="PV_T281_028" Rank="30" Comment="" />
		  <Channel Name="PV_T281_029" Rank="31" Comment="" />
		  <Channel Name="PV_T281_030" Rank="32" Comment="" />
		  <Channel Name="PV_T281_031" Rank="33" Comment="" />
		  <Channel Name="PV_T281_032" Rank="34" Comment="" />
		  <Channel Name="PV_T281_033" Rank="35" Comment="" />
		  <Channel Name="PV_T281_034" Rank="36" Comment="" />
		  <Channel Name="PV_T281_035" Rank="37" Comment="" />
		  <Channel Name="PV_T281_036" Rank="38" Comment="" />
		  <Channel Name="PV_T281_037" Rank="39" Comment="" />
		  <Channel Name="PV_T281_038" Rank="40" Comment="" />
		  <Channel Name="PV_T281_039" Rank="41" Comment="" />
		  <Channel Name="PV_T281_040" Rank="42" Comment="" />
		  <Channel Name="PV_T281_041" Rank="43" Comment="" />
		  <Channel Name="PV_T281_042" Rank="44" Comment="" />
		  <Channel Name="PV_T281_043" Rank="45" Comment="" />
		  <Channel Name="PV_T281_044" Rank="46" Comment="" />
		  <Channel Name="PV_T281_045" Rank="47" Comment="" />
		  <Channel Name="PV_T281_046" Rank="48" Comment="" />
		  <Channel Name="PV_T281_047" Rank="49" Comment="" />
		  <Channel Name="PV_T281_048" Rank="50" Comment="" />
		  <Channel Name="PV_T281_049" Rank="51" Comment="" />
		  <Channel Name="PV_T281_050" Rank="52" Comment="" />
		  <Channel Name="PV_T281_051" Rank="53" Comment="" />
		  <Channel Name="PV_T281_052" Rank="54" Comment="" />
		  <Channel Name="PV_T281_053" Rank="55" Comment="" />
		  <Channel Name="PV_T281_054" Rank="56" Comment="" />
		  <Channel Name="PV_T281_055" Rank="57" Comment="" />
		  <Channel Name="PV_T281_056" Rank="58" Comment="" />
		  <Channel Name="PV_T281_057" Rank="59" Comment="" />
		  <Channel Name="PV_T281_058" Rank="60" Comment="" />
		  <Channel Name="PV_T281_059" Rank="61" Comment="" />
		  <Channel Name="PV_T281_060" Rank="62" Comment="" />
		  <Channel Name="PV_T281_061" Rank="63" Comment="" />
		  <Channel Name="PV_T281_062" Rank="64" Comment="" />
		  <Channel Name="PV_T281_063" Rank="65" Comment="" />
		  <Channel Name="PV_T281_064" Rank="66" Comment="" />
		  <Channel Name="PV_T281_065" Rank="67" Comment="" />
		  <Channel Name="PV_T281_066" Rank="68" Comment="" />
		  <Channel Name="PV_T281_067" Rank="69" Comment="" />
		  <Channel Name="PV_T281_068" Rank="70" Comment="" />
		  <Channel Name="PV_T281_069" Rank="71" Comment="" />
		  <Channel Name="PV_T281_070" Rank="72" Comment="" />
		  <Channel Name="PV_T281_071" Rank="73" Comment="" />
		  <Channel Name="PV_T281_072" Rank="74" Comment="" />
		  <Channel Name="PV_T281_073" Rank="75" Comment="" />
		  <Channel Name="PV_T281_074" Rank="76" Comment="" />
		  <Channel Name="PV_T281_075" Rank="77" Comment="" />
		  <Channel Name="PV_T281_076" Rank="78" Comment="" />
		  <Channel Name="PV_T281_077" Rank="79" Comment="" />
		  <Channel Name="PV_T281_078" Rank="80" Comment="" />
		  <Channel Name="PV_T281_079" Rank="81" Comment="" />
		  <Channel Name="PV_T281_080" Rank="82" Comment="" />
		  <Channel Name="PV_T281_081" Rank="83" Comment="" />
		  <Channel Name="PV_T281_082" Rank="84" Comment="" />
		  <Channel Name="PV_T281_083" Rank="85" Comment="" />
		  <Channel Name="PV_T281_084" Rank="86" Comment="" />
		  <Channel Name="PV_T281_085" Rank="87" Comment="" />
		  <Channel Name="PV_T281_086" Rank="88" Comment="" />
		  <Channel Name="PV_T281_087" Rank="89" Comment="" />
		  <Channel Name="PV_T281_088" Rank="90" Comment="" />
		  <Channel Name="PV_T281_089" Rank="91" Comment="" />
		  <Channel Name="PV_T281_090" Rank="92" Comment="" />
		  <Channel Name="PV_T281_091" Rank="93" Comment="" />
		  <Channel Name="PV_T281_092" Rank="94" Comment="" />
		  <Channel Name="PV_T281_093" Rank="95" Comment="" />
		  <Channel Name="PV_T281_094" Rank="96" Comment="" />
		  <Channel Name="PV_T281_095" Rank="97" Comment="" />
		  <Channel Name="PV_T281_096" Rank="98" Comment="" />
		  <Channel Name="PV_T281_097" Rank="99" Comment="" />
		  <Channel Name="PV_T281_098" Rank="100" Comment="" />
		  <Channel Name="PV_T281_099" Rank="101" Comment="" />
		  <Channel Name="PV_T281_100" Rank="102" Comment="" />
		  <Channel Name="PV_T281_101" Rank="103" Comment="" />
		  <Channel Name="PV_T281_102" Rank="104" Comment="" />
		  <Channel Name="PV_T281_103" Rank="105" Comment="" />
		  <Channel Name="PV_T281_104" Rank="106" Comment="" />
		  <Channel Name="PV_T281_105" Rank="107" Comment="" />
		  <Channel Name="PV_T281_106" Rank="108" Comment="" />
		  <Channel Name="PV_T281_107" Rank="109" Comment="" />
		  <Channel Name="PV_T281_108" Rank="110" Comment="" />
		  <Channel Name="PV_T281_109" Rank="111" Comment="" />
		  <Channel Name="PV_T281_110" Rank="112" Comment="" />
		  <Channel Name="PV_T281_111" Rank="113" Comment="" />
		  <Channel Name="PV_T281_112" Rank="114" Comment="" />
		  <Channel Name="PV_T281_113" Rank="115" Comment="" />
		  <Channel Name="PV_T281_114" Rank="116" Comment="" />
		  <Channel Name="PV_T281_115" Rank="117" Comment="" />
		  <Channel Name="PV_T281_116" Rank="118" Comment="" />
		  <Channel Name="PV_T281_117" Rank="119" Comment="" />
		  <Channel Name="PV_T281_118" Rank="120" Comment="" />
		  <Channel Name="PV_T281_119" Rank="121" Comment="" />
		  <Channel Name="PV_T281_120" Rank="122" Comment="" />
		  <Channel Name="PV_T281_121" Rank="123" Comment="" />
		  <Channel Name="PV_T281_122" Rank="124" Comment="" />
		  <Channel Name="PV_T281_123" Rank="125" Comment="" />
		  <Channel Name="PV_T281_124" Rank="126" Comment="" />
		  <Channel Name="PV_T281_125" Rank="127" Comment="" />
		  <Channel Name="PV_T281_126" Rank="128" Comment="" />
		  <Channel Name="PV_T281_127" Rank="129" Comment="" />
		  <Channel Name="PV_T281_128" Rank="130" Comment="" />
		  <Channel Name="PV_T281_129" Rank="131" Comment="" />
		  <Channel Name="PV_T281_130" Rank="132" Comment="" />
		  <Channel Name="PV_T281_131" Rank="133" Comment="" />
		  <Channel Name="PV_T281_132" Rank="134" Comment="" />
		  <Channel Name="PV_T281_133" Rank="135" Comment="" />
		  <Channel Name="PV_T281_134" Rank="136" Comment="" />
		  <Channel Name="PV_T281_135" Rank="137" Comment="" />
		  <Channel Name="PV_T281_136" Rank="138" Comment="" />
		  <Channel Name="PV_T281_137" Rank="139" Comment="" />
		  <Channel Name="PV_T281_138" Rank="140" Comment="" />
		  <Channel Name="PV_T281_139" Rank="141" Comment="" />
		  <Channel Name="PV_T281_140" Rank="142" Comment="" />
		  <Channel Name="PV_T281_141" Rank="143" Comment="" />
		  <Channel Name="PV_T281_142" Rank="144" Comment="" />
		  <Channel Name="PV_T281_143" Rank="145" Comment="" />
		  <Channel Name="PV_T281_144" Rank="146" Comment="" />
		  <Channel Name="PV_T281_145" Rank="147" Comment="" />
		  <Channel Name="PV_T281_146" Rank="148" Comment="" />
		  <Channel Name="PV_T281_147" Rank="149" Comment="" />
		  <Channel Name="PV_T281_148" Rank="150" Comment="" />
		  <Channel Name="PV_T281_149" Rank="151" Comment="" />
		  <Channel Name="PV_T281_150" Rank="152" Comment="" />
		  <Channel Name="PV_T281_151" Rank="153" Comment="" />
		  <Channel Name="PV_T281_152" Rank="154" Comment="" />
		  <Channel Name="PV_T281_153" Rank="155" Comment="" />
		  <Channel Name="PV_T281_154" Rank="156" Comment="" />
		  <Channel Name="PV_T281_155" Rank="157" Comment="" />
		  <Channel Name="PV_T281_156" Rank="158" Comment="" />
		  <Channel Name="PV_T281_157" Rank="159" Comment="" />
		  <Channel Name="PV_T281_158" Rank="160" Comment="" />
		  <Channel Name="PV_T281_159" Rank="161" Comment="" />
		  <Channel Name="PV_T281_160" Rank="162" Comment="" />
		  <Channel Name="PV_T281_161" Rank="163" Comment="" />
		  <Channel Name="PV_T281_162" Rank="164" Comment="" />
		  <Channel Name="PV_T281_163" Rank="165" Comment="" />
		  <Channel Name="PV_T281_164" Rank="166" Comment="" />
		  <Channel Name="PV_T281_165" Rank="167" Comment="" />
		  <Channel Name="PV_T281_166" Rank="168" Comment="" />
		  <Channel Name="PV_T281_167" Rank="169" Comment="" />
		  <Channel Name="PV_T281_168" Rank="170" Comment="" />
		  <Channel Name="PV_T281_169" Rank="171" Comment="" />
		  <Channel Name="PV_T281_170" Rank="172" Comment="" />
        </DataBlock>
      </DiscreteInputs>
	  <Coils Size="4">
		<!-- Assume Function code 15 and not 5, because 15 => in one frame all bit, otherwise 5 =>  1 bit for each frame: 3 bits => 3 frames , it is not atomic, then dangerous
			to have fonction code 5 i guess the config should be , 1 DataBlock for each bit, to be confirmed if really required-->
			
		<DataBlock Name="T252Link Automatic TWP Operation Request" Type="BOOL" Direction="Output" Size="3" StartingAddress="0" Comment="T252Link: to send controls to the T281 washing machine">
          <Channel Name="PV_T252_001_Bit0" Rank="1" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
          <Channel Name="PV_T252_001_Bit1" Rank="2" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
          <Channel Name="PV_T252_001_Bit2" Rank="3" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
        </DataBlock>
		<DataBlock Name="T252Link Train  movement" Type="BOOL" Direction="Output" Size="1" StartingAddress="3" Comment="T252Link: to send controls to the T281 washing machine">
		  <Channel Name="PV_T252_002" Rank="1" Comment="Train  movement: 0 –stopped , 1 –moving" />
        </DataBlock>
		
		
		 <!--
		<DataBlock Name="T252Link 1" Type="BOOL" Direction="Output" Size="1" StartingAddress="0" Comment="T252Link: to send controls to the T281 washing machine">
          <Channel Name="PV_T252_001_Bit0" Rank="1" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
		</DataBlock>
		<DataBlock Name="T252Link 2" Type="BOOL" Direction="Output" Size="1" StartingAddress="1" Comment="T252Link: to send controls to the T281 washing machine">
          <Channel Name="PV_T252_001_Bit1" Rank="1" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
		</DataBlock>
		<DataBlock Name="T252Link 3" Type="BOOL" Direction="Output" Size="1" StartingAddress="2" Comment="T252Link: to send controls to the T281 washing machine">
          <Channel Name="PV_T252_001_Bit2" Rank="1" Comment="Automatic TWP Operation Request: 1 – DWR, 2 – DWD, 3 – WW, 4 – NWF, 5 – WR" />
		</DataBlock>
		
		<DataBlock Name="T252Link Train  movement" Type="BOOL" Direction="Output" Size="1" StartingAddress="3" Comment="T252Link: to send controls to the T281 washing machine">
		  <Channel Name="PV_T252_002" Rank="1" Comment="Train  movement: 0 –stopped , 1 –moving" />
        </DataBlock>
		-->
      </Coils>
    </Server>
  </OPCMODBUSCFG>
</xml>
</xsl:template>
</xsl:stylesheet>