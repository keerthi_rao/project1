<%inherit file="/Group_tem.mako"/>

<%block name="classes">	
	<xsl:variable name="sigArea" select="//Signalisation_Area[sys:cbi/@id = $CBI_ID and (Area_Type='ATS_Local_Server' or Area_Type='ATS_Central_Server')]"/>
	<xsl:variable name="Rts" select="//Signal[not(sys:sigarea/@id=$sigArea/@ID or sys:cbi/@id=$CBI_ID or @ID = //Exit_Gate/Exit_Gate_Signal_ID) and (@ID = //Route[sys:cbi/@id=$CBI_ID]/Destination_Signal_ID)]"/>
	 <Class name="Signal" rules="update" traces="error">
      <Objects>
        <xsl:apply-templates select="$Rts"/>
     </Objects>
    </Class>
    <Class name="SignalEquipment" rules="update" traces="error">
      <Objects>
        <xsl:apply-templates select="//Equipment"/>
      </Objects>
    </Class>
    <Class name="SignalHILC" rules="update" traces="error">
      <Objects>
        <xsl:apply-templates select="//Class[@name='SignalHILC']/Objects/*"/>
      </Objects>
    </Class>
</%block>

<xsl:template match="Signal"> 
        <Object name="{@Name}" rules="update" traces="error">
            <xsl:apply-templates select="." mode="prop"/>
        </Object>
</xsl:template>


<xsl:template match="Equipment|Object">
    <xsl:variable name="id" select="@sys:ID"/>
    <xsl:variable name="obj" select="//(Signal)[@ID=$id]"/>
    <xsl:variable name="cbiid" select="$obj/sys:cbi/@id"/>  
        <Object name="{@name}" rules="update" traces="error">
            <xsl:apply-templates select="$obj" mode="prop"/>
        </Object>
</xsl:template>

<xsl:template match="Signal" mode="prop"> 
          <Properties>
            <Property name="AreaGroup" dt="string">Area<xsl:if test="./sys:line/@name"><xsl:value-of select="concat('/',./sys:line[@name][1]/@name)"/></xsl:if><xsl:if test="./sys:main_depot/@name"><xsl:value-of select="concat('/',./sys:main_depot[@name][1]/@name)"/></xsl:if><xsl:if test="./sys:terr/@name"><xsl:value-of select="concat('/',./sys:terr[@name][1]/@name)"/></xsl:if><xsl:if test="./sys:geo/@name"><xsl:value-of select="concat('/',./sys:geo[@name][1]/@name)"/></xsl:if></Property>
            <Property name="FunctionGroup" dt="string">Function/Signalling/Signal/<xsl:value-of select="@Name"/></Property>
          </Properties>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="test_zc, var"/>
<%namespace file="/lib_group_tem.mako" import="create_path"/>
