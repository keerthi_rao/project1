<%inherit file="/Object_tem.mako"/>
<%! class_ = "LCClient" %>

<%block name="classes">
	<Class name="LCClient">
		<Objects>
			<xsl:apply-templates select="//ATC_Equipments/ATC_Equipment[ATC_Equipment_Type = 'LC' and @ID =$LC_ID]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="ATC_Equipment">
		<xsl:variable name="LCID" select="@ID"/>
		<xsl:variable name="nm" select="@Name"/>
		<xsl:variable name="uid" select="//Urbalis_Sector[LC_ID=$LCID]/@ID"/>
		<xsl:variable name="omsa" select="if($uid='1' or $uid='2')
											then (concat('Sector ','SEC_MDD_',$uid,':Depot ATS FEP','- LCs_',@ID ,' Total Communication Failure'))
										  else if($uid='17' or $uid='18')
											then (concat('Sector ','SEC_ECID_',$uid,':Local ATS FEP','- LCs_',@ID,' Total Communication Failure'))
										  else(concat('Sector ','SEC_',$uid,':Local ATS FEP','- LCs_',@ID,' Total Communication Failure'))"/>
		<xsl:variable name="cl01" select="if($uid='1' or $uid='2') then (concat('MDD_',$uid,'/SIG/ATS'))
											else if($uid='17' or $uid='18') then (concat('ECID_',$uid,'/SIG/ATS'))
											else (concat('TE',$uid,'/SIG/ATS'))"></xsl:variable>	
		<xsl:variable name="CustomLabel02">B</xsl:variable>													
			<Object name="{concat('LCS_',$LCID)}" rules="update_or_create">
				<Properties>
					${multilingualvalue("$omsa","OperatingModeStateAlarmLabel")}
					${multilingualvalue("$cl01","CustomLabel01")}
					${multilingualvalue("$CustomLabel02","CustomLabel02")}
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>

