<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" version="2.0" xmlns:sys="http://www.systerel.fr/Alstom/IDP" exclude-result-prefixes="sys xs fn">
  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/">
<ICONIS_KERNEL_PARAMETER_DESCRIPTION xsi:noNamespaceSchemaLocation="SyPD_Kernel.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:sys="http://www.systerel.fr/Alstom/IDP">
<RouteSettingPoints>
    <xsl:apply-templates select="//RouteSettingPoints/RouteSettingPoint"/>
</RouteSettingPoints>
</ICONIS_KERNEL_PARAMETER_DESCRIPTION>
</xsl:template>

<xsl:template match="RouteSettingPoint">
	<xsl:copy>
		<xsl:apply-templates select="@*"/>
		<xsl:apply-templates select="TrackPortions_List"/>
		<xsl:apply-templates select="sys:signal,Direction"/>
		
		<xsl:variable name="rid" select="RelatedRegulationPointID"/>
		<xsl:variable name="signal" select="//Signal[@Name = substring-after(current()/@Name,'APPROACH_')]"/>
		<xsl:variable name="isCOD" select="if (//Stopping_Area[@ID = current()/RelatedRegulationPointID and (every $oa in Original_Area_List/Original_Area/@Original_Area_Type satisfies $oa = 'Change Of Direction Area')]) then 'Yes' else 'No'"/>
		<xsl:variable name="rp_dir" select="if ($isCOD = 'Yes') then (//Change_Of_Direction_Area[@Name = substring-after(//Stopping_Area[@ID = current()/RelatedRegulationPointID]/@Name,'STA_')]/Direction_After_Change_Of_Direction) else null"/>
		
		<xsl:variable name="blocks" select="//Block[@ID = //Stopping_Area[@ID = current()/RelatedRegulationPointID]/sys:Block_ID_List/sys:Block_ID]"/>
		<xsl:variable name="saBlock" select="if($signal/Direction = 'Down') then $blocks[1] else $blocks[last()]"/>
		<xsl:variable name="dist_currsig" select="count(current()/TrackPortions_List/TrackPortion_ID[. = $saBlock/@ID]/following-sibling::*)"/>
		
		<xsl:variable name="isRP">
			<xsl:for-each select="//RouteSettingPoint[@ID != current()/@ID and RelatedRegulationPointID = current()/RelatedRegulationPointID]">
				<xsl:variable name="rsp" select="."/>
				<xsl:variable name="sgn" select="//Signal[@Name = substring-after($rsp/@Name,'APPROACH_')]"/>
				<xsl:variable name="dist_sig" select="count(current()/TrackPortions_List/TrackPortion_ID[. = $saBlock/@ID]/following-sibling::*)"/>
				<xsl:if test="($dist_sig &lt; $dist_currsig) and (($signal/Direction = $sgn/Direction) or (($signal/Direction != $sgn/Direction) and ($rp_dir != $signal/Direction) and ($rp_dir != 'Both') and (//Stopping_Area[@ID = $rid]/Direction != 'Both') ) )">False</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="cod_direction" select="//Change_Of_Direction_Area[@Name = substring-after(//Stopping_Area[@ID = current()/RelatedRegulationPointID]/@Name,'STA_')]/Direction_After_Change_Of_Direction"/>
		<xsl:variable name="cod_cond" select="if ($isCOD = 'Yes') then ( if (($signal/Direction = $cod_direction) or ($cod_direction = 'Both')) then ('Yes') else ('No') ) else 'Yes'"/>
		<xsl:if test="$isRP != 'False' and RelatedRegulationPointID and $cod_cond = 'Yes'">
			<RelatedRegulationPointID><xsl:value-of select="RelatedRegulationPointID"/></RelatedRegulationPointID>
		</xsl:if>
		
		<xsl:apply-templates select="Departure_Trigger_For_Route_Setting,Delay_For_Route_Setting"/>
	</xsl:copy>
</xsl:template>
<xsl:template match="RelatedRegulationPointID"/>
<xsl:template match="@* | node()">
	<xsl:copy>
		<xsl:apply-templates select="@* | node()"/>
	</xsl:copy>
</xsl:template>

<xsl:param name="sysdb" select="/docs/ATS_SYSTEM_PARAMETERS"/>

<xsl:include href="../../lib_common/lib_xslt.xsl"/>
</xsl:stylesheet>

<%namespace file="lib_tem_tel.mako" import="node"/>