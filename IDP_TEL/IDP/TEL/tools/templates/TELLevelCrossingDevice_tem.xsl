<%inherit file="/Object_tem.mako"/>
<%! class_ = "TELLevelCrossingDevice" %>

<%block name="classes">
	<Class name="TELLevelCrossingDevice">
	     <Objects>
		    <xsl:apply-templates select="//LX_Device[Type='Aspect Indicator' and  LX_ID=//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:cbi/@id = $CBI_ID]/@ID]/@ID]/@ID]/@ID]"/>
		 </Objects>
	</Class>
</%block>

<xsl:template match="LX_Device">
	<xsl:variable name="nm" select="@Name"/>
	<xsl:variable name="lxvar" select="//LX_Devices_Variable/LX_Device[@ID =current()/@ID]"/>
	<Object name="{@Name}.TEL" rules="update_or_create">
		  <Properties>
		  		<xsl:variable name="keys"><xsl:if test="$lxvar/LXAI and  $lxvar/LXLP">LXAI_RED;LXAI_RED_BLINKING;LXAI_WRITE;LXAI_WRITE_BLINKING</xsl:if>

				</xsl:variable>
				<Property name="ManagedKeys" dt="string"><xsl:value-of select="if (ends-with($keys,';')) then (substring($keys,1,string-length($keys)-1)) else normalize-space($keys)"/></Property>
				<Property name="Delay" dt="i4">1000</Property>
				${multilingualvalue('$nm','Name')}	
		 </Properties>
	  </Object>	
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue"/>
