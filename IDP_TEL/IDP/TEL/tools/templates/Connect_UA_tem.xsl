<%inherit file="/Object_tem.mako"/>
<%! 
	class_ = "Connect_UA"
%>
<xsl:param name="AREA_TYPE"/>
<%block name="classes">
	<Class name="Connect_UA">
		<Objects>
			<xsl:apply-templates select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID]) else ($sysdb//Signalisation_Area[@ID=$SERVER_ID])"/>
			<xsl:apply-templates select="if ($AREA_TYPE='ATS_Central_Server') then (//Signalisation_Area[@ID=//Function[@Function_Type='Level_1']/@Signalisation_Area_ID and not(contains(Localisation_Type,'Depot'))]) else ($sysdb//Signalisation_Area[@ID=$SERVER_ID])" mode="scu"/>
		</Objects>
	</Class>
</%block>
<xsl:template match="Signalisation_Area">
	<xsl:variable name="sid" select="@ID"/>
	<xsl:variable name="fep_sig" select="sys:sigarea/@id[.=//Signalisation_Area[Area_Type='ATS']/@ID]"/>
	<xsl:variable name="atseq" select="//ATS_Equipment[ATS_Equipment_Type='FEP' and (*/Signalisation_Area_ID=$fep_sig or Signalisation_Area_ID=$fep_sig)]/@Name"/>
	<Object name="Connect_SNMP_{@ID}" rules="update_or_create">
		<Properties>
			${prop('Server1Url','opc.tcp://<xsl:value-of select="$atseq[1]"/>:50088')}
			${prop('Server2Url','opc.tcp://<xsl:value-of select="$atseq[2]"/>:50088')}
			${prop('SOI_OPCClient','OPCClient_SNMP_<xsl:value-of select="$sid"/>')}
			${prop('SOI_OPCGroup','OPCGroup_SNMP_<xsl:value-of select="$sid"/>')}
			${prop('SharedOnIdentifier','Connect_SNMP_<xsl:value-of select="$sid"/>')}
			${prop('SharedOnIdentifierDefinition','True','boolean')}
		</Properties>
	</Object>
</xsl:template>
<xsl:template match="Signalisation_Area" mode="scu">
	<xsl:variable name="sid" select="@ID"/>
	<xsl:variable name="fep_sig" select="sys:sigarea/@id[.=//Signalisation_Area[Area_Type='ATS']/@ID]"/>
	<xsl:variable name="atseq" select="//ATS_Equipment[ATS_Equipment_Type='FEP' and (*/Signalisation_Area_ID=$fep_sig or Signalisation_Area_ID=$fep_sig)]/@Name"/>
	<Object name="Connect_SNMP_Webprotocol_{@ID}" rules="update_or_create">
		<Properties>
			${prop('Server1Url','opc.tcp://<xsl:value-of select="$atseq[1]"/>:50089')}
			${prop('Server2Url','opc.tcp://<xsl:value-of select="$atseq[2]"/>:50089')}
			${prop('SOI_OPCClient','OPCClient_SNMP_Webprotocol_<xsl:value-of select="$sid"/>')}
			${prop('SOI_OPCGroup','OPCGroup_SNMP_Webprotocol_<xsl:value-of select="$sid"/>')}
			${prop('SharedOnIdentifier','Connect_SNMP_Webprotocol_<xsl:value-of select="$sid"/>')}
			${prop('SharedOnIdentifierDefinition','True','boolean')}
		</Properties>
	</Object>
</xsl:template>
## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="prop"/>