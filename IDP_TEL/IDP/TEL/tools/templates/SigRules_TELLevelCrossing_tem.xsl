<%inherit file="/SigRules_tem.mako"/>
<%block name="main">
		<xsl:apply-templates select="//LX[LX_Basic_Multitrack_Zone_ID_List/LX_Basic_Multitrack_Zone_ID=//Basic_Multitrack_Zone[Elementary_Zone_ID_List/Elementary_Zone_ID=//Elementary_Zone[Elementary_Zone_Block_ID_List/Block_ID=//Block[sys:cbi/@id = $CBI_ID]/@ID]/@ID]/@ID]"/>
</%block>

<xsl:template match="LX">
<xsl:variable name="nm" select="@Name"/>
<xsl:variable name="lxvar" select="$sysdb//LXs_Variable/LX[@Name=current()/@Name]"/>
<xsl:variable name="gavar" select="$sysdb//Generic_ATS_IOs_Variable/Generic_ATS_IO[@Name=//Generic_ATS_IO/@Name]"/>
<xsl:variable name="vv1" select="//Generic_ATS_IOs/Generic_ATS_IO[@Name=replace($nm,substring-before($nm, '_'),'GAIO_LXGB')]/@Name"/>
<xsl:variable name="gio" select="replace($nm,substring-before($nm,'_'),'GATSM_GAIO_LXGB')"/>

<%
	tt='<xsl:value-of select="@Name"/>'
%>
<%
	vv='<xsl:value-of select="$gio"/>'
%>	
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="Gate">
			<States>
				<xsl:if test="$lxvar/LXGC">
					<Equation>LX_TEL_GATE_NOT_CLOSED[0]::LXGC_${tt}==0</Equation>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$lxvar/LXGC and $gavar/GATSM and $vv1">
						<Equation>LX_TEL_GATE_CLOSED[1]::LXGC_${tt}==1  &amp;&amp; ${vv}==0</Equation>		
					</xsl:when>
					<xsl:when test="$lxvar/LXGC">
						<Equation>LX_TEL_GATE_CLOSED[1]::LXGC_${tt}==1</Equation>		
					</xsl:when>
				</xsl:choose>	
				<xsl:if test="$lxvar/LXGC and $gavar/GATSM and $vv1"><Equation>LX_TEL_GATE_OVERRIDDEN[2]::${vv}==1</Equation></xsl:if>
			</States>
</Equipment>	 
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="Position">
			<States>
			<xsl:if test="$lxvar/LXPO and $lxvar/LXGC">
				<Equation>LX_TEL_POSITION_RELEASE_REQUEST[0]::LXGC_${tt}==1 &amp;&amp; LXPO_${tt}==1</Equation>
				<Equation>LX_TEL_POSITION_OPENED[1]::LXGC_${tt}==0 &amp;&amp; LXPO_${tt}==1</Equation>
			</xsl:if>
			<xsl:if test="$lxvar/LXPC and $lxvar/LXGC">
				<Equation>LX_TEL_POSITION_CLOSED[2]::LXPC_${tt}==1 &amp;&amp; LXGC_${tt}==1</Equation>
				<Equation>LX_TEL_POSITION_LOCK_REQUEST[3]::LXGC_${tt}==0 &amp;&amp; LXPC_${tt}==1</Equation>
			</xsl:if>
			</States>
</Equipment>
<xsl:if test="$lxvar/LXSTP">	 
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="SafeToProceedStatus">
			<States>
				<Equation>LX_TEL_NOT_ACTIVATED[0]::LXSTP_${tt}==0</Equation>
				<Equation>LX_TEL_ACTIVATED[1]::LXSTP_${tt}==1</Equation>
			</States>
</Equipment>
</xsl:if>
<xsl:if test="$lxvar/LXS"><xsl:if test="$lxvar/LXRS">
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="ResumableStatus">
			<States>
				<Equation>LX_TEL_NOT_ATSRESUMABLE[0]::LXS_${tt}==1 || LXRS_${tt}==0</Equation>
				<Equation>LX_TEL_ATSRESUMABLE[1]::LXS_${tt}==0 &amp;&amp; LXRS_${tt}==1</Equation>
			</States>
</Equipment>
</xsl:if></xsl:if>
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="RedStatus">
			<States>
			<xsl:if test="$lxvar/LXPC and $lxvar/LXGC">
				<Equation>LX_TEL_CLOSED_LOCK[0]::LXPC_${tt}==1 &amp;&amp; LXGC_${tt}==1</Equation>
			</xsl:if>
			<xsl:if test="$lxvar/LXPO and $lxvar/LXSTP">
				<Equation>LX_TEL_UNBLOCK_REQUESTED[1]::LXPO_${tt}==1 &amp;&amp; LXSTP_${tt}==1</Equation>
			</xsl:if>
			<xsl:if test="$lxvar/LXGC">
				<Equation>LX_TEL_NOT_CLOSED_LOCKED[2]::LXGC_${tt}==0</Equation>
		    </xsl:if>
			</States>
</Equipment>
<xsl:if test="$lxvar/LXS">
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="LXStatus">
			<States>
				<Equation>LX_TEL_NOT_CLOSED[0]::LXS_${tt}==0</Equation>
				<Equation>LX_TEL_CLOSED[1]::LXS_${tt}==1</Equation>
			</States>
</Equipment>
</xsl:if>
<xsl:if test="$lxvar/LXF">
<Equipment name="{@Name}.TEL" type="TELLevelCrossing" flavor="FireAlarm">
			<States>
				<Equation>LX_TEL_FIREALARM_ACTIVATED[0]::LXF_${tt}==0</Equation>
				<Equation>LX_TEL_FIREALARM_NOT_ACTIVATED[1]::LXS_${tt}==1</Equation>
			</States>
</Equipment>
</xsl:if>
</xsl:template>
