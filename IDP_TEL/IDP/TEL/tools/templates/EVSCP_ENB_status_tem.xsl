<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KEvents_Merge" %>

<%block name="classes">
	<Class name="S2KEvents_Merge">
		<Objects>
				<xsl:apply-templates select="$sysdb//Generic_ATS_IO[ATS_Signalisation_Area_ID=//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID]/sys:sigarea/@id and contains(@Name,'ENB') and not(contains(@Name,'NOT_ENB'))]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Generic_ATS_IO">
			<xsl:variable name="TTName" select="substring-after(@Name,'GAIO_LXGB_')"/>
			<xsl:variable name="objname" select="concat(@Name,'.EventBypass')"/>
			<xsl:variable name="uname" select="concat(substring-after(//Signalisation_Area[@ID=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID and sys:sigarea/@id = current()/ATS_Signalisation_Area_ID][1]/sys:terr[1]/@name[1],'_'),'/SIG/CBI')"/>
			<Object name="{$objname}" rules="update_or_create">
				<Properties>
					<Property name="Alarm_Label" dt="string">Backup EVSCP status</Property>			
					${prop('AlarmInhib','1','boolean')}			
					<Property name="BstrParam1" dt="string">-</Property>							
					${multilingualvalue('$TTName','Name')}						
					${multilingualvalue('$uname','EquipmentID')}	
					${multilingualvalue("'-'",'MMS')}						
					<Property name="Name_PV_Trig" dt="string"><xsl:value-of select="//IO_Mapping[contains(Variable_Name,current()/@Name)][1]/Variable_Name"/></Property>				
					<Property name="Out_PV_Trig" dt="string">Value</Property>	
					<Property name="State_PV_Trig" dt="i4">1</Property>	
					<Property name="Operator" dt="string"><xsl:value-of select="//Signalisation_Area[@ID=current()/ATS_Signalisation_Area_ID][1]/sys:terr/@name"/>.TAS</Property>			
			    	${multilingualvalue("'O'",'OMB')}					
			    	<Property name="Severity" dt="i4">-</Property>		
					${multilingualvalue("'-'",'Value')}					
				</Properties>
			</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
