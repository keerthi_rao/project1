<%inherit file="/Object_tem.mako"/>
<%! class_ = "PSDStatus" %>

<%block name="classes">
  	<Class name="PSDStatus">
  		<Objects>
  			<!-- Generate the items of Stopping_Area table -->
  			<xsl:apply-templates select="$sysdb//Platform_Screen_Doors[sys:sigarea/@id=$SERVER_ID]"/>
  		</Objects>
  	</Class>
</%block>

<!-- Template to match the Platform table -->
<xsl:template match="Platform_Screen_Doors">
	<xsl:variable name="sta" select="concat(@Name,'.PSDStatus')"/>
	<xsl:variable name="stai" select="@Name"/>
	<xsl:variable name="pltform" select="//Platform[PSD_ID=current()/@ID]"/>
	<xsl:variable name="PSDPlatformInfoObj" select="//Stopping_Area[Original_Area_List/Original_Area[@ID = $pltform/@ID and @Original_Area_Type = 'Platform']]/@Name"/>
	<xsl:variable name="PSDPlatformInfoDoor" select="//Platform[PSD_ID=current()/@ID]/@Name"/>
	<Object name="{$sta}" rules="update_or_create">
		<Properties>
			<!-- MAKO to generate properties -->
			${props([('ID', 'string' ,'<xsl:value-of select="$sta"/>'),
					 ('SharedOnIdentifier', 'string', '<xsl:value-of select="$sta"/>'),('SharedOnIdentifierDefinition', 'boolean', '1')])}
			${multilingualvalue("$stai")} 
			${multilingualvalue("$PSDPlatformInfoObj[1]", "PSDPlatformInfoObj")}
			${multilingualvalue("$PSDPlatformInfoDoor","PSDPlatformInfoDoor")}
		</Properties>
	</Object>
</xsl:template>

## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props"/>