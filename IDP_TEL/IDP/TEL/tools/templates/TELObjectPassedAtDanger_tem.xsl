<%inherit file="/Object_tem.mako"/>
<%! class_ = "TELObjectPassedAtDanger" %>

<%block name="classes">
	<Class name="TELObjectPassedAtDanger">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signal[sys:sigarea/@id=//Function[@ID=$FUNCTION_ID]/@Signalisation_Area_ID and not(starts-with(@Name,'VS'))]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Signal">
	<Object name="{@Name}.OPAD" rules="update_or_create">
		<Properties>
			<Property name="Name" dt="string"><xsl:value-of select="@Name"/>.OPAD</Property>
			<Property name="TrackingObjectRef" dt="string"><xsl:value-of select="//Secondary_Detection_Device[@ID=current()/Secondary_Detection_Device_ID]/@Name"/>.TrackingObject</Property>	
		</Properties>
	</Object>
</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>

