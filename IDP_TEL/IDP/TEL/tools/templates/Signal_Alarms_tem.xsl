<%inherit file="/Object_tem.mako"/>
<%! class_ = "S2KAlarms_Gen" %>

<%block name="classes">
	<Class name="S2KAlarms_Gen">
		<Objects>
			<xsl:apply-templates select="$sysdb//Signal[sys:cbi/@id=//Signalisation_Area[@ID=$SERVER_ID]/sys:cbi/@id]"/>	
		</Objects>
	</Class>
</%block>	
	
	
<xsl:template match="Signal">
			<xsl:variable name="TTName" select="@Name"/>
			<xsl:variable name="objname" select="concat(@Name,'.Alarms_Gen')"/>
			<xsl:variable name="terrname" select= "sys:terr/@name"/>
			<xsl:variable name="uname" select="concat(substring-after(sys:terr/@name,'_'),'/SIG/CBI/',@Name)"/>
			<xsl:if test="//Signalisation_Area[@ID=current()/sys:sigarea/@id]/Localisation_Type='Depot'">
				${obj('{$objname}',[('LPSPER','Yellow Signal %s1 : 50% or more LEDs Failed','0')])}
			</xsl:if>
			<xsl:if test="//Signalisation_Area[@ID=current()/sys:sigarea/@id]/Localisation_Type='Depot'">
				${obj('{$objname}',[
				   ('LPSPER','Red Signal %s1 : 50% or more LEDs Failed','0'),
				   ('LPSPER','White Signal %s1 : 50% or more LEDs Failed','0')	 
				   ])}
			</xsl:if>
			

</xsl:template>


## ********************************************** -->
##               PYTHON PART                      -->
## ********************************************** -->
<%namespace file="/lib_tem.mako" import="multilingualvalue, props, prop"/>
<%def name="obj(nm,list)">
	% for key, description,stpv in list:
		<Object name="${nm}" rules="update_or_create">
		<xsl:variable name="mulval" select="'${description}'"/>
			<Properties>
				<Property name="Alarm_Label" dt="string"><xsl:value-of select="$mulval"/></Property>			
			<!-- 	${prop('AlarmInhib','1','boolean')}			 -->
				${multilingualvalue("'-'",'Avalanche')}			
				<Property name="BstrParam1" dt="string"><xsl:value-of select="$TTName"/></Property>			
				${multilingualvalue('$TTName','Name')}			
				${multilingualvalue('$uname','EquipmentID')}			
				${multilingualvalue("'-'",'MMS')}		
				<Property name="Name_PV_Trig" dt="string">${key}<xsl:value-of select="concat('_',$TTName)"/></Property>
				<Property name="Out_PV_Trig" dt="string">Value</Property>
				<Property name="State_PV_Trig" dt="i4"><xsl:value-of select="${stpv}"/></Property>			
				<Property name="Operator" dt="string"><xsl:value-of select="$terrname"/>.TAS</Property>			
				${multilingualvalue("'M'",'OMB')}			
				<Property name="Severity" dt="i4">2</Property>		
				${multilingualvalue("'-'",'Value')}
			</Properties>
		</Object>
	% endfor
</%def>
