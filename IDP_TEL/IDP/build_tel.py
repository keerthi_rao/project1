#!/usr/bin/env python2
# -*- coding: utf8 -*-

from lib_common.fabricate import *  # @UnusedWildImport
import lib_common.build_gen  # @UnusedImport
from operator import contains
lib_common.build_gen.base_dir = 'TEL/'
from lib_common.build_gen import *  # @UnusedWildImport
import stat
import yaml

setup(depsname='.deps_TEL')

def all():  # @ReservedAssignment
    remove_read_only()
    import_input_files(authorized_file_regex = ['^(ATS_UOCAT2).*','^(ATS_SUBSYSTEM_DATABASE)_.*', '^(ATS_PARAMETER_DESCRIPTION)_.*', '^(ATS_TMS)_.*','(.*_DATA_INTERFACE).*','^(ATS_PARAMETER_CUSTOM).*'])
    after()
    prepare_input(lib_common_templates=[ 'Stopping_Area_blocks','group_info_tel', 'variables', 'LocalRoles', 'Inter_Stopping_Area_blocks'],
           tables_files=['ATS_SUBSYSTEM_DATABASE.xml', 'ATS_PARAMETER_DESCRIPTION.xml', 'ATS_TMS.xml', 'ATS_PARAMETER_CUSTOM.xml','ATS_UOCAT2.xml'],
           information_add = [('group_info_tel', []),('Stopping_Area_blocks', []),('Inter_Stopping_Area_blocks', []),
                              ('variables', ['../../lib_common/variable_map.xml'] + get_data_interface_files())])
    after()
    run('python','lib_common/copy_file.py', "lib_common/lib_xslt.xsl", base_dir+"bin_tools/")
    after()
    run_xslt('inputs/' + 'Input_Interface_Variables.xml', 'bin_tools/variables_interface.xsl', ['../bin/group_info_tel.xml', '../../lib_common/Variable_map_2.xml'] + get_data_interface_files())
    after()
    run_xslt_LIB('bin/int/' + 'split_variables.xml', 'spliter.xsl', ['Input_Interface_Variables.xml'])
    after()
    # Generate versions files
    gen_versions_file()
    after()
    # Create list of handled equipments	
    run_xslt('bin/Equipments.yml', 'tools/extract_equipments.xsl', ['ATSs.xml', 'Urbalis_Sectors.xml', 'Signalisation_Areas.xml', 'ATS_Equipments.xml','ATS_Test_Equipments.xml', 'CBIs.xml', 'ZCs.xml', 'Projects.xml', 'DeploymentConfiguration.xml', 'Functions.xml','ATC_Equipments.xml'])
    after()
    # Transform XML files and run python scripts
    print_action("Transform XML files and run python scripts")
    run_xslt('bin/LocalesRoles.xml', 'bin_tools/LocalRoles.xsl', ['Projects.xml']) 
    after()
   #for sigrule_RouteSettingObject Generation -BEGIN
    mkdir(base_dir+'bin/separate', isdir=True)
    after()
    gr_tp1 = 'trackportion_1'
    gr_tp2 = 'trackportion_2'
    run_xslt('bin/TrackPortions.xmlX', 'bin_tools/TrackPortions_pass.xsl', ['Blocks.xml', 'TI_Distributions.xml', 'Static_Dark_Zones.xml', 'Signalisation_Areas.xml', 'ZCs.xml'], [], group=gr_tp1)
    run('python', 'lib_common/inc_x.py', base_dir + 'bin/TrackPortions.xmlX', base_dir + 'bin/TrackPortions_pass.xml', 'XY', '10001', group=gr_tp2, after=gr_tp1)
    run(cmd_xsl, '-o:'+base_dir+"bin/separate/TrackPortions.xml", base_dir+"bin/TrackPortions_pass.xml" , "lib_common/clean_sys.xsl", after=gr_tp2)  
    after()
    gr_regpoint1 = 'regpoint_1'
    run_xslt('bin/RegulationPoints_pass.xml', 'bin_tools/RegulationPoints_pass.xsl', ['TrackPortions_pass.xml', 'Stations.xml', 'Stopping_Areas.xml', 'Platforms.xml', 'Stablings_Location.xml', 'Transfer_Tracks.xml', 'Washing_Zones.xml', 'Isolation_Areas.xml', 'Coupling_Uncoupling_Areas.xml', 'Change_Of_Direction_Areas.xml','Depots.xml'], [], group=gr_regpoint1)
    run_xslt('bin/separate/RegulationPoints.xml', 'bin_tools/RegulationPoints.xsl', ['../bin/RegulationPoints_pass.xml'], [], after=gr_regpoint1)
    gr_routepoint1 = 'routepoint_1'
    gr_routepoint2 = 'routepoint_2'
    after()
    run_xslt('bin/RouteSettingPoints_pass.xml', 'bin_tools/RouteSettingPoints.xsl', ['Change_Of_Direction_Areas.xml','Signals.xml', 'Cross_Triggers.xml', 'Blocks.xml', 'TrackPortions_pass.xml', '../bin/separate/RegulationPoints.xml', 'Stopping_Areas.xml', 'Platforms.xml'], [], group=gr_routepoint1)
    run_xslt('bin/RouteSettingPoints_pass1.xml', 'bin_tools/RouteSettingPoints_pass.xsl', ['Change_Of_Direction_Areas.xml','Blocks.xml','Stopping_Areas.xml','Signals.xml','../bin/RouteSettingPoints_pass.xml','../bin/TrackPortions_pass.xml', '../bin/separate/RegulationPoints.xml'], [],group= gr_routepoint2, after=gr_routepoint1)
    run(cmd_xsl, '-o:'+base_dir+"bin/separate/RouteSettingPoints.xml","TEL/bin/RouteSettingPoints_pass1.xml" , "lib_common/clean_sys.xsl", after=gr_routepoint2)    
    after()
    run_xslt('bin/RouteSettingObjects.xml', 'bin_tools/RouteSettingObjects.xsl', ['Functions.xml','Routes.xml', 'Blocks.xml', 'Mainlines.xml', 'Depots.xml', 'Signals.xml', 'Signalisation_Areas.xml', 'Cross_Triggers.xml', 'Specific_Routes.xml', 'System_Constraints_Constants.xml', 'Tracks.xml', '../bin/TrackPortions_pass.xml', '../bin/RouteSettingPoints_pass.xml','../bin/RegulationPoints_pass.xml'], [])  
    after()
    #for sigrule_RouteSettingObject Generation -END
    #Extract Router Information          
    sync_point = 'gen_router'
    run_xslt('bin/router_info.yml', 'tools/router_info.xsl', ['ATSs.xml','DCS_Equipments.xml'], group=sync_point)
    run('python', base_dir + 'tools/router_info.py', base_dir + 'bin/router_info.yml', base_dir + 'bin/router_info.xml', after=sync_point) 

    
    # Load yaml equipments file
    eqt_file = file(base_dir + "bin/Equipments.yml")
    eqt_file_data = yaml.load(eqt_file)
    after()
    run_xslt('bin/Stopping_Areas.xml', 'bin_tools/Stoppoint.xsl', ["Stopping_Areas.xml"])
    after()
    run_xslt_LIB('bin_tools/alias.xsl', 'gen_alias.xsl', ['Alias_Rules.xml', 'Alias_Fields.xml'])

    locales_roles = "../bin/LocalesRoles.xml"
    # Copy static files    
    copy_static_files()
    output_xslt('ISCSDat/Common/TrackPortionToTrackID.xml', 'bin_tools/TrackPortionToTrackID.xsl', ['Blocks.xml','Projects.xml'])
    output_xslt('IDC/TEL/HMITrainRSMCommand.xml', 'bin_tools/HMITrainRSMCommand.xsl', ['Trains_Unit.xml', '../bin/LocalesRoles.xml']) 
    output_xslt('IDC/TEL/SigRules_HMITrainRSMCommand.xml', 'bin_tools/SigRules_HMITrainRSMCommand.xsl', ['Trains_Unit.xml', '../bin/LocalesRoles.xml']) 
    #output_xslt('IDC/TEL/HMITrainAlarmMonitored.xml', 'bin_tools/HMITrainAlarmMonitored.xsl', ['HMITrainAlarmMonitored_Loco.xml','Trains_Unit_Characteristics.xml','HMITrainAlarmMonitored.xml', 'Trains_Unit.xml','../bin/LocalesRoles.xml']) 
    #output_xslt('IDC/Group/Group_HMITrainAlarmMonitored.xml', 'bin_tools/Group_HMITrainAlarmMonitored.xsl', ['Trains_Unit_Characteristics.xml','HMITrainAlarmMonitored_Loco.xml','HMITrainAlarmMonitored.xml', 'Trains_Unit.xml','../bin/LocalesRoles.xml']) 
    output_xslt('IDC/TEL/HMITrainAttributes.xml', 'bin_tools/HMITrainAttributes.xsl', ['Trains_Unit.xml','HMITrainAttributes.xml','../bin/LocalesRoles.xml'])
    output_xslt('IDC/TEL/CCM_Parameter.xml', 'bin_tools/CCM_Parameter.xsl', ['Trains_Unit.xml','../bin/LocalesRoles.xml'])
    output_xslt('IPName/IDP_IPName.xml','bin_tools/IDP_IPName.xsl',['ATS_Equipments.xml','ATSs.xml','Lines.xml','Signalisation_Areas.xml','Technical_Rooms.xml','Urbalis_Sectors.xml','External_Master_Clocks.xml','Projects.xml','DCS_Equipments.xml','../bin/LocalesRoles.xml','../bin/router_info.xml'])
    output_xslt('IDC/TEL/AlarmCustomLabel.xml', 'bin_tools/AlarmCustomLabel.xsl', ['../bin/LocalesRoles.xml'])    
    output_xslt('IDC/Group/S2KUser.xml', 'bin_tools/S2KUser.xsl', ['Projects.xml','../bin/LocalesRoles.xml'])
    output_xslt('ICM/Common/StationsTranslation.xml','bin_tools/StationsTranslation.xsl',['Stations.xml','Stopping_Areas.xml'])
    output_xslt('MMSDat/Common/MMSEqptMapping.xml','bin_tools/MMSEqptMapping.xsl',['HMITrainAlarmMonitored.xml','Trains_Unit.xml'])

    for urbalisid in eqt_file_data["URBALIS_SECTORS"]:
        output_xslt('IDC/TEL/TSRAppliedTimeout_%s.xml'%urbalisid["ID"], 'bin_tools/TSRAppliedTimeout.xsl', ['Blocks.xml','Urbalis_Sectors.xml'],['URBALIS_ID=%s' % urbalisid["ID"]])
	
    for server_id in eqt_file_data["Function"]:
        if not('FEP' in server_id["LEVEL"]):
            output_xslt('IDC/TEL/PSDTimeSyncModule_%s.xml' % (server_id["ID"]), 'bin_tools/PSDTimeSyncPV.xsl', ['Signalisation_Areas.xml','Platforms_Screen_Doors.xml','PIMPoints.xml','PIMStations.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
            output_xslt('IDC/TEL/SigRules_PSDTimeSyncModule_%s.xml' %(server_id["ID"]), 'bin_tools/SigRules_PSDTimeSyncModule.xsl', ['Platforms_Screen_Doors.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]]) 
            output_xslt('IDC/TEL/ISCSStation_%s.xml' % (server_id["ID"]), 'bin_tools/ISCSStation.xsl', ['ISCSStations.xml','Geographical_Areas.xml','Signalisation_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' % levels(server_id["LEVEL"])])
            output_xslt('IDC/TEL/BlockParams_%s.xml' % (server_id["ID"]), 'bin_tools/BlockParams.xsl', ['Blocks.xml','Urbalis_Sectors.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
            output_xslt('IDC/TEL/PointParams_%s.xml' % (server_id["ID"]), 'bin_tools/PointParams.xsl', ['Points.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
            output_xslt('IDC/TEL/AHMLastStopLink_%s.xml' % (server_id["ID"]), 'bin_tools/AHMLastStopLink.xsl', ['../bin/Stopping_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
            output_xslt('IDC/TEL/TTISStationLink_%s.xml' % (server_id["ID"]), 'bin_tools/TTISStationLink.xsl', ['../bin/Stopping_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
            output_xslt('IDC/TEL/ATSSupervisionTEL_%s.xml' % (server_id["ID"]), 'bin_tools/ATSSupervisionTEL.xsl', ['Signalisation_Areas.xml', 'ATS_Equipments.xml', '../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'SERVER_TYPE=%s' % server_id["AREA_TYPE"],'FUNCTION_ID=%s' % server_id["ID"]])
            output_xslt('IDC/TEL/SigRules_ATSSupervisionTEL_%s.xml' % (server_id["ID"]), 'bin_tools/SigRules_ATSSupervisionTEL.xsl', ['Signalisation_Areas.xml', 'ATS_Equipments.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'SERVER_TYPE=%s' % server_id["AREA_TYPE"]])
         
        for server_level in server_id["LEVEL"]:
            if 'FEP' in server_level:
                output_xslt('IDC/TEL/PSDOPCServer_%s.xml' % (server_id["ID"]), 'bin_tools/PSDOPCServer.xsl', ['Platforms_Screen_Doors.xml', 'Signalisation_Areas.xml', 'ATS_Equipments.xml', 'Functions.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'FUNCTION_ID=%s' %server_id["ID"]])
                output_xslt('IDC/TEL/MergeOPCUAConnectedXY_%s.xml'%(server_id["ID"]),'bin_tools/MergeOPCUAConnectedXY.xsl',['Functions.xml','Signalisation_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                if 'LV1' in server_id["AssocaitedLevel"]:
                    output_xslt('IDC/TEL/S2KOPCUAClient_%s.xml' %(server_id["ID"]), 'bin_tools/OPCClient_SW_RT1_Status.xsl',['DCSAlarm.xml','ATS_Equipments.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                
            else:
                is_both_level = 'NO'
                if len(server_id["LEVEL"]) == 2:
                    is_both_level = 'YES'
                output_xslt('IDC/TEL/GenericAreaTEL_%s%s.xml'%(server_id["ID"],levels(server_level)),'bin_tools/GenericAreaTEL.xsl',['Blocks.xml','Signalisation_Areas.xml','PIMPoints.xml','PIMPlatforms.xml','ISCSStations.xml','Geographical_Areas.xml','EmergencyPointAreas.xml','RegulationPointAreas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"], 'SERVER_LEVEL=%s'%levels(server_level)])
                output_xslt('IDC/TEL/TPBerthTrainIndicator_%s.xml'%(server_id["ID"]), 'bin_tools/TPBerthTrainIndicator.xsl', ['Blocks.xml', 'Signalisation_Areas.xml', '../bin/LocalesRoles.xml' ], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level)])
                output_xslt('IDC/TEL/OPCClient_TEL_External_%s%s.xml'%(server_id["ID"],levels(server_level)),'bin_tools/OPCCLient_TEL_External.xsl',['Signalisation_Areas.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level)])
                output_xslt('IDC/TEL/Links_Status_EXT_%s.xml'  % (server_id["ID"]), 'bin_tools/Links_Status_EXT.xsl',['Signalisation_Areas.xml','ZCs.xml','CBIs.xml','ATC_Equipments.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level)])
                output_xslt('IDC/TEL/Links_Status_%s.xml'  % (server_id["ID"]), 'bin_tools/Links_Status.xsl',['Functions.xml','ATS_Equipments.xml','Signalisation_Areas.xml','ZCs.xml','CBIs.xml','ATC_Equipments.xml','Platforms_Screen_Doors.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],['SERVER_LEVEL=%s' %levels(server_level)],['FUNCTION_ID=%s' %server_id["ID"]]])
                output_xslt('IDC/TEL/ParametersKB_%s%s.xml'  % (server_id["ID"],levels(server_level)), 'bin_tools/ParametersKB.xsl',['Projects.xml'])
                output_xslt('IDC/Group/Group_S2KAlarms_Gen_%s.xml'  % (server_id["ID"]), 'bin_tools/Group_S2KAlarm_Gen.xsl',['Depots.xml','Mainlines.xml','Tunnels.xml','PSD_Operational.xml','MSS_CC.xml','MSS_SMIO.xml','ATC_Equipments.xml','CBIs.xml','Platforms.xml','PSD_Maintenance.xml','PSDDoor_Maintenance.xml','PSDDoor_Operational.xml','SPKS_Actuators.xml','Points.xml','MSS_Label.xml','MSS_lruState.xml','MSS_PDC.xml','MSS_SCU.xml','Adjacent_Monitoring_Areas.xml','ATS_Equipments.xml','Urbalis_Sectors.xml','Tracks.xml','Trains_Unit.xml','Blocks.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','LXs.xml','LX_Devices.xml','Signal_Lamp_Types.xml','Aspect_Indicators.xml','Direction_Indicators.xml','Platforms_Screen_Doors.xml','Signals.xml','Generic_ATS_IOs.xml','Secondary_Detection_Devices.xml','Geographical_Areas.xml','Territories.xml','SPKSs.xml','Signalisation_Areas.xml','Functions.xml','Platforms_Screen_Doors_Characteristics.xml','Stations.xml','Secondary_Detection_Devices_Characteristics.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level),'FUNCTION_ID=%s' % server_id["ID"]])
                output_xslt('IDC/Group/Group_S2KEvents_Merge_%s.xml'  % (server_id["ID"]), 'bin_tools/Group_S2KEvents_Merge.xsl',['Generic_ATS_IOs.xml','LX_Devices.xml','LXs.xml','Blocks.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Functions.xml','CBIs.xml','GAMA_Zones.xml','Events.xml','Signalisation_Areas.xml','Trains_Unit.xml','Traffic_Lockings.xml','Routes.xml','Points.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level),'FUNCTION_ID=%s' % server_id["ID"]])
                
		if "Server" in server_id["AREA_TYPE"]:
                    output_xslt('IDC/LL2CS_TEL_Group_%s.xml' %server_id["ID"], 'bin_tools/LL2CS_Group.xsl', [], []) 
                    comp_tem(base_dir+'tools/bat_file/RunAutomaticIDCTEL.bat', base_dir+'output/IDC/RunAutomaticIDCTEL_%s%s.bat'%(server_id["ID"],levels(server_level)), {'SERVER_LEVEL':levels(server_level),'SERVER_ID':server_id["ID"],'PROJECT':eqt_file_data['Project']["Name"], 'COUNT':len(server_id["LEVEL"])})
                    #output_xslt('IDC/IDCTEL_ATS_%s%s.xml'%(server_id["ID"],levels(server_level)), 'bin_tools/IDCTEL.xsl', ['ATS_Equipments.xml','ZCs.xml','Projects.xml', 'Signalisation_Areas.xml','Urbalis_Sectors.xml','CBIs.xml','Functions.xml','Blocks.xml' ], ['SIG_SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'SERVER_ID=%s' % server_id["ID"],'SERVER_LEVEL=%s' %levels(server_level),'IS_BOTH_LEVEL=%s' %is_both_level])
                if server_level != 'NA' :
                    output_xslt('IDC/ParametersTEL_%s%s.xml'%(server_id["ID"],levels(server_level)), 'bin_tools/ParametersTEL.xsl', ['Train_Washings.xml','Stopping_Areas.xml','Terminus_Managements.xml','Lines.xml','Projects.xml','Signalisation_Areas.xml','Urbalis_Sectors.xml' ], ['SERVER_ID=%s' % server_id["ID"], 'SERVER_LEVEL=%s' %levels(server_level),'SERVER_NAME=%s' %server_id["NAME"]])
                    output_xslt('IDC/TEL/Train_Event%s.xml' %(levels(server_level)), 'bin_tools/Train_Event.xsl', ['Events.xml','Trains_Unit.xml','../bin/LocalesRoles.xml'],[ 'SERVER_LEVEL=%s' %levels(server_level)])
                    output_xslt('IDC/TEL/General_Events_%s.xml'  % (server_id["ID"]), 'bin_tools/General_Events.xsl', ['Events.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'SERVER_LEVEL=%s' %levels(server_level)])
                     
                if levels(server_level) == '_LV2':
                    output_xslt('IDC/TEL/PSD_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/PSD_Alarms.xsl', ['ATSs.xml','Signalisation_Areas.xml','Functions.xml','ATS_Equipments.xml','Platforms_Screen_Doors.xml','Technical_Rooms.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"],'SERVER_LEVEL=%s' %levels(server_level)])
                    output_xslt('IDC/TEL/ATRMissed_%s.xml' % (server_id["ID"]), 'bin_tools/ATRMissed.xsl', ['../bin/Stopping_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SAHMEmerStopPointLink_%s.xml'%server_id["ID"], 'bin_tools/SAHMEmerStopPointLink.xsl', ['Stopping_Areas.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SAHMRegPoint_%s.xml'%server_id["ID"], 'bin_tools/SAHMRegPoint.xsl', ['Signalisation_Areas.xml','Monitoring_Trains_Zones.xml', 'Blocks.xml','Inter_Stopping_Areas.xml','Stopping_Areas.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/AISSetterPoint_%s.xml'%server_id["ID"], 'bin_tools/AISSetterPoint.xsl', ['Routes.xml','Signals.xml','Aspect_Indicators.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/TELLV2_%s.xml'%server_id["ID"],'bin_tools/TELLV2.xsl',['Signalisation_Areas.xml','ISCSStations.xml','ATS_Equipments.xml','ISCSCentrals.xml','PIMStations.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/OPCClient_TEL_SRV_%s_LV2.xml'%server_id["ID"],'bin_tools/OPCClient_TEL_SRV.xsl',['ATS_Equipments.xml','Signalisation_Areas.xml','Geographical_Areas.xml','Blocks.xml','PIMPlatforms.xml','PIMPoints.xml','ISCSStations.xml','EmergencyPointAreas.xml','Functions.xml'],['FUNCTION_ID=%s' % server_id["ID"],'SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'SERVER_LEVEL=%s' %levels(server_level),'IS_BOTH_LEVEL=%s' %(is_both_level)])
                    output_xslt('IDC/TEL/EVACSGlobalHold_%s.xml'%server_id["ID"], 'bin_tools/EVACSGlobalHold.xsl', ['Mainlines.xml','Stopping_Areas.xml','Evacuation_Zones_Sec.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])                    
                    output_xslt('IDC/TEL/SkipStatus_%s.xml'%server_id["ID"], 'bin_tools/SkipStatus.xsl', ['Trains_Unit.xml','../bin/LocalesRoles.xml'])
                    output_xslt('IDC/TEL/SkipPFStatus_%s.xml'%server_id["ID"], 'bin_tools/SkipPFStatus.xsl', ['Stopping_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('TELSM/Common/SleepingZones_%s.xml'%server_id["ID"], 'bin_tools/SleepingZones.xsl', ['Stopping_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/AISStatusAspect_%s.xml'%server_id["ID"], 'bin_tools/AISStatusAspect.xsl', ['Routes.xml','Signals.xml','Aspect_Indicators.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/GenericAreaPropertyBagTEL_%s%s.xml'%(server_id["ID"],levels(server_level)),'bin_tools/GenericAreaPropertyBagTEL.xsl',['SAHMPlatforms.xml','Signalisation_Areas.xml','Stopping_Areas.xml','Blocks.xml','Inter_Stopping_Areas.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"], 'SERVER_LEVEL=%s'%levels(server_level),'FUNCTION_ID=%s'%server_id["ID"]])   
                    output_xslt('TTIS/Common/TTISStationCode_%s.xml'%(server_id["ID"]),'bin_tools/TTISStationCode.xsl',['Platforms.xml','Stopping_Areas.xml','Projects.xml', 'Stations.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])   
		    output_xslt('IDC/TEL/Platform_Stopping_Area_%s.xml'%(server_id["ID"]),'bin_tools/Platform_Stopping_Area.xsl',['Platforms_Screen_Doors.xml','Platforms.xml','Stopping_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])   
 		    output_xslt('IDC/TEL/Platform_PSD_%s.xml'%(server_id["ID"]),'bin_tools/Platform_PSD.xsl',['Platforms.xml','Platforms_Screen_Doors.xml','Stopping_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])   
                    output_xslt('PIMDat/Common/PIMPlatforms_%s.xml'%(server_id["ID"]), 'bin_tools/PIMPlatforms.xsl', ['PIMPlatforms.xml','Projects.xml', 'Blocks.xml', 'Signalisation_Areas.xml','PIMStations.xml','Platforms.xml','Stopping_Areas.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/PIMMonitor_%s.xml'%server_id["ID"],'bin_tools/PIMMonitor.xsl',['PIMPlatforms.xml','PIMStations.xml','PIMMonitors.xml','Stopping_Areas.xml','Platforms.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/Sigrules_RouteSettingObject_%s.xml'%(server_id["ID"]),'bin_tools/Sigrules_RouteSettingObject.xsl',['TWPRoutes.xml','Routes.xml','Signalisation_Areas.xml','../bin/RouteSettingObjects.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/HoldSkipPointTEL_%s.xml'%(server_id["ID"]),'bin_tools/HoldSkipPoint.xsl',['Signalisation_Areas.xml','../bin/Stopping_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    #output_xslt('IDC/TEL/MergeOPCUAConnectedXY_%s.xml'%(server_id["ID"]),'bin_tools/MergeOPCUAConnectedXY.xsl',['ATS_Equipments.xml','Functions.xml','Signalisation_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/Alarm_PSD_%s.xml'%(server_id["ID"]),'bin_tools/Alarm_PSD.xsl',['PSD_Operational.xml','PSD_Maintenance.xml','PSDDoor_Maintenance.xml','Technical_Rooms.xml','Platforms.xml','PSDDoor_Operational.xml','Signalisation_Areas.xml','Platforms_Screen_Doors_Characteristics.xml','Platforms_Screen_Doors.xml','Stations.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])   
                    output_xslt('IDC/TEL/PSDStatus_%s.xml' % (server_id["ID"]), 'bin_tools/PSDStatus.xsl', ['Platforms_Screen_Doors.xml','Stopping_Areas.xml','Platforms.xml', '../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SigRules_PSDStatus_%s.xml' %(server_id["ID"]), 'bin_tools/SigRules_PSDStatus.xsl', ['Platforms_Screen_Doors.xml','Platforms.xml','Stopping_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]]) 
                    output_xslt('IDC/TEL/PSDDoorStatus_%s.xml' % (server_id["ID"]), 'bin_tools/PSDDoorStatus.xsl', ['Platforms_Screen_Doors.xml', '../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SigRules_PSDDoorStatus_%s.xml' %(server_id["ID"]), 'bin_tools/SigRules_PSDDoorStatus.xsl', ['Platforms_Screen_Doors.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]]) 
                    output_xslt('IDC/TEL/HoldOverrideSkip_%s.xml'%(server_id["ID"]),'bin_tools/HoldOverrideSkip.xsl',['Signalisation_Areas.xml','Stopping_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                   
                else:
                    output_xslt('IDC/TEL/TELLV1_%s.xml'%server_id["ID"],'bin_tools/TELLV1.xsl',['Signalisation_Areas.xml','Projects.xml','ISCSStations.xml','ATS_Equipments.xml','ISCSCentrals.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                if levels(server_level) == '_LV1':
                    output_xslt('IDC/TEL/MMSDat_%s.xml'%server_id["ID"], 'bin_tools/MMSDat.xsl', ['Trains_Unit.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/HoldJustif_%s.xml'%server_id["ID"], 'bin_tools/HoldJustif.xsl', ['Trains_Unit.xml','../bin/LocalesRoles.xml'])
                    output_xslt('IDC/TEL/HMIStringInformation_%s.xml'%server_id["ID"], 'bin_tools/HMIStringInformation.xsl', ['Generic_ATS_IOs.xml','Signal_Lamp_Types.xml','LX_Devices.xml','LXs.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Transfer_Tracks.xml','TT_Stabling_Unstabling_Orders.xml','Non_UEVOL_Function_Zones.xml','CBI_Actuators.xml','Secondary_Detection_Devices.xml','Evacuation_Zones_Req.xml','Evacuation_Zones_Sec.xml','Blocks.xml','Reduction_Speeds.xml' ,'Signalisation_Areas.xml', 'Points.xml', 'Emergency_Stop_Areas.xml', 'Cycles.xml', 'Signals.xml', 'Sub_Routes.xml', 'Routes.xml', 'SPKSs.xml', 'GAMA_Zones.xml', 'ZCs.xml', 'ATSs.xml', 'ATC_Equipments.xml', 'Switchs.xml', 'CBIs.xml', 'Stopping_Areas.xml', 'Calculated_Work_Zone_Boundarys.xml', 'Kilometric_Counter_Detectors.xml', 'Trains_Unit.xml', 'Elementarys_GAMA.xml', 'TI_Distributions.xml', 'Exit_Gates.xml', 'Exit_Gates.xml', 'Depots.xml', 'Mainlines.xml', 'Stations.xml', 'Platforms.xml', 'SDD_Groups_In_Operation.xml', '../bin/LocalesRoles.xml','../tools/IconisHMI.HeadcodeDestination.xml'], ['SERVER_ID=%s'%server_id["Signalisation_Area_ID"], 'SERVER_LEVEL=%s' %levels(server_level)])
                    output_xslt('IDC/TEL/CalcHMITrainAttributes.xml', 'bin_tools/CalcTrainTemp.xsl', ['Trains_Unit.xml','ComputedTrainAttributes.xml'])
                    output_xslt('IDC/TEL/SPKSSummary_%s.xml'%server_id["ID"], 'bin_tools/SPKSSummary.xsl', ['Platforms.xml','Stopping_Areas.xml','SPKSs.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SigRules_SPKSSummary_%s.xml'%server_id["ID"], 'bin_tools/SigRules_SPKSSummary.xsl', ['Platforms.xml', 'System_Constraints_Constants.xml', 'Stopping_Areas.xml', 'SPKSs.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('AIS/Common/AspectIndicatorConf.xml', 'bin_tools/AspectIndicatorConf.xsl', ['Stopping_Areas.xml','Stablings_Location.xml'],[])
                    output_xslt('IDC/TEL/RSTOMSettingRequest.xml', 'bin_tools/RSTOMSettingRequest.xsl', ['Trains_Unit.xml'],[])
                    output_xslt('IDC/TEL/HMITrainPropertyBag_%s.xml'%server_id["ID"], 'bin_tools/HMITrainPropertyBag.xsl', ['Trains_Unit.xml'], [])
                    output_xslt('ISCSDat/Common/StationBoundaryPoint_%s.xml' % (server_id["ID"]), 'bin_tools/StationBoundaryPoint.xsl', ['ISCSStations.xml','Projects.xml','Geographical_Areas.xml','Signalisation_Areas.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/HMIStablingLocation_%s.xml' % (server_id["ID"]), 'bin_tools/HMIStablingLocation.xsl', ['Secondary_Detection_Devices.xml','Blocks.xml','Stopping_Areas.xml','Stablings_Location.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/TagNoteManager_%s.xml' % (server_id["ID"]), 'bin_tools/TagNoteManager.xsl', ['Elementary_Zones.xml','Basic_Multitrack_Zones.xml','LXs.xml','Evacuation_Zones_Req.xml','Evacuation_Zones_Sec.xml','Trains_Unit.xml','GAMA_Zones.xml','Signals.xml','Points.xml','Evacuation_Zones_Req.xml','Blocks.xml','Secondary_Detection_Devices.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/PointOut_%s.xml' % (server_id["ID"]), 'bin_tools/PointOut.xsl', ['Tunnels.xml','Points.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/SW_RT_Status_%s.xml' % (server_id["ID"]), 'bin_tools/SW_RT_Status.xsl', ['Technical_Rooms.xml','Signalisation_Areas.xml','MSS_Label.xml','MSS_lruState.xml','MSS_PDC.xml','CBIs.xml','Trains_Unit.xml','MSS_CC.xml','ATS_Equipments.xml','MSS_SMIO.xml','../bin/LocalesRoles.xml','Functions.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"],'AREA_TYPE=%s'% server_id["AREA_TYPE"]])    
                    output_xslt('IDC/TEL/FEP_Status_%s.xml' % (server_id["ID"]), 'bin_tools/FEP_Status.xsl', ['Functions.xml','Signalisation_Areas.xml','ATS_Equipments.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                   # output_xslt('IDC/TEL/S2KIOOPCUAClient_%s.xml' %(server_id["ID"]), 'bin_tools/OPCClient_SW_RT1_Status.xsl',['ATS_Equipments.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                    #output_xslt('IDC/TEL/Train_Event.xml', 'bin_tools/Train_Event.xsl', ['Events.xml','Trains_Unit.xml','../bin/LocalesRoles.xml'],[])
                    output_xslt('IDC/TEL/TAS_%s_Alarms.xml'%(server_id["ID"]),'bin_tools/TAS_Alarms.xsl',['Urbalis_Sectors.xml','Blocks.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"], 'SERVER_LEVEL=%s'%levels(server_level)])
                    output_xslt('IDC/TEL/OPCClient_TEL_IntraServer_%s.xml'%(server_id["ID"]),'bin_tools/OPCClient_TEL_IntraServer.xsl',['Functions.xml','Signalisation_Areas.xml','ATS_Equipments.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/S2KGALRedundantPing_%s.xml'%(server_id["ID"]),'bin_tools/S2KGALRedundantPing.xsl',['ATS_Equipments.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/TrainRestrictionAreaTEL_%s.xml'%server_id["ID"],'bin_tools/TrainRestrictionArea.xsl',['Blocks.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'%server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/MovementConditionsTEL_%s.xml'%(server_id["ID"]), 'bin_tools/MovementConditionsTEL.xsl',['Driving_Contexts.xml','Tracks_Container.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/LX_Alarm_Side_%s.xml'%(server_id["ID"]), 'bin_tools/LX_Alarm_Side.xsl',['Signalisation_Areas.xml','Signal_Lamp_Types.xml','LX_Devices.xml','LXs.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Blocks.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/U400Equipement_%s_TEL.xml'%(server_id["ID"]), 'bin_tools/LXLampSide.xsl',['Signalisation_Areas.xml','Signal_Lamp_Types.xml','LX_Devices.xml','LXs.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Blocks.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/SigRules_LXLampSide_%s.xml'%(server_id["ID"]), 'bin_tools/SigRules_LXLampSide.xsl',['Signalisation_Areas.xml','Signal_Lamp_Types.xml','LX_Devices.xml','LXs.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Blocks.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/EVSCP_ENB_status_%s.xml'%(server_id["ID"]), 'bin_tools/EVSCP_ENB_status.xsl',['IOs_Mapping.xml','Functions.xml','Urbalis_Sectors.xml','Generic_ATS_IOs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/EVSCP_NOT_ENB_status_%s.xml'%(server_id["ID"]), 'bin_tools/EVSCP_NOT_ENB_status.xsl',['IOs_Mapping.xml','Functions.xml','Urbalis_Sectors.xml','Generic_ATS_IOs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/TELObjectPassedAtDanger_%s.xml'%(server_id["ID"]), 'bin_tools/TELObjectPassedAtDanger.xsl',['Secondary_Detection_Devices.xml','Signals.xml','Functions.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/TrackingObjectAE_%s.xml'%(server_id["ID"]), 'bin_tools/TrackingObjectAE.xsl',['Secondary_Detection_Devices.xml','Functions.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    output_xslt('IDC/TEL/Surge_Arrestor_%s.xml'%(server_id["ID"]), 'bin_tools/Surge_Arrestor.xsl',['IOs_Mapping.xml','Functions.xml','Urbalis_Sectors.xml','Generic_ATS_IOs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s'%server_id["ID"]])
                    #output_xslt('IDC/TEL/S2KEvents_Merge_Alarms_%s.xml' % (server_id["ID"]), 'bin_tools/S2KEvents_Merge_Alarms.xsl', ['HMITrainAlarmMonitored_Loco.xml','Trains_Unit_Characteristics.xml','HMITrainAlarmMonitored.xml', 'Trains_Unit.xml','../bin/LocalesRoles.xml'])                     
                    output_xslt('IDC/TEL/TC_Temperature_%s.xml' % (server_id["ID"]), 'bin_tools/TC_Temperature.xsl', ['Functions.xml','Generic_ATS_IOs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['FUNCTION_ID=%s' % server_id["ID"]])                     
                    output_xslt('IDC/TEL/Alarm_MS_%s.xml' % (server_id["ID"]), 'bin_tools/Alarm_MS.xsl', ['Functions.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','MSS_Label.xml','MSS_lruState.xml','MSS_PDC.xml','CBIs.xml','ATS_Equipments.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"],'AREA_TYPE=%s'%server_id["AREA_TYPE"]])                     
                    output_xslt('IDC/TEL/TOMTTDepotMgr_%s.xml' % (server_id["ID"]), 'bin_tools/TOMTTDepotMgr.xsl', ['Signalisation_Areas.xml','ATS_Equipments.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/Alarm_SUP_%s.xml' % (server_id["ID"]), 'bin_tools/Alarm_SUP.xsl', ['ATS_Equipments.xml','Signalisation_Areas.xml','Urbalis_Sectors.xml','Technical_Rooms.xml','../tools/ATS_ALARM_OPE_LATS_TE03_B_Conf.xml','../tools/ATS_ALARM_OPE_LATS_TE03_A_Conf.xml','../tools/ATS_ALARM_OPE_LATS_TE02_B_Conf.xml','../tools/ATS_ALARM_OPE_LATS_TE02_A_Conf.xml','../tools/ATS_ALARM_OPE_DATS_B_Conf.xml','../tools/ATS_ALARM_OPE_DATS_A_Conf.xml','../tools/ATS_ALARM_OPE_CATS_B_Conf.xml','../tools/ATS_ALARM_OPE_CATS_A_Conf.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/AspectIndicator_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/AspectIndicator.xsl', ['Aspect_Indicators.xml','Direction_Indicators.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    #output_xslt('IDC/TEL/Signal_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/Signal_Alarms.xsl', ['Urbalis_Sectors.xml','Signals.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])
                    output_xslt('IDC/TEL/TrackSection_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/TrackSection_Alarm.xsl', ['Secondary_Detection_Devices_Characteristics.xml','Tunnels.xml','Secondary_Detection_Devices.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    #output_xslt('IDC/TEL/PSD_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/PSD_Alarms.xsl', ['ATSs.xml','Signalisation_Areas.xml','Functions.xml','ATS_Equipments.xml','Platforms_Screen_Doors.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/SPKS_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/SPKS_Alarms.xsl', ['Technical_Rooms.xml','Blocks.xml','Tracks.xml','SPKSs.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/Lamp_Alarm_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/Lamp_Alarm.xsl', ['Secondary_Detection_Devices.xml','Tunnels.xml','Blocks.xml','Elementary_Zones.xml','LX_Devices.xml','LXs.xml','Trains_Unit.xml','Basic_Multitrack_Zones.xml','Tracks.xml','Signals.xml','Signal_Lamp_Types.xml','Aspect_Indicators.xml','Direction_Indicators.xml','Technical_Rooms.xml','Mainlines.xml','Depots.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/Level_Crossing_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/Level_Crossing_Alarm.xsl', ['Blocks.xml','Elementary_Zones.xml','LX_Devices.xml','LXs.xml','Basic_Multitrack_Zones.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/Level_Crossing_%s_Events.xml'%(server_id["ID"]),'bin_tools/Level_Crossing_Events.xsl',['LX_Devices.xml','Technical_Rooms.xml','Secondary_Detection_Devices.xml','Events.xml','Tracks.xml','LXs.xml','Basic_Multitrack_Zones.xml','Elementary_Zones.xml','Blocks.xml','Functions.xml','Urbalis_Sectors.xml','Generic_ATS_IOs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/HMITrainMaxDwellTimeActivation.xml', 'bin_tools/HMITrainMaxDwellTimeActivation.xsl', ['Trains_Unit.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/SigRules_MaxDwellTimeActivation.xml', 'bin_tools/SigRules_MaxDwellTimeActivation.xsl', ['Trains_Unit.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])                     
                    output_xslt('IDC/TEL/OPCClient_TEL_MS_Server_%s.xml'%(server_id["ID"]),'bin_tools/OPCClient_TEL_MS_Server.xsl',['ATS_Test_Equipments.xml','Functions.xml','Signalisation_Areas.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/MS_Variable_%s.xml'%(server_id["ID"]),'bin_tools/MS_Variable.xsl',['ATS_Test_Equipments.xml','Functions.xml','Signalisation_Areas.xml','Urbalis_Sectors.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' % server_id["ID"]])
                    output_xslt('IDC/TEL/FEP_MS_Links_Alarms_%s.xml' %(server_id["ID"]), 'bin_tools/FEP_MS_Links_Alarms.xsl',['ATS_Test_Equipments.xml','ATS_Equipments.xml','Signalisation_Areas.xml','Functions.xml','Urbalis_Sectors.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/ServerLinks_%s.xml' %(server_id["ID"]), 'bin_tools/ServerLinks.xsl',['../../lib_common/Alarms_TEL.xml','Urbalis_Sectors.xml','ATS_Equipments.xml','Signalisation_Areas.xml','Functions.xml','Technical_Rooms.xml','../bin/LocalesRoles.xml','Adjacent_Monitoring_Areas.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/MergeOPCConnected12Y_%s.xml' %(server_id["ID"]), 'bin_tools/MergeOPCDAConnected12Y.xsl',['Signalisation_Areas.xml','Functions.xml','../bin/LocalesRoles.xml','Adjacent_Monitoring_Areas.xml', 'CBIs.xml'],['SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/Server_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/Server_Alarms.xsl', ['Signalisation_Areas.xml','Technical_Rooms.xml','ATS_Equipments.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'AREA_TYPE=%s'%server_id["AREA_TYPE"]])
                    output_xslt('IDC/TEL/Equipment_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/Equipment_Alarms.xsl', ['ATC_Equipments.xml','Technical_Rooms.xml','ATSs.xml','CBIs.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/MS_SMIO_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/MS_SMIO_Alarms.xsl',['MSS_SMIO.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
                    output_xslt('IDC/TEL/CBI_Points_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/CBI_Points_Alarms.xsl',['Tunnels.xml','ATS_Equipments.xml','Points.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
                    output_xslt('IDC/TEL/MS_Server_Network_FAILURE_%s.xml' %(server_id["ID"]),'bin_tools/MS_Server_Network_FAILURE.xsl',['Signalisation_Areas.xml','MSS_lruState.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
                    output_xslt('IDC/TEL/Train_Alarm.xml','bin_tools/Train_Alarms.xsl',['Trains_Unit.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
                    output_xslt('IDC/TEL/SPKS_Inconsistent_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/SPKS_Inconsistent.xsl',['Technical_Rooms.xml','SPKS_Actuators.xml','SPKSs.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
                    output_xslt('IDC/TEL/NMS_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/NMS_Alarms.xsl',['Urbalis_Sectors.xml','Signalisation_Areas.xml','ATS_Equipments.xml','DCSAlarm.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])                                                                                                                                       
		    output_xslt('IDC/TEL/OPCAnalog_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/OPCAnalog.xsl',['MSSAlarm.xml','Signals.xml','../bin/LocalesRoles.xml'],['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"]])
		    output_xslt('IDC/TEL/Connect_UA_%s.xml' %(server_id["ID"]),'bin_tools/Connect_UA.xsl',['Functions.xml','ATS_Equipments.xml','Signalisation_Areas.xml'],['FUNCTION_ID=%s' %server_id["ID"],'SERVER_ID=%s'  %server_id["Signalisation_Area_ID"], 'AREA_TYPE=%s'%server_id["AREA_TYPE"]])
		    output_xslt('IDC/TEL/MS_CC_%s_Alarms.xml' %(server_id["ID"]),'bin_tools/MS_CC_Alarms.xsl',['Trains_Unit.xml','MSS_CC.xml','../bin/LocalesRoles.xml'],[])
		    output_xslt('IDC/TEL/LinksAB_%s_Alarms.xml' % (server_id["ID"]), 'bin_tools/LinksAB_Alarm.xsl', ['ATC_Equipments.xml','Technical_Rooms.xml','ATSs.xml','CBIs.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])		               
                    output_xslt('IDC/TEL/RateOfChange_%s.xml' % (server_id["ID"]), 'bin_tools/RateOfChange.xsl', ['ATC_Equipments.xml','Technical_Rooms.xml','ATSs.xml','CBIs.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])
                    output_xslt('IDC/TEL/S2KOPCUAClient_%s.xml' % (server_id["ID"]), 'bin_tools/S2KOPCUAClient.xsl', ['ATS_Equipments.xml','ATSs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['SERVER_ID=%s'  %server_id["Signalisation_Area_ID"],'FUNCTION_ID=%s' %server_id["ID"]])

                if "Central_Server" in server_id["AREA_TYPE"]:
                    output_xslt('IDC/TEL/PIMLATSStatus_%s.xml' % (server_id["ID"]), 'bin_tools/PIMLATSStatus.xsl', ['Signalisation_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])
                if "Local_Server" in server_id["AREA_TYPE"]:
                    output_xslt('IDC/TEL/CATSStatus_%s.xml' % (server_id["ID"]), 'bin_tools/CATStatus.xsl', ['Signalisation_Areas.xml'], ['SERVER_ID=%s' % server_id["Signalisation_Area_ID"]])

    deployment_file_generated=False
    for zc_id in eqt_file_data["ZC"]:
        output_xslt('IDC/TEL/EvacuationZoneSecCause_%s.xml' %(zc_id["ID"]), 'bin_tools/EvacuationZoneSecCause.xsl', ['Evacuation_Zones_Sec.xml','Basic_Multitrack_Zones.xml','Elementary_Zones.xml', 'Blocks.xml', '../bin/LocalesRoles.xml'], ['ZC_ID=%s' %zc_id["ID"]])
        output_xslt('IDC/TEL/SigRules_EvacuationZoneSecCause_%s.xml' %(zc_id["ID"]), 'bin_tools/SigRules_EvacuationZoneSecCause.xsl', ['SPKSs.xml','Evacuation_Zones_Sec.xml','Basic_Multitrack_Zones.xml','Elementary_Zones.xml', 'Blocks.xml', '../bin/LocalesRoles.xml'], ['ZC_ID=%s' %zc_id["ID"]])
        #output_xslt('IDC/TEL/ZCClient_%s.xml' % (zc_id["ID"]), 'bin_tools/ZCClient.xsl', ['ATC_Equipments.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','ZCs.xml','../bin/LocalesRoles.xml'], ['ZC_ID=%s'  %zc_id["ID"]])
        output_xslt('IDC/TEL/SDDGroup_%s.xml' % (zc_id["ID"]), 'bin_tools/SDDGroup.xsl', ['ATC_Equipments.xml','SDD_Groups_In_Operation.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','ZCs.xml','../bin/LocalesRoles.xml'], ['ZC_ID=%s'  %zc_id["ID"]])
        #output_xslt('IDC/Group/Group_ZCClient_%s.xml' % zc_id["ID"], 'bin_tools/Group_ZCClient.xsl', ['ZCs.xml'], ['ZC_ID=%s' % zc_id["ID"]])
        output_xslt('IDC/TEL/Sector_%s.xml' % (zc_id["ID"]), 'bin_tools/Sector.xsl', ['ZCs.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['ZC_ID=%s'  %zc_id["ID"]])
        output_xslt('IDC/TEL/EvacuationZoneRequest_Event_%s.xml' % (zc_id["ID"]), 'bin_tools/EvacuationZoneRequest_Event.xsl', ['Evacuation_Zones_Req.xml','../bin/LocalesRoles.xml'],['ZC_ID=%s' % zc_id["ID"]])      
        output_xslt('IDC/TEL/GAMA_Zone_%s_Events.xml' % (zc_id["ID"]), 'bin_tools/GAMA_Zone_Events.xsl', ['Secondary_Detection_Devices.xml','Technical_Rooms.xml','Tracks.xml','Blocks.xml','Elementary_Zones.xml','Basic_Multitrack_Zones.xml','Events.xml','GAMA_Zones.xml','../bin/LocalesRoles.xml'],['ZC_ID=%s' % zc_id["ID"]])      
                              
    for cbi_id in eqt_file_data["CBI"]:
        output_xslt('IDC/TEL/Route_%s_Events.xml' % (cbi_id["ID"]), 'bin_tools/Route.xsl', ['Technical_Rooms.xml','Signals.xml','Blocks.xml','Tracks.xml','Events.xml','Routes.xml','CBIs.xml','../bin/LocalesRoles.xml'], ['CBI_ID=%s'  %cbi_id["ID"]])
        #output_xslt('IDC/TEL/Interlocking_%s.xml' % (cbi_id["ID"]), 'bin_tools/Interlocking.xsl', ['ATC_Equipments.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','CBIs.xml','../bin/LocalesRoles.xml'], ['CBI_ID=%s'  %cbi_id["ID"]])
        output_xslt('IDC/TEL/Signal_%s.xml' % (cbi_id["ID"]), 'bin_tools/Signal.xsl', ['Urbalis_Sectors.xml','Signals.xml','CBIs.xml','../bin/LocalesRoles.xml'], ['CBI_ID=%s'  %cbi_id["ID"]])
        output_xslt('IDC/TEL/Point_%s_Events.xml' % (cbi_id["ID"]), 'bin_tools/Point.xsl', ['Tunnels.xml','Tracks.xml','Events.xml','Urbalis_Sectors.xml','Points.xml','CBIs.xml','../bin/LocalesRoles.xml'], ['CBI_ID=%s'  %cbi_id["ID"]])
        output_xslt('IDC/TEL/SPKS_%s.xml' % (cbi_id["ID"]), 'bin_tools/SPKS.xsl', ['Urbalis_Sectors.xml','SPKSs.xml','CBIs.xml','../bin/LocalesRoles.xml'], ['CBI_ID=%s'  %cbi_id["ID"]])
        #output_xslt('IDC/Group/Group_Interlocking_%s.xml' % cbi_id["ID"], 'bin_tools/Group_Interlocking.xsl', ['CBIs.xml'], ['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/SigRules_Interlocking_%s.xml'%cbi_id["ID"], 'bin_tools/SigRules_Interlocking.xsl', ['CBIs.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/TELLevelCrossing_%s.xml'%cbi_id["ID"], 'bin_tools/TELLevelCrossing.xsl', ['Generic_ATS_IOs.xml','Elementary_Zones.xml','LXs.xml','Basic_Multitrack_Zones.xml','Blocks.xml','CBIs.xml','../bin/LocalesRoles.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/SigRules_TELLevelCrossing_%s.xml'%cbi_id["ID"], 'bin_tools/SigRules_TELLevelCrossing.xsl', ['../bin/int/Generic_ATS_IOs_Variable.xml','../bin/int/LXs_Variable.xml','Generic_ATS_IOs.xml','Elementary_Zones.xml','LXs.xml','Basic_Multitrack_Zones.xml','Blocks.xml','CBIs.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/TELLevelCrossingDevice_%s.xml'%cbi_id["ID"], 'bin_tools/TELLevelCrossingDevice.xsl', ['../bin/int/LX_Devices_Variable.xml','LX_Devices.xml','Elementary_Zones.xml','LXs.xml','Basic_Multitrack_Zones.xml','Blocks.xml','../bin/LocalesRoles.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/SigRules_TELLevelCrossingDevice_%s.xml'%cbi_id["ID"], 'bin_tools/SigRules_TELLevelCrossingDevice.xsl', ['../bin/int/LX_Devices_Variable.xml','LX_Devices.xml','Elementary_Zones.xml','LXs.xml','Basic_Multitrack_Zones.xml','Blocks.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/TrafficLocking_%s_Events.xml'%cbi_id["ID"], 'bin_tools/Traffic_Locking_Event.xsl', ['Technical_Rooms.xml','Tracks.xml','Events.xml','Signals.xml','Traffic_Lockings.xml','../bin/LocalesRoles.xml'],['CBI_ID=%s' % cbi_id["ID"]])
        output_xslt('IDC/TEL/CBI_Sector_%s_Events.xml'%cbi_id["ID"], 'bin_tools/CBI_Sector_Events.xsl', ['Technical_Rooms.xml','Blocks.xml','Tracks.xml','Urbalis_Sectors.xml','CBIs.xml','Events.xml','../bin/LocalesRoles.xml'],['CBI_ID=%s' % cbi_id["ID"]])

        after()  
        output_xslt('IDC/DLC/Interlocking_Adjacent_%s.xml' % cbi_id["ID"], 'bin_tools/Interlocking_Adjacent.xsl', ['Urbalis_Sectors.xml','Direction_Indicators.xml','Aspect_Indicators.xml','Projects.xml','Switch_Local_Control_Keys.xml','../bin/int/Switch_Local_Control_Keys_Variable.xml','Generic_ATS_IOs.xml','../bin/int/Generic_ATS_IOs_Variable.xml','CBI_Routes_Cycles_Control_Keys.xml','../bin/int/CBI_Routes_Cycles_Control_Keys_Variable.xml','Isolation_Areas.xml','../bin/int/Isolation_Areas_Variable.xml','Remotes_IO.xml','../bin/int/Remotes_IO_Variable.xml','Platforms_Screen_Doors.xml','../bin/int/Platforms_Screen_Doors_Variable.xml','VMMIs.xml','../bin/int/VMMIs_Variable.xml','Secondary_Detection_Devices.xml','../bin/int/Secondary_Detection_Devices_Variable.xml','SPKSs.xml','../bin/int/SPKSs_Variable.xml','Overlaps.xml','../bin/int/Overlaps_Variable.xml','Points.xml','../bin/int/Points_Variable.xml','Give_Take_Isolation_Zones.xml','../bin/int/Give_Take_Isolation_Zones_Variable.xml','Sub_Routes.xml','../bin/int/Sub_Routes_Variable.xml','Signals.xml','../bin/int/Signals_Variable.xml','Routes.xml','../bin/int/Routes_Variable.xml','Signalisation_Areas.xml','../bin/int/Signalisation_Areas_Variable.xml','ATS_Equipments.xml','../bin/int/ATS_Equipments_Variable.xml','Switchs.xml','../bin/int/Switchs_Variable.xml','Cycles.xml','../bin/int/Cycles_Variable.xml','CBIs.xml','../../lib_common/Variable_map_2.xml','../bin/int/CBIs_Variable.xml','Blocks.xml','../bin/int/Blocks_Variable.xml','../output/IDC/LocalesRoles.xml','ESPs.xml' ,'../bin/int/ESPs_Variable.xml','CBI_Actuators.xml','../bin/int/CBI_Actuators_Variable.xml','SPKS_Actuators.xml', '../bin/int/SPKS_Actuators_Variable.xml','../bin/int/Aspect_Indicators_Variable.xml','../bin/int/Direction_Indicators_Variable.xml','Traffic_Lockings.xml','../bin/int/Traffic_Lockings_Variable.xml'], ['CBI_ID=%s' % cbi_id["ID"]])
        gr_sigrules_signal_pass1 = 'sigrules_signal_pass1_%s' % cbi_id["ID"]
        run_xslt('bin/IDC/SigRule/SigRules_Signal_pass1_%s.xml' % cbi_id["ID"], 'bin_tools/SigRules_Signal_pass1.xsl', ['Signals.xml', 'Routes.xml', 'Signal_Lamp_Types.xml', 'Specific_Routes.xml'],
                     group=gr_sigrules_signal_pass1)
        after()
        gr_sigrules_signal = 'sigrules_signal_%s'%cbi_id["ID"]
        run_xslt2('IDC/DLC/SigRules_Signal_Adjacent_%s.xml' % cbi_id["ID"], 'bin_tools/SigRules_Signal_Adjacent.xsl', ['../bin/IDC/SigRule/SigRules_Signal_pass1_%s.xml' % cbi_id["ID"], 'Exit_Gates.xml', 'Urbalis_Sectors.xml' , 'Routes.xml', 'Signalisation_Areas.xml'], ['CBI_ID=%s' % cbi_id["ID"]],
                      after=gr_sigrules_signal_pass1, group=gr_sigrules_signal)
        gr_signal = 'signal_%s'%cbi_id["ID"]
        run_xslt2('IDC/DLC/Signal_Adjacent_%s.xml' % cbi_id["ID"], 'bin_tools/Signal_Adjacent.xsl', ['../bin/IDC/SigRule/SigRules_Signal_pass1_%s.xml' % cbi_id["ID"], 'Signalisation_Areas.xml','Exit_Gates.xml', 'Urbalis_Sectors.xml' , 'Routes.xml', 'Crossing_Detections.xml', 'Signal_Lamp_Types.xml', 'Secondary_Detection_Devices.xml', 'Tracks.xml', 'Cycles.xml', 'Specific_Routes.xml', locales_roles], ['CBI_ID=%s' % cbi_id["ID"]],
                      group=gr_signal)
        after()
        output_xslt('IDC/DLC/Group_Signal_Adjacent_%s.xml' % cbi_id["ID"], 'bin_tools/Group_Signal_Adjacent.xsl', ['Signals.xml', '../bin/IDC/DLC/Signal_Adjacent_%s.xml' % cbi_id["ID"], '../bin/IDC/DLC/SigRules_Signal_Adjacent_%s.xml' % cbi_id["ID"],'Signalisation_Areas.xml', 'Signal_Lamp_Types.xml','Routes.xml', 'Exit_Gates.xml', 'Crossing_Detections.xml'],['CBI_ID=%s' % cbi_id["ID"]], after=[gr_sigrules_signal, gr_signal])
        output_xslt('IDC/TEL/TrackSection_TEL_%s.xml' % cbi_id["ID"], 'bin_tools/TrackSection.xsl', ['../bin/int/Secondary_Detection_Devices_Variable.xml','SDD_Groups_In_Operation.xml','Routes.xml','Secondary_Detection_Devices.xml', 'Urbalis_Sectors.xml', 'Adjacent_Monitoring_Areas.xml', 'Secondary_Detection_Devices_Characteristics.xml', 'Blocks.xml',  '../bin/LocalesRoles.xml'], ['CBI_ID=%s' % cbi_id["ID"]])              

    #for lc_id in eqt_file_data["LC"]:
        #output_xslt('IDC/TEL/LCClient_%s.xml' % (lc_id["ID"]), 'bin_tools/LCClient.xsl', ['ATC_Equipments.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml','../bin/LocalesRoles.xml'], ['LC_ID=%s'  %lc_id["ID"]])
        #output_xslt('IDC/Group/Group_LCClient_%s.xml' % lc_id["ID"], 'bin_tools/Group_LCClient.xsl', ['ATC_Equipments.xml','Urbalis_Sectors.xml','Signalisation_Areas.xml'], ['LC_ID=%s' % lc_id["ID"]])
                                
    for ats_equipments in eqt_file_data["ATS_EQUIPMENTS"]:

        if(ats_equipments["ATS_EQUIP_TYPE"]=='Central Server' or ats_equipments["ATS_EQUIP_TYPE"]=='Local Server'):
            comp_tem(base_dir+'tools/reg_file/RddOPCMDBGTW.reg', base_dir+'output/RDD/ByComputer/RddTELOPCMDBGTW_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME1":ats_equipments["MACHINE1"],"MACHINE_NAME2":ats_equipments["MACHINE2"] })
            comp_tem(base_dir+'tools/reg_file/RddPIMCOM.reg', base_dir+'output/RDD/ByComputer/RddPIMCOM_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME1":ats_equipments["MACHINE1"], "MACHINE_NAME2":ats_equipments["MACHINE2"]})
            if(ats_equipments["NAME"]=='MDDDSERSRVA' or ats_equipments["NAME"]=='MDDDSERSRVB'):
                comp_tem(base_dir+'tools/reg_file/T281OPCMDBGTW.reg', base_dir+'output/OPCMDBGTW/ByComputer/T281OPCMDBGTW_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"]})				
                output_xslt('OPCMDBGTW/Common/T281OPCMDBGTW_%s.xml'%ats_equipments["NAME"], 'bin_tools/T281OPCMDBGTW.xsl')
            
        if((ats_equipments["ATS_EQUIP_TYPE"]=='Central Server' or ats_equipments["ATS_EQUIP_TYPE"]=='Local Server') and ('Depot' in ats_equipments["Localisation_Type"])):
            comp_tem(base_dir+'tools/reg_file/RddMMSCOM.reg', base_dir+'output/RDD/ByComputer/RddMMSCOM_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME1":ats_equipments["MACHINE1"], "MACHINE_NAME2":ats_equipments["MACHINE2"]})
        
        if(ats_equipments["ATS_EQUIP_TYPE"]=='Gateway' or ats_equipments["ATS_EQUIP_TYPE"]=='Local Server'):
            output_xslt('PIMCom/ByComputer/PIMCOM_%s.xml'%ats_equipments["NAME"],'bin_tools/PIMCOM.xsl',['Projects.xml','PIMStations.xml','Signalisation_Areas.xml','PIMPlatforms.xml','ATS_Equipments.xml'],['MACHINE_NAME=%s'%ats_equipments["NAME"], "SIG_AREA_TYPE=%s"%ats_equipments["SIG_AREA_TYPE"]])
            comp_tem(base_dir+'tools/reg_file/PIMCOM.reg', base_dir+'output/PIMCom/ByComputer/PIMCOM_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"]})
            output_xslt('TELOPCMDBGTW/ByComputer/TELOPCMDBGTW_%s.xml'%ats_equipments["NAME"],'bin_tools/OPCMDBGTW.xsl',['Projects.xml','ISCSCentrals.xml','ISCSSettings.xml','ISCSStations.xml','Signalisation_Areas.xml', 'ATS_Equipments.xml'],['MACHINE_NAME=%s'%ats_equipments["NAME"], "ATS_EQUIP_TYPE=%s"%ats_equipments["SIG_AREA_TYPE"]])
            comp_tem(base_dir+'tools/reg_file/OPCMDBGTW.reg', base_dir+'output/TELOPCMDBGTW/ByComputer/TELOPCMDBGTW_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"]})
            comp_tem(base_dir+'tools/reg_file/FEPRdd_.reg', base_dir+'output/FEPRdd/ByComputer/FEPRdd_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME2":ats_equipments["NAME"],"MACHINE_NAME1":ats_equipments["MACHINE2"] if ats_equipments["NAME"]== ats_equipments["MACHINE1"] else ats_equipments["MACHINE1"],"SERVER_NUMBER":ats_equipments["SERVER_NUMBER"],"ATS_EQTYPE":ats_equipments["ATS_EQUIP_TYPE"],"Localisation_Type":ats_equipments["Localisation_Type"]})
            
        if(ats_equipments["ATS_EQUIP_TYPE"]=='Gateway'):
	    comp_tem(base_dir+'tools/reg_file/MMSCOM.reg', base_dir+'output/MMSCom/ByComputer/MMSCOM_%s.reg'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"]})
            output_xslt('MMSCom/ByComputer/MMSCOM_%s.xml'%ats_equipments["NAME"],'bin_tools/MMSCOM.xsl',['Signalisation_Areas.xml','ATS_Equipments.xml'],['MACHINE_NAME=%s'%ats_equipments["NAME"], "SIG_AREA_TYPE=%s"%ats_equipments["SIG_AREA_TYPE"]])
				
        if(ats_equipments["ATS_EQUIP_TYPE"]=='FEP'):
            output_xslt('TELOPCMDBGTW/ByComputer/TELOPCMDBGTW_%s.xml'%ats_equipments["NAME"],'bin_tools/OPCMDBGTW.xsl',['Projects.xml','ISCSCentrals.xml','ISCSSettings.xml','ISCSStations.xml','Signalisation_Areas.xml', 'PSDOPCServer.xml','Platforms_Screen_Doors.xml','ATS_Equipments.xml'],['MACHINE_NAME=%s'%ats_equipments["NAME"], "ATS_EQUIP_TYPE=%s"%ats_equipments["ATS_EQUIP_TYPE"]])
        
        if(ats_equipments["ATS_EQUIP_TYPE"]=='Data_Logger_Computer' or ats_equipments["ATS_EQUIP_TYPE"]=='Workstation'):
            #bat files
            comp_tem(base_dir+'tools/bat_file/PerfLog.bat', base_dir+'output/System/ByComputer/PerfLog_%s.bat'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"],"ATS_EQUIPMENT_TYPE":ats_equipments["ATS_EQUIP_TYPE"]})
            comp_tem(base_dir+'tools/bat_file/PerfLogBandwith.bat', base_dir+'output/System/ByComputer/PerfLogBandwith_%s.bat'%(ats_equipments["NAME"]), {"MACHINE_NAME":ats_equipments["NAME"], "ATS_EQUIPMENT_TYPE":ats_equipments["ATS_EQUIP_TYPE"]})
           
            #conf files
            comp_tem(base_dir+'tools/conf_file/PerfLog.conf', base_dir+'output/System/ByComputer/PerfLog_%s.conf'%(ats_equipments["NAME"]), {"ATS_EQUIPMENT_TYPE":ats_equipments["ATS_EQUIP_TYPE"]})
            comp_tem(base_dir+'tools/conf_file/PerfLogBandwith.conf', base_dir+'output/System/ByComputer/PerfLogBandwith_%s.conf'%(ats_equipments["NAME"]), {"ATS_EQUIPMENT_TYPE":ats_equipments["ATS_EQUIP_TYPE"]})
        #atvcm00987480
        #if (ats_equipments["DeploymentID"] != 'NA'):
            #output_xslt('Deployment/ByComputer/Deployment_%s.xml'%ats_equipments["NAME"], 'bin_tools/Deployment.xsl',['Signalisation_Areas.xml', 'ATSs.xml','ATS_Equipments.xml','../../lib_common/Deployment_TEL.xml','DeploymentMacros.xml','DeploymentConfiguration.xml'], ['COMPUTER_NAME=%s'%ats_equipments["NAME"], "ATS_EQUIP_TYPE=%s"%ats_equipments["ATS_EQUIP_TYPE"], "SIG_AREA_ID=%s"%ats_equipments["SIG_AREA_ID"], "DEP_ID=%s"%ats_equipments["DeploymentID"]])
    after()
    remove_empty_files('TEL')
    after()
    for server_id in eqt_file_data["Function"]:
	for server_level in server_id["LEVEL"]:
            if "Server" in server_id["AREA_TYPE"]:
                output_xslt('IDC/IDCTEL_ATS_%s%s.xml'%(server_id["ID"],levels(server_level)), 'bin_tools/IDCTEL.xsl', ['ATS_Equipments.xml','ZCs.xml','Projects.xml', 'Signalisation_Areas.xml','Urbalis_Sectors.xml','CBIs.xml','Functions.xml','Blocks.xml' ], ['SIG_SERVER_ID=%s' % server_id["Signalisation_Area_ID"], 'SERVER_ID=%s' % server_id["ID"],'SERVER_LEVEL=%s' %levels(server_level),'IS_BOTH_LEVEL=%s' %is_both_level])
    after()          
    copystaticfiles()
    after()
    copy_to_output()
    after()		
    print_action("Generation Done")
    
def levels(sl):
    if "LV" in sl:
        return sl
    else:
        return ""
    
def remove_read_only():
    """
    Remove read-only mode for all the files in the project folder
    """
    for root, _, files in os.walk(os.getcwd()):
        for file_name in files:
            fpath = os.path.join(root,file_name)
            if not os.chmod(fpath, stat.S_IWRITE):
                os.chmod(fpath, stat.S_IWRITE)           
def copystaticfiles():
    """
    Copy product specific output into a global output directory.
    """
    dictfilname = {'CriticalAlarmInterface.xml': 'ISCSDat/Common', 'MultipathAlarmConf.xml': 'MAM/Common', 'MultiPathAlarmEvents_User.xml': 'IMC/Common', 'RSMAlarmEvents_User.xml': 'IMC/Common', 'RSMInterface.xml': 'RSM/Common'}
    for filenm,filepath in dictfilname.items():
        sources = main.options.prefix if main.options.prefix is not None else 'ThomsonLine'
        inputs_path = "data/"+sources+"/inputs/"+filenm
        os.chmod(os.path.join(base_dir, "output"), stat.S_IWRITE)
        output_path = os.path.join(base_dir, "output/"+filepath)
        mkdir(output_path, isdir=True)
        after()
        if os.path.exists(inputs_path):
            run('python','lib_common/copy_file.py',inputs_path,output_path)
        else:
             print_action("Error: '"+inputs_path + "' is not found in the source location"+ sources)
def delivery():
    set_date()
    all()

if __name__ == '__main__':
    import multiprocessing  # @Reimport
    main(extra_options= options, parallel_ok=True, jobs=multiprocessing.cpu_count())
