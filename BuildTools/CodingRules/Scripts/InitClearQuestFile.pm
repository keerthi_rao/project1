#----------------------------------------------------------------------------
# Note: Description
# InitClearQuestFile contains  functions and public strings for clear quest generation.
#----------------------------------------------------------------------------
package InitClearQuestFile;

use strict;
use TestUtil;

my $DEBUG = 0;

our %changeResquestRow = (
	"product",                {libelle => "product",                position => 1,  default_value => "ICONIS_ATS_KE",                        component_property => ""},
	"headline",               {libelle => "headline",               position => 2,  default_value => "",                                       component_property => ""},
	"analyst",                {libelle => "analyst",                position => 3,  default_value => "unknown",                                 component_property => "owner"},
	"submitter",              {libelle => "submitter",              position => 4,  default_value => "guillaume_daunes-ext",                   component_property => ""},
	"key_requirements",       {libelle => "key_requirements",       position => 5,  default_value => "Code Review",                            component_property => ""},
	"defect_detection_phase", {libelle => "defect_detection_phase", position => 6,  default_value => "Detailed Design",                        component_property => ""},
	"severity",               {libelle => "severity",               position => 7,  default_value => "Bypassing",                              component_property => ""},
	"priority",               {libelle => "priority",               position => 8,  default_value => "Medium",                                 component_property => ""},
	"frequency",              {libelle => "frequency",              position => 9,  default_value => "Every Time",                             component_property => ""},
	"product_version",        {libelle => "product_version",        position => 10, default_value => "ICONIS_ATS KERNEL EXTENDED",			component_property => ""},
	"site",                   {libelle => "site",                   position => 11, default_value => "ICONIS ATS DEV -- ICONIS_ATS_KE",		component_property => ""},
	"State",                  {libelle => "State",                  position => 12, default_value => "Recorded",                              component_property => ""},
	"substate",               {libelle => "substate",               position => 13, default_value => "new",                                    component_property => ""},
	"submitter_CR_type",      {libelle => "submitter_CR_type",      position => 14, default_value => "Defect",                                 component_property => ""},
	"submitter_severity",     {libelle => "submitter_severity",     position => 15, default_value => "Bypassing",                              component_property => ""},
	"submitter_priority",     {libelle => "submitter_priority",     position => 16, default_value => "Medium",                                 component_property => ""},
	"CR_type",                {libelle => "CR_type",                position => 17, default_value => "Defect",                                 component_property => ""},
	"customer_access",        {libelle => "customer_access",        position => 18, default_value => "ProductAccess",							component_property => ""},
	"description",            {libelle => "description",            position => 19, default_value => "unknown",									component_property => ""},
	"sub_system",             {libelle => "sub_system",             position => 20, default_value => "unknown",									component_property => "sub_system"},
	"component",              {libelle => "component",              position => 21, default_value => "unknown",									component_property => "component"},
	"CR_category",            {libelle => "CR_category",            position => 22, default_value => "Internal",								component_property => ""},
);

our %componentProperties = (
	"AE_Interface",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "AE Interface", owner =>"" },
	"ARS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "ARS Interface", owner =>"" },
	"ATR_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "ATR Interface", owner =>"" },
	"CAT",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "CAT", owner =>"" },
	"CAT_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "CAT Interface", owner =>"" },
	"CBIS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "CBIS Interface", owner =>"" },
	"COMMON-FEP-UE",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "COMMON-FEP-UE", owner =>"" },
	"Common",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "Common", owner =>"" },
	"ERM_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "ERM Interface", owner =>"" },
	"FSServerDLL2",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "FSServerDLL2", owner =>"" },
	"FSServerDLL2\\opcbase",	{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "FSServerDLL2", owner =>"" },
	"HILCS",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "HILCS", owner =>"" },
	"HILCS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "HILCS Interface", owner =>"" },
	"HMI\\HILC_DLL",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "HMI-HILC", owner =>"" },
	"HSH",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "HSH", owner =>"" },
	"HSH_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "HSH Interface", owner =>"" },
	"IAS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "IAS Interface", owner =>"" },
	"IconisToolBox_Interface",	{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "IconisToolbox Interface", owner =>"" },
	"IconisUtilities",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "IconisUtilities", owner =>"" },
	"MAV",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MAV", owner =>"" },
	"MAV_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MAV Interface", owner =>"" },
	"MBZ",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MBZ", owner =>"" },
	"MBZ_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MBZ Interface", owner =>"" },
	"MMG_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MMG Interface", owner =>"" },
	"MSA",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MSA", owner =>"" },
	"MSA_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "MSA Interface", owner =>"" },
	"OPCHMI_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "OPCHMI Interface", owner =>"" },
	"OPC_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "OPC Interface", owner =>"" },
	"RDD_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "RDD Interface", owner =>"" },
	"RSMU500",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "RSMU500", owner =>"" },
	"RSMU500_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "RSMU500 Interface", owner =>"" },
	"SOQ_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "SOQ Interface", owner =>"" },
	"SigRule_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "SIGR Interface", owner =>"" },
	"TABs_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TABs Interface", owner =>"" },
	"TDS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TDS Interface", owner =>"" },
	"TMMU500",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TMM U500", owner =>"" },
	"TMMU500_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TMM U500 Interface", owner =>"" },
	"TOM8\\Include",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "S2K Interface", owner =>"" },
	"TPM_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TPM Interface", owner =>"" },
	"TTC_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TTC Interface", owner =>"" },
	"TTR",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TTR", owner =>"" },
	"TTR_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TTR Interface", owner =>"" },
	"TSRS",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TSRS", owner =>"" },
	"TSRS_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TSRS Interface", owner =>"" },
	"Topology_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TOPOLOGY Interface", owner =>"" },
	"TraceOPC_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "TraceOPC Interface", owner =>"" },
	"U500AP",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "U500 AP", owner =>"" },
	"U500AP_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "U500 AP Interface", owner =>"" },
	"UDP2OPC",			{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "UDP2OPC", owner =>"" },
	"UDP2OPC_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "UDP2OPC Interface", owner =>"" },
	"iTC",				{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "iTC", owner =>"" },
	"iTC_Interface",		{sub_system => "Sw_Riyadh,XXX",        Type => "basics",	component =>  "iTC Interface", owner =>"" },
);
#----------------------------------------------------------------------------
# Creates ClearQuest file by component
#----------------------------------------------------------------------------
sub CreatesClearQuestComponent #(components)
{
	my (%components) = @_;

	# Initialise the default value from environment variables (depending of the project)
	$changeResquestRow{"product"}->{default_value}			= $TestUtil::clearQuestProduct;
	$changeResquestRow{"product_version"}->{default_value}	= $TestUtil::clearQuestProductVersion;

	my %ComponentByRuleList;

	# Build the description for the CR for the rule for the component
	foreach my $componentName (sort keys(%components))
	{
		foreach my $fileName (sort keys(%{$components{$componentName}}))
		{
			my $firstItemRule = 1;
			foreach my $ruleID (sort keys(%{$components{$componentName}->{$fileName}}))
			{
				my $rec = $components{$componentName}->{$fileName}->{$ruleID};

				my $result = $rec->{result};
				my $remark = $rec->{remark};

				# Calculate FILE result
				if($result eq "ERROR")
				{
					# Build the map for component and by rule
					if ($firstItemRule)
					{
						$firstItemRule = 0;
					}
					else
					{
						$ComponentByRuleList{$componentName}->{$ruleID} .= "|";
					}

					$ComponentByRuleList{$componentName}->{$ruleID} .= "File ".$fileName." : ".removeHTMLBalise($remark);
				} # ERROR
			} # for each rule in the file
		} # for each file in the component
	} # for each component

	my %changeRequestList;

	foreach my $componentName (sort keys(%ComponentByRuleList))
	{
		# Check if the component is allready know
		if (! defined $componentProperties{$componentName})
		{
			print "Unknow component for directory $componentName\n";
		}

		foreach my $ruleID (sort keys(%{$ComponentByRuleList{$componentName}}))
		{
			my $crKEY = "$componentName"."_".$ruleID;

			# Initialise the default value for the change request
			%changeRequestList = InitChangeRequest($crKEY, $componentName, %changeRequestList);

			$changeRequestList{$crKEY}->{$changeResquestRow{"headline"}->{libelle}}		= "$ruleID : $componentName";
			$changeRequestList{$crKEY}->{$changeResquestRow{"description"}->{libelle}}	= $ComponentByRuleList{$componentName}->{$ruleID};
		}
	}

	# Write the txt file with the CR for clear quest
	WriteClearQuest(%changeRequestList);
} #sub CreatesClearQuestComponent

#----------------------------------------------------------------------------
# Creates ClearQuest file
#----------------------------------------------------------------------------
sub CreatesClearQuestFile #(components)
{
	my (%components) = @_;
	my %changeRequestList;

	# Initialise the default value from environment variables (depending of the project)
	$changeResquestRow{"product"}->{default_value}			= $TestUtil::clearQuestProduct;
	$changeResquestRow{"product_version"}->{default_value}	= $TestUtil::clearQuestProductVersion;

	foreach my $componentName (sort keys(%components))
	{
		print "Component [$componentName]\n" if $DEBUG;

		# Check if the component is allready know
		if (! defined $componentProperties{$componentName})
		{
			print "Unknow component for directory $componentName\n";
		}

		foreach my $fileName (sort keys(%{$components{$componentName}}))
		{
			print "   File=[$fileName]\n" if $DEBUG;

			foreach my $ruleID (sort keys(%{$components{$componentName}->{$fileName}}))
			{
				my $rec = $components{$componentName}->{$fileName}->{$ruleID};

				my $result = $rec->{result};
				my $remark = $rec->{remark};

				print "	   Rule=[$ruleID] Result=[$result]\n" if $DEBUG;

				# Calculate FILE result
				if($result eq "ERROR")
				{
					my $crKEY = $componentName."_".$fileName."_".$ruleID;

					# Initialise the default value for the change request
					%changeRequestList = InitChangeRequest($crKEY, $componentName, %changeRequestList);

					$changeRequestList{$crKEY}->{$changeResquestRow{"headline"}->{libelle}}		= "$ruleID : $componentName - $fileName";
					$changeRequestList{$crKEY}->{$changeResquestRow{"description"}->{libelle}}	= removeHTMLBalise($remark);
				} # ERROR
			} # for each rule in the file
		} # for each file in the component
	} # for each component

	# Write the txt file with the CR for clear quest
	WriteClearQuest(%changeRequestList);
} #sub CreatesClearQuestFile

#----------------------------------------------------------------------------
# Init all record of a change resquest
#----------------------------------------------------------------------------
sub InitChangeRequest #($crKEY, $componentName, %changeRequestList)
{
	my ($crKEY, $componentName, %changeRequestList) = @_;

	my $ComponentUnknow = 1;

	if (defined $componentProperties{$componentName})
	{
		$ComponentUnknow = 0;
	}

	foreach my $CRRow (sort keys(%changeResquestRow))
	{
		my $component_property = $changeResquestRow{$CRRow}->{component_property};
		if ($ComponentUnknow or (!$component_property))
		{
			$changeRequestList{$crKEY}->{$changeResquestRow{$CRRow}->{libelle}} = $changeResquestRow{$CRRow}->{default_value};
		}
		else
		{
			$changeRequestList{$crKEY}->{$changeResquestRow{$CRRow}->{libelle}} = $componentProperties{$componentName}->{$component_property};
		}
	}# for each row of clear quest change request

	return (%changeRequestList);
} #sub InitChangeRequest

#----------------------------------------------------------------------------
# Remove the HTML balise from the detail result and replace the ; by  - 
#----------------------------------------------------------------------------
sub removeHTMLBalise #($remark)
{
	my ($remark) = @_;
	my $remarkWithoutBalise;

	my $insideBalise = 1;
	my $posRemark = 0;

	while ($posRemark < length($remark))
	{
		my $carRemark = substr($remark,$posRemark,1);
		$posRemark++;

		if ($carRemark eq '<') 
		{
			$insideBalise = 1;
		}

		if ($insideBalise == 0)
		{
			if ($carRemark eq ';')
			{
				$remarkWithoutBalise .= " - ";
			}
			elsif ($carRemark eq '"')
			{
				$remarkWithoutBalise .= " ";
			}
			else
			{
				$remarkWithoutBalise .= $carRemark;
			}
		}

		if ($carRemark eq '>')
		{
			$insideBalise = 0;
		}
	}

	return ($remarkWithoutBalise);
}

#----------------------------------------------------------------------------
# Creates ClearQuest file
#----------------------------------------------------------------------------
sub WriteClearQuest #(changeResquests)
{
	my (%changeResquests) = @_;

	my $ClearQuestFileName = $TestUtil::targetPath . "clearQuest.txt";

	print "Generate $ClearQuestFileName\n";

	open CLEARQUEST, ">$ClearQuestFileName";
	#printf CLEARQUEST "\"product\";\"headline\";\"analyst\";\"submitter\";\"key_requirements\";\"defect_detection_phase\";\"severity\";\"priority\";\"frequency\";\"product_version\";\"site\";\"State\";\"substate\";\"submitter_CR_type\";\"submitter_severity\";\"submitter_priority\";\"CR_type\";\"customer_access\";\"description\";\"sub_system\";\"component\";\"CR_category\"\n";

	# Write the line for the headers
	my $firstRow = 1;
	foreach my $CRRow (sort { $changeResquestRow{$a}->{position} <=> $changeResquestRow{$b}->{position} } keys(%changeResquestRow))
	{
		if ($firstRow)
		{
			$firstRow = 0;
		}
		else
		{
			print CLEARQUEST ";";
		}

		print CLEARQUEST "\"";
		print CLEARQUEST $changeResquestRow{$CRRow}->{libelle};
		print CLEARQUEST "\"";
	}# for each row of clear quest change request
	print CLEARQUEST "\n";

	# Write the value of the CR
	foreach my $CReq (sort keys(%changeResquests))
	{
		my $firstRow = 1;
		foreach my $CRRow (sort { $changeResquestRow{$a}->{position} <=> $changeResquestRow{$b}->{position} } keys(%changeResquestRow))
		{
			if ($firstRow)
			{
				$firstRow = 0;
			}
			else
			{
				print CLEARQUEST ";";
			}

			print CLEARQUEST "\"";
			print CLEARQUEST $changeResquests{$CReq}->{$changeResquestRow{$CRRow}->{libelle}};
			print CLEARQUEST "\"";
		}# for each row of clear quest change request

		print CLEARQUEST "\n";

	} # for each change request

	close CLEARQUEST;
} #sub CreatesClearQuestFile

#----------------------------------------------------------------------------
# Return of the package
#----------------------------------------------------------------------------

return 1;
