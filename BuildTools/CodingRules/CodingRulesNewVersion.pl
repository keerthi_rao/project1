use XML::Parser;
use Cwd;
use File::Copy;

#---------------------------------------------------------------------------------------------------------------
# Check inputs
#---------------------------------------------------------------------------------------------------------------

if (! (defined $ARGV[0]))
  {
    printf ("This script need 1 parameter\n");
    printf ("CodingRulesNewVersion.pl [Major|Minor]\n");
    exit;
  }
if ($ARGV[0] ne "Major" && $ARGV[0] ne "Minor")
{
    printf ("Wrong parameter [%s]\n", $ARGV[0]);
    printf ("CodingRulesNewVersion.pl [Major|Minor]\n");
    exit;
}

my $ReleaseType = $ARGV[0];
printf ("\n------------    %s release    --------------------\n\n", $ReleaseType);

#-----------------------------------------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------------------------------------

my @SynergyProjects;
my %SlnByProjects;
my $Release;
my $ReleaseTM;
my $Prefix;

#-----------------------------------------------------------------------------------------------------------
# Open OutputFile
#-----------------------------------------------------------------------------------------------------------

open OUTPUT, "> CodingRulesNewVersion.txt" or die "Impossible d'ouvrir CodingRulesNewVersion.txt : $!";

#-----------------------------------------------------------------------------------------------------------
# Parse external parameters
#-----------------------------------------------------------------------------------------------------------

my $ParametersFile = "CodingRulesReport.xml";

# Does the file exist
if (! (-s $ParametersFile))
  {
    printf OUTPUT ("The parameter file [%s] does not exist.\n", $ParametersFile);
    exit;
  }

# Open and parse Parameters file

my $Configproc = XML::Parser->new (Handlers => {Init  => \&Parameters_doc_debut,
						Final => \&Parameters_doc_fin,
						Start => \&Parameters_debut,
						End   => \&Parameters_fin,
						Char  => \&Parameters_texte});

$Configproc->parsefile ($ParametersFile);

#-----------------------------------------------------------------------------------------------------------
# Search the IconisVersionFile
#-----------------------------------------------------------------------------------------------------------

my $IconisVersionsFile = "..\\..\\AsDesign\\Interface\\Common\\Common\\IconisVersions.h";

# Open file IconisVersions.h
if (! (open VAR, "< $IconisVersionsFile"))
{
	printf OUTPUT "Impossible d'ouvrir $IconisVersionsFile (lecture): $!\n";
}
else
{
    	printf OUTPUT "Read IconisVersions.h\n";	
    	for (<VAR>)
    	{
	    	if (m/URBALISPRODUCTVERSIONNB ([0-9]+,[0-9]+,[0-9]+,[0-9][0-9][0-9][0-9])/)
    		{
    				print OUTPUT "Release IAU $1\n";
    				$Release = $1;
    				$Release =~ s/,/./g; 
    		}
	    	if (m/ICONISPRODUCTVERSIONNB ([0-9]+,[0-9]+,[0-9]+),[0-9][0-9][0-9][0-9]/)
    		{
    				print OUTPUT "Release $Prefix $1\n";
    				$ReleaseTM = $1;
    				$ReleaseTM =~ s/,/./g; 
    		}
	}
}
close VAR;

#-----------------------------------------------------------------------------------------------------------
# Confirm the release
#-----------------------------------------------------------------------------------------------------------

while (1)
{
	printf "Release $Prefix $ReleaseTM (y/n): ";
	chomp ($ligne = <STDIN>);

	if ($ligne eq "y")
	{
		last;
	}
	else
	{
		printf "Gives the release: ";
		chomp ($ligne = <STDIN>);
		$ligne =~ s/^$Prefix //;
		$ReleaseTM = $ligne;
		printf "\nNew release $Prefix $ligne\n";		
	}
}

#-----------------------------------------------------------------------------------------------------------
# Check if a default task is defined (This task will be used to checkout files of revision)
#-----------------------------------------------------------------------------------------------------------

while (1)
{
	printf "\nThe default task will be used to checkout files of revision.\nDid you select a default task under synergy (y/n): ";
	chomp ($ligne = <STDIN>);

	if ($ligne eq "y")
	{
		last;
	}
}

#-----------------------------------------------------------------------------------------------------------
# Start coding rules
#-----------------------------------------------------------------------------------------------------------

printf OUTPUT "------------------------------------------------------------------\n";
printf OUTPUT "    Update revision files\n";
printf OUTPUT "------------------------------------------------------------------\n\n";

for (@SynergyProjects)
  {
    my $ProjectWithRelease = $_;
    
    $ProjectWithRelease =~ s/XXX/$Prefix $ReleaseTM/;

    s/,.+$//;
    my $ProjectWithoutRelease = $_;
    printf OUTPUT "================================================================\n";
    printf OUTPUT "Start $ProjectWithRelease\n\n";
    printf OUTPUT "chdir: ". 'D:\\BuildManagerATS\\' . $ProjectWithRelease . '\\' . $ProjectWithoutRelease . "\\BuildTools\n";
    chdir ('D:\\BuildManagerATS\\' . $ProjectWithRelease . '\\' . $ProjectWithoutRelease . '\\BuildTools');

    # Check if the file with revisions exist 
    my $VarFile = "CodingRulesInputs\\CodingRulesRevisions.txt";
    my @CodingRulesRevisions;
    if (! (-s $VarFile))
    {
    	printf OUTPUT "The file [D:\\BuildManagerATS\\%s\\%s\\BuildTools\\%s] does not exist.\n", $ProjectWithRelease, $ProjectWithoutRelease, $VarFile;
    }
    else
    {
    	#-----------------------------------------------------------    	
    	# Open file with variables
    	if (! (open VAR, "< $VarFile"))
    	{
    		printf OUTPUT "Impossible d'ouvrir CodingRulesRevisions.txt (lecture): $!\n";
    		printf "Impossible d'ouvrir CodingRulesRevisions.txt (lecture): $!\n";
    	}
    	else
    	{
    		printf OUTPUT "Read CodingRulesRevisions.txt\n";
    		my $LastLine;	
    		for (<VAR>)
    		{
    			push (@CodingRulesRevisions, $_);
    			$LastLine = $_;
    		}
    		close VAR;

    		my @LastDatas;
    		if (defined ($LastLine))
    		{
    			#printf ("LAST LINE: $LastLine\n");
    			(@LastDatas) = split (/\|/, $LastLine);
    			#for (@LastDatas)
    			#{
    			#printf ("LAST DATAs: [%s]\n", $_);
    			#}
    		}

    		#-----------------------------------------------------------
		# Search new release
		
		my $NewRelease;
		
		if (defined ($LastDatas[0]))
		{
			if ($ReleaseType eq "Major")
			{
				if ($LastDatas[0] =~ m/([A-Z]*)([A-Z]{1}?)([0-9]*)/)
				{
					#printf "[$1] [$2] [$3]\n";
					if ($2 ne "Z")
					{
						$NewRelease = $1 . chr (ord ($2) + 1);						
					}
					elsif ($1 eq "")
					{
						$NewRelease = "AA";
					}
					else
					{
						$NewRelease = chr (ord ($1) + 1) . "A";
					}
				}
				else
				{
					#printf "[EMPTY]\n";
					$NewRelease = "A";
				}
			}
			else
			{
				if ($LastDatas[0] =~ m/([A-Z]*)([0-9]+)/)
				{
					#printf "[$1] [$2]\n";
					$NewRelease = $1 . ($2 + 1);
				}
				elsif ($LastDatas[0] =~ m/([A-Z]*)/)
				{
					#printf "[EMPTY 1]\n";
					$NewRelease = $1 . "1";
				}
				else
				{
					#printf "[EMPTY 2]\n";
					$NewRelease = "1";
				}
			}			
		}
		else
		{
			if ($ReleaseType eq "Major")
			{
				$NewRelease = "A";
			}
			else
			{
				$NewRelease = "1";				
			}
		}		
		
    		#-----------------------------------------------------------
    		printf OUTPUT "Update %s\n\n", $VarFile;
    	
    		printf "Update the project %s (%s -> %s) ? (y/n): ", $ProjectWithRelease, $LastDatas[0], $NewRelease;
    		chomp ($ligne = <STDIN>);

		if ($ligne ne "y")
    		{
    			printf "%s not updated.\n", $ProjectWithRelease;
    			next;
    		}    	

    		#-----------------------------------------------------------
    		my $Command = "ccm co " . '"D:\\BuildManagerATS\\' . $ProjectWithRelease . '\\' . $ProjectWithoutRelease . '\\BuildTools\\' . $VarFile . '"';
    		print OUTPUT "Command: $Command\n";
    		my $Result = `$Command`;
    		print OUTPUT "Result: $Result\n";
    	
	    	# Open file with variables
    		if (! (open VAR, "> $VarFile"))
	    	{
    			printf OUTPUT "Impossible d'ouvrir CodingRulesRevisions.txt (ecriture): $!\n";
    		}
	    	else
    		{
	    		printf OUTPUT "Write CodingRulesRevisions.txt\n";
	    		for (@CodingRulesRevisions)
	    		{
					print VAR $_;
	    		}
	    		my $TheLocaltime = localtime;

	    		print VAR "\n$NewRelease|Christophe Coyez|$TheLocaltime||IAU $Release";
    			close VAR;
		}
    	}
    }
    
    # Check if the file with revisions exist 
    my $VarFileExtended = "CodingRulesInputs\\CodingRulesExtendedRevisions.txt";
    my @CodingRulesExtendedRevisions;
    if (! (-s $VarFileExtended))
    {
    	printf OUTPUT "The file [D:\\BuildManagerATS\\%s\\%s\\BuildTools\\%s] does not exist.\n", $ProjectWithRelease, $ProjectWithoutRelease, $VarFileExtended;
    }
    else
    {
    	#-----------------------------------------------------------    	
    	# Open file with variables
    	if (! (open VAR, "< $VarFileExtended"))
    	{
    		printf OUTPUT "Impossible d'ouvrir CodingRulesExtendedRevisions.txt (lecture): $!\n";
    		printf "Impossible d'ouvrir CodingRulesExtendedRevisions.txt (lecture): $!\n";
    	}
    	else
    	{
    		printf OUTPUT "Read CodingRulesExtendedRevisions.txt\n";
    		my $LastLine;	
    		for (<VAR>)
    		{
    			push (@CodingRulesExtendedRevisions, $_);
    			$LastLine = $_;
    		}
    		close VAR;

    		my @LastDatas;
    		if (defined ($LastLine))
    		{
    			#printf ("LAST LINE: $LastLine\n");
    			(@LastDatas) = split (/\|/, $LastLine);
    			#for (@LastDatas)
    			#{
    			#printf ("LAST DATAs: [%s]\n", $_);
    			#}
    		}

    		#-----------------------------------------------------------
		# Search new release
		
		my $NewRelease;
		
		if (defined ($LastDatas[0]))
		{
			if ($ReleaseType eq "Major")
			{
				if ($LastDatas[0] =~ m/([A-Z]*)([A-Z]{1}?)([0-9]*)/)
				{
					#printf "[$1] [$2] [$3]\n";
					if ($2 ne "Z")
					{
						$NewRelease = $1 . chr (ord ($2) + 1);						
					}
					elsif ($1 eq "")
					{
						$NewRelease = "AA";
					}
					else
					{
						$NewRelease = chr (ord ($1) + 1) . "A";
					}
				}
				else
				{
					#printf "[EMPTY]\n";
					$NewRelease = "A";
				}
			}
			else
			{
				if ($LastDatas[0] =~ m/([A-Z]*)([0-9]+)/)
				{
					#printf "[$1] [$2]\n";
					$NewRelease = $1 . ($2 + 1);
				}
				elsif ($LastDatas[0] =~ m/([A-Z]*)/)
				{
					#printf "[EMPTY 1]\n";
					$NewRelease = $1 . "1";
				}
				else
				{
					#printf "[EMPTY 2]\n";
					$NewRelease = "1";
				}
			}			
		}
		else
		{
			if ($ReleaseType eq "Major")
			{
				$NewRelease = "A";
			}
			else
			{
				$NewRelease = "1";				
			}
		}		
		
    		#-----------------------------------------------------------
    		printf OUTPUT "Update %s\n\n", $VarFileExtended;
    	
    		printf "Update the project %s extended (%s -> %s) ? (y/n): ", $ProjectWithRelease, $LastDatas[0], $NewRelease;
    		chomp ($ligne = <STDIN>);

		if ($ligne ne "y")
    		{
    			printf "%s not updated.\n", $ProjectWithRelease;
    			next;
    		}    	

    		#-----------------------------------------------------------
    		my $Command = "ccm co " . '"D:\\BuildManagerATS\\' . $ProjectWithRelease . '\\' . $ProjectWithoutRelease . '\\BuildTools\\' . $VarFileExtended . '"';
    		print OUTPUT "Command: $Command\n";
    		my $Result = `$Command`;
    		print OUTPUT "Result: $Result\n";
    	
	    	# Open file with variables
    		if (! (open VAR, "> $VarFileExtended"))
	    	{
    			printf OUTPUT "Impossible d'ouvrir CodingRulesExtendedRevisions.txt (ecriture): $!\n";
    		}
	    	else
    		{
	    		printf OUTPUT "Write CodingRulesExtendedRevisions.txt\n";
	    		for (@CodingRulesExtendedRevisions)
	    		{
					print VAR $_;
	    		}
	    		my $TheLocaltime = localtime;

	    		print VAR "\n$NewRelease|Jean-Francois COPY|$TheLocaltime||IAU $Release";
    			close VAR;
		}
    	}
    }

    printf "\n";
    printf OUTPUT "End\n";
    printf OUTPUT "================================================================\n\n\n";
  }


#-----------------------------------------------------------------------------------------------------------
# Close OutputFile
#-----------------------------------------------------------------------------------------------------------

close OUTPUT;

#-----------------------------------------------------------------------------------------------------------
# Subroutine used by the Config XML parser
#-----------------------------------------------------------------------------------------------------------

sub Parameters_doc_debut
  {
    printf "\n";
  }

#-----------------------------------------------------------------------------------------------------------

sub Parameters_doc_fin
  {
  }
	
#-----------------------------------------------------------------------------------------------------------

sub Parameters_debut
  {
    my ($expat, $nom, %atts) = @_;

    if ($nom eq "SYNERGY")
      	{
		$Prefix = $atts{"Prefix"};
	}
    elsif ($nom eq "Project")
      {
      	# Projects without Sln are only for search successor
      	if (defined ($atts{"Sln"}) && ($atts{"Sln"} ne ""))
      	{
		my $Project = $atts{"Name"};
		push (@SynergyProjects, $Project);
		$SlnByProjects{$Project} = $atts{"Sln"};
      	}
      }
  }

#-----------------------------------------------------------------------------------------------------------

sub Parameters_fin
  {
    my ($expat, $nom) = @_;
  }

#-----------------------------------------------------------------------------------------------------------

sub Parameters_texte
  {
    my ($expat, $texte) = @_;
  }

#-----------------------------------------------------------------------------------------------------------
