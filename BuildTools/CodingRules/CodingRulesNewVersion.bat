@echo off

@echo  -----------    Coding rules new release  -----------

:question1
@echo Do you want to build :
@echo      0 : Out
@echo      1 : Major release
@echo      2 : Minor release
set /P resp=Your choice : 

if %resp%==0 goto fin

if %resp%==1 goto choix1
if %resp%==2 goto choix2
goto question1

:choix1
perl -w .\CodingRulesNewVersion.pl Major
pause
goto :eof

:choix2
perl -w .\CodingRulesNewVersion.pl Minor
pause
goto :eof


:fin
goto :eof