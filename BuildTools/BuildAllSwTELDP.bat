@echo off



set BuildToolsDir=%cd%
cd %~dp0
cd ..
set SwTELDir=%cd%
cd %BuildToolsDir%
set SolName=Sw_TEL_DP.sln
set ProjName=Sw_TELDP

@echo CURRENT DIR = %BuildToolsDir%
@echo PROJECT   = %SwTELDir%
@echo SOLUTION  = %SolName%

:question
@echo Do you want to build :
@echo      0 : Out
@echo      1 : Sw_TELDP


set /P resp=Your choice : 

if %resp%==0 goto fin

@echo -----------    Set Visual 2010 environment   -----------
call "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\vcvarsall.bat" x86

if %resp%==1 goto choix1

goto question

:choix1
REM pause

REM *************************************
REM Clean Solution

@echo -----------    Clean solution   -----------

@echo -----------    Clean folders    -----------
del /F /Q /S "%SwTELDir%\IconisBin8\PRODUCT"
del /F /Q /S "%SwTELDir%\IconisBin8\ReleaseU"

@echo -----------    Clean by msbuild -----------
msbuild "%SwTELDir%\%SolName%"  /t:Clean /p:Configuration="Unicode Release";Platform="Win32" /l:FileLogger,Microsoft.Build.Engine;logfile="%BuildToolsDir%\BuildAllTELIDP.log"

REM *************************************
REM Start Build

@echo -----------    Create the global version file   -----------
cd "%SwTELDir%\BuildTools\Version"
perl -w "%SwTELDir%\BuildTools\Version\ConcatVersion.pl" "1" "ICONIS_LVL_1_VERSIONNB" "%SwTELDir%\BuildTools\Version\IconisVersions_TEL_IDP.h"
cd %BuildToolsDir%

@echo -----------    Add the version number in vdproj   -----------
set IconisVersionHeader="%SwTELDir%\BuildTools\Version\IconisConcatenedVersion.txt"

"%SwTELDir%\BuildTools\Version\BuildTask.exe" "%SwTELDir%\IDP_TEL\SetupIDP_TEL\SetupIDP_TEL.vdproj" %IconisVersionHeader% "LVL_1"

@echo -----------    Build solution   -----------
REM old method
REM kwinject -o "%BuildToolsDir%\%ProjName%_Cpp.out" msbuild  "%SwTELDir%\%SolName%" /t:Build /p:Configuration="Unicode Release";Platform="Win32" /l:FileLogger,Microsoft.Build.Engine;logfile="%BuildToolsDir%\BuildAllTELHMI.log";append=true /verbosity:n
REM kwcsprojparser -o "%BuildToolsDir%\%ProjName%_CS.out" msbuild  "%SwTELDir%\%SolName%" /t:Build /p:Configuration="Unicode Release";Platform="Win32" /l:FileLogger,Microsoft.Build.Engine;logfile="%BuildToolsDir%\BuildAllTELHMI.log";append=true /verbosity:n

REM kwinject -o "%BuildToolsDir%\%ProjName%_Cpp.out" devenv "%SwTELDir%\%SolName%" /build "Unicode Release|Win32" /out "%BuildToolsDir%\BuildAllTELHMI.log"
REM kwcsprojparser "%SwTELDir%\%SolName%" --config "Unicode Release|Win32" --output "%BuildToolsDir%\%ProjName%_CS.out"

devenv "%SwTELDir%\%SolName%" /build "Unicode Release|Win32" /out "%BuildToolsDir%\BuildAllTELIDP.log"

REM @echo -----------    Build vdproj   -----------
REM old method
REM devenv "%SwTELDir%\%SolName%" /build "Unicode Release" /project "IDP_TEL\SetupIDP_TEL\SetupIDP_TEL.vdproj" /out "%BuildToolsDir%\BuildAllTELIDP.log"



goto :eof


:fin
goto :eof